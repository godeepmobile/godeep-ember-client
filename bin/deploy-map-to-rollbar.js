// bin/deploy-map-to-rollbar.js
// this is a node application
// to execute, type 'node bin/deploy-map-to-rollbar'
var stdio = require('stdio'); // for processing command line arguments
var getVersion = require('git-repo-version'); // to get the current GIT repo version
var querystring = require('querystring'); // for setting up POST parameters
var http = require('https'); // for http requests, primarily POST
var path = require('path'); // for filename/path manipulation
var glob = require('glob'); // for file wildcard expansion
var fs = require('fs-extra'); // for file operations with extras
var request = require('request'); // super simple http client

// collect command line parameters into global variables
var command_line_options = stdio.getopt({
    'url': {
        key: 'u',
        args: 1,
        mandatory: true,
        description: 'website url base, i.e. http://gd-api-qa.herokuapp.com'
    },
    'environment': {
        key: 'e',
        args: 1,
        mandatory: true,
        description: 'environment, i.e. production or qa or uat'
    }
});

// set globals
var access_token = '802c2b1fab5b4ff794143cd1db308ea8';
var app_url_base = command_line_options.url;
var deployment_environment = command_line_options.environment;
var tmp_dir = path.resolve(__dirname, './tmp');

var deployment_version = getVersion();

// create temp copies of JS and MAP files
create_temp_copies_of_source_and_map_files(function(err){
    if (err) {
        return cleanup_and_exit(-1);
    }

    create_rollbar_deployment_for_this_version(function(err){
        if (err) {
            return cleanup_and_exit(-2);
        }
        upload_source_and_map_files_to_rollbar(function(err){
            if (err) {
                return cleanup_and_exit(-3);
            }
            // ok we're done.
            return cleanup_and_exit(0);
        });
    });
});


function upload_source_and_map_files_to_rollbar(next) {
    var expanded_source_paths = glob.sync(path.resolve(__dirname, "./tmp/godeep*.js"));
    var expanded_map_paths = glob.sync(path.resolve(__dirname, "./tmp/godeep*.map"));
    upload_one_source_map_to_rollbar(expanded_source_paths[0], expanded_map_paths[0], function(err) {
        if (err) {
            console.error('error ', err);
            return next(err);
        } else {
            // upload vendor* JS and MAP files
            expanded_source_paths = glob.sync(path.resolve(__dirname, "./tmp/vendor*.js"));
            expanded_map_paths = glob.sync(path.resolve(__dirname, "./tmp/vendor*.map"));
            upload_one_source_map_to_rollbar(expanded_source_paths[0], expanded_map_paths[0], function(err) {
                if (err) {
                    console.error('error ', err);
                    return next(err);
                } else {
                    return next();
                }
            });
        }
    });
}

function upload_one_source_map_to_rollbar(source_path, map_path, next) {
    console.log('uploading sourcemap to rollbar: \n  source %s\n  map    %s', source_path, map_path);
    // This is an async file read
    var base_name = path.basename(source_path); // filename without path
    var minified_url = app_url_base + '/coach/assets/' + base_name;

    /*
      curl "https://api.rollbar.com/api/1/sourcemap"
      -F access_token=802c2b1fab5b4ff794143cd1db308ea8 \
      -F version="$version" \
      -F minified_url="$app_url_base/coach/assets/$base_name" \
      -F source_map=@"$map_path"
    */

    var formData = {
        access_token: access_token,
        version: deployment_version,
        minified_url: minified_url,
        source_map: fs.createReadStream(source_path)
    };
    request.post({
        url: 'https://api.rollbar.com/api/1/sourcemap',
        formData: formData
    }, function optionalCallback(err, httpResponse, body) {
        if (err) {
            console.error('upload sourcemap failed with ', err);
            return next(err);
        }
        console.log('upload sourcemap successful.');
        return next();
    });
}

function create_rollbar_deployment_for_this_version(next) {
    console.log("creating %s deployment version %s", deployment_environment, deployment_version);
    /*
    curl https://api.rollbar.com/api/1/deploy/ \
      -F access_token=802c2b1fab5b4ff794143cd1db308ea8 \
      -F revision=$1 \
      -F environment=$2
    echo
    */
    var formData = {
        access_token: access_token,
        revision: deployment_version,
        environment: deployment_environment
    };
    request.post({
        url: 'https://api.rollbar.com/api/1/deploy',
        formData: formData
    }, function optionalCallback(err, httpResponse, body) {
        if (err) {
            console.error('create deployment failed with ', err);
            return next(err);
        }
        console.log('new deployment created successfully.');
        return next();
    });
}


function cleanup_and_exit(value) {
    // remove temp copies
    fs.emptyDirSync(tmp_dir);
    process.exit(value);
}

function create_temp_copies_of_source_and_map_files(next) {
    fs.ensureDirSync(tmp_dir);

    var source_and_map_wildcard_paths = [
        '../dist/assets/godeep*.js',
        '../dist/assets/godeep*.map',
        '../dist/assets/vendor*.js',
        '../dist/assets/vendor*.map'
    ];

    var base_name;
    var expanded_source_files;
    source_and_map_wildcard_paths.forEach(function(f) {
        expanded_source_files = glob.sync(path.resolve(__dirname, f));
        if (expanded_source_files.length === 1) {
            base_name = path.basename(expanded_source_files[0]);
            fs.copySync(expanded_source_files[0], path.join(tmp_dir, base_name));
        } else {
            console.error('need 1 file, found %d, expanding %s', expanded_source_files.length, f);
            return next(new Error("wrong number of files in dist directory"));
        }
    });
    return next();
}
