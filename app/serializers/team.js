import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
	serialize: function(snapshot, options) {
		var json = this._super(snapshot, options);
        // some fields are read-only, remove them before sending to server
        delete json.name;
        delete json.conferenceName;
        delete json.conferenceLevel;
        delete json.isProTeam;
        delete json.numPlayGrades;
		return json;
	},

	serializeIntoHash: function(hash, type, record, options) {
	    Ember.merge(hash, this.serialize(record, options));
	},

    // updating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
    extractUpdateRecord: function(store, type, payload, id, requestType) {
        var wrappedObj = {};
        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
    },
    // creating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
	extractCreateRecord: function(store, type, payload, id, requestType) {
	    var wrappedObj = {};
        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
	}
});
