import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {

    serialize: function(snapshot, options) {
        var json = this._super(snapshot, options), score, opponentScore;
        json.date = moment.isMoment(json.date) ? json.date.toDate() : moment(json.date).toDate();
        delete json.opponentName;   // delete this read-only field before writing
        delete json.opponentShortName;   // delete this read-only field before writing
        delete json.opponentAbbreviation;   // delete this read-only field before writing
        delete json.opponentMascot;   // delete this read-only field before writing

        delete json.teamName;   // delete this read-only field before writing
        delete json.teamShortName;  // delete this read-only field before writing
        delete json.teamAbbreviation;   // delete this read-only field before writing
        delete json.teamMascot; // delete this read-only field before writing
        delete json.isPractice; // delete this read-only field before writing
        delete json.isMatch; // delete this read-only field before writing
        json.time = this.validTime(json.time);

        // discarting scores if their empty spaces
        score = json.score;
        opponentScore = json.opponentScore;

        json.score = score ? score : null;
        json.opponentScore = opponentScore ? opponentScore : null;

        return json;
    },

	serializeIntoHash: function(hash, type, record, options) {
	    Ember.merge(hash, this.serialize(record, options));
	},

    // creating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
    extractCreateRecord: function(store, type, payload, id, requestType) {
        var wrappedObj = {};

        delete payload.gameEventData; // We don't need this auxiliar data block anymore

        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
    },

    // updating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
	extractUpdateRecord: function(store, type, payload, id, requestType) {
	    var wrappedObj = {};
        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
	},

    validTime: function(time) {
        if (time) {
            var time_regex = /^(.*?)([0-9]+:[0-9]+:[0-9])+(.*?)/;
            var result = time_regex.exec(time);
            return result[2];
        }
        return null;
    }
});
