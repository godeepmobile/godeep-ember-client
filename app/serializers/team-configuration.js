import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {

    serializeIntoHash: function(hash, type, record, options) {
        Ember.merge(hash, this.serialize(record, options));
    },

    // creating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
    extractCreateRecord: function(store, type, payload, id, requestType) {
        var wrappedObj = {};
        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
    },

    // updating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
	extractUpdateRecord: function(store, type, payload, id, requestType) {
	    var wrappedObj = {};
        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
	}
});
