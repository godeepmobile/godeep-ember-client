import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
    attrs: {
        team: { embedded: 'always' },
        teamGraders: { embedded: 'always' },
        teamConfiguration: { embedded: 'always' },
        teamRoles: { embedded: 'always' }
    }
});
