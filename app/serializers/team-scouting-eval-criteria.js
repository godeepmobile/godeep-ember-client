import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {

    serializeIntoHash: function(hash, type, record, options) {
        Ember.merge(hash, this.serialize(record, options));
    },

    // creating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
    extractCreateRecord: function(store, type, payload, id, requestType) {
        var wrappedObj = {};
        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
    },

    // updating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
    extractUpdateRecord: function(store, type, payload, id, requestType) {
        var wrappedObj = {};

        if (payload.calculatedTargetScore) {
            if (payload.calculatedTargetScore % 1 === 0) {
                // The server returns an integer, we need to add 00 at the end
                payload.calculatedTargetScore += '.00' ;
            } else {
                // the server returns a float, we need to add a zero if only one decimal place was returned
                if ((payload.calculatedTargetScore + '').split('.')[1].length < 2) {
                    payload.calculatedTargetScore += '0';
                }
            }
        }

        if (payload.customTargetScore) {
            if (payload.customTargetScore % 1 === 0) {
                // The server returns an integer, we need to add 00 at the end
                payload.customTargetScore += '.00' ;
            } else {
                // the server returns a float, we need to add a zero if only one decimal place was returned
                if ((payload.customTargetScore + '').split('.')[1].length < 2) {
                    payload.customTargetScore += '0';
                }
            }
        }

        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
    }
});
