import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
    serialize: function(snapshot, options) {
        var json = this._super(snapshot, options);
        delete json.addedPlayers;
        delete json.assignedPlayers;
        delete json.gradePlayers;
        delete json.gsisParticipation; //Read-only fields
        delete json.playData; //Read-only fields
        return json;
    },

	serializeIntoHash: function(hash, type, record, options) {
	    Ember.merge(hash, this.serialize(record, options));
	},

    // creating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
    extractCreateRecord: function(store, type, payload, id, requestType) {
        var wrappedObj = {};

        if (typeof payload.playData === 'string') {
            // The server returns the nested object as an string
            payload.playData = JSON.parse(payload.playData);
        }

        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
    },

    // updating a record on the server returns the record without the model name
    // need to wrap return value in top level object called model name
	extractUpdateRecord: function(store, type, payload, id, requestType) {
	    var wrappedObj = {};

        if (typeof payload.playData === 'string') {
            // The server returns the nested object as an string
            payload.playData = JSON.parse(payload.playData);
        }

        wrappedObj[type.typeKey] = payload;
        return this._super(store, type, wrappedObj, id, requestType);
	}
});
