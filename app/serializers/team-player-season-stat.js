import DS from 'ember-data';

export default DS.RESTSerializer.extend({
	attrs: {
        teamPlayer: { embedded: 'always' },
        teamPlayerAssignment: { embedded: 'always' }
    },

    normalizePayload: function(payload) {
    	var teamPlayerAssignments = []
    	  , teamPlayers           = []
    	  , teamPlayer
    	  , teamPlayerAssignment;

    	if (payload.TeamPlayerSeasonStats) {
            // collection
            payload.TeamPlayerSeasonStats.forEach(function(teamPlayerSeasonStat) {
        		teamPlayerAssignment = teamPlayerSeasonStat.teamPlayerAssignment;
        		teamPlayer           = teamPlayerSeasonStat.teamPlayer;

        		if (teamPlayerAssignment) {
        			if (!teamPlayerAssignments.isAny('id', teamPlayerAssignment.id)) {
        				teamPlayerAssignments.push(teamPlayerAssignment);
        			}
        			teamPlayerSeasonStat.teamPlayerAssignment = teamPlayerAssignment.id;
        		}

        		if (teamPlayer) {
        			if (!teamPlayers.isAny('id', teamPlayer.id)) {
        				teamPlayers.push(teamPlayer);
        			}
        			teamPlayerSeasonStat.teamPlayer = teamPlayer.id;
        		}
        	});

        	payload['TeamPlayerAssignments'] = teamPlayerAssignments;
        	payload['TeamPlayers']           = teamPlayers;
        } else {
            if (payload.TeamPlayerSeasonStat) {
                //single object
                teamPlayerAssignment = payload.TeamPlayerSeasonStat.teamPlayerAssignment;
                teamPlayer           = payload.TeamPlayerSeasonStat.teamPlayer;

                if (teamPlayerAssignment) {
                    payload['TeamPlayerAssignments'] = [teamPlayerAssignment];
                    payload.TeamPlayerSeasonStat.teamPlayerAssignment = teamPlayerAssignment.id;
                }

                if (teamPlayer) {
                    payload['TeamPlayers'] = [teamPlayer];
                    payload.TeamPlayerSeasonStat.teamPlayer = teamPlayer.id;
                }
            }
        }

    	return payload;
    }
});
