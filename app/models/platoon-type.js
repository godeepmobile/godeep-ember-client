import DS from 'ember-data';

var attr = DS.attr;

var PlatoonType = DS.Model.extend({
    name: attr(),
    shortName: attr(),
    description: attr(),

    teamCutups: DS.hasMany('teamCutup', {async:true}),

    isAllPlatoon: function() {
    	return this.get('id') === '4';
    }.property('id'),

    isOffense: function() {
        return this.get('id') === '1';
    }.property('id'),

    isDefense: function() {
        return this.get('id') === '2';
    }.property('id'),

    isSpecialTeams: function() {
    	return this.get('id') === '3';
    }.property('id'),

    isUnassigned: function() {
    	return this.get('id') === '0';
    }.property('id')
});

export default PlatoonType;
