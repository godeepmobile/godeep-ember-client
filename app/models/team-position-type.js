import DS from 'ember-data';
import Ember from 'ember';

var attr = DS.attr;
var belongsTo = DS.belongsTo;

var TeamPositionType = DS.Model.extend({
    name             : attr(),  // read-only, comes from related go_positon_type
    shortName        : attr(),  // read-only, comes from related go_position_type
    teamPositionGroup: attr(),
    platoonType      : attr(),
    endDate          : attr(),
    positionType     : belongsTo('positionType', {async:true})
});
TeamPositionType.reopenClass({
    FIXTURES: [
        {
            "id": 2,
            "positionType": 1,
            "name": "Quarterback",
            "shortName": "QB",
            "team": 1,
            "teamPositionGroup": 2
        },
        {
            "id": 3,
            "positionType": 2,
            "name": "Linebacker",
            "shortName": "LB",
            "team": 1,
            "teamPositionGroup": 1
        }
    ]
});

export default TeamPositionType;
