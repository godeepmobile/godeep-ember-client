import DS from 'ember-data';

export default DS.Model.extend({

  	name: DS.attr(),
	criteria: DS.attr(),
	criteriaRange: DS.attr(),
	scope: DS.attr(),
	scopeRange: DS.attr(),
	condition: DS.attr(),
	conditionRange: DS.attr(),
	isPublic: DS.attr(),
	user: DS.attr(),

	type: DS.belongsTo('reportType', {async:true}),
	eventType: DS.belongsTo('eventType', {async:true})
});
