import DS from 'ember-data';

var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

var GameStat = DS.Model.extend({
    teamPlayer: belongsTo('teamPlayer',{async:true}),
    season: attr(),             // which season, currently year as number
    game: attr(),               // currently game #
    gameDate: attr(),           // TODO currently a string, needs to be a timestamp
    numPlays: attr(),           // total plays per game
    cat1Grade: attr(),      // average grade across all plays
    cat2Grade: attr(),      // average grade across all plays
    cat3Grade: attr(),      // average grade across all plays
    overallGrade: attr(),       // average grade across all plays
    impactPlatoonGrade: attr(),
    impactPosGroupGrade: attr()
});

export default GameStat;
