// app/models/team-prospect.js

import Ember from 'ember';
import DS from 'ember-data';

var attr = DS.attr;
var hasMany = DS.hasMany;
var belongsTo = DS.belongsTo;

export default DS.Model.extend({
    firstName: attr(),
    middleName: attr(),
    lastName: attr(),
    jerseyNumber: attr(),
    classYear: attr(),
    birthDate: attr(),
    height: attr(),
    weight: attr(),
    graduationYear: attr(),
    wingspan: attr(),
    fortyYard: attr(),
    handSize: attr(),
    verticalJump: attr(),
    sat: attr(),
    act: attr(),
    gpa: attr(),
    highSchool: attr(),
    coach: attr(),
    coachPhone: attr(),

    level: belongsTo('level', {async:true}),
    bodyType: belongsTo('bodyType', {async:true}),
    team: belongsTo('team', {async:true}),
    state: belongsTo('state', {async:true}),
    projectedPosition: belongsTo('positionType', {async:true}),
    currentPosition: belongsTo('positionType', {async:true}),
    position3: belongsTo('positionType', {async:true}),
    positionST: belongsTo('positionType', {async:true}),

    fullName: function() {
        return this.get('firstName') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    firstInitial: function() {
        var name = this.get('firstName');
        if (name && name.length > 0) {
            return name.substring(0, 1) + '.';
        }
        return '?.';
    }.property('firstName'),

    shortNameInitialFirst: function() {
        return this.get('firstInitial') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    shortNameInitialLast: function() {
        return this.get('lastName') + ', ' + this.get('firstInitial');
    }.property('firstName', 'lastName'),

    fullNameInitialLast: function() {
        var name     = this.get('firstName')
            , lastName = this.get('lastName');

        if ( name && lastName ) {
            return this.get('lastName') + ', ' + this.get('firstName');
        }

        return '?';
    }.property('firstName', 'lastName'),

    heightFormatted: function() {
        var height = this.get('height');
        if ( height ) {
            return height;
        }
        return '?';
    }.property('height'),

    weightFormatted: function() {
        var weight = this.get('weight');
        if ( weight ) {
            return weight;
        }
        return '?';
    }.property('weight'),

    birthDateFormatted: function() {
        if ( this.get('birthDate') ) {
            var bd = new Date(this.get('birthDate'));
            this.set('birthDate', moment(bd).format('YYYY-MM-DD'));

            return moment(bd).format('MMMM D, YYYY');
        }
        return '';
    }.property('birthDate'),

    name: function() {
        return this.get('firstName') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    allPositions: function() {
        // tell call that we promise to return an array eventually
        return DS.PromiseArray.create({
                // this is the promise part of the PromiseArray
                promise: new Ember.RSVP.Promise(function(resolve) {
                    // RSVP.all waits for all promises to resolve, and returns responses in an array
                    Ember.RSVP.all([
                        this.get('projectedPosition'),
                        this.get('currentPosition'),
                        this.get('positionST')
                    ])
                        // now we have all 4 positions loaded and in an array called positions
                        .then(function(positions) {
                            var result = [];
                            // start collecting non-blank positions into a result array
                            positions.forEach(function(position){
                                if (position) {
                                    result.push(position);
                                }
                            });
                            // resolve our promise returning just the unique positions
                            resolve(result.uniq());
                        });
                }.bind(this)) // pass in our context
            }
        );
    }.property('projectedPosition','currentPosition','positionST')
});
