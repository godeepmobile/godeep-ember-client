import DS from 'ember-data';

var OrganizationType = DS.Model.extend({
    name: DS.attr('string')
});
OrganizationType.reopenClass({
    FIXTURES: [
        {
            "id":2,
            "name":"College Sports"
        }
    ]
});

export default OrganizationType;
