import DS from 'ember-data';

export default DS.Model.extend({
    // dimensions
    positionType : DS.belongsTo('positionType', {async:true}),
    teamEvent : DS.belongsTo('teamEvent', {async:true}),
    quarter: DS.attr(),

    // measures
    numPlays: DS.attr(),
    cat1Grade : DS.attr(),
    cat2Grade : DS.attr(),
    cat3Grade : DS.attr(),
    avgOverallGrade : DS.attr()
});
