// app/models/team-user.js

import DS from 'ember-data';
var attr = DS.attr;
var belongsTo = DS.belongsTo;

export default DS.Model.extend({
    username   : attr(),
    firstName  : attr(),
    middleName : attr(),
    lastName   : attr(),
    email      : attr(),
    jobTitle        : attr(),
    officePhone     : attr(),
    mobilePhone     : attr(),
    password       : attr(),
    role            : belongsTo('userRoleType', {async:true}),
    groupAssignments: DS.hasMany('teamUserPositionGroupAssignment', {async:true}),

    fullName: function() {
        return this.get('lastName') + ', ' + this.get('firstName');
    }.property('firstName', 'lastName'),

    firstInitial: function() {
        var name = this.get('firstName');
        if (name && name.length > 0) {
            return name.substring(0, 1) + '.';
        }
        return '?.';
    }.property('firstName'),

    shortNameInitialFirst: function() {
        return this.get('firstInitial') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    watchRelationshipChanges: function() {
        this.set('currentState.isDirty', true);
    }.observes('role'),

    name: function() {
        return this.get('firstName') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName')
});
