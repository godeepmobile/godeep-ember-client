// app/models/team-player-career-stat.js

import DS from 'ember-data';

var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

var TeamPlayerCareerStat = DS.Model.extend({
    teamPlayer: belongsTo('teamPlayer', {async:true}),

    seasonCount: attr(),          // how many seasons has this player played?
    isVeteran: attr(),

    gameCount: attr(),           // total num games in this season
    gameNumPlays: attr(),           // total num plays in this season
    gameCat1Grade: attr(),
    gameCat2Grade: attr(),
    gameCat3Grade: attr(),
    gameOverallGrade: attr(),
    gameImpactPlatoonGrade: attr(),
    gameImpactPosGroupGrade: attr(),

    practiceCount: attr(),           // total num games in this season
    practiceNumPlays: attr(),           // total num plays in this season
    practiceCat1Grade: attr(),
    practiceCat2Grade: attr(),
    practiceCat3Grade: attr(),
    practiceOverallGrade: attr(),
    practiceImpactPlatoonGrade: attr(),
    practiceImpactPosGroupGrade: attr(),

    allEventCount: attr(),           // total num games in this season
    allEventNumPlays: attr(),           // total num plays in this season
    allEventCat1Grade: attr(),
    allEventCat2Grade: attr(),
    allEventCat3Grade: attr(),
    allEventOverallGrade: attr(),
    allEventImpactPlatoonGrade: attr(),
    allEventImpactPosGroupGrade: attr(),
});

export default TeamPlayerCareerStat;
