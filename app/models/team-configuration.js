// app/models/team-configuration.js

import DS from 'ember-data';
var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

export default DS.Model.extend({
    team: belongsTo("team", {async:true}),
    gradeCat1Label: attr(),
    gradeCat2Label: attr(),
    gradeCat3Label: attr(),
    gradeBase: attr(),
    gradeIncrement: attr(),
    scoutEvalScoreMin: attr(),
    scoutEvalScoreMax: attr(),
    numGradeCategories: attr(),
    canEvaluate: attr(),
    createCutupForEachPlatoon: attr(),
    playersCanSeeRanks: attr(),
    playersCanSeeAverages: attr(),
    gradeFields: attr()
});
