import DS from 'ember-data';

export default DS.Model.extend({
	positionType: DS.belongsTo('positionType', {async:true}),
	importance: DS.belongsTo('importance', {async:true})
});
