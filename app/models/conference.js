import DS from 'ember-data';

var belongsTo = DS.belongsTo;

var Conference = DS.Model.extend({
    name: DS.attr('string'),
    description: DS.attr('string'),
    level: DS.attr()
    //level: belongsTo('level', {async:true})
});
Conference.reopenClass({
    FIXTURES: [
        {
            "name": "ACC",
            "id": 1
        },
        {
            "name": "SEC",
            "id": 2
        },
        {
            "name": "IVY",
            "id": 5
        },
        {
            "name": "PFL",
            "id": 13
        },
        {
            "name": "PL",
            "id": 14
        }
    ]
});

export default Conference;
