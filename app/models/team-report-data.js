import DS from 'ember-data';

export default DS.Model.extend({
    // dimensions
    season : DS.attr(),
    date : DS.attr(),
    //TODO: fieldCondition : DS.belongsTo('fieldConditionType',{async:true}),
    surfaceType: DS.belongsTo('surfaceType',{async:true}),
    opponent: DS.belongsTo('organization', {async:true}),
    isHomeGame: DS.attr(),
    teamEvent : DS.belongsTo('teamEvent', {async:true}),
    teamCutup : DS.belongsTo('teamCutup', {async:true}),
    cutupPlatoonShortName : DS.attr(),
    playNumber: DS.attr(),
    quarter: DS.attr(),
    gameDown: DS.attr(),
    gameDistance: DS.attr(),
    yardLine: DS.attr(),
    playType: DS.belongsTo('playType',{async:true}),
    gamePossession: DS.attr(),
    playResultType: DS.belongsTo('playResultType',{async:true}),
    teamPlayer : DS.belongsTo('teamPlayer', {async:true}),
    positionType: DS.belongsTo('positionType',{async:true}),
    platoonType: DS.belongsTo('platoonType',{async:true}),
    teamPositionGroup: DS.belongsTo('teamPositionGroup',{async:true}),
    grader: DS.belongsTo('teamUser', {async:true}),
    hard: DS.attr(),
    factorInPlay: DS.attr(),
    redZone: DS.attr(),

    // measures
    cat1Grade : DS.attr(),
    cat2Grade : DS.attr(),
    cat3Grade : DS.attr(),
    sumOverallGrade : DS.attr(),
    avgOverallGrade : DS.attr()
});
