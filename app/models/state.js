import DS from 'ember-data';

var attr = DS.attr;

var State = DS.Model.extend({
    name: attr(),
});

export default State;
