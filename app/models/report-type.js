import DS from 'ember-data';

export default DS.Model.extend({

	name: DS.attr(),
	description: DS.attr(),
	path: DS.attr(),

	teamReports: DS.hasMany('teamReport', {async:true})
  
});
