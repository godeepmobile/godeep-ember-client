import DS from 'ember-data';

export default DS.Model.extend({

	name: DS.attr(),
	shortName: DS.attr(),
	description: DS.attr(),
	isPractice: DS.attr(),
	isMatch: DS.attr(),

	teamEvents: DS.hasMany('teamEvent', {async:true}),

	isGame: function() {
		return !this.get('isPractice') && this.get('isMatch');
	}.property('isPractice', 'isMatch'),

	isPracticeGame: function() {
		return this.get('isPractice') && this.get('isMatch');
	}.property('isPractice', 'isMatch'),

	isRegularPractice: function() {
		return this.get('isPractice') && !this.get('isMatch');
	}.property('isPractice', 'isMatch')
});
