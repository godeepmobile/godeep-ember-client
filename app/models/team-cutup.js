import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
	numPlays: DS.attr(),
	fileUri: DS.attr(),
	whichPlatoon: DS.belongsTo('platoonType', {async:true}),
	team: DS.belongsTo('team', {async:true}),
	teamEvent: DS.belongsTo('teamEvent', {async:true}),
	teamPlays: DS.hasMany('teamPlay', {async:true}),
	teamEventGradeStatuses: DS.hasMany('teamEventGradeStatus', {async:true}),
	teamPlayerGameCutupStats: DS.hasMany('teamPlayerGameCutupStat', {async:true}),
	
	// Property that is lazily setted by itself using promises, when it's first accessed and
	// any time the referenced properties change (gameEvent, description) it will kick off
	// the calculation and return a result when it's done, this result is cached and returned
	// the other times either if called as a getter or setter.
	//
	// Once the promise resolves, it calls set on the same property with the calculated value. This
	// just return in the setter and it's now cached as the value of the property
	platoonName: Ember.computed('whichPlatoon', {
		get : function() {
			var label, self = this;

			this.get('whichPlatoon').then(function(platoonType) {
				label = platoonType.get('name');
				self.set('platoonName', label);
			});

			return label;
		},

		set: function(key, value) {
			return value;
		}
	}),

	gameEventTitle: function() {
		return (this.get('teamEvent.isMatch') ? this.get('teamEvent.gameTitle') + ' - ' + this.get('platoonName') : this.get('teamEvent.eventLabel'));
	}.property('teamEvent.isMatch', 'teamEvent.gameTitle', 'platoonName'),

	eventLabel: function() {
		return this.get('teamEvent.eventLabel') + ' - ' + this.get('platoonName');
	}.property('teamEvent.eventLabel', 'platoonName')
});
