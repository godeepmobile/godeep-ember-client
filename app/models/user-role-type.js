// app/models/go-user.js

import DS from 'ember-data';
var attr = DS.attr;

export default DS.Model.extend({
    name       : attr(),
    description: attr(),

    isCoach: function() {
    	return this.get('id') === '3';
    }.property('id'),

    isAdmin: function() {
    	return this.get('id') === '4';
    }.property('id')
});