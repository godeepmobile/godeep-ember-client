// app/models/team-position-group.js

import DS from 'ember-data';

var attr = DS.attr;

var TeamPositionGroup = DS.Model.extend({
    name             : attr(),
    team             : attr(),
    platoonType      : attr(),
    endDate          : attr(),
    positionTypeIds  : attr()
});
export default TeamPositionGroup;
