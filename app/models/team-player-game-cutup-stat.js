import DS from 'ember-data';

export default DS.Model.extend({
  teamPlayer : DS.belongsTo('teamPlayer', {async:true}),
  teamCutup : DS.belongsTo('teamCutup', {async:true}),
  season : DS.attr(),
  numPlays : DS.attr(),
  avgCat1Grade : DS.attr(),
  avgCat2Grade : DS.attr(),
  avgCat3Grade : DS.attr(),
  avgOverallGrade : DS.attr()
});
