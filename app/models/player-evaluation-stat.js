// app/models/player-stat.js

import DS from 'ember-data';

var attr    = DS.attr,
  belongsTo = DS.belongsTo;

export default DS.Model.extend({
    season          : attr(),
    targetMatch     : attr(),
    overallEvalAvg  : attr(),
    majorFactors    : attr(),
    criticalFactors : attr(),
    positionSkills  : attr(),
    rankOverall     : attr(),
    rankAtPost      : attr(),
    teamPlayer      : attr(),
    date            : attr(),
    team            : attr(),
    teamScoutingEvalScoreGroup   : attr(),
    user         : belongsTo('teamUser', {async:true})
});
