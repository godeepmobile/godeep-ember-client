// app/models/team-scouting-eval-criteria.js

import DS from 'ember-data';

var attr      = DS.attr
  , belongsTo = DS.belongsTo;

export default DS.Model.extend({
    name                     : attr(),
    team                     : attr(),
    isCritical               : attr(),
    calculatedTargetScore    : attr(),
    customTargetScore        : attr(),
    useCalculatedTargetScore : attr(),
    positionType             : attr(),
    scoutingEvalCriteriaType : attr(),
    sortIndex                : attr(),
    importance               : attr()
});
