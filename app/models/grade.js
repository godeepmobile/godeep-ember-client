import DS from 'ember-data';

var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

var Grade = DS.Model.extend({
    //id                                        // Org_Event_Player_Grade_Id
    teamPlay: belongsTo('teamPlay', {async:true}),           // Org_Event_PlayData_Id
    teamPlayer: belongsTo('teamPlayer', {async:true}),               // Player_Profile_Id
    positionPlayed: belongsTo('positionType', {async:true}),                          // Position_Cd
    cat1Grade: attr(),                     // Main_Cat_1_Grade
    cat2Grade: attr(),                     // Main_Cat_2_Grade
    cat3Grade: attr(),                     // Main_Cat_3_Grade
    playFactor: belongsTo('playFactor', {async:true}),
    hard: attr(),
    notes: attr(),
    playersCanSeeNotes: attr(),
    user: attr(),
    team: belongsTo('team', {async:true}),
    //teamPositionGroup: belongsTo('teamPositionGroup', {async:true}),
    //platoonType: belongsTo('platoonType', {async:true})
    platoonType: attr(),
    customFields: attr()
});

export default Grade;


/*
 CREATE TABLE [dbo].[Org_Event_Player_Grades](
 [Org_Event_Player_Grade_Id] [bigint] NOT NULL,
 [Org_Event_PlayData_Id] [bigint] NOT NULL,
 [Player_Profile_Id] [bigint] NOT NULL,
 [Position_Cd] [varchar](3) NOT NULL,
 [Organization_Id] [bigint] NOT NULL,
 [Main_Cat_1_Grade] [bigint] NOT NULL,
 [Main_Cat_2_Grade] [bigint] NOT NULL,
 [Main_Cat_3_Grade] [bigint] NOT NULL,
 [Created_DateTime] [timestamp] NOT NULL,
 [Tech_Align_Cd] [varchar](3) NULL,
 [Player_Play_Grade_Comments] [varchar](500) NULL,
 [Org_Grade_Increment_Value] [int] NOT NULL,
 CONSTRAINT [PK_Game_Player_Grade] PRIMARY KEY CLUSTERED
 (
 [Org_Event_Player_Grade_Id] ASC
 )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
 ) ON [PRIMARY]
 */
