import DS from 'ember-data';

export default DS.Model.extend({
    platoonType: DS.belongsTo('platoonType', {async:true}),
    playDataFields: DS.attr()
});
