import DS from 'ember-data';
import Ember from 'ember';

var attr = DS.attr;
var belongsTo = DS.belongsTo;
var hasMany = DS.hasMany;

export default DS.Model.extend({
  	team: belongsTo('team', {async:true}),
    isVeteran: attr(),
    jerseyNumber: attr(),
    position1: belongsTo('positionType', {async:true}),
    position2: belongsTo('positionType', {async:true}),
    position3: belongsTo('positionType', {async:true}),
    positionST: belongsTo('positionType', {async:true}),
    teamPlayer: belongsTo('teamPlayer', {async:true}),
    isSpecialTeams: attr(),
    isOffense: attr(),
    isDefense: attr(),
    season: attr(),
    startDate: attr(),
    endDate: attr(),
    temporarilyInactive: attr(),
    teamPlayerSeasonStats: hasMany('teamPlayerSeasonStat',{async:true}),
    teamPlayerCareerStat: belongsTo('teamPlayerCareerStat', {async:true}),
    teamPlayerGameStats: hasMany('teamPlayerGameStat', {async:true}),
    teamCutupsWithGrades: hasMany('teamCutup', {async:true}),

    playsWhereAdded: attr(),

    allPositions: function() {
        // tell call that we promise to return an array eventually
        return DS.PromiseArray.create({
                // this is the promise part of the PromiseArray
                promise: new Ember.RSVP.Promise(function(resolve) {
                    // RSVP.all waits for all promises to resolve, and returns responses in an array
                    Ember.RSVP.all([
                        this.get('position1'),
                        this.get('position2'),
                        this.get('position3'),
                        this.get('positionST')
                    ])
                        // now we have all 4 positions loaded and in an array called positions
                        .then(function(positions) {
                            var result = [];
                            // start collecting non-blank positions into a result array
                            positions.forEach(function(position){
                                if (position) {
                                    result.push(position);
                                }
                            });
                            // resolve our promise returning just the unique positions
                            resolve(result.uniq());
                        });
                }.bind(this)) // pass in our context
            }
        );
    }.property('position1','position2','position3','positionST'),

    allPositionGroups: function() {
        // tell call that we promise to return an array eventually
        return DS.PromiseArray.create({
            // this is the promise part of the PromiseArray
            promise: new Ember.RSVP.Promise(function(resolve){
                var result = [];
                // asynchronously get all positions into an array
                this.get('allPositions')
                    // when this returns, we have all positions in allPositions
                    .then(function(allPositions) {
                        // start collecting position groups into a result array
                        allPositions.forEach(function(position){
                            result.push(position.get('teamPositionGroup'));
                        });
                        // resolve our promise returning just the unique position groups
                        resolve(result.uniq());
                    });
            }.bind(this)) // pass in our context
        });
    }.property('allPositions')
});
