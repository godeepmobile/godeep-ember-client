// app/models/team-event.js

import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
	date: DS.attr(),
	description: DS.attr(),
	gradeBase: DS.attr(),
	gradeIncrement: DS.attr(),
	// data: DS.attr(), // `data` is a reserved property name on DS.Model objects
	season: DS.attr(),
	eventType: DS.belongsTo('eventType', {async:true}),
	gameEvent: DS.belongsTo('gameEvent', {async:true}),
	gameEventData: DS.attr(),
	teamPlays: DS.hasMany('teamPlay', {async:true}),

	teamEventGradeStatuses: DS.hasMany('teamEventGradeStatus', {async:true}),
	teamPlayerGameStats: DS.hasMany('teamPlayerGameStat', {async:true}),

	teamCutups: DS.hasMany('teamCutup', {async:true}),

	name: DS.attr(),

	basedEvent: DS.belongsTo('teamEvent', {async:true}),

	// from GameEvent
	score: DS.attr(),
	opponentScore: DS.attr(),
	opponent: DS.belongsTo('organization', {async:true}),
	opponentName: DS.attr(),			// read-only
	opponentShortName: DS.attr(),		// read-only
	opponentAbbreviation: DS.attr(),	// read-only
	opponentMascot: DS.attr(),			// read-only
	teamName: DS.attr(),				// read-only
    teamShortName: DS.attr(),			// read-only
    teamAbbreviation: DS.attr(),		// read-only
    teamMascot: DS.attr(),				// read-only
	isHomeGame: DS.attr(),
	time: DS.attr(),
	location: DS.attr(),
	city: DS.attr(),
	state: DS.belongsTo('state', {async:true}),
	isPractice: DS.attr(),
	isMatch: DS.attr(),

	practiceGameTeam1: DS.attr(),
	practiceGameTeam2: DS.attr(),

	practiceGameTeam1Score: DS.attr(),
	practiceGameTeam2Score: DS.attr(),

	isBasedOnAnEvent: function() {
		return this.get('basedEvent.id') ? true : false;
	}.property('basedEvent'),

	formattedDate: function() {
		return moment.utc(new Date(this.get('date'))).format('M-D-YY');
	}.property('date'),

	numPlays: function() {
		return this.get('teamPlays.length');
	}.property('teamPlays'),

	dateForPicker: Ember.computed('date', {
		get: function() {
			var _date = this.get('date');
			return _date ? moment.utc(new Date(_date)).format('MM/DD/YYYY') : undefined;
		},

		set: function(key, value) {
			this.set('date', value);
			return value;
		}
	}),

	eventLabel: function() {
		return this.get('name') || (this.get('opponent.id') ? this.get('gameTitle') : this.get('description'));
	}.property('name', 'opponent', 'gameTitle', 'description'),

	gameTitle: function() {
		var opponentNameAndScore = this.get('opponentShortName') + (this.get('opponentScore') ? ' ' + this.get('opponentScore') : ' 0')
		  , teamNameAndScore     = this.get('teamShortName') + (this.get('score') ? ' ' + this.get('score') : ' 0');

		return (this.get('isHomeGame') ? 
			opponentNameAndScore + ' @ ' + teamNameAndScore : 
			teamNameAndScore + ' @ ' + opponentNameAndScore) + ' - ' + this.get('formattedDate');
	}.property('opponentShortName', 'opponentScore', 'teamShortName', 'score', 'isHomeGame', 'formattedDate'),

	gameTitleAbbr: function() {
		var opponentNameAndScore = this.get('opponentAbbreviation') + (this.get('opponentScore') ? ' ' + this.get('opponentScore') : ' 0')
		  , teamNameAndScore     = this.get('teamAbbreviation') + (this.get('score') ? ' ' + this.get('score') : ' 0');

		return (this.get('isHomeGame') ? 
			opponentNameAndScore + ' @ ' + teamNameAndScore : 
			teamNameAndScore + ' @ ' + opponentNameAndScore) + ' - ' + this.get('formattedDate');
	}.property('opponentShortName', 'opponentScore', 'teamShortName', 'score', 'isHomeGame', 'formattedDate'),

	gameName: function() {
		return this.get('name') + ' - ' + this.get('formattedDate');
	}.property('name', 'formattedDate'),

	picklistName: function() {
		return this.get('season') + ' - ' + this.get('name');
	}.property('season', 'name')
});
