import DS from 'ember-data';

var attr      = DS.attr,
    belongsTo = DS.belongsTo;

var UserRole = DS.Model.extend({
    fullName: attr(),
    role    : attr(),
    team    : belongsTo('team', {async:true})
});

export default UserRole;