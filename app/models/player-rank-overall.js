import DS from 'ember-data';

var attr      = DS.attr
  , belongsTo = DS.belongsTo;

export default DS.Model.extend({
    team                  : attr(),
    season                : attr(),
    rankOverall           : attr(),
    overallEvaluationRank : attr(),
    overallGradeGameRank  : attr(),
    rankAtPost            : attr(),
    rankAtEvaluationPost  : attr(),
    rankAtGradePost       : attr(),
    rankAtOverallPost     : attr(),
    gameGradeAvg          : attr(),
    targetMatch           : attr(),
    overallEvalAvg        : attr(),
    majorFactorsEvalAvg   : attr(),
    criticalFactorsEvalAvg: attr(),
    positionSkillsEvalAvg : attr(),
    numPlays              : attr(),
    teamPlayer            : belongsTo('teamPlayer', {async:true}),
    teamPlayerAssignment  : belongsTo('teamPlayerAssignment', {async:true})
});
