// app/models/go-user.js

import DS from 'ember-data';
var attr = DS.attr;

export default DS.Model.extend({
    username             : attr(),
    firstName            : attr(),
    middleName           : attr(),
    lastName             : attr(),
    email                : attr(),
    jobTitle             : attr(),
    officePhone          : attr(),
    mobilePhone          : attr(),
    firstTimeLogin       : attr(),
    displayWelcomeDialog : attr(),
});