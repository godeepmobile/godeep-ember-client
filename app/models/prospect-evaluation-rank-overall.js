import DS from 'ember-data';

var attr      = DS.attr
  , belongsTo = DS.belongsTo;

export default DS.Model.extend({
    team                  : attr(),
    season                : attr(),
    rankOverall           : attr(),
    rankAtPost            : attr(),
    targetMatch           : attr(),
    overallEvalAvg        : attr(),
    majorFactorsEvalAvg   : attr(),
    criticalFactorsEvalAvg: attr(),
    positionSkillsEvalAvg : attr(),
    teamProspect          : belongsTo('teamProspect', {async:true}),
    teamProspectAssignment: belongsTo('teamProspectAssignment', {async:true})
});
