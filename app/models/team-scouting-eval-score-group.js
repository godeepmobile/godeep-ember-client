// app/models/team-scouting-eval-score-group.js

import DS from 'ember-data';

var attr    = DS.attr,
  belongsTo = DS.belongsTo,
  hasMany   = DS.hasMany;

export default DS.Model.extend({
    team         : attr(),
    date         : attr(),
    season       : attr(),
    lastUpdate   : attr(),
    teamPlayer   : attr(),
    user         : belongsTo('teamUser', {async:true}),
    positionType : belongsTo('positionType', {async:true}),
    teamScoutingEvalScores: hasMany('teamScoutingEvalScore')
});
