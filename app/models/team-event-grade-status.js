import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({

	team: DS.belongsTo('team', {async:true}),
  	user: DS.attr(),
  	status: DS.attr(),
  	teamEvent: DS.belongsTo('teamEvent', {async:true}),
    teamCutup: DS.belongsTo('teamCutup', {async:true}),
  	lastSubmitDate: DS.attr(),
  	startedDate: DS.attr(),
    lastGradedPlay: DS.attr(),

  	isCompleted: function() {
  		return this.get('status') === 'completed';
  	}.property('status'),

  	isInProcess: function() {
  		return this.get('status') === 'in process';
  	}.property('status'),

  	isNotStarted: function() {
  		return this.get('status') === 'not started';
  	}.property('status')
    
});
