import DS from 'ember-data';

var attr = DS.attr;

var SurfaceType = DS.Model.extend({
    name: attr()
});

export default SurfaceType;