import DS from 'ember-data';

var attr = DS.attr;

var BodyType = DS.Model.extend({
    name: attr(),
    description: attr()
});

export default BodyType;
