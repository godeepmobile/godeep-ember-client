// app/models/team-player.js

import Ember from 'ember';
import DS from 'ember-data';

var attr = DS.attr;
var hasMany = DS.hasMany;
var belongsTo = DS.belongsTo;

var TeamPlayer = DS.Model.extend({
    firstName: attr(),
    middleName: attr(),
    lastName: attr(),
    jerseyNumber: attr(),
    classYear: attr(),
    birthDate: attr(),
    height: attr(),
    weight: attr(),
    graduationYear: attr(),
    wingspan: attr(),
    fortyYard: attr(),
    handSize: attr(),
    verticalJump: attr(),
    email: attr(),
    sat: attr(),
    act: attr(),
    gpa: attr(),
    isVeteran: attr(),
    highSchool: attr(),
    coach: attr(),
    coachPhone: attr(),

    level: belongsTo('level', {async:true}),
    bodyType: belongsTo('bodyType', {async:true}),
    team: belongsTo('team', {async:true}),
    state: belongsTo('state', {async:true}),
    position1: belongsTo('positionType', {async:true}),
    position2: belongsTo('positionType', {async:true}),
    position3: belongsTo('positionType', {async:true}),
    positionST: belongsTo('positionType', {async:true}),

    grades: DS.hasMany('grade', {async:true}),
    teamPlayerAssignments: DS.hasMany('teamPlayerAssignments', {async:true}),

    // treat these attributes as read-only, do not post back to the server
    isSpecialTeams: attr(),
    isOffense: attr(),
    isDefense: attr(),
    teamPlayerSeasonStats: hasMany('teamPlayerSeasonStat',{async:true}),
    teamPlayerCareerStat: belongsTo('teamPlayerCareerStat', {async:true}),
    teamPlayerGameStats: DS.hasMany('teamPlayerGameStat', {async:true}),

    // computed property that returns an array of all unique/non-blank positions
    // the core of what we want to do is:
    // *    wait for all positions to load asynchronously
    // *    then add non-blank positions to a result array
    // *    return the result.
    // this is complicated b/c loading a position asynchronously returns a promise,
    // so we must wait for all 4 positions to load, and then we can process them.
    // we need to give the caller a promise until we can process the positions,
    // but it needs to be an array AND a promise,
    // so we wrap everything in a DS.PromiseArray which has both.
    allPositions: function() {
        // tell call that we promise to return an array eventually
        return DS.PromiseArray.create({
                // this is the promise part of the PromiseArray
                promise: new Ember.RSVP.Promise(function(resolve) {
                    // RSVP.all waits for all promises to resolve, and returns responses in an array
                    Ember.RSVP.all([
                        this.get('position1'),
                        this.get('position2'),
                        this.get('position3'),
                        this.get('positionST')
                    ])
                        // now we have all 4 positions loaded and in an array called positions
                        .then(function(positions) {
                            var result = [];
                            // start collecting non-blank positions into a result array
                            positions.forEach(function(position){
                                if (position) {
                                    result.push(position);
                                }
                            });
                            // resolve our promise returning just the unique positions
                            resolve(result.uniq());
                        });
                }.bind(this)) // pass in our context
            }
        );
    }.property('position1','position2','position3','positionST'),   // do it all again if any of the positions changes.

    // computed property that returns an array of all unique position groups for this player
    // the core of what we want to do is:
    // *    wait for all positions to load asynchronously
    // *    then add position groups to a result array
    // *    return the result.
    // this is complicated b/c loading all position asynchronously returns a promise array,
    // so we must wait for the positions to load, and then we can process them.
    // we need to give the caller a promise until we can process the positions,
    // but it needs to be an array AND a promise, so we wrap everything in a DS.PromiseArray which has both.
    allPositionGroups: function() {
        // tell call that we promise to return an array eventually
        return DS.PromiseArray.create({
            // this is the promise part of the PromiseArray
            promise: new Ember.RSVP.Promise(function(resolve){
                var result = [];
                // asynchronously get all positions into an array
                this.get('allPositions')
                    // when this returns, we have all positions in allPositions
                    .then(function(allPositions) {
                        // start collecting position groups into a result array
                        allPositions.forEach(function(position){
                            result.push(position.get('teamPositionGroup'));
                        });
                        // resolve our promise returning just the unique position groups
                        resolve(result.uniq());
                    });
            }.bind(this)) // pass in our context
        });
    }.property('allPositions'), // do this whenever allPositions changes

    isNewPlayer: function( ) {
        return this.get('classYear') === 'FR';
    }.property('classYear'),

    fullName: function() {
        return this.get('firstName') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    fullNameFirstNameLast: function() {
        return  this.get('lastName') + ', ' + this.get('firstName');
    }.property('firstName', 'lastName'),

    firstInitial: function() {
        var name = this.get('firstName');
        if (name && name.length > 0) {
            return name.substring(0, 1) + '.';
        }
        return '?.';
    }.property('firstName'),

    shortNameInitialFirst: function() {
        return this.get('firstInitial') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    shortNameInitialLast: function() {
        return this.get('lastName') + ', ' + this.get('firstInitial');
    }.property('firstName', 'lastName'),

    fullNameInitialLast: function() {
        var name     = this.get('firstName')
            , lastName = this.get('lastName');

        if ( name && lastName ) {
            return this.get('lastName') + ', ' + this.get('firstName');
        }

        return '?';
    }.property('firstName', 'lastName'),

    heightFormatted: function() {
        var height = this.get('height');
        if ( height ) {
            return height;
        }
        return '?';
    }.property('height'),

    weightFormatted: function() {
        var weight = this.get('weight');
        if ( weight ) {
            return weight;
        }
        return '?';
    }.property('weight'),

    birthDateFormatted: function() {
        if ( this.get('birthDate') ) {
            var bd = new Date(this.get('birthDate'));
            this.set('birthDate', moment(bd).format('YYYY-MM-DD'));

            return moment(bd).format('MMMM D, YYYY');
        }
        return '';
    }.property('birthDate'),

    name: function() {
        return this.get('firstName') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName')
});

export default TeamPlayer;
