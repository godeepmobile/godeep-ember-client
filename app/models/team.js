import DS from 'ember-data';
var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

export default DS.Model.extend({
    shortName: attr(),
    abbreviation: attr(),
    mascot: attr(),
    conference: belongsTo("conference", {async:true}),
    sport: attr(),
    street1: attr(),
    street2: attr(),
    city: attr(),
    state: belongsTo("state", {async:true}),
    postal: attr(),
    phoneMain: attr(),
    phoneFax: attr(),
    homeStadium: attr(),
    surfaceType: belongsTo("surfaceType", {async:true}),
    filmSystem: attr(),
    assetBaseUrl: attr(),        // root url for pulling team-specific assets, including team logo
    numPlayGrades:attr(),

    // read-only properties. do not send these to the server
    conferenceName: attr(),
    name: attr(),
    conferenceLevel: attr(),
    isProTeam: attr(),

    teamCutups: DS.hasMany('teamCutup', {async:true}),
    organization: belongsTo("organization", {async:true}),

    watchRelationshipChanges: function() {
        this.set('currentState.isDirty', true);
    }.observes('state', 'conference', 'surfaceType', 'organization')
});
