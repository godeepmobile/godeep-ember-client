import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
	playNumber: DS.attr(),
	gameDown: DS.attr(),
	gameDistance: DS.attr(),
	gamePossession: DS.attr(),
	playData: DS.attr(),
	quarter: DS.attr(),
	yardLine: DS.attr(),
	description: DS.attr(),
	gsisParticipation: DS.attr(),
	teamEvent: DS.belongsTo('teamEvent', {async:true}),
	playType: DS.belongsTo('playType', {async:true}),
	playResultType: DS.belongsTo('playResultType', {async:true}),
	teamPracticeDrillType: DS.belongsTo('teamPracticeDrillType', {async:true}),

	grades: DS.hasMany('grade', {async:true}),

	gradePlayers: DS.attr(),
	assignedPlayers: DS.attr(),
	addedPlayers: DS.attr(),

	team: DS.belongsTo('team', {async:true}),

	teamCutup: DS.belongsTo('teamCutup', {async:true}),

	SPECIAL_TEAMS_PLAY_TYPES: [
		'Punt',
		'Field Goal',
		'PAT - 1',
		'PAT - 2',
		'Kickoff'
	],

	OFFENSE_PLATOON: 'Offense',
	DEFENSE_PLATOON: 'Defense',
	SPECIAL_TEAMS_PLATOON: 'Special Teams',

	ballOn: function() {
		return this.getPlayData('ball_on');
	}.property('playData'),

	prevPlayNumber: function() {
		return this.get('playNumber') - 1;
	}.property('playNumber'),

	nextPlayNumber: function() {
		return this.get('playNumber') + 1;
	}.property('playNumber'),

	getPlayData: function(attr) {
		var playData = this.get('playData');
		return attr && playData ? playData[attr] : undefined;
	},

	addGradePlayer: function(teamPlayer) {
		var gradePlayers = this.get('gradePlayers');
		if (!gradePlayers) {
			gradePlayers = Ember.A([]);
			this.set('gradePlayers', gradePlayers);
		}
		this.addPlayer(teamPlayer, gradePlayers);
		//this.removePlayer(teamPlayer, this.get('assignedPlayers'));
	},

	removeGradePlayer: function(teamPlayer) {
		var gradePlayers = this.get('gradePlayers')
		  , index;
		if (gradePlayers) {
			this.removePlayer(teamPlayer, gradePlayers);
			//this.addPlayer(teamPlayer, this.get('assignedPlayers'));
		}
	},

	teamPlayers: function() {
		var teamPlayers = []
		  , gradePlayers = this.get('gradePlayers')
		  , assignedPlayers = this.get('assignedPlayers');

		if (gradePlayers) {
			teamPlayers = teamPlayers.concat(gradePlayers);
		}
		if (assignedPlayers) {
			teamPlayers = teamPlayers.concat(assignedPlayers);
		}
		return teamPlayers;
	}.property('gradePlayers', 'assignedPlayers'),

	addPlayer: function(player, players) {
		players.push(player);
	},

	removePlayer: function(player, players) {
		var index = players.indexOf(player);
		if (index !== -1) {
			players.splice(index, 1);
		}
	},

	hasNotes: function() {
		return this.get('grades').isAny('notes');
	}.property('grades.@each.notes'),

	hasSpecialPlayCodes: function() {
		return this.get('grades').isAny('customFields.specialPlay');
	}.property('grades.@each.customFields')
});
