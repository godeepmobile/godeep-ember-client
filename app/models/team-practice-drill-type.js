import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr(),
	description: DS.attr(),
	gradeBase: DS.attr(),

	teamPlays: DS.hasMany('teamPlay', {async:true})
});
