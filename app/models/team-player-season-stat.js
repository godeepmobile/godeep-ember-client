// app/models/team-player-season-stat.js

import DS from 'ember-data';

var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

var TeamPlayerSeasonStat = DS.Model.extend({
    teamPlayer: belongsTo('teamPlayer'),
    teamPlayerAssignment: belongsTo('teamPlayerAssignment'),
    season: attr(),             // which season, currently year as number
    isVeteran: attr(),

    gameCount: attr(),           // total num games in this season
    gameNumPlays: attr(),           // total num plays in this season
    gameCat1Grade: attr(),
    gameCat2Grade: attr(),
    gameCat3Grade: attr(),
    gameOverallGrade: attr(),
    gameImpactPlatoonGrade: attr(),
    gameImpactPosGroupGrade: attr(),
    gameUnitImpactGrade: attr(),

    lastGameNumPlays: attr(),           // total num plays in this season
    lastGameCat1Grade: attr(),
    lastGameCat2Grade: attr(),
    lastGameCat3Grade: attr(),
    lastGameOverallGrade: attr(),
    lastGameImpactPlatoonGrade: attr(),
    lastGameImpactPosGroupGrade: attr(),

    practiceCount: attr(),           // total num games in this season
    practiceNumPlays: attr(),           // total num plays in this season
    practiceCat1Grade: attr(),
    practiceCat2Grade: attr(),
    practiceCat3Grade: attr(),
    practiceOverallGrade: attr(),
    practiceImpactPlatoonGrade: attr(),
    practiceImpactPosGroupGrade: attr(),

    allEventCount: attr(),           // total num games in this season
    allEventNumPlays: attr(),           // total num plays in this season
    allEventCat1Grade: attr(),
    allEventCat2Grade: attr(),
    allEventCat3Grade: attr(),
    allEventOverallGrade: attr(),
    allEventImpactPlatoonGrade: attr(),
    allEventImpactPosGroupGrade: attr(),

    rankOverallGame: attr(),
    rankOverallPractice: attr(),
    rankPracticeAtPost: attr(),
    rankGameAtPost: attr(),

    rankAtGradePost: attr(),

    avgImpact: function() {
        return ((parseFloat(this.get('gameImpactPlatoonGrade')) || 0) + (parseFloat(this.get('gameImpactPosGroupGrade')) || 0)) / 2;
    }.property('gameImpactPlatoonGrade', 'gameImpactPosGroupGrade')
});

export default TeamPlayerSeasonStat;
