import DS from 'ember-data';

export default DS.Model.extend({
	date: DS.attr(),
	homeScore: DS.attr(),
	awayScore: DS.attr(),
	season: DS.attr(),
	homeTeam: DS.belongsTo('team', {async:true}),
	awayTeam: DS.belongsTo('team', {async:true}),
	teamEvents: DS.hasMany('teamEvent', {async:true}),
	time: DS.attr(),
	location: DS.attr(),
	city: DS.attr(),
	state: DS.belongsTo('state', {async:true})
});
