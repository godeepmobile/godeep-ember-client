// app/models/team-configuration.js

import DS from 'ember-data';
var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

export default DS.Model.extend({
    userName: attr(),
    role: attr(),
    lastSubmitDate: attr()
});
