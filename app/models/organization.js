import DS from 'ember-data';

var attr = DS.attr,
    //hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

var Organization = DS.Model.extend({
    name: attr(),
    shortName: attr(),
    espnName: attr(),
    mascot: attr(),
    organizationType: belongsTo('OrganizationType'),
    conference: belongsTo('Conference'),
    //conference: attr(),

    teams: DS.hasMany('team', {async:true})
});

/*Organization.reopenClass({
    FIXTURES: [
        {
            "id": 8,
            "name": "Brown University",
            "shortName": "Brown",
            "espnName": "BROW",
            "mascot": "Bears",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 7,
            "name": "Colgate University",
            "shortName": "Colgate",
            "espnName": "COLG",
            "mascot": "Raiders",
            "organizationType": 2,
            "conference":14
        },
        {
            "id": 6,
            "name": "Columbia University",
            "shortName": "Columbia",
            "espnName": "CLMB",
            "mascot": "Lions",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 10,
            "name": "Cornell University",
            "shortName": "Cornell",
            "espnName": "COR",
            "mascot": "Big Red",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 13,
            "name": "Dartmouth College",
            "shortName": "Dartmouth",
            "espnName": "DART",
            "mascot": "Big Green",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 5,
            "name": "Davidson College",
            "shortName": "Davidson",
            "espnName": "DAV",
            "mascot": "Wildcats",
            "organizationType": 2,
            "conference":13
        },
        {
            "id": 1,
            "name": "Georgia Institute of Technology",
            "shortName": "Georgia Tech",
            "espnName": "GT",
            "mascot": "Yellow Jackets",
            "organizationType": 2,
            "conference":1
        },
        {
            "id": 9,
            "name": "Harvard University",
            "shortName": "Harvard",
            "espnName": "HARV",
            "mascot": "Crimson",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 3,
            "name": "Princeton University",
            "shortName": "Princeton",
            "espnName": "PRIN",
            "mascot": "Tigers",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 2,
            "name": "University of Georgia",
            "shortName": "Georgia",
            "espnName": "UGA",
            "mascot": "Bulldogs",
            "organizationType": 2,
            "conference":2
        },
        {
            "id": 11,
            "name": "University of Pennsylvania",
            "shortName": "Penn",
            "espnName": "PENN",
            "mascot": "Quakers",
            "organizationType": 2,
            "conference":5
        },
        {
            "id": 4,
            "name": "University of San Diego",
            "shortName": "San Diego",
            "espnName": "USD",
            "mascot": "Toreros",
            "organizationType":2,
            "conference":13
        },
        {
            "id": 12,
            "name": "Yale University",
            "shortName": "Yale",
            "espnName": "YALE",
            "mascot": "Bulldogs",
            "organizationType": 2,
            "conference":5
        }
    ]
});*/


export default Organization;
