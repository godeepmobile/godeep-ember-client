// app/models/team-user-position-group-assignment.js

import DS from 'ember-data';
var attr = DS.attr;

export default DS.Model.extend({
    team             : attr(),
    teamPositionGroup: attr(),
    startDate        : attr(),
    endDate          : attr(),
    user             : DS.belongsTo('teamUser', {async:true})
});