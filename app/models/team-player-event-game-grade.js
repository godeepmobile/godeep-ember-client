// app/models/team-player-event-game-grade.js

import DS from 'ember-data';
var attr = DS.attr;

export default DS.Model.extend({
  playerId       : attr(),
  season         : attr(),
  team           : attr(),
  eventName      : attr(),
  isPractice     : attr(),
  avgCat1Grade   : attr(),
  avgCat2Grade   : attr(),
  avgCat3Grade   : attr(),
  avgOverallGrade: attr()
});