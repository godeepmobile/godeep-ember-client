// app/models/team-scouting-eval-score.js

import DS from 'ember-data';

var attr = DS.attr;
var belongsTo = DS.belongsTo;

export default DS.Model.extend({
    team                       : attr(),
    teamScoutingEvalCriteria   : belongsTo('teamScoutingEvalCriteria', {async:true}),
    teamScoutingEvalScoreGroup : belongsTo('teamScoutingEvalScoreGroup', {async:true}),
    scoutingEvalCriteriaType   : attr(),
    note                       : attr(),
    score                      : attr()
});
