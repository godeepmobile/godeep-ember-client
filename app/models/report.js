import DS from 'ember-data';

var Report = DS.Model.extend({
	name: DS.attr(),
  	description: DS.attr()
});

Report.reopenClass({
    FIXTURES: [
        {
            id: 1,
            name: "Report 1",
            description: "blah"
        },
        {
            id: 2,
            name: "Report 2",
            description: "The TPS Report (don't forget the cover sheet)"
        }

    ]
});

export default Report;
