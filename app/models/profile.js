// app/models/profile.js

import DS from 'ember-data';
var attr = DS.attr,
    hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;

export default DS.Model.extend({
    team: belongsTo("team"),
    teamGraders: hasMany("teamGrader", {async:true}),
    teamConfiguration: belongsTo("teamConfiguration", {async:true}),
    teamPlatoonConfiguration: hasMany("teamPlatoonConfiguration", {async:true}),
    teamPositionGroups: hasMany("teamPositionGroup", {async:true}),
    teamPlayers: hasMany("teamPlayer", {async:true}),
    positionTypes: hasMany('positionType', {async:true}),
    teamRoles: hasMany('userRoleType', {async:true})
});
