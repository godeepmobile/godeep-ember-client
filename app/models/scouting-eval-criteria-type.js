// app/models/scouting-eval-criteria-type.js

import DS from 'ember-data';

var attr = DS.attr;

export default DS.Model.extend({
    name: attr()
});