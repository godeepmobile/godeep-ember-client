// app/models/position-type.js

import DS from 'ember-data';

var attr = DS.attr,
    belongsTo = DS.belongsTo;

export default DS.Model.extend({
    name       : attr(),
    shortName  : attr(),
    //platoonType: belongsTo('platoonType', {async:true})
    platoonType: attr()
});
