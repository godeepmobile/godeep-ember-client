import Ember from 'ember';

export default Ember.Service.extend({

	teamEventGradeStatusQueue: null,
	teamPlaysQueue: null,
	gradesQueue: null,

	checkWorker: null,

	isUp: true,

	init: function() {
		var offline = this.get('offline')
		  , self    = this;

		offline.on('confirmed-up', function() {
			console.log('Application is online, starting to sync');
			
			self.set('isUp', true);
			self.syncQueue(self.get('gradesQueue'));
			self.syncQueue(self.get('teamEventGradeStatusQueue'));

		});

		offline.on('confirmed-down', function() {
			console.log('Application is offline');
			
			self.set('isUp', false);
		});

		// initializing queues
		this.set('teamEventGradeStatusQueue', this.createQueue());
		//this.set('teamPlaysQueue', this.createQueue());
		this.set('gradesQueue', this.createQueue());
	},

	check: function() {
		this.get('offline').check();
	},

	addToTeamEventGradeStatusQueue: function(teamEventGradeStatus) {
		this.addToQueue(teamEventGradeStatus, this.get('teamEventGradeStatusQueue'));
	},

	addToTeamPlaysQueue: function(teamPlay) {
		this.addToQueue(teamPlay, this.get('teamPlaysQueue'));
	},

	addToGradesQueue: function(grade) {
		this.addToQueue(grade, this.get('gradesQueue'));
	},

	addToQueue: function(model, queue) {
		if (model && queue) {
			if (!queue.isAny('id', model.get('id'))) {
				queue.pushObject(model);
			}
		}
	},

	createQueue: function() {
		return Ember.ArrayProxy.create({
			content : Ember.A([])
		});
	},

	syncQueue: function(queue) {
		if (queue.get('length') > 0) {
			queue.forEach(function(model) {
				model[model.get('isDisabled') ? 'destroyRecord' : 'save']();
			});

			queue.clear();
		}
	}
});
