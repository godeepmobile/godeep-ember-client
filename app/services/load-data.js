// app/services/load-data.js

import Ember from 'ember';
import DS from 'ember-data';
import FetchAllModelsMixin from '../mixins/fetch-all-models';

export default Ember.Service.extend(FetchAllModelsMixin, {
    _profile: null,
    _players: null,
    _playerAssignments: null,
    _prospects: null,
    _prospectAssignments: null,
    _playTypes: null,
    _playResultTypes: null,
    _positionTypes: null,
    _bodyTypes: null,
    _states: null,
    _eventTypes: null,
    _surfaceTypes: null,
    _conferences: null,
    _levels: null,
    _importances: null,
    _teamPlayerSeasonStats: null,
    _teamPositionGroups: null,
    _positionTypesForTeam: null,
    _platoonTypes: null,
    _platoonTypesWithoutAll: null,
    _gameEvents: null,
    _events: null,
    _scoutingEvalCriteriaTypes: null,
    _teamPlayerSeasonRankStats: null,
    _prospectEvaluationRankOveralls: null,
    store: null,
    init: function () {
        this.set('store', this.container.lookup('store:main'));
    },
    reload(which, params) {
        switch(which) {
            case 'teamPlayerSeasonStats':
                this.fetch_teamPlayerSeasonStat(params);
                break;
            case 'teamPlayerSeasonRankStats':
                this.fetchTeamPlayerSeasonRankStat();
                break;
            case 'players' :
                this.fetchPlayerAssignments();
                break;
            case 'prospects' :
                this.fetchProspectAssignments();
                break;
        }
    },


    // READONLY
    currentSeason: Ember.computed({
        get: function () {
            var date = moment.utc(new Date());
            var year = date.get('year');
            if (date.get('month') < 2) {
              // Date on January or February (super bowl)
              year -= 1;
            }
            return year;
        }
    }),

    isCurrentSeason: function() {
        return this.get('season') === this.get('currentSeason');
    }.property('currentSeason', 'season'),

    season: Ember.computed({
        get: function () {
            var date = moment.utc(new Date());
            var year = date.get('year');
            if (date.get('month') < 2) {
              // Date on January or February (super bowl)
              year -= 1;
            }
            // Note(Carlos) temporaly hardcoding the season for demos
            return year;
        }
    }),

    //TODO(Carlos) this should be recovered from the API
    seasons: [2015, 2016, 2017],

    profile: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: profile.get()');
            return new Ember.RSVP.Promise(function (resolve, reject) {
                if (self.get('_profile')) {
                    //console.log('load-data: returning profile immediately');
                    resolve(self.get('_profile'));
                } else {
                    //console.log('load-data: requesting profile from server');
                    store.findById('profile', 1)
                        .then(function (profile) {
                            //console.log('load-data: got profile from server');
                            self.set('_profile', profile);
                            resolve(profile);
                        }, function (err) {
                            //console.log('load-data: error %o requesting profile', err);
                            reject(err);
                        });
                }
            });
        }
    }),
    fetch_teamPlayerSeasonStat(isChangingSeason) {
        var self = this;
        console.log('load-data: fetch_teamPlayerSeasonStat()');

        /*if (this.get('_teamPlayerSeasonStats') && isChangingSeason) {
            this.get('_teamPlayerSeasonStats').forEach(function(record) {
                record.unloadRecord();
            });
        }*/

        return self.fetchAllModelsNamed('teamPlayerSeasonStat', {
                    "filter[where][season]": self.get('season'),
                    "order": ["gameOverallGrade DESC", "gameNumPlays DESC"],
                    //"filter[include]" : ["teamPlayer","teamPlayerAssignment"],
                    "limit":100
        });
    },

    teamPlayerSeasonStats: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            console.log('load-data: teamPlayerSeasonStats.get()');
            if (self.get('_teamPlayerSeasonStats')) {
                console.log('load-data: returning teamPlayerSeasonStats immediately');
            } else {
                console.log('load-data: requesting teamPlayerSeasonStats from server');
                self.set('_teamPlayerSeasonStats', self.fetch_teamPlayerSeasonStat());
            }
            return (self.get('_teamPlayerSeasonStats'));
        }
    }),
    gameEvents: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            var result;
            function addSort(error) {
                if (error) {
                } else {
                    Ember.SortableMixin.apply(result);
                    result.set('sortProperties', ['date']);
                    console.log('load-data: sorting added to game event');
                }
            }

            //console.log('load-data: gameEvents.get()');
            if (self.get('_gameEvents')) {
                //console.log('load-data: returning gameEvents immediately');
            } else {
                //console.log('load-data: requesting gameEvents from server');
                result = self.fetchAllModelsWithCallback('teamEvent', addSort.bind(this), {
                    "filter[where][opponent][neq]": null, "order": ["date"]
                });
                self.set('_gameEvents', result);
            }
            return (self.get('_gameEvents'));
        }
    }),

    events: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            var result;
            function addSort(error) {
                if (error) {
                } else {
                    Ember.SortableMixin.apply(result);
                    result.set('sortProperties', ['date DESC']);
                    console.log('load-data: sorting added to events');
                }
            }

            if (self.get('_events')) {
                //console.log('load-data: returning gameEvents immediately');
            } else {
                result = self.fetchAllModelsWithCallback('teamEvent', addSort.bind(this), {
                    "order": ["date DESC"]
                });
                self.set('_events', result);
            }
            return (self.get('_events'));
        }
    }),

    eventTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            var result;
            function addSort(error) {
                if (error) {
                } else {
                    Ember.SortableMixin.apply(result);
                    result.set('sortProperties', ['name']);
                    console.log('load-data: sorting added to event types');
                }
            }

            //console.log('load-data: eventTypes.get()');
            if (self.get('_eventTypes')) {
                //console.log('load-data: returning eventTypes immediately');
            } else {
                //console.log('load-data: requesting eventTypes from server');

                result = self.fetchAllModelsWithCallback('eventType', addSort.bind(this));
                self.set('_eventTypes', result);

            }
            return (self.get('_eventTypes'));
        }
    }),
    playResultTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: playResultTypes.get()');
            if (self.get('_playResultTypes')) {
                //console.log('load-data: returning playResultTypes immediately');
            } else {
                //console.log('load-data: requesting playResultTypes from server');
                self.set('_playResultTypes', self.fetchAllModelsNamed('playResultType', {
                    "order": ["name"]
                }));
            }
            return (self.get('_playResultTypes'));
        }
    }),
    playTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: playTypes.get()');
            if (self.get('_playTypes')) {
                //console.log('load-data: returning playTypes immediately');
            } else {
                //console.log('load-data: requesting playTypes from server');
                self.set('_playTypes', self.fetchAllModelsNamed('playType', {
                    "order": ["name"]
                }));
            }
            return (self.get('_playTypes'));
        }
    }),
    platoonTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: platoonTypes.get()');
            if (self.get('_platoonTypes')) {
                //console.log('load-data: returning platoonTypes immediately');
            } else {
                //console.log('load-data: requesting platoonTypes from server');
                self.set('_platoonTypes', self.fetchAllModelsNamed('platoonType'));
            }
            return (self.get('_platoonTypes'));
        }
    }),
    platoonTypesWithoutAll: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: platoonTypes.get()');
            if (self.get('_platoonTypesWithoutAll')) {
                //console.log('load-data: returning platoonTypes immediately');
            } else {
                //console.log('load-data: requesting platoonTypes from server');
                self.set('_platoonTypesWithoutAll', self.fetchAllModelsNamed('platoonType', {
                    "filter[where][shortName][neq]": "ALL"}));
            }
            return (self.get('_platoonTypesWithoutAll'));
        }
    }),
    teamPositionGroups: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: teamPositionGroups.get()');
            if (self.get('_teamPositionGroups')) {
                //console.log('load-data: returning teamPositionGroups immediately');
            } else {
                //console.log('load-data: requesting teamPositionGroups from server');
                self.set('_teamPositionGroups', self.fetchAllModelsNamed('teamPositionGroup'));
            }
            return (self.get('_teamPositionGroups'));
        }
    }),
    players: Ember.computed({
        get: function () {
            /*function addSort(error) {
                if (error) {
                } else {
                    Ember.SortableMixin.apply(result);
                    result.set('sortProperties', ['lastName', 'firstName']);
                    console.log('load-data: sorting added to players');
                }
            }*/
            var self = this;
            var store = this.get('store');
            //console.log('load-data: players.get()');
            if (self.get('_players')) {
                //console.log('load-data: returning players immediately');
            } else {
                //console.log('load-data: requesting players from server');
                /*var result;
                result = self.fetchAllModelsWithCallback('teamPlayer', addSort.bind(this));
                self.set('_players', result);*/
                //self.set('_players', self.fetchAllModelsNamed('teamPlayer'));
                self.fetchPlayers();
            }
            return (self.get('_players'));
        }
    }),

    playerAssignments: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: players.get()');
            if (self.get('_playerAssignments')) {
                //console.log('load-data: returning players immediately');
            } else {
                self.fetchPlayerAssignments();
            }
            return (self.get('_playerAssignments'));
        }
    }),

    prospects: Ember.computed({
        get: function () {
            /*function addSort(error) {
                if (error) {
                } else {
                    Ember.SortableMixin.apply(result);
                    result.set('sortProperties', ['lastName', 'firstName']);
                    console.log('load-data: sorting added to prospects');
                }
            }*/
            var self = this;
            var store = this.get('store');
            if (self.get('_prospects')) {
                //console.log('load-data: returning prospects immediately');
            } else {
                //console.log('load-data: requesting prospects from server');
                /*var result;
                result = self.fetchAllModelsWithCallback('teamProspect', addSort.bind(this));
                self.set('_prospects', result);*/
                self.fetchProspects();
            }
            return (self.get('_prospects'));
        }
    }),

    prospectAssignments: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: players.get()');
            if (self.get('_prospectAssignments')) {
                //console.log('load-data: returning players immediately');
            } else {
                self.fetchProspectAssignments();
            }
            return (self.get('_prospectAssignments'));
        }
    }),

    fetchPlayers: function() {
        function addSort(error) {
            if (error) {
            } else {
                Ember.SortableMixin.apply(result);
                result.set('sortProperties', ['lastName', 'firstName']);
                console.log('load-data: sorting added to players');
            }
        }

        var result = this.fetchAllModelsWithCallback('teamPlayer', addSort.bind(this));
        this.set('_players', result);
    },

    fetchPlayerAssignments: function() {
        function addSort(error) {
            if (error) {
            } else {
                /*Ember.SortableMixin.apply(result);
                result.set('sortProperties', ['teamPlayer.lastName', 'teamPlayer.firstName']);*/
                console.log('load-data: sorting added to player assignments');
            }
        }

        var result = this.fetchAllModelsNamed('teamPlayerAssignment');

        this.set('_playerAssignments', result);
    },

    fetchProspects: function() {
        function addSort(error) {
            if (error) {
            } else {
                Ember.SortableMixin.apply(result);
                result.set('sortProperties', ['lastName', 'firstName']);
                console.log('load-data: sorting added to prospects');
            }
        }

        var result = this.fetchAllModelsWithCallback('teamProspect', addSort.bind(this));
        this.set('_prospects', result);
    },

    fetchProspectAssignments: function() {
        function addSort(error) {
            if (error) {
            } else {
                /*Ember.SortableMixin.apply(result);
                result.set('sortProperties', ['teamPlayer.lastName', 'teamPlayer.firstName']);*/
                console.log('load-data: sorting added to prospect assignments');
            }
        }

        var result = this.fetchAllModelsNamed('teamProspectAssignment');


        this.set('_prospectAssignments', result);
    },

    positionTypes: Ember.computed({
        get: function () {
            var result;
            var self = this;
            //console.log('load-data: positionTypes.get()');
            if (self.get('_positionTypes')) {
                //console.log('load-data: returning positionTypes immediately');
            } else {
                //console.log('load-data: requesting positionTypes from server');
                result = self.fetchAllModelsNamed('positionType');
                Ember.SortableMixin.apply(result);
                result.set('sortProperties', ['name']);
                self.set('_positionTypes', result);
            }
            return (self.get('_positionTypes'));
        }
    }),

    bodyTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: bodyTypes.get()');
            if (self.get('_bodyTypes')) {
                //console.log('load-data: returning bodyTypes immediately');
            } else {
                //console.log('load-data: requesting bodyTypes from server');
                self.set('_bodyTypes', self.fetchAllModelsNamed('bodyType', {
                    "order": ["name"]
                }));
            }
            return (self.get('_bodyTypes'));
        }
    }),
    states: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: states.get()');
            if (self.get('_states')) {
                //console.log('load-data: returning states immediately');
            } else {
                //console.log('load-data: requesting states from server');
                self.set('_states', self.fetchAllModelsNamed('state', {
                    "order": ["name"]
                }));
            }
            return (self.get('_states'));
        }
    }),
    surfaceTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: surfaceTypes.get()');
            if (self.get('_surfaceTypes')) {
                //console.log('load-data: returning surfaceTypes immediately');
            } else {
                //console.log('load-data: requesting surfaceTypes from server');
                self.set('_surfaceTypes', self.fetchAllModelsNamed('surfaceType', {
                    "order": ["name"]
                }));
            }
            return (self.get('_surfaceTypes'));
        }
    }),
    conferences: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: conferences.get()');
            if (self.get('_conferences')) {
                //console.log('load-data: returning conferences immediately');
            } else {
                //console.log('load-data: requesting conferences from server');
                self.set('_conferences', self.fetchAllModelsNamed('conference', {
                    "order": ["name"]
                }));
            }
            return (self.get('_conferences'));
        }
    }),

    levels: Ember.computed({
        get: function () {
            var result;
            var self = this;
            if (self.get('_levels')) {
                //console.log('load-data: returning levels immediately');
            } else {
                //console.log('load-data: requesting levels from server');
                result = self.fetchAllModelsNamed('level');
                Ember.SortableMixin.apply(result);
                result.set('sortProperties', ['shortName']);
                self.set('_levels', result);
            }
            return (self.get('_levels'));
        }
    }),

    importances: Ember.computed({
        get: function () {
            var result;
            var self = this;
            if (self.get('_importances')) {
                //console.log('load-data: returning importances immediately');
            } else {
                //console.log('load-data: requesting importances from server');
                result = self.fetchAllModelsNamed('importance');
                self.set('_importances', result);
            }
            return (self.get('_importances'));
        }
    }),

    scoutingEvalCriteriaTypes: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            //console.log('load-data: scoutingEvalCriteriaTypes.get()');
            if (self.get('_scoutingEvalCriteriaTypes')) {
                //console.log('load-data: returning scoutingEvalCriteriaTypes immediately');
            } else {
                //console.log('load-data: requesting scoutingEvalCriteriaTypes from server');
                var result;
                result = self.fetchAllModelsNamed('scoutingEvalCriteriaType');
                self.set('_scoutingEvalCriteriaTypes', result);
            }
            return (self.get('_scoutingEvalCriteriaTypes'));
        }
    }),

    positionTypesForTeam: Ember.computed({
        get: function () {
            var self = this, teamPositionTypes;
            if (self.get('_positionTypesForTeam')) {
                //console.log('load-data: returning the position types for the team immediately');
            } else {
                //console.log('load-data: requesting the position types for the team from server');
                // tell call that we promise to return an array eventually
                var promiseArray = DS.PromiseArray.create({
                    // this is the promise part of the PromiseArray
                    promise: new Ember.RSVP.Promise(function (resolve) {
                            teamPositionTypes = self.fetchAllModelsWithCallback('teamPositionType', function () {
                                    var positionTypes = Ember.A([]);

                                    Ember.SortableMixin.apply(teamPositionTypes);
                                    teamPositionTypes.set('sortProperties', ['name']);
                                    console.log('load-data: sorting added to positionTypesForTeam');

                                    teamPositionTypes.forEach(function(teamPositionType) {
                                        if (!positionTypes.isAny('id', teamPositionType.get('positionType.id'))){
                                            positionTypes.push(teamPositionType.get('positionType'));
                                        }
                                    });
                                    console.log('load-data: filtered positionTypesForTeam');

                                    // resolve our promise returning just the position types for the team
                                    resolve(positionTypes);
                                });
                        }.bind(this)) // pass in our context
                });
                self.set('_positionTypesForTeam', promiseArray);
            }
            return (self.get('_positionTypesForTeam'));
        }
    }),
    teamPlayerSeasonRankStats: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            console.log('load-data: teamPlayerSeasonRankStats.get()');
            if (self.get('_teamPlayerSeasonRankStats')) {
                console.log('load-data: returning teamPlayerSeasonRankStats immediately');
            } else {
                console.log('load-data: requesting teamPlayerSeasonRankStats from server');
                self.set('_teamPlayerSeasonRankStats', self.fetchTeamPlayerSeasonRankStat());
            }
            return (self.get('_teamPlayerSeasonRankStats'));
        }
    }),
    fetchTeamPlayerSeasonRankStat() {
        var self = this;
        console.log('load-data: fetchTeamPlayerSeasonRankStat())');

        /*if (this.get('_teamPlayerSeasonRankStats')) {
            this.get('_teamPlayerSeasonRankStats').forEach(function(record) {
                record.unloadRecord();
            });
        }*/

        return self.fetchAllModelsNamed('playerRankOverall', {
                    "filter[where][season]": self.get('season')
        });
    },
    prospectEvaluationRankOveralls: Ember.computed({
        get: function () {
            var self = this;
            var store = this.get('store');
            console.log('load-data: _prospectEvaluationRankOveralls.get()');
            if (self.get('_prospectEvaluationRankOveralls')) {
                console.log('load-data: returning _prospectEvaluationRankOveralls immediately');
            } else {
                console.log('load-data: requesting _prospectEvaluationRankOveralls from server');
                self.set('_prospectEvaluationRankOveralls', self.fetchProspectEvaluationRankOverall());
            }
            return (self.get('_prospectEvaluationRankOveralls'));
        }
    }),
    fetchProspectEvaluationRankOverall() {
        var self = this;
        console.log('load-data: fetchProspectEvaluationRankOverall())');

        /*if (self.get('_prospectEvaluationRankOveralls')) {
            self.get('_prospectEvaluationRankOveralls').forEach(function(record) {
                record.unloadRecord();
            });
        }*/

        return self.fetchAllModelsNamed('prospectEvaluationRankOverall', {
            "filter[where][season]": self.get('season')
        });
    },
});
