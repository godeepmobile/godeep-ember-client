import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['players/position'],
    thisPage: "Roster & Positions",
    breadcrumbs: []
});
