import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},

    playerDeleteEvaluationUrl: function(adapter, scoreGroupId) {
        return adapter.buildURL('TeamScoutingEvalScoreGroup', scoreGroupId) + '/deleteEvaluation';
    },

    actions: {
        deleteAndTransition: function () {
            var self = this;
            this.send('removeModal');
            var adapter = this.store.adapterFor('application');
            var url = this.playerDeleteEvaluationUrl(adapter, self.get('model').scoreGroupId);
            adapter.ajax(url, 'DELETE').then(function (response) {
              console.log('evaluation deleted');
              self.get('model').router.set('allowTransition', true);
              self.get('model').trans.retry();
            }, function (error) {
              console.log('error: ' , error);
            });
        },

        stayHere: function () {
            this.send('removeModal');
        }
    }
});
