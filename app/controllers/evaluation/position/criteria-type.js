// controllers/evaluation/position/criteria-type.js

import Ember from 'ember';

export default Ember.Controller.extend( {
    queryParams: ['scoreGroup'],
    scoreGroup: null,

    sortedCriteriaTypesSortProperties: ['teamScoutingEvalCriteria.sortIndex:asc'],
    sortedCriteriaTypes: Ember.computed.sort('model', 'sortedCriteriaTypesSortProperties'),

    actions: {
        saveScoreValue: function(value, criteria) {
            var self = this;
            criteria.set('score', value);
            criteria.save();
        },

        addEvalNotes: function() {
            this.send('showModal', 'evaluation/position/notes', this.get('model'));
        },

        addEvalNote: function(criteria) {
            this.send('showModal', 'evaluation/position/note', criteria);
        },

        finishEval: function () {
          var self = this;
          function ajaxSuccess(response) {
              var modelName = self.get('player').constructor.modelName;
              switch(modelName) {
                  case 'team-player':
                    modelName = 'teamPlayer';
                  break;
                  case 'team-prospect':
                    modelName = 'teamProspect';
                  break;
              }
              var url = "/evaluation/summary/" + modelName +'/'+ self.get('player').get('id') + '/' + self.get('scoreGroup') + '/' + self.get('player.assignment.id');
              //this.transitionToRoute('evaluation.position.criteriaType', 'teamProspect', this.get('model'), this.get('postionSelected'), 1, {queryParams: {scoreGroup:null}});
              self.transitionToRoute(url);
            }

            function ajaxFailure(response) {
              console.log('failure...........', response);
            }

            var num = this.criteriaNotScored();
            if (num) {
                var message = 'You forgot to score ' + num + ' criteria';
                this.send('showTooltip', 'tooltip/error', {message: message});
            } else {

                var adapter = this.store.adapterFor('application');
                var url = this.teamScoutingEvalScoreGroupUrl(adapter);
                adapter.ajax(url, 'POST').then(ajaxSuccess, ajaxFailure);
            }
        },

        validateAndGoTo: function (criteriaTypeId) {
            var num = this.criteriaNotScored();
            if (num) {
                var message = 'You forgot to score ' + num + ' criteria';
                this.send('showTooltip', 'tooltip/error', {message: message});
            } else {
                this.transitionToRoute('evaluation.position.criteriaType', criteriaTypeId);
                this.send('temporalTooltip', 'tooltip/saved');
            }
        }
    },

    scoreValues: function() {
        var teamConfig = this.get('session.teamConfiguration');
        var scoreMax = teamConfig.get('scoutEvalScoreMax');
        var scoreMin = teamConfig.get('scoutEvalScoreMin');
        var scores = [];
        for (; scoreMin <= scoreMax; scoreMin++) {
            scores.push(scoreMin);
        }
        return scores;
    }.property('model'),

    criteriaNotScored: function () {
        return this.get('model').filter(function(critieria) {
            return !critieria.get('score');
        }).length;
    },

    teamScoutingEvalScoreGroupUrl: function(adapter, model) {
        return adapter.buildURL('TeamScoutingEvalScoreGroup', this.get('scoreGroup')) + '/finishEvaluation';
    }
});
