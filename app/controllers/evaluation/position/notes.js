// controllers/evaluation/position/notes.js

import Ember from 'ember';

export default Ember.Controller.extend( {

    actions: {
        save: function() {
            var evalScores = this.get('model');
            evalScores.forEach(function(evalScore) {
                evalScore.save();
            });
            this.send('removeModal');
        }
    }
});
