import Ember from 'ember';

export default Ember.Controller.extend({
	error: false,
	teamPlayerToEvaluate: null,

	clearForm: function() {
		this.set('error', false);
		this.set('teamPlayerToEvaluate', null);
	},

	actions: {
		evaluateTeamPlayer: function() {
			var teamPlayerToEvaluate = this.get('teamPlayerToEvaluate');

			if (teamPlayerToEvaluate) {
				this.send('removeModal');
				Ember.run.later(function() {
		            this.send('showModal', 'player/positions', teamPlayerToEvaluate);
		        }.bind(this), 500);
				
			} else {
				this.set('error', true);
			}
			
		},

		selectPlayer: function(teamPlayer) {
			var teamPlayerToEvaluate = this.get('teamPlayerToEvaluate');

			if (teamPlayerToEvaluate) {
				teamPlayerToEvaluate.set('isSelectedToEvaluate', false);
			}

			teamPlayer.set('isSelectedToEvaluate', true);
			this.set('teamPlayerToEvaluate', teamPlayer);
		}
	}
});
