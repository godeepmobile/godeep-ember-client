import Ember from 'ember';

export default Ember.Controller.extend({
	error: false,
	majorFactorsId: 1,
	criticalFactorsId: 2,
	positionSkillsId: 3,

	clearForm: function() {
		this.set('error', false);
	},

	majorFactors: function() {
		return this.get('model.teamScoutingEvalCriterias').filterBy('scoutingEvalCriteriaType', this.majorFactorsId);
	}.property('model.teamScoutingEvalCriterias.@each'),

	criticalFactors: function() {
		return this.get('model.teamScoutingEvalCriterias').filterBy('scoutingEvalCriteriaType', this.criticalFactorsId);
	}.property('model.teamScoutingEvalCriterias.@each'),

	positionSkills: function() {
		return this.get('model.teamScoutingEvalCriterias').filterBy('scoutingEvalCriteriaType', this.positionSkillsId);
	}.property('model.teamScoutingEvalCriterias.@each'),

	criteriaHasEvaluations: function() {
		return this.get('model.criteriaHasEvaluations');
	}.property('model.criteriaHasEvaluations'),

	buildRefreshURL: function(adapter, forPlayer) {
		return adapter.buildURL((forPlayer ? 'Player' : 'ProspectEvaluation') + 'RankOveralls', 'updateSeasonRankingStats');
	},

	actions: {
		setupEvaluationCriteria: function() {
			var errorHandler = function(error) {
	            	this.set('videoCutupData', null);
					console.log('responseJSON ', error.responseJSON);
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.responseText){
						alert(error.responseText);
					}
				}.bind(this)
			  , errors
			  , userScore
			  , hasError = false
			  , adapter  = this.store.adapterFor('application')
			  , date     = moment.utc()
			  , season   = date.get('year');

		  	// calculating current season
		  	if (date.get('month') < 2) {
		  		// Date on January or February
		  		season -= 1;
		  	}

			this.get('model.teamScoutingEvalCriterias').forEach(function(teamScoutingEvalCriteria) {
				// checking user inputs
				errors    = teamScoutingEvalCriteria.get('errors');
				userScore = teamScoutingEvalCriteria.get('customTargetScore');

				errors.set('name', !teamScoutingEvalCriteria.get('name'));

				// checking the custom target if selected, if that's the case validate if it isn't empty and is a number between the team's scouting ranges
				errors.set('target', !teamScoutingEvalCriteria.get('useCalculatedTargetScore') && (
					!userScore ||
					isNaN(userScore) ||
					userScore < this.get('session.teamConfiguration.scoutEvalScoreMin') ||
					userScore > this.get('session.teamConfiguration.scoutEvalScoreMax')
				));

				if (errors.get('name') || errors.get('target')) {
					hasError = true;
				}
			}.bind(this));

			if (!hasError) {
				// proceed to save the criterias
				this.get('model.teamScoutingEvalCriterias').save().then(function() {
					// saving the position importance
					this.get('model.teamPositionImportance').save().then(function() {

						// updating player ranks
						adapter.ajax(this.buildRefreshURL(adapter, true), 'POST', {data : {season : season}}).then(function() {
							// updating prospect ranks
							adapter.ajax(this.buildRefreshURL(adapter), 'POST', {data : {season : season}}).then(function() {

								this.send('removeModal');
								this.send('temporalTooltip', 'tooltip/saved', {message: 'The evaluation criteria has been saved'});

								if (this.get('model.afterSave')) {
									this.send(this.get('model.afterSave'));
								}

							}.bind(this), errorHandler);
						}.bind(this), errorHandler);

					}.bind(this), errorHandler);

				}.bind(this), errorHandler);
			}

		},

		cancelEvaluationCriteria: function() {
			this.get('model.teamScoutingEvalCriterias').invoke('rollback');
			this.send('removeModal');
		},

		toggleCritical: function(factor) {
			if (factor) {
				factor.toggleProperty('isCritical');
			}
		},

		newCriteria: function(criteriaType) {
				var obj = this.store.createRecord('teamScoutingEvalCriteria', {
						team: this.get('session.team.id'),
						isCritical: false,
						useCalculatedTargetScore: true,
						calculatedTargetScore: '5.00',
						positionType: parseInt(this.get('model.positionType.id')),
						scoutingEvalCriteriaType: criteriaType
				});
		},
		delete: function(criteria) {
				criteria.destroyRecord();
				var criteriaId = criteria.get('id') || '';
				Ember.$('#criteria-' + criteriaId).addClass("hidden");
		},
		cancelDelete: function (criteria) {
				var criteriaId = criteria.get('id') || '';
				Ember.$('#criteria-' + criteriaId).addClass("hidden");
		},
		showDeleteConfirmation: function(criteria) {
				var criteriaId = criteria.get('id') || '';
				Ember.$('#criteria-' + criteriaId).removeClass("hidden");
		}
	}
});
