import Ember from 'ember';

export default Ember.Controller.extend({
	error: false,
	teamProspectToEvaluate: null,

	clearForm: function() {
		this.set('error', false);
		this.set('teamProspectToEvaluate', false);
	},

	actions: {
		evaluateTeamProspect: function() {
			var teamProspectToEvaluate = this.get('teamProspectToEvaluate');

			if (teamProspectToEvaluate) {
				this.send('removeModal');
				Ember.run.later(function() {
					this.send('showModal', 'prospect/positions', teamProspectToEvaluate);
				}.bind(this), 500);
			} else {
				this.set('error', true);
			}
			
		},

		selectProspect: function(teamProspect) {
			var teamProspectToEvaluate = this.get('teamProspectToEvaluate');

			if (teamProspectToEvaluate) {
				teamProspectToEvaluate.set('isSelectedToEvaluate', false);
			}

			teamProspect.set('isSelectedToEvaluate', true);
			this.set('teamProspectToEvaluate', teamProspect);
		}
	}
});
