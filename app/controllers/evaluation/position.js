// controllers/evaluation/position.js

import Ember from 'ember';

export default Ember.Controller.extend( {
    needs: ['evaluation/position/criteria-type'],
    currentCriteriaType: null,

    criteryType: function () {
        if (!this.currentCriteriaType) {
            var self = this;
            return this.get('scoutingEvalCriteriaTypes').then(function(types) {
                var current = types.filterBy('id', self.get('criteriaTypeId'))[0];
                self.set('currentCriteriaType', current);
                return current;
            });
        } else {
            var current = this.get('scoutingEvalCriteriaTypes').filterBy('id', this.get('criteriaTypeId'))[0];
            this.set('currentCriteriaType', current);
            return current;
        }
    }.property('scoutingEvalCriteriaTypes', 'currentCriteriaType', 'criteriaTypeId'),

    nextCriteriaType: function() {
        if (this.currentCriteriaType && this.get('scoutingEvalCriteriaTypes')) {
            var len = this.get('scoutingEvalCriteriaTypes').get('length');
            var currentId = parseInt(this.currentCriteriaType.get('id'));
            if (currentId < len) {
                var nextId = currentId + 1;
                return this.get('scoutingEvalCriteriaTypes').filterBy('id', nextId.toString())[0];
            }
        }
        return null;
    }.property('currentCriteriaType'),

    previousCriteriaType: function() {
        if (this.currentCriteriaType && this.get('scoutingEvalCriteriaTypes')) {
            var currentId = parseInt(this.currentCriteriaType.get('id'));
            if (currentId > 1) {
                var nextId = currentId - 1;
                return this.get('scoutingEvalCriteriaTypes').filterBy('id', nextId.toString())[0];
            }
        }
        return null;
    }.property('currentCriteriaType'),

    finalCriteriaType: function() {
        if (this.currentCriteriaType) {
            var len = this.get('scoutingEvalCriteriaTypes').get('length');
            var currentId = parseInt(this.currentCriteriaType.get('id'));
            return currentId === len;
        }
        return false;
    }.property('currentCriteriaType'),

    actions: {
        goNextCriteriaType: function () {
            var nextCriteria = this.get('nextCriteriaType');
            if (nextCriteria) {
                var positionController = this.get('controllers.evaluation/position/criteria-type');
                positionController.send('validateAndGoTo', nextCriteria.get('id'));
            }
        },

        goPreviousCriteriaType: function () {
            var previousCriteriaType = this.get('previousCriteriaType');
            if (previousCriteriaType) {
                var positionController = this.get('controllers.evaluation/position/criteria-type');
                positionController.send('validateAndGoTo', previousCriteriaType.get('id'));
            }
        }
    }
});
