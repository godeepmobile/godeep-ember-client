import Ember from 'ember';

export default Ember.Controller.extend({
	countdown: null,
	countdownTimer: null,
	mustLogout: false,

	startCountdown: function() {
		var self       = this
		  , timeFormat = 'mm:ss'
		  , countdown  = moment('02:00', timeFormat)
		  , interval   = 1000
		  , adapter    = this.store.adapterFor('application')
		  , countdownText;

		this.set('mustLogout', false);
		this.set('countdown', countdown.format(timeFormat));

		this.set('countdownTimer', setInterval(function() {

			countdown.subtract(interval, 'milliseconds');
			countdownText = countdown.format(timeFormat);

			self.set('countdown', countdownText);

			if (moment.duration(countdownText, timeFormat).asSeconds() === 0) {
				// time is over
				self.stopCountdown();
				self.displayLogoutMessage();

				adapter.ajax(adapter.buildURL('gouser', 'logout') , 'POST');
			}

		}, interval));
	},

	stopCountdown: function() {
		if (this.get('countdownTimer')) {
			clearInterval(this.get('countdownTimer'));
		}
	},

	displayLogoutMessage: function() {
		this.set('mustLogout', true);
	},

	actions: {
		continueUsingGoDeep: function() {
			this.stopCountdown();
			this.send('removeModal');
			this.send('enableSessionTimer');
			//this.send('startSessionTimer');
		},

		goToLoginPage: function() {
			this.stopCountdown();
			this.send('removeModal');
			this.send('invalidateSession');
		}
	}
});
