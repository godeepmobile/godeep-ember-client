//app/controllers/prospect.js

import Ember from 'ember';

export default Ember.Controller.extend({
    loadData: Ember.inject.service(),
    thisPage: "Prospect Information",
    breadcrumbs: [{linkTo : "prospects", caption: "Prospect List" }],
    needs: ['prospect/edit'],

    actions: {
        edit: function(prospectAssignment) {
            var editController = this.get('controllers.prospect/edit');
            
            prospectAssignment.get('teamProspect').then(function(teamProspect) {
                editController.set('teamProspectAssignment', prospectAssignment);
                this.send('showModal', 'prospect/edit', teamProspect);
                this.send('focusFirstInput');
            }.bind(this));
        },

        evaluatePlayer: function(prospect) {
            this.send('showModal', 'prospect/positions', prospect);
        }
    }
});
