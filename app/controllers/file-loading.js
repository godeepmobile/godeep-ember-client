import Ember from 'ember';

export default Ember.Controller.extend({

	message: 'Uploading File ...',

	actions: {
		fileUploadSuccess: function(msg) {
			this.set('message', msg || 'The file was successfully uploaded');
		}
	}
});
