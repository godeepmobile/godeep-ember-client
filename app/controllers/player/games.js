import Ember from 'ember';

export default Ember.Controller.extend({
    queryParams: ['season', 'isPractice'],
    season    : null,
    isPractice: null,

    showOneCategory: function() {
       return this.get('session.teamConfiguration.numGradeCategories') === 1;
    }.property('session.teamConfiguration.numGradeCategories'),

    showTwoCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 2;
    }.property('session.teamConfiguration.numGradeCategories'),

    showThreeCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 3;
    }.property('session.teamConfiguration.numGradeCategories')
});
