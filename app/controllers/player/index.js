// app/controllers/player/index.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    isAllExpanded: true,
    isAllEvaluationsExpanded: true,
    isCareerExpanded: true,
    showEvaluations: false,

    displayGrades: true,
    displayEvaluations: false,

    toggleIsAllExpandedForLabel: function() {
        var isAnySeasonExpanded = this.get('attrs.seasons').isAny('isExpanded')
          , isCareerExpanded    = this.get('isCareerExpanded');

        if (!isAnySeasonExpanded && !isCareerExpanded) {
            this.set('isAllExpanded', false);
        }

        if (isAnySeasonExpanded || isCareerExpanded) {
            this.set('isAllExpanded', true);
        }
    },

    toggleIsAllEvaluationsExpandedForLabel: function() {
        var isAnySeasonExpanded;

        this.get('evaluationStats').forEach(function(stat) {
            stat.seasons_data.forEach(function(season) {
                 if (season.isExpanded) {
                    isAnySeasonExpanded = true;
                 }
            });
        });

        this.set('isAllEvaluationsExpanded', isAnySeasonExpanded);
    },

    showOneCategory: function() {
       return this.get('session.teamConfiguration.numGradeCategories') === 1;
    }.property('session.teamConfiguration.numGradeCategories'),

    showTwoCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 2;
    }.property('session.teamConfiguration.numGradeCategories'),

    showThreeCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 3;
    }.property('session.teamConfiguration.numGradeCategories'),

    actions: {
        toggleExpandAllGrades: function () {
            this.toggleProperty('isAllExpanded');
            var flag = this.get('isAllExpanded');

            var seasons = this.get('attrs.seasons');
            seasons.forEach( function setSeasonExpanded(season){
                Ember.set(season, 'isExpanded', flag);
            }, this );
            this.set('isCareerExpanded', this.get('isAllExpanded'));
        },
        toggleExpandAllEvaluations: function () {
            this.toggleProperty('isAllEvaluationsExpanded');
            var flag = this.get('isAllEvaluationsExpanded');

            var evaluationStats = this.get('evaluationStats');

            evaluationStats.forEach(function(stat) {
                stat.seasons_data.forEach(function(season) {
                     Ember.set(season, 'isExpanded', flag);
                });
            });

        },
        toggleExpandSeason: function (index) {
            var season = this.get('attrs.seasons').objectAt(index);
            Ember.set(season, 'isExpanded', !Ember.get(season,'isExpanded'));
            this.toggleIsAllExpandedForLabel();
        },
        toggleExpandCareer: function (index) {
            this.toggleProperty('isCareerExpanded');
            this.toggleIsAllExpandedForLabel();
        },

        toggleExpandSeasonEvaluation: function(season) {
            Ember.set(season, 'isExpanded', !Ember.get(season,'isExpanded'));
            //this.toggleIsAllEvaluationsExpandedForLabel();
        }
    }
});
