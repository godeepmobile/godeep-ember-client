// app/controllers/player/edit.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    attrs: {},
    bodyTypes: null,
    states: null,
    teamPositionTypes: null,
    levels: null,
    graduationYears: null,
    actions: {
        save: function () {
            var self = this
              , teamPlayerAssignment = this.get('teamPlayerAssignment');
            this.validate().then(function() {
                // all validations pass
                // Saving the teamPlayer.
                var model = self.get('model');
                model.save().then(function(modelSaved) {

                    teamPlayerAssignment.save().then(function() {
                        self.send('removeModal');
                        if (modelSaved.get('email')) {
                            self.send('showTooltip', 'tooltip/player-user-account', {
                                player: modelSaved
                            });
                            setTimeout( function() {
                                    self.send('removeTooltip');
                                }, 15000
                            );
                        } else {
                            self.send('temporalTooltip', 'tooltip/saved');
                        }
                    }).catch(function(reason) {
                        self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                    });
                    
                }).catch(function(reason) {
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });

            }).catch(function() {
              // any validations fail
              self.showRequiredMessage();
            }).finally(function() {
              // all validations complete
            });
        },
        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();
            this.get('teamPlayerAssignment').rollback();

            //Closing the modal
            this.send('removeModal');
        }
    },

    showRequiredMessage: function() {
        Ember.$('.message').delay(400).fadeIn(400);
    },

    filteredPositionTypes: Ember.computed.filter('positionTypes', function(positionType, index, array) {
      return parseInt(positionType.get('id')) !== 29;
    }).property('positionTypes', 'model'),

    // Player form validations
    validations: {
        'model.firstName': {
            presence: true
        },

        'model.lastName': {
            presence: true
        },

        'model.email': {
              format: {
                allowBlank: true,
                with      : /^[\w+\-.]+@[a-z\d\-.]+\.[a-z]+$/i,
                message   : 'Invalid e-mail address',
              }
        },

        'teamPlayerAssignment.position1': EmberValidations.validator(function() {
            if (!this.model.get('teamPlayerAssignment.position1.id')) {
                return "can't be blank";
            }
        }),

        /*'model.jerseyNumber': {
            presence: { message: ' ' },
            length  : { maximum: 2, messages: { tooLong: 'Should be less than 3 digits' }},
            format  : {
                with: /^[0-9]+$/,
                message: 'Must be a positive number'
            }
        },*/

        'model.height': {
            presence: true
        },

        'model.weight': {
            presence: true
        }
    }
});
