import Ember from 'ember';
import config from '../config/environment';
import LoginControllerMixin from 'simple-auth/mixins/login-controller-mixin';

export
default Ember.Controller.extend(LoginControllerMixin, {
    errorMessage: "",
    credentialError: false,
    authenticator: 'authenticator:custom',

    displayedOnDesktopBrowser: !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/),

    /*displayFirstTimeLoginTooltips: function() {
        
        Ember.run.schedule("afterRender", this, function() {
            if (this.get('session.user.firstTimeLogin')) {
                this.send('showModal', 'tooltip/mainMenu');
            }
        });

    }.on('init'),*/

    validateSession: function() {
        console.log('ApplicationController: validateSession()');
        var token = window.GoDeep ? window.GoDeep.accessToken : null;
        return new Ember.RSVP.Promise(function(resolve, reject) {
            if (token) {
                var testEndpoint = config.API.host + config.API.testSession;
                var success = function() {
                        window.GoDeep.validToken = true;
                        resolve(true);
                    },
                    failure = function(error) {
                        window.GoDeep.validToken = false;
                        resolve(false);
                    };
                Ember.$.ajax({
                    url: testEndpoint,
                    type: 'GET'
                }).then(success, failure);
            } else {
                window.GoDeep.validToken = false;
                resolve(false);
            }
        }); // Promise({
    },

    authenticate: function() {
        console.log('ApplicationController: authenticate()');
        var godeep = window.GoDeep;
        if (godeep && godeep.validToken) {
            var token = godeep.accessToken;
            if (token) {
                var session = this.get('session');
                session.set('content.token', token.id);
                session.set('content.user', token.user);
                session.set('isAuthenticated', true);
                console.log('ApplicationController: authenticate() username ' + token.user.username);
            }
        }
        //this.transitionToRoute('dashboard');
    },

    isOfflineRoute: function() {
        return this.get('currentPath').indexOf('offline') !== -1;
    }.property('currentPath'),

    actions: {
        authorizationFailed: function() {
            console.log('ApplicationRoute: session authorization failed');
        }
    }
});
