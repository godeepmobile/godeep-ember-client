//app/controllers/evaluation.js

import Ember from 'ember';

export default Ember.Controller.extend({
    thisPage: "Player Evaluation",
    breadcrumbs: [],
    barHeight: 60,
    VIEWS: {
      graphical: 1,
      printFriendly: 2
    },
    EVALUATION_VALUES_COLORS: {
      good: '#2f9d04',
      bad: '#e00404'
    },
    currentView: 2,

    actions: {
      showNote: function (note, notePos, criterias) {
        this.send('showModal', 'evaluation-summary/note', {
          criteria: criterias && criterias.length > 0 ? criterias[notePos] : '',
          note: note
        });
      },

      editPlayerEvaluation: function () {
          // 1: default criteria type
          var groupId        = this.get('teamScoutingEvalScoreGroup.id')
            , playerId       = this.get('playerId')
            , playerType     = this.get('playerType')
            , positionTypeId = this.get('teamScoutingEvalScoreGroup.positionType.id')
            , url            = "/evaluation/" + playerType + "/" + playerId +"/position/"+positionTypeId+"/criteriaType/1?scoreGroup="+groupId;
          this.transitionToRoute(url);
      },

      switchEvaluationReportView: function(view) {
        if (view) {
          this.set('currentView', view);
        }
      }
    },

    isEvaluator: function() {
        var sessionUserId = this.get('session.user.id');
        var evaluatorUserId = this.get('teamScoutingEvalScoreGroup.user.id');
        if (sessionUserId && evaluatorUserId) {
            return parseInt(sessionUserId) === parseInt(evaluatorUserId);
        } else {
            return false;
        }
    }.property('teamScoutingEvalScoreGroup.user'),

    isPlayer: function () {
        return this.get('playerType') === 'teamPlayer';
    }.property('player', 'playerType'),

    isGraphicalCurrentView: function() {
      return this.get('currentView') === this.get('VIEWS.graphical');
    }.property('currentView'),

    isPrintFriendlyCurrentView: function() {
      return this.get('currentView') === this.get('VIEWS.printFriendly');
    }.property('currentView')
});
