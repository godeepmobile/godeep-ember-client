// controllers/rank.js

import Ember from 'ember';

export default Ember.Controller.extend( {
    rankSelected: null,
    prospectsSelected: function() {
        return this.get('rankSelected') === 'prospects';
    }.property('rankSelected'),

    playersSelected: function() {
        return this.get('rankSelected') === 'players';
    }.property('rankSelected'),
});
