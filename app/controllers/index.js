import Ember from 'ember';
import LoginControllerMixin from 'simple-auth/mixins/login-controller-mixin';

export default Ember.Controller.extend(LoginControllerMixin, {
    errorMessage:"",
    credentialError:false,
    authenticator: 'authenticator:custom',
    actions: {
        authenticate: function () {
            //console.log('authenticate username '+this.get('identification'));
                //var credentials = {
                //    username : this.get('identification'),
                //    password : this.get('password')
                //}, self = this;

            var _this = this;
            this._super().then(function(options) {
                //console.log('IndexController: authenticate options %o', options);
                //console.log('IndexController: transitioning to dashboard');
                _this.transitionToRoute('dashboard');
            }, function() {
                console.log('authenticate failure?');
                _this.set('errorMessage', 'Please enter a valid username and password');
                _this.set('credentialError', true);
            });
        }
    }
});
