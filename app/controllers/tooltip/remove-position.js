// controllers/tooltip/remove-position.js

import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        selectPositionType: function() {
            this.selectPositionType();
            this.send('removeTooltip');
        }
    },

    selectPositionType: function() {
        var positionTypeId  = this.get('model.positionTypeId')
            , positionTypes = this.get('model.positionTypes');
        positionTypes.forEach(function(position) {
            if (parseInt(position.get('id')) === parseInt(positionTypeId)){
                position.set('isSelected', true);
            }
        });
    }

});