// controllers/tooltip/player-user-account.js

import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        cancel: function() {
            var self   = this
              , player = this.get('model.player');
            this.send('removeTooltip');
            player.set('email', null);
            player.save().then(function(modelSaved) {
                console.log('player email address deleted');
            }).catch( function(reason) {
                self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
            });
        }
    },

});