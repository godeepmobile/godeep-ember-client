import Ember from 'ember';

export default Ember.Controller.extend({
	setTop: function() {
		Ember.run.later(function() {
			Ember.$('.first-login-tooltip .modal-dialog').css('top', Ember.$('.help-option').offset().top - 105);
		}, 500);
	}.on('init')
});
