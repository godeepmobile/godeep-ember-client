import Ember from 'ember';

export default Ember.Controller.extend({
  thisPage: "Grade Player Performance",
  breadcrumbs: [],
  gameEventType: null,
  practiceEventType: null,
  practiceGameEventType: null,

  gamePeriods: function() {
    // Include games only
    var periods = this.getEventsByYear(this.get('model').rejectBy('isPractice').sortBy('date')).sortBy('year').reverse();
    var firstPeriod = periods.get('firstObject');
    if (firstPeriod) {
      var firstEvent  = firstPeriod.events.get('firstObject');
      if (firstEvent) {
        firstEvent.toggleProperty('isCollapsed');
      }
    }
    return periods;
  }.property('model.@each'),

  practicePeriods: function() {
    // Include practices and practice games
    var periods = this.getEventsByYear(this.get('model').filterBy('isPractice').sortBy('date').reverse()).sortBy('year').reverse();
    var firstPeriod = periods.get('firstObject');
    if (firstPeriod) {
      var firstEvent  = firstPeriod.events.get('firstObject');
      if (firstEvent) {
        firstEvent.toggleProperty('isCollapsed');
      }
      /*var firstEventByDate = firstPeriod.eventsByDate.get('firstObject');
      if (firstEventByDate) {
        var firstEvents = firstEventByDate.events;
        if (firstEvents) {
          firstEvents.forEach(function(evt) {
            evt.toggleProperty('isCollapsed');
          });
        }
      }*/
    }
    return periods;
  }.property('model.@each'),

  getEventsBySeasonAndDate: function(events) {
        var eventArray     = new Ember.A()
          , eventObj       = {}
          , eventObjByYear = {}
          , formattedDate
          , eventDateSeason
          , eventDateSeasonArr
          , eventDate
          , eventYear
          , year;

        events.forEach(function(event) {
            formattedDate = event.get('formattedDate') + '_' + event.get('season');
            if (eventObj[formattedDate]) {
                eventObj[formattedDate].pushObject(event);
            } else {
                eventObj[formattedDate] = new Ember.A([event]);
            }
        });

        // Translating the events' object into an object by season
        for(eventDateSeason in eventObj) {
            eventDateSeasonArr = eventDateSeason.split('_');
            eventDate          = eventDateSeasonArr[0];
            year               = eventDateSeasonArr[1];

            if (eventObjByYear[year]) {
                eventObjByYear[year].pushObject({
                    date   : eventDate
                  , events : eventObj[eventDateSeason]
                });
            } else {
                eventObjByYear[year] = new Ember.A([{
                    date   : eventDate
                  , events : eventObj[eventDateSeason]
                }]);
            }
        }

        // Making the object an array
        for (eventYear in eventObjByYear) {
            eventArray.pushObject({
                year : eventYear
              , eventsByDate : eventObjByYear[eventYear]
            });
        }

        return eventArray.sortBy('year');
    },

  getEventsByYear: function(events) {
        var eventArray = new Ember.A()
          , eventObj   = {}
          , eventYear
          , year;

        events.forEach(function(event) {
            year = event.get('season');
            if (eventObj[year]) {
                eventObj[year].pushObject(event);
            } else {
                eventObj[year] = new Ember.A([event]);
            }
        });

        // Translating the events' object into an array
        for(eventYear in eventObj) {
            eventArray.pushObject({
                year   : eventYear
              , events : eventObj[eventYear]
            });
        }

        return eventArray;
    },

    actions: {
      toggleExpand: function(event) {
        event.toggleProperty('isCollapsed');
      },

      toggleExpandPractice: function(practicePeriod) {
        practicePeriod.events.forEach(function(event) {
          event.toggleProperty('isCollapsed');
        });
      }
    }
});
