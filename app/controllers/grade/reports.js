import Ember from 'ember';

export default Ember.Controller.extend({
	actions : {
		gradePlayerPerformance: function(model) {
			this.sendReportNotificationToPlayers(model);
			this.send('removeModal');
			this.transitionTo('games');
		}
	},

	sendReportNotificationToPlayers: function(model) {
		var adapter = this.store.adapterFor('application');
        adapter.ajax(this.playerReportUrl(adapter, model), 'POST')
            .then(function(response) {
                console.log('notifications send successfully');
            }, this.notificationFails
        );
	},
	playerReportUrl: function(adapter, model) {
        return adapter.buildURL('TeamCutup', model.get('teamCutup.id')) + '/performanceReportByEvent/' + model.get('teamEvent.id');
    },

    notificationFails: function(response) {
        console.log('reportr notifaction fails: ', response);
    }

});
