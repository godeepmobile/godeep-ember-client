import Ember from 'ember';

export default Ember.Controller.extend({
	showAtStartup: true,

	currentView: 0,

	currentViewTitle: null,

	views: {
		MAIN      : 0
	  , DASHBOARD : 1
	  , ROSTER    : 2
	  , GRADING   : 3
	  , REPORTS   : 4
	  , CUTUP     : 5
	  , PLAY_DATA : 6
	},

	viewTitles: {
		DASHBOARD : 'Using the Dashboard'
	  , ROSTER    : 'Updating the Roster and Assigning Positions'
	  , GRADING   : 'Grading Player Performance'
	  , REPORTS   : 'Viewing Performance Reports'
	  , CUTUP     : 'Entering Film Cutups for Grading'
	  , PLAY_DATA : 'Uploading Play Data from Your Video System'
	},

	mainIsCurrentView: function() {
		return this.get('currentView') === this.get('views.MAIN');
	}.property('currentView'),

	dashboardIsCurrentView: function() {
		return this.get('currentView') === this.get('views.DASHBOARD');
	}.property('currentView'),

	rosterIsCurrentView: function() {
		return this.get('currentView') === this.get('views.ROSTER');
	}.property('currentView'),

	gradingIsCurrentView: function() {
		return this.get('currentView') === this.get('views.GRADING');
	}.property('currentView'),

	reportsIsCurrentView: function() {
		return this.get('currentView') === this.get('views.REPORTS');
	}.property('currentView'),

	cutupIsCurrentView: function() {
		return this.get('currentView') === this.get('views.CUTUP');
	}.property('currentView'),

	playDataIsCurrentView: function() {
		return this.get('currentView') === this.get('views.PLAY_DATA');
	}.property('currentView'),

	showAtStartupChanged: function() {
		this.send('setWelcomePopupDefaultState', this.get('session.user.displayWelcomeDialog'));
	}.observes('session.user.displayWelcomeDialog'),

	actions: {
		changeView: function(view, title) {
			this.set('currentView', view);
			
			if (title) {
				this.set('currentViewTitle', title);
			}
		}
	}
});
