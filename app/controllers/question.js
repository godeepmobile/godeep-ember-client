import Ember from 'ember';

export default Ember.Controller.extend({

	clearForm: function() {
		this.set('question', null);
		this.set('hasError', false);
	},

	buildQuestionURL: function(adapter, teamCutupId) {
    	return adapter.buildURL('GoUsers') + '/question';
    },

	actions: {
		sendQuestion: function() {
			var question     = this.get('question')
			  , adapter      = this.store.adapterFor('application')
			  , errorHandler = function(error) {
					console.log('responseJSON ', error.responseJSON);
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.responseText){
						alert(error.responseText);
					}
				}.bind(this);

			if (question) {
				
				adapter.ajax(this.buildQuestionURL(adapter), 'POST', {
					data : {text : question}
				}).then(function(response) {

					this.send('removeModal');
					Ember.run.later(this, function() {
            			this.send('temporalTooltip', 'tooltip/saved', {message: 'Your question has been sent.'});
            		}, 500);

	            }.bind(this), errorHandler);

			} else {
				this.set('hasError', true);
			}
		}
	}
});
