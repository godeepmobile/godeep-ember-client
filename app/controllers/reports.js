// controllers/reports.js

import Ember from 'ember';

export default Ember.Controller.extend({
    report: null,   // selected report
    thisPage: "Player Performance Reports",  // sets page name in breadcrumbs in page header
    breadcrumbs: [],      // sets breadcrumbs in page header
    reportData: null,
    reportExpanded: true,

    savedReportsChanged: function() {
    	if (!this.get('selectedReport')) {
    		setTimeout(function() {
    			Ember.$('.saved-reports-dropdown option:first-child').attr('selected','selected');
    		}, 100);
    	}
    }.observes('savedReports.@each')
});
