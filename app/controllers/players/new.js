// controllers/players/new.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    loadData: Ember.inject.service(),
    attrs: {},

    graduationYears: function() {
        var years       = Ember.A([])
          , currentYear = parseInt(moment().format("YYYY"));

        //this.set('model.graduationYear', currentYear);
        for ( var i = (currentYear - 10) ; i <= currentYear + 10 ; i++ ) {
            var obj = {};
            Ember.set(obj, 'id', i);
            Ember.set(obj, 'name', i);
            years.push(obj);
        }

        return years;
    }.property('model'),

    actions: {
        searchPlayer: function () {
            console.log('Search a player...');
        },

        save: function () {
            var self = this;
            this.validate().then(function() {
                // all validations pass
                // Saving the new teamPlayer.
                var model = self.get('model');
                model.save().then(function(modelSaved) {

                    self.store.createRecord('teamPlayerAssignment', {
                        team         : self.get('session.team')
                      , teamPlayer   : modelSaved
                      , jerseyNumber : modelSaved.get('jerseyNumber')
                      , position1    : modelSaved.get('position1')
                    }).save().then(function() {
                        self.send('removeModal');
                        self.send('reloadUserProfile');
                        self.highlightPlayer(modelSaved);
                        if (modelSaved.get('email')) {
                            self.send('showTooltip', 'tooltip/player-user-account', {
                                player: modelSaved
                            });
                            setTimeout( function() {
                                    self.send('removeTooltip');
                                }, 15000
                            );
                        } else {
                            self.send('temporalTooltip', 'tooltip/saved');
                        }
                    });
                }).catch(function(reason) {
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });
            }).catch(function() {
              // any validations fail
              self.showRequiredMessage();
            }).finally(function() {
              // all validations complete
            });
        },

        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();
            this.send('removeModal');
        }
    },

    bodyTypes: function() {
        return this.get('loadData.bodyTypes');
    }.property('model'),

    /**
     * Returns position types, without:
     * 29;"Athlete";"ATH"
     **/
    positionTypes: Ember.computed.filter('loadDataPositionTypes', function(positionType, index, array) {
      return parseInt(positionType.get('id')) !== 29;
    }).property('loadDataPositionTypes', 'model'),

    loadDataPositionTypes: function() {
        return this.get('loadData.positionTypes');
    }.property('model'),

    states: function() {
        return this.get('loadData.states');
    }.property('model'),

    levels: function() {
        return this.get('loadData.levels');
    }.property('model'),

    showRequiredMessage: function() {
        Ember.$('.message').delay(400).fadeIn(400);
    },

    highlightPlayer : function(player) {
        var playerId = '#roster-postion-' + player.get('id');
        var checkExist = setInterval(function() {
           if (Ember.$(playerId).length) {
                setTimeout( function() {
                    var middleHeight = Ember.$(window).height() / 2;
                    Ember.$('html, body').animate({ scrollTop: Ember.$(playerId).offset().top - middleHeight }, 'slow');
                    setTimeout( function() {
                        Ember.$(playerId).toggleClass( "highlight" );
                      }, 800
                    );
                    setTimeout( function() {
                        Ember.$(playerId).toggleClass( "highlight" );
                      }, 3000
                    );
                  }, 500
                );
              clearInterval(checkExist);
           } else {
                console.log('not found');
           }
        }, 100);

        // wait for html element for 5 seconds
        setTimeout( function() {
            clearInterval(checkExist);
            }, 5000
        );
    },

    // Player form validations
    validations: {
        'model.firstName': {
            presence: true
        },

        'model.lastName': {
            presence: true
        },

        'model.email': {
              format: {
                allowBlank: true,
                with      : /^[\w+\-.]+@[a-z\d\-.]+\.[a-z]+$/i,
                message   : 'Invalid e-mail address',
              }
        },

        'model.position1': EmberValidations.validator(function() {
            if (!this.model.get('model.position1.id')) {
                return "can't be blank";
            }
        }),

        /*'model.jerseyNumber': {
            presence: { message: ' ' },
            length  : { maximum: 2, messages: { tooLong: 'Should be less than 3 digits' }},
            format  : {
                with: /^[0-9]+$/,
                message: 'Must be a positive number'
            }
        },*/

        'model.height': {
            presence: true
        },

        'model.weight': {
            presence: true
        }
    }
});
