import Ember from 'ember';

export default Ember.Controller.extend(Ember.Evented, {
	loadData: Ember.inject.service(),
	thisPage: 'Upload Roster',
	breadcrumbs: [{
        linkTo  : 'players',
        caption : 'Roster & Positions'
    }],

    tableOptions: {
    	colHeaders : [
			'<span class="required-col">Jersey No</span>',
			'<span class="required-col">First Name</span>',
			'<span class="required-col">Last Name</span>',
			'<span class="required-col">Height</span>',
			'<span class="required-col">Weight</span>',
			'<span class="required-col">Position</span>',
			'2nd Position',
			'ST Position',
			'School/Team',
			'State',
			'DOB'
		], 
		startCols: 11,
		startRows: 13,
		maxCols: 11,
		minSpareCols: 0,
		stretchH: 'all'
	},

	actions: {
		cancelRosterUpload: function() {
			this.trigger('clearTableData');
			this.transitionTo('players');
		}
	}
});
