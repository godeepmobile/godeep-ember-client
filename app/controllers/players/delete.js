// controllers/players/delete.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    actions: {
        deletePlayerAssignment: function (_isPermanent) {
            var teamPlayerAssignment = this.get('model')
              , adapter              = this.store.adapterFor('application');

            this.set('isDeleting', true);

            adapter.ajax(this.inactiveTeamPlayerAssignmentUrl(adapter, teamPlayerAssignment, _isPermanent), 'DELETE').then(function(response) {

                console.log('team player deleted successfully');
                
                if (_isPermanent) {
                    // Hides player
                    this.hidePlayerAssignment(teamPlayerAssignment.get('teamPlayer.id'));
                    teamPlayerAssignment.unloadRecord();

                    //Closing the modal
                    this.set('isDeleting', false);
                    this.send('removeModal');
                } else {
                    teamPlayerAssignment.reload().then(function() {
                        //Closing the modal
                        this.set('isDeleting', false);
                        this.send('removeModal');
                    }.bind(this));
                }

            }.bind(this), this.teamPlayerAssignmentDeleteFailure);

            
        },
        cancel: function () {
            //Closing the modal
            this.set('isDeleting', false);
            this.send('removeModal');
        }
    },

    inactiveTeamPlayerAssignmentUrl: function(adapter, model, _isPermanent) {
        return adapter.buildURL('TeamPlayerAssignment', model.get('id')) + '/logicalDelete/' + (_isPermanent ? 'true' : 'false');
    },

    teamPlayerAssignmentDeleteFailure: function(response) {
        // in case of failure
    },

    hidePlayerAssignment: function(_playerId) {
        var playerId = '#roster-postion-' + _playerId;
        setTimeout( function() {
            Ember.$(playerId).toggleClass( "highlight" );
            setTimeout( function() {
                Ember.$(playerId).fadeOut( 500 );
              }, 1000
            );
          }, 100
        );
    }
});