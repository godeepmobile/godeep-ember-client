// app/controllers/players/position.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    selectedPositionType: null,
    teamPositionTypes: null,
    teamPositionTypesOff: null,
    teamPositionTypesDef: null,
    teamPositionTypesST: null,

    actions: {
        positionTypeViewChanged: function(newValue) {
            var position         = this.get('attrs.position'),
                playerAssignment = this.get('model'), 
                currentPosType   = playerAssignment.get(position).get('id');

            if ( newValue === 1 ) {
                this.set('teamPositionTypes', this.get('teamPositionTypesOff'));
            } else if ( newValue === 2 ) {
                this.set('teamPositionTypes', this.get('teamPositionTypesDef'));
            } else if ( newValue === 3 ) {
                this.set('teamPositionTypes', this.get('teamPositionTypesST'));
            }

            if (currentPosType) {
                this.set('attrs.isEditing', true);
                if (this.get('teamPositionTypes').isAny('id', currentPosType)) {
                    // Changed to the current platoon type
                    this.set('selectedPositionType', playerAssignment.get(position));
                } else {
                    // Changed to a new platoon
                    this.set('selectedPositionType', this.get('teamPositionTypes').toArray()[0]);
                }
                
            } else {
                this.set('selectedPositionType', this.get('teamPositionTypes').toArray()[0]);
                this.set('attrs.isDisabled', false);
            }
        },
        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();

            this.transitionToRoute('players');

            //Closing the modal
            this.send('removeModal');
        },
        save: function (teamPlayerAssignment) {
            teamPlayerAssignment.set(this.get('attrs.position'), this.get('selectedPositionType'));
            teamPlayerAssignment.save();

            //Closing the modal
            this.send('removeModal');
            this.highlightPlayerAssignment(teamPlayerAssignment.get('teamPlayer.id'));
        },
        remove: function (teamPlayerAssignment) {
            teamPlayerAssignment.set(this.get('attrs.position'), null);
            teamPlayerAssignment.save();

            //Closing the modal
            this.send('removeModal');
            this.highlightPlayerAssignment(teamPlayerAssignment.get('teamPlayer.id'));
        }
    },

    highlightPlayerAssignment : function(_playerId) {
        var playerId = '#roster-postion-' + _playerId;
        setTimeout( function() {
            Ember.$(playerId).toggleClass( "highlight" );
            setTimeout( function() {
                Ember.$(playerId).toggleClass( "highlight" );
              }, 1000
            );
          }, 100
        );
    },
});
