// controllers/players/index.js

import Ember from 'ember';

export default Ember.Controller.extend({
    // don't call it sortProperties, as that is the name used by the buggy arrayController sorting logic.
    loadData: Ember.inject.service(),

    mySortProperties: ['jerseyNumber','lastName'],

    season: null,

    seasons: [],

    // use a computed property that returns a sorted copy of the model
    sortedPlayers: Ember.computed.sort('model', 'mySortProperties'),

    arrangedContent :function() {
        //return this.get('sortedPlayers').filterBy('season', this.get('loadData.season'));
        return this.get('sortedPlayers').filter(function(teamPlayer) {
            return (teamPlayer.get('temporarilyInactive') || !teamPlayer.get('endDate')) && teamPlayer.get('season') === this.get('loadData.season');
        }.bind(this));
    }.property('sortedPlayers', 'sortedPlayers.[]', 'loadData.season'),

    seasonChanged: function() {
         this.send('changeCurrentSeason', this.get('season.value'));
    }.observes('season'),

    formattedSeasons: function() {
        var seasons = Ember.A([]);

        this.get('seasons').forEach(function(season) {
            seasons.push({
                value : season,
                label : season + ' Season'
            });
        });

        return seasons;
    }.property('seasons'),

    isCurrentSeasonRoster: function() {
        return this.get('loadData.isCurrentSeason'); 
    }.property('loadData.season'),

    actions: {
        addPlayer: function() {
            this.send('showModal', 'players/new', this.store.createRecord('teamPlayer', {
                team: this.get('session.team')
            }));
            this.send('focusFirstInput');
        },

        uploadRoster: function() {
            this.transitionToRoute('players.upload');
        }
    },
});
