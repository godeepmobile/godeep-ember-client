// controllers/reports/game-performance.js

import Ember from 'ember';

export default Ember.Controller.extend({

    loadData: Ember.inject.service(),

    //thisPage: "Game Performance",

    queryParams: ['player','platoon','group','game','season', 'runReport'],
    player: null,   // queryParam.player
    platoon: null,  // queryParam.platoon
    group: null,    // queryParam.group
    game: null,     // queryParam.game
    season: null,   // queryParam.season
    runReport: false, // queryParam.runReport

    message:null,           // text message to user

    // detail table properties
    reportData:[],          // this holds data for report table
    eventData:[],

    reportExpanded: false,

    isAllExpanded: false,

    // chart properties
    chartBaseLine: null,    // we will set this with base value from team to show baseLine on chart
    chartData: null,        // this holds data for chart
    chartPeriod: null,      // which type of chart? for a game or for a season?
    isPeriodGame: function() {
        return this.get('chartPeriod') === 'teamEvent';
    }.property('chartPeriod'),
    isPeriodSeason: function() {
        return this.get('chartPeriod') === 'season';
    }.property('chartPeriod'),
    showBowtie: function() {
        return this.get('eventData.length') > 1;
    }.property('eventData'),
    chartCategories: null,       // what categories are used for x-axis?

    // report headline properties
    cat1Label: null,
    cat2Label: null,
    cat3Label: null,
    cat1Grade: null,        // category 1 grade
    cat2Grade: null,        // category 2 grade
    cat3Grade: null,        // category 3 grade
    overallGrade: null,     // overall grade

    reportScope: function() {
        var picklists = this.get('model.picklists');

        if (this.get('player')) {
            return picklists.players.findBy('id', this.get('player'));
        }

        if (this.get('platoon')) {
            return picklists.platoons.findBy('id', this.get('platoon'));
        }

        if (this.get('group')) {
            return picklists.groups.findBy('id', this.get('group'));
        }
    }.property('player', 'platoon', 'group', 'model.picklists.platoons.@each', 'model.picklists.groups.@each', 'model.picklists.players.@each'),

    reportPeriod: function() {
        var picklists = this.get('model.picklists');

        if (this.get('game')) {
            return picklists.games.findBy('id', this.get('game'));
        }

        if (this.get('season')) {
            return picklists.seasons.findBy('id', this.get('season'));
        }
    }.property('game', 'season', 'model.picklists.games.@each'),

    selectedPlatoonChanged: function() {
        var platoonCriteria = this.get('CRITERIA.platoon');

        if (this.get('selectedCriteria') !== platoonCriteria) {
            this.set('selectedCriteria', platoonCriteria);
        }
    }.observes('selectedPlatoon'),

    selectedGroupChanged: function() {
        var groupCriteria = this.get('CRITERIA.positionGroup');

        if (this.get('selectedCriteria') !== groupCriteria) {
            this.set('selectedCriteria', groupCriteria);
        }
    }.observes('selectedGroup'),

    selectedPlayersChanged: function() {

        if (this.get('playerAssignmentsForSeason').isAny('selectedForReport')) {
            this.set('selectedCriteria', this.get('CRITERIA.players'));
        }

    }.observes('playerAssignmentsForSeason.@each.selectedForReport'),

    playerAssignmentsForSeasonChanged: function() {
        var season
          , fromDateSeason
          , toDateSeason
          , playerAssignments = []
          , differentSeasons  = false;

        this.set('playerAssignmentMessage', null);

        switch (this.get('selectedScope')) {
            case this.get('SCOPE.event'):
                season = this.get('selectedGame.season');
                break;

            case this.get('SCOPE.season'):
                season = this.get('selectedSeason.id');
                break;

            case this.get('SCOPE.period'):

                if (this.get('fromDate') && this.get('toDate')) {

                    fromDateSeason = this.get('fromDate').get('month') < 2 ? this.get('fromDate').get('year') - 1 : this.get('fromDate').get('year');
                    toDateSeason   = this.get('toDate').get('month') < 2 ? this.get('toDate').get('year') - 1 : this.get('toDate').get('year');

                    if (fromDateSeason !== toDateSeason) {
                        differentSeasons = true;
                    } else {
                        season = toDateSeason;
                    }
                }

                break;
        }

        if (this.get('errors')) {
            this.set('errors.season', differentSeasons);
        }

        if (season) {
            playerAssignments = Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
                content        : this.get('playerAssignments').filterBy('season', season)
              , sortProperties : ['teamPlayer.lastName', 'teamPlayer.firstName']
            });
            
            if (playerAssignments.get('length') === 0) {
                this.set('playerAssignmentMessage', 'No players were found for ' + season + ' season.');
            }
        } else {

            this.set('playerAssignmentMessage', 'Select a game, season or time period for the report to see the available players.');
        }

        this.set('playerAssignmentsForSeason', playerAssignments);

    }.observes('playerAssignments', 'playerAssignments.[]', 'loadData.season', 'selectedScope', 'selectedGame', 'selectedSeason', 'fromDate', 'toDate'),

    selectedGameChanged: function() {
        var gameScope = this.get('SCOPE.event');

        if (this.get('selectedScope') !== gameScope) {
            this.set('selectedScope', gameScope);
        }

        this.set('selectedFilter', this.get('selectedGame.eventType.id'));

    }.observes('selectedGame'),

    selectedSeasonChanged: function() {
        var seasonScope = this.get('SCOPE.season');

        if (this.get('selectedScope') !== seasonScope) {
            this.set('selectedScope', seasonScope);
        }
    }.observes('selectedSeason'),

    selectedPeriodChanged: function() {
        var periodScope = this.get('SCOPE.period');

        if (this.get('selectedScope') !== periodScope) {
            this.set('selectedScope', periodScope);
        }
    }.observes('fromDate', 'toDate'),

    teamReportChanged: function() {
        var teamReport = this.get('teamReport');

        if (teamReport) {
            this.send('populateTeamReport', teamReport, this);
        }

    }.observes('teamReport'),

    actions: {
        toggleExpandAllPlayerReports: function() {
            this.toggleProperty('isAllExpanded');
            var flag = this.get('isAllExpanded');

            this.get('eventData').forEach(function(evt) {
                evt.teamPlayers.forEach(function(player) {
                    Ember.set(player,'isExpanded', flag);
                });
            });
        }
    }

});
