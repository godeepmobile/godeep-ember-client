import Ember from 'ember';

export default Ember.Controller.extend({
    loadData: Ember.inject.service(),

	selectedPlatoonChanged: function() {
        var platoonCriteria = this.get('CRITERIA.platoon');

        if (this.get('selectedCriteria') !== platoonCriteria) {
            this.set('selectedCriteria', platoonCriteria);
        }
    }.observes('selectedPlatoon'),

    selectedGroupChanged: function() {
        var groupCriteria = this.get('CRITERIA.positionGroup');

        if (this.get('selectedCriteria') !== groupCriteria) {
            this.set('selectedCriteria', groupCriteria);
        }
    }.observes('selectedGroup'),

    selectedPlayersChanged: function() {

        if (this.get('playerAssignmentsForSeason').isAny('selectedForReport')) {
            this.set('selectedCriteria', this.get('CRITERIA.players'));
        }

    }.observes('playerAssignmentsForSeason.@each.selectedForReport'),

    selectedPlaysChanged: function() {
    	var partGameScope = this.get('SCOPE.partGame');

        if (this.get('selectedScope') !== partGameScope) {
            this.set('selectedScope', partGameScope);
        }

    }.observes('initPlay', 'toPlay'),

	selectedGameChanged: function() {
        var selectedGame = this.get('selectedGame');

        if (selectedGame) {
            this.set('selectedFilter', selectedGame.get('eventType.id'));
        }

    }.observes('selectedGame'),

    teamReportChanged: function() {
        var teamReport = this.get('teamReport');

        if (teamReport) {
            this.send('populateTeamReport', teamReport, this);
        }

    }.observes('teamReport'),

    playerAssignmentsForSeasonChanged: function() {
        var season
          , playerAssignments = [];

        this.set('playerAssignmentMessage', null);

        if (this.get('selectedGame')) {
            season = this.get('selectedGame.season');
        }

        if (season) {
            playerAssignments = Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
                content        : this.get('playerAssignments').filterBy('season', season)
              , sortProperties : ['teamPlayer.lastName', 'teamPlayer.firstName']
            });
            
            if (playerAssignments.get('length') === 0) {
                this.set('playerAssignmentMessage', 'No players were found for ' + season + ' season.');
            }
        } else {

            this.set('playerAssignmentMessage', 'Select a game for the report to see the available players.');
        }

        this.set('playerAssignmentsForSeason', playerAssignments);

    }.observes('playerAssignments', 'playerAssignments.[]', 'loadData.season', 'selectedGame')
});
