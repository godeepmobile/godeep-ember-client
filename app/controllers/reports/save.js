import Ember from 'ember';

export default Ember.Controller.extend({
	isPublic: false,
	error: false,

	clearForm: function() {

		this.set('error', false);
		this.set('isPublic', false);
		this.set('name', null);

	},

	actions: {

		goToSaveReport: function() {
			var name = this.get('name');

			if (name) {
				this.send('saveReport', name, this.get('isPublic'));
				this.send('removeModal');
				this.set('error', false);
			} else {
				this.set('error', true);
			}
			
		}
	}
});
