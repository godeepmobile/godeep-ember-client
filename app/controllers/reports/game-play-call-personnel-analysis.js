import Ember from 'ember';

export default Ember.Controller.extend({
    loadData: Ember.inject.service(),

	selectedPlatoonChanged: function() {
        var platoonCriteria = this.get('CRITERIA.platoon');

        if (this.get('selectedCriteria') !== platoonCriteria) {
            this.set('selectedCriteria', platoonCriteria);
        }
    }.observes('selectedPlatoon'),

    selectedGroupChanged: function() {
        var groupCriteria = this.get('CRITERIA.positionGroup');

        if (this.get('selectedCriteria') !== groupCriteria) {
            this.set('selectedCriteria', groupCriteria);
        }
    }.observes('selectedGroup'),

    selectedPlayersChanged: function() {

        if (this.get('playerAssignmentsForSeason').isAny('selectedForReport')) {
            this.set('selectedCriteria', this.get('CRITERIA.players'));
        }

    }.observes('playerAssignmentsForSeason.@each.selectedForReport'),

    selectedGameChanged: function() {
        var gameScope = this.get('SCOPE.event');

        if (this.get('selectedScope') !== gameScope) {
            this.set('selectedScope', gameScope);
        }

        this.set('selectedFilter', this.get('selectedGame.eventType.id'));

    }.observes('selectedGame'),

    selectedSeasonChanged: function() {
        var seasonScope = this.get('SCOPE.season');

        if (this.get('selectedScope') !== seasonScope) {
            this.set('selectedScope', seasonScope);
        }
    }.observes('selectedSeason'),

    selectedPeriodChanged: function() {
        var periodScope = this.get('SCOPE.period');

        if (this.get('selectedScope') !== periodScope) {
            this.set('selectedScope', periodScope);
        }
    }.observes('fromDate', 'toDate'),

    teamReportChanged: function() {
        var teamReport = this.get('teamReport');

        if (teamReport) {
            this.send('populateTeamReport', teamReport, this);
        }

    }.observes('teamReport'),

    playerAssignmentsForSeasonChanged: function() {
        var season
          , fromDateSeason
          , toDateSeason
          , playerAssignments = []
          , differentSeasons  = false;

        this.set('playerAssignmentMessage', null);

        switch (this.get('selectedScope')) {
            case this.get('SCOPE.event'):
                season = this.get('selectedGame.season');
                break;

            case this.get('SCOPE.season'):
                season = this.get('selectedSeason.id');
                break;

            case this.get('SCOPE.period'):

                if (this.get('fromDate') && this.get('toDate')) {

                    fromDateSeason = this.get('fromDate').get('month') < 2 ? this.get('fromDate').get('year') - 1 : this.get('fromDate').get('year');
                    toDateSeason   = this.get('toDate').get('month') < 2 ? this.get('toDate').get('year') - 1 : this.get('toDate').get('year');

                    if (fromDateSeason !== toDateSeason) {
                        differentSeasons = true;
                    } else {
                        season = toDateSeason;
                    }
                }

                break;
        }

        if (this.get('errors')) {
            this.set('errors.season', differentSeasons);
        }

        if (season) {
            playerAssignments = Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
                content        : this.get('playerAssignments').filterBy('season', season)
              , sortProperties : ['teamPlayer.lastName', 'teamPlayer.firstName']
            });
            
            if (playerAssignments.get('length') === 0) {
                this.set('playerAssignmentMessage', 'No players were found for ' + season + ' season.');
            }
        } else {

            this.set('playerAssignmentMessage', 'Select a game, season or time period for the report to see the available players.');
        }

        this.set('playerAssignmentsForSeason', playerAssignments);

    }.observes('playerAssignments', 'playerAssignments.[]', 'loadData.season', 'selectedScope', 'selectedGame', 'selectedSeason', 'fromDate', 'toDate')
});
