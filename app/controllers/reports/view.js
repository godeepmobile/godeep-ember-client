import Ember from 'ember';

export default Ember.Controller.extend({
	queryParams: ['type', 'filter', 'criteria', 'criteriaRange', 'scope', 'scopeRange', 'condition', 'conditionRange'],

	reportHeader: null,
	reportData: null,
	chartData: null,
	chartCategories: null,
	chartBaseLine: null,
	eventNumbers: null,

	type           : null,
	filter         : null,
	criteria       : null,
	criteriaRange  : null,
	scope          : null,
	scopeRange     : null,
	condition      : null,
	conditionRange : null,

	isAllExpanded: false,
	noReportDataFound: false,

	chartFooter: null,
	twoLineChartFooter: false,

	cat1Label: function() {
		return this.get('session.teamConfiguration.gradeCat1Label');
	}.property('session.teamConfiguration.gradeCat1Label'),

	cat2Label: function() {
		return this.get('session.teamConfiguration.gradeCat2Label');
	}.property('session.teamConfiguration.gradeCat2Label'),

	cat3Label: function() {
		return this.get('session.teamConfiguration.gradeCat3Label');
	}.property('session.teamConfiguration.gradeCat3Label'),

	showOneCategory: function() {
       return this.get('session.teamConfiguration.numGradeCategories') === 1;
    }.property('session.teamConfiguration.numGradeCategories'),

    showTwoCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 2;
    }.property('session.teamConfiguration.numGradeCategories'),

    showThreeCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 3;
    }.property('session.teamConfiguration.numGradeCategories'),

	reportTitle: function() {
		var type = this.get('type');

		if (type) {
			return this.get('REPORT_TYPES_TITLES.' + type);
		}
	}.property('type', 'REPORT_TYPES_TITLES'),

	reportCriteria: function() {
		var criteria        = this.get('criteria')
		  , criteriaRange   = this.get('criteriaRange')
		  , CRITERIA        = this.get('model.CRITERIA')
		  , _reportCriteria = this.get('session.team.shortName') + ' ' + this.get('session.team.mascot') + ' - '
		  , type            = this.get('type')
		  , playData        = ''
		  , splitValue
		  , distance
		  , splittedPlayData
		  , platoon
		  , group
		  , player
		  , playerAssignment
		  , event;

		if (type === 'gameGroupReview') {
			event = this.get('model.games').findBy('id', this.get('scopeRange').split(',')[0]);
			if (event) {
				_reportCriteria = event.get('name') + ' - ';
			}
		}

		switch (criteria) {
			case CRITERIA.platoon:
				platoon = this.get('model.platoons').findBy('id', criteriaRange);
				if (platoon) {
					_reportCriteria += platoon.get('name');
				}
				break;

			case CRITERIA.positionGroup:
				group = this.get('model.groups').findBy('id', criteriaRange);
				if (group) {
					_reportCriteria += group.get('name');
				}
				break;

			case CRITERIA.players:
				if (criteriaRange.split(',').length > 1) {
					_reportCriteria += 'Specific Players';
				} else {
					playerAssignment = this.get('model.playerAssignments').findBy('teamPlayer.id', criteriaRange);
					_reportCriteria += playerAssignment.get('teamPlayer.fullNameInitialLast') + ' #' + playerAssignment.get('jerseyNumber') + ' / ' + playerAssignment.get('position1.shortName');
				}
				break;
		}

		if (type === 'gameGroupReview' && event) {
			_reportCriteria += ' - ' +  event.get('formattedDate');
		}

		if (type === 'playerComparison' && this.get('conditionRange')) {
			playerAssignment = this.get('model.playerAssignments').findBy('teamPlayer.id', this.get('conditionRange'));
			player = playerAssignment.get('teamPlayer');

			if (player) {
				this.set('compareCriteria', 'Compared to: ' + _reportCriteria.split(' - ')[1]);
				return player.get('fullName') + ' #' + playerAssignment.get('jerseyNumber') + ' / ' + playerAssignment.get('position1.shortName');
			}
			// shortcircuit to avoid unnecessary calculations
			return;
		}

		if (type === 'gamePlayCallPersonnelAnalysis') {

			this.get('conditionRange').split(',').forEach(function(playDataField, index) {
				splittedPlayData = playDataField.split('=');
				if (index === 0) {
					playData += splittedPlayData[1] + '=' + splittedPlayData[2];
				} else {
					playData += ' ' + splittedPlayData[0] + ' ' + splittedPlayData[1] + '=' + splittedPlayData[2];
				}
			});

			this.set('compareCriteria', 'Plays with ' + playData);

		}

		if (type === 'gameFieldPositionAnalysis') {
			splitValue = this.get('conditionRange').split(',');
			this.set('compareCriteria', 'Plays between: ' + this.fieldLabel(splitValue[0]) + ' and ' + this.fieldLabel(splitValue[1]));
		}

		if (type === 'gameDownDistanceAnalysis') {
			splitValue = this.get('conditionRange').split(',');
			distance   = this.getDistanceLabel(splitValue[1]);

			this.set('compareCriteria', (splitValue[0] && !isNaN(splitValue[0]) ? 'Down: ' + splitValue[0] + ' ' : '') + (distance ? 'Distance: ' + distance : ''));
		}

		return _reportCriteria;

	}.property('criteria', 'criteriaRange', 'model.platoons.@each', 'model.groups.@each', 'model.players.@each', 'type', 'scopeRange', 'conditionRange'),

	reportScope: function() {
		var scope      = this.get('scope')
		  , scopeRange = this.get('scopeRange')
		  , SCOPE      = this.get('model.SCOPE')
		  , eventType  = this.get('model.eventTypes').findBy('id', this.get('filter'))
		  , _reportScope
		  , splitValue
		  , event;

		switch (scope) {
			case SCOPE.period :
				splitValue   = scopeRange.split(',');
				_reportScope = splitValue[0] + ' to ' + splitValue[1];
				break;
			case SCOPE.season :
				_reportScope = scopeRange;
				break;
			case SCOPE.partGame :
				splitValue = scopeRange.split(',');
				_reportScope = 'Plays ' + splitValue[1] + ' to ' + splitValue[2];
				break;
			case SCOPE.event :
				event = this.get('model.games').findBy('id', scopeRange);
				if (this.get('type') === 'gameGroupReview') {
					_reportScope = 'All plays';
				} else {
					if (event) {
						_reportScope = event.get('gameName');
					}
				}
				break;
		}

		if (!this.get('isSingleEventReport') && eventType) {
			_reportScope += ' - ' + eventType.get('name') + 's';
		}

		return _reportScope;

	}.property('scope', 'scopeRange', 'model.games.@each', 'filter', 'model.eventTypes.@each', 'condition', 'conditionRange'),

	isRegularPractice: function() {

		var eventType  = this.get('model.eventTypes').findBy('id', this.get('filter'));

		return eventType && eventType.get('isRegularPractice');

	}.property('model.eventTypes.@each', 'filter'),

	eventLabel: function() {
		var eventType = this.get('model.eventTypes').findBy('id', this.get('filter'));
		if (eventType) {
			return eventType.get('isMatch') ? 'Game' : 'Practice';
		}
	}.property('filter', 'model.eventTypes.@each'),

	isSingleEventReport: function() {
		var scope = this.get('scope');
		return scope === this.get('model.SCOPE.event') || scope === this.get('model.SCOPE.partGame');
	}.property('scope'),

	isReportByPlays: function() {
		return this.get('type') === 'gameGroupReview';
	}.property('type'),

	playersWithNoData: function() {
		var players         = []
		  , playerAssignment
		  , selectedPlayers = this.get('criteriaRange')
		  , playersWithData = this.get('reportData.tableDetails');

		if (this.get('criteria') === this.get('model.CRITERIA.players') && selectedPlayers) {
			selectedPlayers.split(',').forEach(function(playerId) {
				if (playersWithData && !playersWithData.isAny('id', playerId)) {

					playerAssignment = this.get('model.playerAssignments').findBy('teamPlayer.id', playerId);

					if (playerAssignment) {

						players.push({
							firstName    : playerAssignment.get('teamPlayer.firstName'),
							lastName     : playerAssignment.get('teamPlayer.lastName'),
							jerseyNumber : playerAssignment.get('jerseyNumber'),
							position     : playerAssignment.get('position1.shortName'),
							overalls     : {
								avgCat1Grade : 0,
								avgCat2Grade : 0,
								avgCat3Grade : 0,
								avgOverallGrade : 0,
								numPlays : 0
							}
						});

					}
				}
			}.bind(this));
		}

		return players;

	}.property('criteria', 'criteriaRange', 'model.players.@each', 'reportData.tableDetails'),

	getDistanceLabel: function(dist) {
		switch (dist) {
			case '-:5' : return '<5';
			case '-:10' : return '<10';
			case '10:15' : return '10-15';
			case '15:20' : return '15-20';
			case '20:-' : return '>20';
			default : return '';
		}
	},

	userCanEditReport: function() {
		if (this.get('teamReport')) {
			return this.get('teamReport.user') === this.get('session.user.id');
		} else {
			// it's a new report
			return true;
		}

	}.property('teamReport'),

	fieldLabel: function(field) {
		switch (field) {
			case '-0.1':
				return 'Minus Goal Line';
			case '0.1' :
				return 'Plus Goal Line';
			default :
				return parseInt(field) > 0 ? '+' + field : field;
		}
	}
});
