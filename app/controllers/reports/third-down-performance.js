// controllers/reports/third-down-performance.js

import Ember from 'ember';

export default Ember.Controller.extend({
    queryParams: ['player','platoon','group','game','season'],
    player: null,   // queryParam.player
    platoon: null,  // queryParam.platoon
    group: null,    // queryParam.group
    game: null,     // queryParam.game
    season: null,   // queryParam.season

    message:null,           // text message to user
    reportData:[],          // this holds data for report table
    chartBaseLine: null,    // we will set this with base value from team to show baseLine on chart
    chartData: null,        // this holds data for chart
    chartOptions: null,      // this holds chart definition for this specific report

    // actual player grades
    cat1Grade: null,        // category 1 grade
    cat2Grade: null,        // category 2 grade
    cat3Grade: null,        // category 3 grade
    overallGrade: null,     // overall grade

    // cat labels are used as first sections of labels with category grades
    cat1Label: null,
    cat2Label: null,
    cat3Label: null
});
