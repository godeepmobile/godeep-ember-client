import Ember from 'ember';

export default Ember.Controller.extend({
	editTeamReport: 'edit',
	isPublic: false,
	error: false,

	clearForm: function() {

		this.set('error', false);
		this.set('isPublic', false);
		this.set('name', null);
		this.set('editTeamReport', 'edit');

		Ember.run.scheduleOnce('afterRender', this, function() {
			var teamReport = this.get('model');
			this.set('name', teamReport.get('name'));
			this.set('isPublic', teamReport.get('isPublic'));
		});

	}.on('init'),

	editTeamReportFlagChanged: function() {
		var teamReport = this.get('model');

		if (this.get('editTeamReport') === 'edit') {
			this.set('name', teamReport.get('name'));
			this.set('isPublic', teamReport.get('isPublic'));
		} else {
			this.set('name', null);
			this.set('isPublic', false);
		}

	}.observes('editTeamReport'),

	actions: {

		goToEditReport: function() {
			var name     = this.get('name')
			  , isPublic = this.get('isPublic');

			if (name) {
				if (this.get('editTeamReport')  === 'edit') {
					this.send('saveReport', name, this.get('isPublic'), this.get('model'));
				} else {
					this.send('saveReport', name, this.get('isPublic'));
				}
				this.send('removeModal');
				this.set('error', false);
			} else {
				this.set('error', true);
			}
			
		}
	}
});
