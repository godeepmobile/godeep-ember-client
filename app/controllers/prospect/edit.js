// controllers/prospects/edit.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    loadData: Ember.inject.service(),
    attrs: {},

    graduationYears: function() {
        var years       = Ember.A([])
          , currentYear = parseInt(moment().format("YYYY"));

        //this.set('model.graduationYear', currentYear);
        for ( var i = (currentYear - 10) ; i <= currentYear + 10 ; i++ ) {
            var obj = {};
            Ember.set(obj, 'id', i);
            Ember.set(obj, 'name', i);
            years.push(obj);
        }

        return years;
    }.property('model'),

    actions: {

        save: function () {
            var self = this
              , teamProspectAssignment = this.get('teamProspectAssignment');
            this.validate().then(function() {
                // all validations pass
                // Saving the new prospect.
                var model = self.get('model');
                model.save().then(function(modelSaved) {

                    teamProspectAssignment.save().then(function() {
                        self.send('removeModal');
                        self.send('temporalTooltip', 'tooltip/saved');
                    }).catch(function(reason) {
                        self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                    });

                }).catch(function(reason) {
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });
            }).catch(function() {
              // any validations fail
              self.showRequiredMessage();
            }).finally(function() {
              // all validations complete
            });
        },

        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();
            this.get('teamProspectAssignment').rollback();
            this.send('removeModal');
            this.send('removeTooltip');
        }
    },

    bodyTypes: function() {
        return this.get('loadData.bodyTypes');
    }.property('model'),

    /**
     * Returns position types, without:
     * 29;"Athlete";"ATH"
     **/
    projectedPositions: Ember.computed.filter('positionTypes', function(positionType, index, array) {
      return parseInt(positionType.get('id')) !== 29;
    }).property('positionTypes', 'model'),

    positionTypes: function() {
        return this.get('loadData.positionTypes');
    }.property('model'),

    states: function() {
        return this.get('loadData.states');
    }.property('model'),

    /*
     * Excluding NFL level from prospects
     * 1;"National Football League";"NFL"
     */
    levels: function() {
        return this.store.find('level', {"filter[where][id][neq]": 1});
    }.property('model'),

    showRequiredMessage: function() {
        this.send('showTooltip', 'tooltip/error', {message: 'You must fill out required fields.'});
    },

    // Prospect form validations
    validations: {
        'model.firstName': {
            presence: true
        },

        'model.lastName': {
            presence: true
        },

        'teamProspectAssignment.projectedPosition': EmberValidations.validator(function() {
            if (!this.model.get('teamProspectAssignment.projectedPosition.id')) {
                return "can't be blank";
            }
        }),

        'teamProspectAssignment.currentPosition': EmberValidations.validator(function() {
            if (!this.model.get('teamProspectAssignment.currentPosition.id')) {
                return "can't be blank";
            }
        }),

        'model.level': EmberValidations.validator(function() {
            if (!this.model.get('model.level.id')) {
                return "can't be blank";
            }
        }),

        'model.highSchool': {
            presence: true
        },

        'model.height': {
            presence: true
        },

        'model.weight': {
            presence: true
        }
    }
});
