// app/controllers/prospect/index.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    isAllEvaluationsExpanded: true,
    actions: {
        toggleExpandAllEvaluations: function () {
            this.toggleProperty('isAllEvaluationsExpanded');
            var flag = this.get('isAllEvaluationsExpanded');

            var evaluationStats = this.get('evaluationStats');

            evaluationStats.forEach(function(stat) {
                stat.seasons_data.forEach(function(season) {
                     Ember.set(season, 'isExpanded', flag);
                });
            });

        },
        toggleExpandSeasonEvaluation: function(season) {
            Ember.set(season, 'isExpanded', !Ember.get(season,'isExpanded'));
            //this.toggleIsAllEvaluationsExpandedForLabel();
        }
    }
});
