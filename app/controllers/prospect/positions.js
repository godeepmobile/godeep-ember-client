// controllers/prospect/positions.js

import Ember from 'ember';

export default Ember.Controller.extend( {
    loadData: Ember.inject.service(),
    postionSelected: null,

    actions: {
        startEvaluating: function ( position ) {
            this.send('removeModal');
            this.createEvaluation();
        },

        selectPosition: function(position) {
            this.set('postionSelected', position);
            Ember.$(".position").removeClass("true");
            Ember.$('#position-id-' + position.get('id')).addClass("true");
        }
    },

    /**
     * Returns position types, without:
     * 25;"Gunner";"GUN"
     * 27;"Holder";"H-K"
     * 29;"Athlete";"ATH"
     * They wont be evaluated
     **/
    postinionTypesNotToBeEvaluated: [25, 27, 29],
    positionTypes: Ember.computed.filter('loadDataPositionTypes', function(positionType, index, array) {
      return this.postinionTypesNotToBeEvaluated.indexOf(parseInt(positionType.get('id'))) === -1;
    }).property('loadDataPositionTypes', 'model'),

    loadDataPositionTypes: function() {
        return this.get('loadData.positionTypes');
    }.property('model'),

    createEvaluation: function() {
        var self = this;
        var adapter = this.store.adapterFor('application');
        var url = this.createEvaluationUrl(adapter);

        var positionId = null;
        if (self.get('postionSelected')) {
            positionId = self.get('postionSelected').get('id');
        } else {
            positionId = self.get('model').get('projectedPosition.id');
        }

        function ajaxSuccess(response) {
            // 1: default criteria type
            var url = "/evaluation/teamProspect/" + self.get('model').get('teamProspect.id') +"/position/"+positionId+"/criteriaType/1?scoreGroup="+response.teamScoutingEvalScoreGroup.id;
            //this.transitionToRoute('evaluation.position.criteriaType', 'teamProspect', this.get('model'), this.get('postionSelected'), 1, {queryParams: {scoreGroup:null}});
            self.transitionToRoute(url);
        }

        function ajaxFailure(error) {
            console.log('reportAggregateGrades error %o', error);
        }

        var user = self.get('session.user.id');
        var teamScoutingEvalScoreGroup = {
            user         : user,
            season       : self.get('loadData.season'),
            team         : self.get('session.team.id'),
            positionType : positionId,
            teamPlayer   : self.get('model').get('teamProspect.id')
        };
        adapter.ajax(url, 'POST', {data : teamScoutingEvalScoreGroup}).then(ajaxSuccess, ajaxFailure);
    },

    createEvaluationUrl: function(adapter) {
        return adapter.buildURL('TeamScoutingEvalScoreGroup', 'createEvaluation');
    }
});
