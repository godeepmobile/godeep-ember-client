import Ember from 'ember';

export default Ember.Controller.extend({

	setupController: function(offlineData, teamCutup) {
		var now = moment().toDate()
		  , currentPlay;

		this.set('teamPlays', offlineData.teamPlays);
		this.set('whichPlatoon', offlineData.platoonTypes.findBy('id', teamCutup.get('whichPlatoon.id')));
		this.set('playFactors', offlineData.playFactors);

		this.set('gradeStatus', offlineData.gradeStatus.findBy('teamCutup.id', teamCutup.get('id')) || this.store.createRecord('teamEventGradeStatus', {
            teamEvent      : teamCutup.get('teamEvent')
          , teamCutup      : teamCutup
          , user           : this.get('session.user.id')
          , team           : this.get('session.team')
          , startedDate    : now
          , lastSubmitDate : now
          , lastGradedPlay : 1
        }));

        currentPlay = offlineData.teamPlays.find(function(teamPlay) {
        	return teamPlay.get('teamCutup.id') === teamCutup.get('id') && teamPlay.get('playNumber') === (this.get('gradeStatus.lastGradedPlay') || 1);
        }.bind(this));

		this.set('currentPlay', currentPlay);
		this.set('playerAssignments', offlineData.playerAssignments);

		this.set('teamCutup', teamCutup);
	},

  thisPage: function() {
    var teamCutup = this.get('teamCutup');
    return teamCutup.get('teamEvent.' + (teamCutup.get('teamEvent.isMatch') ? 'gameName' : 'eventLabel')) + ' - ' + teamCutup.get('whichPlatoon.name');
  }.property('teamCutup'),

	hasPrevPlay: function() {
    	var plays = this.get('teamPlays');
      	return plays && plays.isAny('playNumber', this.get('currentPlay.prevPlayNumber'));
    }.property('currentPlay', 'model.teamPlays'),

    hasNextPlay: function() {
    	var plays = this.get('teamPlays');
      	return plays && plays.isAny('playNumber', this.get('currentPlay.nextPlayNumber'));
    }.property('currentPlay', 'teamPlays'),

    showOneCategory: function() {
       return this.get('session.teamConfiguration.numGradeCategories') === 1;
    }.property('session.teamConfiguration.numGradeCategories'),
    
    showTwoCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 2;
    }.property('session.teamConfiguration.numGradeCategories'),

    showThreeCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 3;
    }.property('session.teamConfiguration.numGradeCategories'),

    playerAssignmentsChanged: function() {
        var gsisParticipation      = this.get('currentPlay.gsisParticipation')
          , assignedPlayers        = this.get('playerAssignments')
          , positionTypes          = this.get('session.positionTypes')
          , previousGradedPlayers  = this.get('previousPlay.gradePlayers')
          , teamCutupPlatoon       = this.get('whichPlatoon')
          , alreadyAddedGrades     = this.get('currentPlay.grades')
          , teamPlayerParticipated = function(teamPlayer) {
                return gsisParticipation.indexOf(teamPlayer.get('jerseyNumber')) !== -1;
            }
          , teamPlayerIsGraded     = function(playerAssignment) {
                return alreadyAddedGrades.isAny('teamPlayer.id', playerAssignment.get('teamPlayer.id'));
            }
          , positionTypesOnPlatoon;

        if (assignedPlayers) {

            if (!teamCutupPlatoon.get('isAllPlatoon') && assignedPlayers) {
                // Filter the players with positions in the cutup's platoon
                positionTypesOnPlatoon = this.get('session.teamPositionGroups').filterBy('platoonType', parseInt(teamCutupPlatoon.get('id'))).getEach('positionTypeIds');
                assignedPlayers        = assignedPlayers.filter(function(playerAssignment) {
               
                    return positionTypes.isAny('id', playerAssignment.get('position1.id') + '') ||
                		positionTypes.isAny('id', playerAssignment.get('position2.id') + '') ||
                		positionTypes.isAny('id', playerAssignment.get('position3.id') + '') ||
                		positionTypes.isAny('id', playerAssignment.get('positionST.id') + '');// ||
                        //teamPlayerIsGraded(playerAssignment);

                });
            }

            if (this.get('session.team.isProTeam') && gsisParticipation) {

                assignedPlayers = assignedPlayers.filter(teamPlayerParticipated).concat(assignedPlayers.reject(teamPlayerParticipated));

            } else if (previousGradedPlayers) {

                assignedPlayers = previousGradedPlayers.concat(assignedPlayers).uniq();
            }

            this.set('assignedPlayers', alreadyAddedGrades && alreadyAddedGrades.get('length') > 0 ? assignedPlayers.filter(teamPlayerIsGraded).concat(assignedPlayers.reject(teamPlayerIsGraded)) : assignedPlayers);
            this.assignedPlayersChanged();
        }
    }.observes('playerAssignments', 'currentPlay'),

    assignedPlayersChanged: function() {
    	var play            = this.get('currentPlay')
        , assignedPlayers = this.get('assignedPlayers')
        , teamPlayer;

    	play.get('grades').forEach(function(grade) {
	        teamPlayer = grade.get('teamPlayer');
	        if (!assignedPlayers.isAny('teamPlayer.id', teamPlayer.get('id'))) {
	        	this.send('selectPlayerToGrade', this.get('playerAssignments').findBy('teamPlayer.id', teamPlayer.get('id')));
	        }
    	}.bind(this));

     	this.send('addPlayersToCurrentGrade');

    }//.observes('assignedPlayers')

});
