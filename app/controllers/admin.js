import Ember from 'ember';

export default Ember.Controller.extend({
    thisPage: "admin",
    breadcrumbs: [],
    linkTo: 'dashboard'
});
