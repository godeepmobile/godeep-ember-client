import Ember from 'ember';

export default Ember.Controller.extend({
    thisPage: "organizations",
    breadcrumbs: [],
    linkTo: 'dashboard'
});
