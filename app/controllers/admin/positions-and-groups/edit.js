// controllers/admin/positions-and-groups/edit.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    attrs: {},

    actions: {
        save: function () {
            var self = this;
            //var self = this;
            this.validate().then(function() {
                // all validations pass
                var model = self.get('model');
                var isNew = model.get('id') === null;
                model.save().then(function(modelSaved) {
                    self.updateTeamPositionTypes(modelSaved, self, isNew);
                    self.send('removeModal');
                    self.send('temporalTooltip', 'tooltip/saved');
                }).catch(function(reason){
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });
            }).catch(function() {
              // any validations fail
              console.log('validation fails');
            });
            self.send('removeTooltip');
        },

        cancel: function () {
            //Closing the modal
            this.get('model').rollback();
            this.send('removeModal');
            this.send('removeTooltip');
        },

        validatePositionTypeSelection: function (checkbox) {
            var self           = this
              , adapter        = this.store.adapterFor('application')
              , positionTypeId = checkbox.value.id;
            if (self.isTeamPositionType(positionTypeId)) {
                if (!checkbox.checked) {
                    adapter.ajax(self.playersAssignedToPositionUrl(adapter, positionTypeId), 'GET').then(function(response) {
                        if (response.teamPlayer && response.teamPlayer.length) {
                            self.selectPositionType(positionTypeId);
                            self.send('showTooltip', 'tooltip/error-remove-position', {
                                players         : response.teamPlayer,
                                positionTypeName: checkbox.value.get('shortName')
                            });
                        } else {
                            self.send('showTooltip', 'tooltip/remove-position', {
                                positionTypes   : self.get('positionTypes'),
                                positionTypeId  : positionTypeId,
                                positionTypeName: checkbox.value.get('shortName')
                            });
                        }
                    });
                }
            }
        },
    },

    /**
    * Persists position types changes in the database.
    *
    * @param {TeamPositionGroup} model the current object saved
    * @param {Boolean} isNew flag to indicate if model is a new object
    * @returns
    */
    updateTeamPositionTypes: function(model, self, isNew) {
        var positionsToBeSaved = self.teamPositionTypesToBeSaved(model);
        var adapter = self.store.adapterFor('application');
        adapter.ajax(self.teamPositionTypesUpdateUrl(adapter, model), 'PUT',
                {data : positionsToBeSaved}
            ).then(function(response) {
                console.log('teamPositionTypes updated successfully');
                model.set('teamPositionTypes', positionsToBeSaved);
                if (isNew) {
                    self.get('positionGroups').pushObject(model);
                }
            }, self.teamPositionTypesUpdateFailure);
    },

    /**
    * Retrieves position types to be included in a position group.
    *
    * @param {TeamPositionGroup} model the current object saved
    * @returns {TeamPositionType}
    */
    teamPositionTypesToBeSaved: function(model) {
        var store           = this.store;
        var positions       = this.get('positionTypes').toArray();
        var selected        = positions.filterBy('isSelected', true);
        var positionsToSave = [];

        selected.forEach(function(position) {
            var teamPositionType = store.createRecord('teamPositionType', {
                positionType     : position,
                platoonType      : position.get('platoonType'),
                teamPositionGroup: model.get('id')
            });
            positionsToSave.push(teamPositionType);
        });
        return positionsToSave;
    },

    /**
    * Retrieves team position types to be pre selected in the form.
    *
    * @returns {TeamPositionType}
    */
    positionTypesSelected: function() {
        var self = this;
        var positionTypes     = this.get('positionTypes');
        var teamPositionTypes = this.get('model').get('teamPositionTypes').toArray();
        positionTypes.forEach(function(position) {
            position.set('isSelected', self.contains(teamPositionTypes, position));
        });
        return positionTypes;

    }.property('positionTypes').volatile(),

    selectPositionType: function(positionTypeId) {
        var positionTypes = this.get('positionTypes');
        positionTypes.forEach(function(position) {
            if (parseInt(position.get('id')) === parseInt(positionTypeId)){
                position.set('isSelected', true);
            }
        });
    },

    isTeamPositionType: function(positionTypeId) {
        var teamPositionTypes = this.get('model').get('teamPositionTypes').toArray();
        var result = false;
        teamPositionTypes.forEach(function(teamPosition) {
            if (parseInt(teamPosition.get('positionType').get('id')) === parseInt(positionTypeId)){
                result = true;
            }
        });
        return result;
    },

    contains: function(a, obj) {
         var i = a.length;
        while (i--) {
           if (parseInt(a[i].get('positionType').get('id')) === parseInt(obj.id)) {
               return true;
           }
        }
        return false;
    },

    teamPositionTypesUpdateUrl: function(adapter, model) {
        return adapter.buildURL('TeamPositionGroup', model.get('id')) + '/teamPositionTypes';
    },

    playersAssignedToPositionUrl: function(adapter, positionTypeId) {
        return adapter.buildURL('TeamPlayer', 'playersAssignedToPosition') + '/' + positionTypeId;
    },

    teamPositionTypesUpdateFailure: function(response) {
        // in case of failure
    },

    validations: {
        'model.name': {
            presence: true
        },

        /*'model.teamPositionTypes': EmberValidations.validator(function() {
            console.log('this.model.get(model.positionTypes)');
            console.log(this.model.get('positionTypes'));
            if (!this.model.get('model.positionTypes')) {
                return "can't be blank";
            }
        })*/
    }
});
