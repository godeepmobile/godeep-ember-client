// controllers/admin/positions-and-groups/delete.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    actions: {
        delete: function () {
            var self = this;
            var model = this.get('model');
            var adapter = this.store.adapterFor('application');
            adapter.ajax(this.teamPositionTypesDeleteUrl(adapter, model), 'DELETE')
                .then(function(response) {
                    console.log('teamPositionTypes deleted successfully');
                    self.hidePositionsAndGroup(model);
                }, this.teamPositionTypesDeleteFailure);
            this.send('removeModal');
        },

        cancel: function () {
            //Closing the modal
            this.send('removeModal');
        }
    },

    teamPositionTypesDeleteUrl: function(adapter, model) {
        return adapter.buildURL('TeamPositionGroup', model.get('id')) + '/logicalDelete';
    },

    teamPositionTypesDeleteFailure: function(response) {
        // in case of failure
    },

    hidePositionsAndGroup: function(model) {
        Ember.$('#pos-group-' + model.get('id')).fadeOut( 500 );
    }
});
