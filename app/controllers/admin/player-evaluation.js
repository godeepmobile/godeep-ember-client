// app/controllers/admin/player-evaluation.js

import Ember from 'ember';

export default Ember.Controller.extend({

    needs: ['admin/player-evaluation/position'],

    currentCriteriaType: null,

    actions: {
        saveNext: function () {
            var nextCriteria = this.nextCriteriaType();
            var positionController = this.get('controllers.admin/player-evaluation/position');
            positionController.send('save');
            if (nextCriteria) {
                this.set('currentCriteriaType', nextCriteria);
                this.transitionToRoute('admin.playerEvaluation.position', this.get('position'), {queryParams: {criteriaType: nextCriteria.get('id')}});
            }
        },

        savePrev: function () {
            var previousCriteriaType = this.previousCriteriaType();
            var positionController = this.get('controllers.admin/player-evaluation/position');
            positionController.send('save');
            if (previousCriteriaType) {
                this.set('currentCriteriaType', previousCriteriaType);
                this.transitionToRoute('admin.playerEvaluation.position', this.get('position'), {queryParams: {criteriaType: previousCriteriaType.get('id')}});
            }
        },

        new: function () {
            var positionController = this.get('controllers.admin/player-evaluation/position');
            positionController.send('new');
        },

        cancel: function () {
            var positionController = this.get('controllers.admin/player-evaluation/position');
            positionController.send('cancel');
        },

        targetScoreSetup: function () {
            console.log('targetScoreSetup...........');
        },

        showTargetScoreInfo: function () {
            this.send('showModal', 'admin/player-evaluation/target-score-info');
        }
    },

    /**
     * Returns position types, without:
     * 25;"Gunner";"GUN"
     * 27;"Holder";"H-K"
     * 29;"Athlete";"ATH"
     * They wont be evaluated
     **/
    positionTypes: function () {
      var arr = [25, 27, 29];
      var positions = this.get('model').filter(function(positionType) {
          var positionTypeId = parseInt(positionType.get('id'));
          return arr.indexOf(positionTypeId) === -1;
      });
      return positions;
    }.property('currentCriteriaType'),

    criteryType: function () {
        if (!this.currentCriteriaType) {
            var self = this;
            return this.get('scoutingEvalCriteriaTypes').then(function(types) {
                self.set('currentCriteriaType', types.get('firstObject'));
                return types.get('firstObject');
            });
        } else {
            return this.currentCriteriaType;
        }
    }.property('currentCriteriaType'),

    nextCriteriaType: function() {
        if (this.currentCriteriaType && this.get('scoutingEvalCriteriaTypes')) {
            var len = this.get('scoutingEvalCriteriaTypes').get('length');
            var currentId = parseInt(this.currentCriteriaType.get('id'));
            if (currentId < len) {
                var nextId = currentId + 1;
                return this.get('scoutingEvalCriteriaTypes').filterBy('id', nextId.toString())[0];
            }
        }
        return null;
    },

    previousCriteriaType: function() {
        if (this.currentCriteriaType && this.get('scoutingEvalCriteriaTypes')) {
            var currentId = parseInt(this.currentCriteriaType.get('id'));
            if (currentId > 1) {
                var nextId = currentId - 1;
                return this.get('scoutingEvalCriteriaTypes').filterBy('id', nextId.toString())[0];
            }
        }
        return null;
    },
});
