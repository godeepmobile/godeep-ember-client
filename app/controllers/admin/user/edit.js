// controllers/admin/user/edit.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    attrs: {},
    selectAllOff:false,
    selectAllDef:false,
    selectAllSp:false,
    _teamPositionGroups: [],
    _groupAssignments: [],

    disableSubmitBtn: function() {
        return !this.get("isValid") ||
            !(this.get("model.currentState.isDirty") ||
                this.get('_teamPositionGroups').filterBy('selected').length !== this.get('_groupAssignments.length') ||
                this.get('_teamPositionGroups').filterBy('selected').reject(function(_teamPositionGroup) {
                    return this.get('_groupAssignments').isAny('teamPositionGroup', parseInt(_teamPositionGroup.get('id')));
                }.bind(this)).length > 0
            );
    }.property('isValid', 'model.currentState', 'model.currentState.isDirty', '_teamPositionGroups.@each.selected', '_groupAssignments'),

    actions: {
        save: function () {
            var self = this;
            // Saving the new/updated user.
            var model = this.get('model');
            this.validate().then(function() {
                // all validations pass
                model.save().then(function(modelSaved) {
                    self.updateUserRole(modelSaved);
                    self.updateTeamPositionGroups(modelSaved);
                    self.send('removeModal');
                    self.send('temporalTooltip', 'tooltip/saved');
                }).catch(function(){
                    self.send('showTooltip', 'tooltip/error', {message: 'An problem occured trying to save the user record.'});
                });
            }).catch(function() {
              // any validations fail
              console.log('validation fails');
            });
        },

        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();
            this.preselectPositionGroups();
            this.send('removeModal');
        }
    },

    updateUserRole: function(model) {
        var adapter = this.store.adapterFor('application');
        adapter.ajax(this.roleUrl(adapter, model), 'PUT')
            .then(function(response) {
                console.log('role updated successfully');
            }, this.roleUpdateFailure);
    },

    updateTeamPositionGroups: function(model) {
        var positions = this.get('_teamPositionGroups').toArray();
        positions  = positions.filterBy('selected', true)
            .map(function(position){
                return {id: position.id};
        });
        var adapter = this.store.adapterFor('application');
        adapter.ajax(this.positionGroupUrl(adapter, model), 'PUT',
                {data : positions}
            ).then(function(response) {
                console.log('grading group updated successfully');
            }, this.positionGroupUpdateFailure);
    },

    groupAssignments: function() {
        var model = this.get('model');
        return this.store.find('teamUserPositionGroupAssignment', {
            filter : {
                where : {
                    and : [
                        {user   : model.get('id')},
                        {endDate: 'null'}
                    ]
                }
            }
        });
    }.property('model').volatile(),

    watchTeamPositionGroups: function() {
        var self = this;
        this.get('groupAssignments').then(function(groupAssignments){
            self.get('defenses').then(function(defenses){
                self.get('offensives').then(function(offensives) {
                    var teamPositionGroups = defenses.toArray().concat(offensives.toArray());
                    self.get('specialists').then(function(specialists){
                        teamPositionGroups = teamPositionGroups.concat(specialists.toArray());
                        self.set('_teamPositionGroups', teamPositionGroups);
                        self.set('_groupAssignments', groupAssignments);
                        self.preselectPositionGroups();
                    });
                });
            });
        });
    }.observes('model'),

    preselectPositionGroups: function() {
        var self = this;
        this.set('selectAllOff', false);
        this.set('selectAllDef', false);
        this.set('selectAllSp', false);
        var teamPositionGroups = this.get('_teamPositionGroups').toArray()
        , groupAssignments     = this.get('_groupAssignments').toArray();
        teamPositionGroups.forEach(function(teamPositionGroup){
            teamPositionGroup.set('selected', self.contains(groupAssignments, teamPositionGroup));
        });
    },

    contains: function(a, obj) {
        var i = a.length;
        while (i--) {
           if (parseInt(a[i].get('teamPositionGroup')) === parseInt(obj.id)) {
               return true;
           }
        }
        return false;
    },

    roleUrl: function(adapter, model) {
        var userId = model.get('id')
        , roleId   = model.get('role').get('id');
        return adapter.buildURL('TeamUser', userId) + '/role/' + roleId;
    },

    positionGroupUrl: function(adapter, model) {
        return adapter.buildURL('TeamUser', model.get('id')) + '/positionGroupAssignments';
    },

    roleUpdateFailure: function(response) {
        // in case of failure
    },

    positionGroupUpdateFailure: function(response) {
        // in case of failure
    },

    rollbackGroupAssignments: function(positions) {
        positions.forEach(function(position) {
            position.set('selected', false);
        });
    },

    showSaveMessage: function() {
        Ember.$('.message').fadeIn( 400 ).delay( 1000 ).fadeOut( 400 );
    },

    watchselectAllOff: function() {
        var positions = this.get('offensives');
        this.setCheckBoxes(positions, this.selectAllOff);
    }.observes('selectAllOff'),

    watchselectAllDef: function() {
        var positions = this.get('defenses');
        this.setCheckBoxes(positions, this.selectAllDef);
    }.observes('selectAllDef'),

    watchselectAllSp: function() {
        var positions = this.get('specialists');
        this.setCheckBoxes(positions, this.selectAllSp);
    }.observes('selectAllSp'),

    setCheckBoxes: function(positions, value) {
        positions.forEach(function(position) {
            position.set('selected', value);
        });
    },

    // User form validations
    validations: {
        'model.firstName': {
            presence: true
        },

        'model.lastName': {
            presence: true
        },

        'model.email': {
            presence: { message: ' ' },
            format: {
                with      : /^[\w+\-.]+@[a-z\d\-.]+\.[a-z]+$/i,
                message   : 'Invalid e-mail address',
            }
        },

        'model.jobTitle': {
            presence: true
        },

        'model.role': EmberValidations.validator(function() {
            if (!this.model.get('model.role.id')) {
                return "can't be blank";
            }
        })
    }
});
