// controllers/admin/user/delete.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    actions: {
        delete: function () {
            var self = this;
            var model = this.get('model');
            var adapter = this.store.adapterFor('application');
            self.hideUser(model);
            adapter.ajax(this.userDeleteUrl(adapter, model), 'DELETE')
                .then(function(response) {
                    console.log('user deleted successfully');
                    self.hideUser(model);
                }, this.userDeleteFailure);
            this.send('removeModal');
        },

        cancel: function () {
            //Closing the modal
            this.send('removeModal');
        }
    },

    userDeleteUrl: function(adapter, model) {
        return adapter.buildURL('TeamUser', model.get('id')) + '/logicalDelete';
    },

    userDeleteFailure: function(response) {
        // in case of failure
        console.log('delete user fails: ', response);
    },

    hideUser: function(model) {
        Ember.$('#user-id-' + model.get('id')).fadeOut( 500 );
    }
});
