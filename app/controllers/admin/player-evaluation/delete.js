// controllers/admin/player-evaluation/criteria/delete.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    actions: {
        delete: function () {
            this.get('model').destroyRecord();
            this.send('removeModal');
        }
    }
});
