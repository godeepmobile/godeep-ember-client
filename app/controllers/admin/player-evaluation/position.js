// controllers/admin/player-evaluation/position.js

import Ember from 'ember';

export default Ember.Controller.extend({
    queryParams: ['criteriaType'],
    criteriaType: 1,
    actions: {
        save: function() {
            this.get('teamScoutingEvalCriterias').forEach(function(teamScoutingEvalCriteria) {
                teamScoutingEvalCriteria.save();
            });
        },

        new: function() {
            var obj = this.store.createRecord('teamScoutingEvalCriteria', {
                team: this.get('session.team.id'),
                positionType: this.get('positionType'),
                scoutingEvalCriteriaType: this.get('criteriaType')
            });
            this.get('teamScoutingEvalCriterias').addObject(obj);
        },

        cancel: function() {
          this.get('teamScoutingEvalCriterias').forEach(function(teamScoutingEvalCriteria) {
              teamScoutingEvalCriteria.rollback();
          });
        },
    }
});
