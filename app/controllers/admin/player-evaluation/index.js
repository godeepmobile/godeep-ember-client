// controllers/admin/player-evaluation/index.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    attrs: {},
    
    actions: {
        save: function () {
            // Saving the new/updated teamConfiguration.
            var self = this;

            this.validate().then(function() {
                // all validations pass
                self.get('model').save().then(function(modelSaved) {
                    self.send('temporalTooltip', 'tooltip/saved');
                }).catch(function(reason) {
                    //handling server error
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });
            }).catch(function() {
              // any validations fail
              console.log('validation fails');
              self.send('showTooltip', 'tooltip/error', {message: 'Invalid data please fill out properly'});
            });
        },
        
        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();
            //this.send('removeTooltip');
        }
    },

    // Evaluation form validations
    validations: {
        'model.scoutEvalScoreMin': {
            presence: true
        },

        'model.scoutEvalScoreMax': {
            presence: true
        }
    }
});
