// controllers/admin/team.js

import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['admin/team/edit'],
    attrs: {},

    actions: {
        edit: function(model) {
            model.set('currentState.isDirty', false);
            var editController = this.get('controllers.admin/team/edit');
            this.send('showModal', 'admin/team/edit', model);
            this.send('focusFirstInput');
        }
    }
});
