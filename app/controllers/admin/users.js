// controllers/admin/users.js

import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['admin/user/edit'],
    attrs: {},

    actions: {
    	edit: function(user) {
        user.set('currentState.isDirty', false);
        this.setupEditUserController();
    		this.send('showModal', 'admin/user/edit', user);
        this.send('focusFirstInput');
    	},
        add: function() {
            this.setupEditUserController();
        	var teamUser = this.store.createRecord('teamUser');
            this.send('showModal', 'admin/user/edit', teamUser);
            this.send('focusFirstInput');
        },

        delete: function(user) {
            this.send('showModal', 'admin/user/delete', user);
        }
    },

    setupEditUserController: function() {
        var editController = this.get('controllers.admin/user/edit');
        editController.set('roles', this.get('roles'));
        editController.set('offensives', this.get('offensives'));
        editController.set('defenses', this.get('defenses'));
        editController.set('specialists', this.get('specialists'));
    },

    roles: function() {
        // id=1 is the team player that will not be created from this page.
        return this.store.find('userRoleType', {"filter[where][id][neq]": 1, "order":["name"]});
    }.property('users'),

    offensives: function() {
        return this.store.find('teamPositionGroup', { filter: { where: { platoonType: 1 } } });
    }.property('model'),

    defenses: function() {
        return this.store.find('teamPositionGroup', { filter: { where: { platoonType: 2 } } });

    }.property('model'),

    specialists: function() {
        return this.store.find('teamPositionGroup', { filter: { where: { platoonType: 3 } } });
    }.property('model')

});
