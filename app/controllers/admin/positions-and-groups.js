// app/controllers/admin/positions-and-groups.js

import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['admin/positions-and-groups/edit'],
    attrs: {},
    platoonTypeOffense:1,
    platoonTypeDefense:2,
    platoonTypeSTeams :3,
    isOffensivesExpanded  : true,
    isDefensesExpanded    : false,
    isSpecialTeamsExpanded: false,
    actions: {
        toggleExpandOffensives: function () {
            this.set('isDefensesExpanded', false);
            this.set('isSpecialTeamsExpanded', false);
            this.toggleProperty('isOffensivesExpanded');
        },

        toggleExpandDefenses: function () {
            this.set('isOffensivesExpanded', false);
            this.set('isSpecialTeamsExpanded', false);
            this.toggleProperty('isDefensesExpanded');
        },

        toggleExpandSpecialTeams: function () {
            this.set('isOffensivesExpanded', false);
            this.set('isDefensesExpanded', false);
            this.toggleProperty('isSpecialTeamsExpanded');
        },

        delete: function(positionGroup) {
            this.send('showModal', 'admin/positions-and-groups/delete', positionGroup);
        },

        edit: function(positionGroup) {
            var editController = this.get('controllers.admin/positions-and-groups/edit');
            var positionsTypes = this.positionsTypesByPlatoonType(positionGroup.get('platoonType'));
            editController.set('positionTypes', positionsTypes);
            this.send('showModal', 'admin/positions-and-groups/edit', positionGroup);
            this.send('focusFirstInput');
        },

        new: function(platoonType) {
            var positionGroup = this.store.createRecord('teamPositionGroup', {
                platoonType: platoonType,
                team       : this.get('session').get('team')
            });
            positionGroup.set('teamPositionTypes', Ember.A([]));
            var editController = this.get('controllers.admin/positions-and-groups/edit');
            var positionsTypes = this.positionsTypesByPlatoonType(platoonType);
            editController.set('positionTypes', positionsTypes);
            editController.set('positionGroups', this.get('model'));
            this.send('showModal', 'admin/positions-and-groups/edit', positionGroup);
            this.send('focusFirstInput');
        }
    },

    offensives: function () {
        return this.get('model').filter(function(item, index, enumerable) {
            return item.get('platoonType') === 1;
        });
    }.property('model.[]', 'platoonType'),
    defenses: function () {
        return this.get('model').filter(function(item, index, enumerable) {
            return item.get('platoonType') === 2;
        });
    }.property('model.[]', 'platoonType'),

    specialTeams: function () {
        return this.get('model').filter(function(item, index, enumerable) {
            return item.get('platoonType') === 3;
        });
    }.property('model.[]', 'platoonType'),

    positionsTypesByPlatoonType: function(platoonType) {
        var positionsTypes  = null;
        switch(platoonType) {
            case this.platoonTypeOffense: //"Offense"
                positionsTypes = this.get('positionTypeOffensives');
                break;
            case this.platoonTypeDefense: //"Defense"
                positionsTypes = this.get('positionTypeDefenses');
                break;
            case this.platoonTypeSTeams: //"Special Teams"
                positionsTypes = this.get('positionTypeSpecialTeams');
                break;
            default: //0 "Unknown/Unassigned"
                console.log("Unknown/Unassigned");
        }
        return positionsTypes;
    }
});
