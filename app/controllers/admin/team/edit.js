// controllers/admin/configurations/team/edit.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    loadData: Ember.inject.service(),
    attrs: {},

    disableSubmitBtn: function() {
        return !this.get("isValid") || !this.get("model.currentState.isDirty");
    }.property("isValid", "model.currentState", "model.currentState.isDirty"),

    actions: {
        save: function () {
            var self = this;
            this.validate().then(function() {
                // all UI validations pass
                self.get('model').save().then(function(modelSaved) {
                    self.send('removeModal');
                    self.send('temporalTooltip', 'tooltip/saved');
                }).catch(function(reason) {
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });
            }).catch(function() {
              // any validations fail
              console.log('validation fails');
            });
        },

        cancel: function () {
            //Closing the modal
            this.get('model').rollback();
            this.send('removeModal');
        }
    },

    states: function() {
        return this.get('loadData.states');
    }.property('model'),

    conferences: function() {
        return this.get('loadData.conferences');
    }.property('model'),

    surfaceTypes: function() {
        return this.get('loadData.surfaceTypes');
    }.property('model'),

    showSaveMessage: function() {
        Ember.$('.message').delay( 400 ).fadeIn( 400 ).delay( 1000 ).fadeOut( 400 );
    },

    // Team form validations
    validations: {
        'model.shortName': {
            presence: true
        },

        'model.abbreviation': {
            presence: true
        },

        'model.mascot': {
            presence: true
        },

        'model.conference': EmberValidations.validator(function() {
            if (!this.model.get('model.conference.id')) {
                return "can't be blank";
            }
        })
    }
});
