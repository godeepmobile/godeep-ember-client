// controllers/admin/grading.js

import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
    attrs: {},
    numOfAllowedGradeCategories: [1,2,3],

    twoCategories: false,
    threeCategories: false,

    disableSubmitBtn: function() {
        return !this.get("isValid") || !this.get("model.currentState.isDirty");
    }.property("isValid", "model.currentState", "model.currentState.isDirty"),

    changeCategoryLabelForm: function() {  
        var numGradeCategories = this.get('model.numGradeCategories');

        this.set('twoCategories', numGradeCategories === 2);
        this.set('threeCategories', numGradeCategories === 3);

    }.observes('model.numGradeCategories'),

    actions: {
        save: function () {
            // Saving the new/updated teamConfiguration.
            var self = this;

            this.validate().then(function() {
                // all validations pass
                self.get('model').save().then(function(modelSaved) {
                    self.send('temporalTooltip', 'tooltip/saved');
                }).catch(function(reason) {
                    //handling server error
                    self.send('showTooltip', 'tooltip/error', {message: reason.responseJSON.error.message});
                });
            }).catch(function() {
              // any validations fail
              console.log('validation fails');
            });
        },

        cancel: function () {
            // Restoring original values before editing.
            this.get('model').rollback();
            //this.send('removeTooltip');
        }
    },

    // Grading form validations
    validations: {
        'model.gradeBase': {
            presence: true
        },

        'model.gradeIncrement': {
            presence: true
        },

        'model.gradeCat1Label': {
            presence: true
        },

        'model.gradeCat2Label': EmberValidations.validator(function() {
            if (this.get('twoCategories') || this.get('threeCategories')) {
                return !this.model.get('model.gradeCat2Label') ? "can't be blank" : null;
            }
        }),

        'model.gradeCat3Label': EmberValidations.validator(function() {
            if (this.get('threeCategories')) {
                return !this.model.get('model.gradeCat3Label') ? "can't be blank" : null;
            }
        }),

        'model.numGradeCategories': {
            presence: true
        }
    }
});
