import Ember from 'ember';

export default Ember.Controller.extend({

	setupController: function(offlineData) {
		this.set('teamEvents', offlineData.teamEvents);
	}

});
