// controllers/rank/player-rank.js

import Ember from 'ember';

export default Ember.Controller.extend( {
    isUpdatingPlayerSeasonStats: false,
    rankBasedValue: null,

    season: null,
    seasons: [],

    loadData: Ember.inject.service(),

    formattedSeasons: function() {
        var seasons = Ember.A([]);

        this.get('seasons').forEach(function(season) {
            seasons.push({
                value : season,
                label : season + ' Season'
            });
        });

        return seasons;
    }.property('seasons'),

    seasonChanged: function() {
        var season = this.get('season.value');

        if (this.get('loadData.season') !== season) {
          this.send('changeCurrentSeason', this.get('season.value'));
          this.send('refreshPlayerRanking');
        }
    }.observes('season'),

    //rankOverallSortProperties: ['rankOverall:asc'],
    //overallGradeGameRankSortProperties: ['overallGradeGameRank:asc'],
    // overallEvaluationRankSortProperties: ['overallEvaluationRank:asc'],

    //sortedRankOverall: Ember.computed.sort('model', 'rankOverallSortProperties'),
    sortedRankOverall: Ember.computed.sort('model', function(a, b) {
        var aa = parseInt(a.get('rankOverall'))
          , bb = parseInt(b.get('rankOverall'));
        if (aa && bb) {
            if (aa > bb) {
                return 1;
            } else if (aa < bb) {
                return -1;
            } else {
                return 0;
            }
        } else {
            if (aa) {
                return -1;
            } else {
                if (bb) {
                    return 1;
                } else {
                    var cc = parseFloat(a.get('gameGradeAvg'))
                      , dd = parseFloat(b.get('gameGradeAvg'));
                    if (cc < dd) {
                        return 1;
                    } else if (cc > dd) {
                        return -1;
                    } else {
                        var ee = parseInt(a.get('overallEvaluationRank'))
                          , ff = parseInt(b.get('overallEvaluationRank'));
                        if (ee && ff) {
                            if (ee > ff) {
                                return 1;
                            } else if (ee < ff) {
                                return -1;
                            } else {
                                return 0;
                            }
                        } else {
                            if (ee) {
                                return -1;
                            } else {
                                if (ff) {
                                    return 1;
                                } else {
                                    return 0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }).property('model.@each.rankOverall'),

    //sortedRankGrades: Ember.computed.sort('model', 'overallGradeGameRankSortProperties'),
    sortedRankGrades: Ember.computed.sort('model', function(a, b) {
        var aa = parseInt(a.get('overallGradeGameRank'))
          , bb = parseInt(b.get('overallGradeGameRank'));
        if (aa && bb) {
            if (aa > bb) {
                return 1;
            } else if (aa < bb) {
                return -1;
            } else {
                return 0;
            }
        } else {
            if (aa) {
                return -1;
            } else {
                if (bb) {
                    return 1;
                } else {
                  var ee = parseInt(a.get('overallEvaluationRank'))
                    , ff = parseInt(b.get('overallEvaluationRank'));
                  if (ee && ff) {
                      if (ee > ff) {
                          return 1;
                      } else if (ee < ff) {
                          return -1;
                      } else {
                          return 0;
                      }
                  } else {
                      if (ee) {
                          return -1;
                      } else {
                          if (ff) {
                              return 1;
                          } else {
                              return 0;
                          }
                      }
                  }
                }
            }
        }
    }).property('model.@each.overallGradeGameRank'),
    // sortedRankEvaluations: Ember.computed.sort('model', 'overallEvaluationRankSortProperties'),
    sortedRankEvaluations: Ember.computed.sort('model', function(a, b) {
        var aa = parseInt(a.get('overallEvaluationRank'))
          , bb = parseInt(b.get('overallEvaluationRank'));
        if (aa && bb) {
            if (aa > bb) {
                return 1;
            } else if (aa < bb) {
                return -1;
            } else {
                return 0;
            }
        } else {
            if (aa) {
                return -1;
            } else {
                if (bb) {
                    return 1;
                } else {
                  var ee = parseInt(a.get('overallGradeGameRank'))
                    , ff = parseInt(b.get('overallGradeGameRank'));
                  if (ee && ff) {
                      if (ee > ff) {
                          return 1;
                      } else if (ee < ff) {
                          return -1;
                      } else {
                          return 0;
                      }
                  } else {
                      if (ee) {
                          return -1;
                      } else {
                          if (ff) {
                              return 1;
                          } else {
                              return 0;
                          }
                      }
                  }
                }
            }
        }
    }).property('model.@each.overallEvaluationRank'),

    allRank : function() {
        if (!this.rankBasedValue || this.rankBasedValue === 'rankOverall') {
            return this.get('sortedRankOverall');
        } else {
            if (this.rankBasedValue === 'overallGradeGameRank') {
                return this.get('sortedRankGrades');
            } else {
                return this.get('sortedRankEvaluations');
            }
        }
    }.property('model', 'model.[]', 'model.@each.overallEvaluationRank',
               'model.@each.rankOverall', 'model.@each.overallGradeGameRank',
               'sortedrankOverall', 'rankBasedValue')
});
