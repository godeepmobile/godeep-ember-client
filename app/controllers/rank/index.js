// controllers/rank/index.js

import Ember from 'ember';

export default Ember.Controller.extend( {

    isUpdatingProspectSeasonStats: false,

    season: null,
    seasons: [],

    loadData: Ember.inject.service(),

    formattedSeasons: function() {
        var seasons = Ember.A([]);

        this.get('seasons').forEach(function(season) {
            seasons.push({
                value : season,
                label : season + ' Season'
            });
        });

        return seasons;
    }.property('seasons'),

    seasonChanged: function() {
        var season = this.get('season.value');

        if (this.get('loadData.season') !== season) {
            this.send('changeCurrentSeason', this.get('season.value'));
            this.send('refreshProspectRanking');
        }
    }.observes('season'),

    sortedRankOveralls: Ember.computed.sort('model', function(a, b) {
        var aa = parseInt(a.get('rankOverall'))
          , bb = parseInt(b.get('rankOverall'));
        if (aa && bb) {
            if (aa > bb) {
                return 1;
            } else if (aa < bb) {
                return -1;
            } else {
                return 0;
            }
        } else {
            if (aa) {
                return -1;
            } else {
                if (bb) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }).property('model.@each.rankOverall'),

    allRank : function() {
        return this.get('sortedRankOveralls');
    }.property('model', 'model.[]', 'model.@each.rankOverall', 'sortedRankOveralls'),
});
