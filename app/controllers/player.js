//app/controllers/player.js

import Ember from 'ember';

export default Ember.Controller.extend({
    loadData: Ember.inject.service(),
    needs: ['player/edit'],
    pageName:'Player Information',
    message:'This is the player page',
    hasBigLogo: false,
    linkTo: 'players',

    bodyTypes: function() {
        return this.get('loadData.bodyTypes');
    }.property('model'),

    states: function() {
        return this.get('loadData.states');
    }.property('model'),

    positionTypes: function() {
        return this.get('loadData.positionTypesForTeam');
    }.property('model'),

    conferences: function() {
        return this.get('loadData.conferences');
    }.property('model'),

    levels: function() {
        return this.get('loadData.levels');
    }.property('model'),
    graduationYears: function() {
        var years       = Ember.A([]),
            currentYear = parseInt(moment().format("YYYY"));

        for ( var i = (currentYear - 10) ; i <= currentYear + 10 ; i++ ) {
            var obj = {};
            Ember.set(obj, 'id', i);
            Ember.set(obj, 'name', i);
            years.push(obj);
        }

        return years;
    }.property('model'),

    actions: {
        playerEdit: function(playerAssignment) {
            var editController = this.get('controllers.player/edit');
            editController.set('bodyTypes', this.get('bodyTypes'));
            editController.set('states', this.get('states'));
            editController.set('positionTypes', this.get('positionTypes'));
            //editController.set('teamPositionTypes', this.get('teamPositionTypes'));
            editController.set('levels', this.get('levels'));
            editController.set('graduationYears', this.get('graduationYears'));

            playerAssignment.get('teamPlayer').then(function(teamPlayer) {
                editController.set('teamPlayerAssignment', playerAssignment);
                this.send('showModal', 'player/edit', teamPlayer);
                this.send('focusFirstInput');
            }.bind(this));

        },

        evaluatePlayer: function(playerAssignment) {
            this.send('showModal', 'player/positions', playerAssignment);
        }
    }
});
