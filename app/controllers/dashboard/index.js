// controllers/dashboard/index.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    caption:'Top Ten Overall Performers',

    mySortProperties: ['gameOverallGrade:desc'],

    needs: ['dashboard'],

    //sortedStats: Ember.computed.sort('attrs.allStats', 'mySortProperties'),
    sortedStats: function() {

        return Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
            content        : this.get('attrs.allStats'),
            sortProperties : ['gameOverallGrade', 'teamPlayer.lastName'],
            sortFunction   : function(sortAttrA, sortAttrB) {

                if (sortAttrA === sortAttrB) {
                    return 0;
                }

                if (!isNaN(sortAttrA) || !isNaN(sortAttrB)) {
                    sortAttrA = parseFloat(sortAttrA);
                    sortAttrB = parseFloat(sortAttrB);

                    // sorting by game overall
                    return sortAttrA > sortAttrB ? -1 : 1;

                } else {
                    // sorting by player last name
                    return sortAttrA > sortAttrB ? 1 : -1;
                }

            }
        });

    }.property('attrs.allStats', 'attrs.allStats.@each'),

    allStats : function() {
        return this.get('attrs.allStats');
    }.property('attrs.allStats'),

    unslicedContent :function() {
        return this.get('sortedStats');
    }.property('sortedStats', 'sortedStats.[]'),

    sortedContent :function() {
        return this.get('sortedStats');
    }.property('sortedStats', 'sortedStats.[]')
});
