// controllers/dashboard/veteran-watch-list.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    caption:'Veteran Watch List',
    linkToUrl: 'dashboard/veteranWatchList',
    mySortProperties: ['gameOverallGrade:desc'],
    sortAscending: false,

    //sortedStats: Ember.computed.sort('attrs.allStats', 'mySortProperties'),
    // using a custom function instead due the gameOverallGrade is a string
    sortedStats: Ember.computed.sort('attrs.allStats', function(stat1, stat2) {
        var gameOverallGrade1 = parseFloat(stat1.get('gameOverallGrade'))
          , gameOverallGrade2 = parseFloat(stat2.get('gameOverallGrade'));

        if (gameOverallGrade1 > gameOverallGrade2) {
            return -1;
        } else if (gameOverallGrade1 < gameOverallGrade2) {
            return 1;
        }

        return 0;
    }),

    arrangedContent :function() {
        return this.get('sortedStats');
    }.property('sortedStats', 'sortedStats.[]'),

    top5PlayerStats: function() {
        return this.get('arrangedContent').slice(0,5);
    }.property('sortedStats', 'sortedStats.[]'),

    top10PlayerStats: function() {
        return this.get('arrangedContent').slice(0,10);
    }.property('sortedStats', 'sortedStats.@each'),

    allPlayerStats: function() {
        return this.get('arrangedContent').slice(0,10);
    }.property('sortedStats', 'sortedStats.[]')
});
