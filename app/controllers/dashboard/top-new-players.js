// controllers/dashboard/top-new-players.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    caption:'Top New Players',
    linkToUrl: 'dashboard/topNewPlayers',
    mySortProperties: ['gameOverallGrade:desc'],
    sortAscending: false,

    needs: ['dashboard'],

    sortedStats: function() {

        return Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
            content        : this.get('attrs.allStats'),
            sortProperties : ['gameOverallGrade', 'teamPlayer.lastName'],
            sortFunction   : function(sortAttrA, sortAttrB) {

                if (sortAttrA === sortAttrB) {
                    return 0;
                }

                if (!isNaN(sortAttrA) || !isNaN(sortAttrB)) {
                    sortAttrA = parseFloat(sortAttrA);
                    sortAttrB = parseFloat(sortAttrB);

                    // sorting by gameOverallGrade
                    return sortAttrA > sortAttrB ? -1 : 1;

                } else {
                    // sorting by player last name
                    return sortAttrA > sortAttrB ? 1 : -1;
                }

            }
        });

    }.property('attrs.allStats', 'attrs.allStats.@each'),

    arrangedContent :function() {
        return this.get('sortedStats');
    }.property('sortedStats', 'sortedStats.[]'),

    top5PlayerStats: function() {
        return this.get('arrangedContent').slice(0,5);
    }.property('sortedStats', 'sortedStats.[]'),

    allPlayerStats: function() {
        return this.get('arrangedContent');
    }.property('sortedStats', 'sortedStats.[]'),

    top10PlayerStats: function() {
        return this.get('arrangedContent').slice(0,10);
    }.property('sortedStats', 'sortedStats.[]')
});
/*
 "gr" = overall game average in current season
 "gm" = total number of games played in current season

 sorted by "gr" descending
 first year or minimal play time last year

*/
