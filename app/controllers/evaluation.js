//app/controllers/evaluation.js

import Ember from 'ember';

export default Ember.Controller.extend({
    loadData: Ember.inject.service(),
    thisPage: "Player Evaluation",
    breadcrumbs: [],
    infoPanelStretched: true
});
