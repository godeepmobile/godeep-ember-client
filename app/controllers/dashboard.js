// controllers/dashboard.js

import Ember from 'ember';

export default Ember.Controller.extend({
    thisPage: "home",
    breadcrumbs: [],
    linkTo: 'dashboard',

    /*teamSelected: true,
    offenseSelected: false,
    defenseSelected: false,
    groupSelected: false,*/

    isUpdatingSeasonTotals: false,

    season: null,
    seasons: [],

    queryParams: ['view'],
    view: 'team',

    VIEWS: {
        team        : 'team'
      , offense     : 'offense'
      , defense     : 'defense'
      , gradeGroups : 'grade-group'
    },

    loadData: Ember.inject.service(),

    formattedSeasons: function() {
        var seasons = Ember.A([]);

        this.get('seasons').forEach(function(season) {
            seasons.push({
                value : season,
                label : season + ' Season'
            });
        });

        return seasons;
    }.property('seasons'),

    seasonChanged: function() {
        var season = this.get('season.value');

        if (this.get('loadData.season') !== season) {
            this.send('changeCurrentSeason', season);
            this.send('dashboardUpdateSeasonTotals', true);
        }
    }.observes('season'),

    teamSelected: function() {
        return this.get('view') === this.get('VIEWS.team');
    }.property('view'),

    offenseSelected: function() {
        return this.get('view') === this.get('VIEWS.offense');
    }.property('view'),

    defenseSelected: function() {
        return this.get('view') === this.get('VIEWS.defense');
    }.property('view'),

    groupSelected: function() {
        return this.get('view') === this.get('VIEWS.gradeGroups');
    }.property('view'),

    actions: {
        changeSelected: function(newValue) {
            if (this.get('selectedView')!== newValue) {
                this.set('teamSelected', 'team' === newValue);
                this.set('offenseSelected', 'offense' === newValue);
                this.set('defenseSelected', 'defense' === newValue);
                this.set('groupSelected', 'grade-group' === newValue);
            }
        }
    }
});
