import Ember from 'ember';

export default Ember.Controller.extend({
	needs: ['game/index', 'offline-grade'],

	currentPlay: function() {
		return this.get('controllers.game/index').get('currentPlay') || this.get('controllers.offline-grade').get('currentPlay');
	}.property('controllers.game/index.currentPlay', 'controllers.offline-grade'),

	actions: {
		addPlayersToGrade: function() {
			this.send('addPlayersToCurrentGrade');
			this.send('removeModal');
		},

		selectPlayer: function(teamPlayerAssignment) {
			this.send('selectPlayerToGrade', teamPlayerAssignment);
		}
	}
});
