import Ember from 'ember';

export default Ember.Controller.extend({

	forCoachesOnly: false,

	modalTitle: function() {
		var grades = this.get('model');
		return grades && grades.isAny('notes') ? 'Edit Notes' : 'Add Notes';
	}.property('model.@each.notes'),

	grades: function() {
		return this.getGradeNotes();
	}.property('model', 'model.@each'),

	getGradeNotes: function() {
		var _grades = Ember.A([]);

		// Note(Carlos) this is due we don't need the grades binding
		this.get('model').forEach(function(grade) {
			_grades.push({
			    id               : grade.get('id')
			  , teamPlayer       : grade.get('teamPlayer')
			  , playerAssignment : grade.get('playerAssignment')
			  , notes            : grade.get('notes')
			});
		});

		return _grades;
	},

	actions: {
		addNotes: function(playersCanSeeNotes) {
			var grades = this.get('grades');

			this.get('model').forEach(function(grade) {
				grade.set('playersCanSeeNotes', playersCanSeeNotes);
				grade.set('notes', grades.findBy('teamPlayer.id', grade.get('teamPlayer.id')).notes);
			});

			this.send('cancelNotes');
		},

		cancelNotes: function() {
			// restoring grades after cancel, in order to discard dirty changes
			//this.set('grades', this.getGradeNotes());
			this.set('model', []);
		}
	}
});
