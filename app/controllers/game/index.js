import Ember from 'ember';

export default Ember.Controller.extend({

    playDataHiddenAttrs: ['quarter', 'ball_on'],
    queryParams: ['minimizedVersion', 'sid'],
    minimizedVersion: false,
    sid: null,
    playDataPanelStretched: true,
    displayedOnDesktopBrowser: !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/),

    setupController: function() {
        this.addJQueryLogic();
    }.on('init'),

    addJQueryLogic: function() {
        var code, self = this;
        
        Ember.run.schedule("afterRender", this, function() {
          Ember.$('#play-number').keyup(function(evt) {
            code = evt.keyCode ? evt.keyCode : evt.which;

            if (code === 13) {
                // The user hits the enter key
                self.send('goToPlay', Ember.$('#play-number').val());
            }
          });

            if (self.get('minimizedVersion')) {
                // removing this constraint for minimized grading tool
                Ember.$('html').css('minWidth', 'initial');
                Ember.$('body').css('minWidth', 'initial');
            }
        });

    },

    hasPrevPlay: function() {
    	var plays = this.get('model.teamPlays');
      	return plays && plays.isAny('playNumber', this.get('currentPlay.prevPlayNumber'));
    }.property('currentPlay', 'model.teamPlays'),

    hasNextPlay: function() {
    	var plays = this.get('model.teamPlays');
      	return plays && plays.isAny('playNumber', this.get('currentPlay.nextPlayNumber'));
    }.property('currentPlay', 'model.teamPlays'),

    assignedPlayers: function() {
        var gsisParticipation      = this.get('currentPlay.gsisParticipation')
          , assignedPlayers        = this.get('currentPlay.assignedPlayers')
          , previousGradedPlayers  = this.get('previousPlay.gradePlayers')
          , teamCutupPlatoon       = this.get('model.whichPlatoon')
          , alreadyAddedGrades     = this.get('currentPlay.grades')
          , teamPlayerParticipated = function(teamPlayer) {
                return gsisParticipation.indexOf(teamPlayer.get('jerseyNumber')) !== -1;
            }
          , teamPlayerIsGraded     = function(playerAssignment) {
                return alreadyAddedGrades.isAny('teamPlayer.id', playerAssignment.get('teamPlayer.id'));
            }
          , positionTypesOnPlatoon;

        if (assignedPlayers) {

            if (assignedPlayers && !(teamCutupPlatoon.get('isAllPlatoon') || teamCutupPlatoon.get('isSpecialTeams') || teamCutupPlatoon.get('isUnassigned'))) {
                // Filter the players with positions in the cutup's platoon
                positionTypesOnPlatoon = this.get('session.teamPositionGroups').filterBy('platoonType', parseInt(teamCutupPlatoon.get('id'))).getEach('positionTypeIds');
                assignedPlayers        = assignedPlayers.filter(function(playerAssignment) {
                    return positionTypesOnPlatoon.find(function(positionTypesIds) {
                        return positionTypesIds.indexOf(parseInt(playerAssignment.get('position1.id'))) !== -1 || 
                            positionTypesIds.indexOf(parseInt(playerAssignment.get('position2.id'))) !== -1 ||
                            positionTypesIds.indexOf(parseInt(playerAssignment.get('position3.id'))) !== -1 ||
                            positionTypesIds.indexOf(parseInt(playerAssignment.get('positionST.id'))) !== -1 ||
                            teamPlayerIsGraded(playerAssignment);
                    });
                });
            }

            if (this.get('session.team.isProTeam') && gsisParticipation) {

                assignedPlayers = assignedPlayers.filter(teamPlayerParticipated).concat(assignedPlayers.reject(teamPlayerParticipated));

            } else if (previousGradedPlayers) {

                assignedPlayers = previousGradedPlayers.concat(assignedPlayers).uniq();
            }

            return alreadyAddedGrades && alreadyAddedGrades.get('length') > 0 ? assignedPlayers.filter(teamPlayerIsGraded).concat(assignedPlayers.reject(teamPlayerIsGraded)) : assignedPlayers;
        }
    }.property('currentPlay.assignedPlayers'),

    // Observer to setting the header title
    changePageTitle: function() {
    	var model = this.get('model');
    	this.set('thisPage', model.get('teamEvent.' + (model.get('teamEvent.isMatch') ? 'gameName' : 'eventLabel')) + ' - ' + model.get('platoonName'));
    }.observes('model.teamEvent.gameName', 'model.platoonName', 'model.teamEvent.isMatch'),

    showOneCategory: function() {
       return this.get('session.teamConfiguration.numGradeCategories') === 1;
    }.property('session.teamConfiguration.numGradeCategories'),
    
    showTwoCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 2;
    }.property('session.teamConfiguration.numGradeCategories'),

    showThreeCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 3;
    }.property('session.teamConfiguration.numGradeCategories')
});
