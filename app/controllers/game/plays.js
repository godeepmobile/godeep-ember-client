import Ember from 'ember';

export default Ember.Controller.extend({
	thisPage: "Enter Play Data",
	breadcrumbs: [{
        linkTo  : "games",
        caption : "Grade Game"
    }]
});
