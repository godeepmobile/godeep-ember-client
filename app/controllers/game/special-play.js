import Ember from 'ember';

export default Ember.Controller.extend({

	modalTitle: function() {
		return 'Add ' + this.get('session.teamConfiguration.gradeFields.specialPlay.label');
	}.property('session.teamConfiguration.gradeFields.specialPlay'),

	grades: function() {
		return this.getGrades();
	}.property('model', 'model.@each'),

	getGrades: function() {
		var _grades = Ember.A([]);

		// Note(Carlos) this is due we don't need the grades binding
		this.get('model').forEach(function(grade) {

			var customFields = Ember.$.extend(true, {}, grade.get('customFields'));

			if (customFields) {
				if (!customFields.specialPlay) {
					Ember.set(customFields, 'specialPlay', null);
				}
			} else {
				customFields = {specialPlay: null};
			}

			_grades.push({
			    id               : grade.get('id')
			  , teamPlayer       : grade.get('teamPlayer')
			  , playerAssignment : grade.get('playerAssignment')
			  , customFields     : customFields
			});
		});

		return _grades;
	},

	actions: {
		addSpecialPlays: function() {
			var grades = this.get('grades');

			this.get('model').forEach(function(grade) {
				grade.set('customFields', grades.findBy('teamPlayer.id', grade.get('teamPlayer.id')).customFields);
			});

			this.send('cancelSpecialPlays');
		},

		cancelSpecialPlays: function() {
			// restoring grades after cancel, in order to discard dirty changes
			// this.set('grades', this.getGrades());
			this.set('model', []);
		}
	}
});
