import Ember from 'ember';

export default Ember.Controller.extend({

	numberOfQuarters: 4,
	numberOfDowns: 4,
	numberOfDistances: 99,

	modalTitle: '',

	setupValues: function() {
		var play             = this.get('model')
		  , ballOnValues     = []
		  , isHomeGame       = play.get('teamEvent.isHomeGame')
		  , isPractice       = play.get('teamEvent.isPractice')
		  , homeTeamEspnName = isHomeGame ? play.get('teamEvent.teamAbbreviation') : play.get('teamEvent.opponentAbbreviation')
		  , awayTeamEspnName = isHomeGame ? play.get('teamEvent.opponentAbbreviation') : play.get('teamEvent.teamAbbreviation')
		  , previousPlay     = this.get('previousPlay')
		  , teamPracticeDrillType = null
		  , a
		  , b;

		// TODO: This method should be faster
		for (a = 1; a<50; a++) {
			ballOnValues.push(homeTeamEspnName + ' ' + a);
		}

		ballOnValues.push('50');

		for (b = 49; b>0; b--) {
			ballOnValues.push(awayTeamEspnName + ' ' + b);
		}

		/*if (isPractice) {
			if (play.get('teamPracticeDrillType.id')) {
				teamPracticeDrillType = play.get('teamPracticeDrillType');
			} else {
				// The play has no previous stored drill type
				if (previousPlay && previousPlay.get('teamPracticeDrillType.id')) {
					teamPracticeDrillType = previousPlay.get('teamPracticeDrillType');
				}
			}
		}*/

		this.setProperties({
			quarter        : play.get('quarter')
		  , down           : play.get('gameDown')
		  , toGo           : play.get('gameDistance')
		  , gamePossession : play.get('gamePossession')
		  , playType       : play.get('playType')
		  , playResultType : play.get('playResultType')
		  , ballOn         : play.get('ballOn')
		  , ballOnValues   : ballOnValues
		  , possesions     : [homeTeamEspnName, awayTeamEspnName]
		  , quarters       : new Array(this.numberOfQuarters).join().split(',').map(function(item, index){ return ++index;})
		  , downs          : new Array(this.numberOfDowns).join().split(',').map(function(item, index){ return ++index;})
		  , distances      : new Array(this.numberOfDistances).join().split(',').map(function(item, index){ return ++index;})
		  , isPractice     : !play.get('teamEvent.isMatch')
		  , modalTitle     : play.get('teamEvent.isMatch') ? 'Change Situation/Result' : 'Edit Group and Activity Description'
		  , description    : ''
		  , teamPracticeDrillType : null
		  /*, description    : isPractice ? (play.get('description') || (previousPlay ? previousPlay.get('description') : '')) : ''
		  , teamPracticeDrillType : teamPracticeDrillType*/
		});
	},

	actions: {
		changePlayData: function() {
			var model   = this.get('model')
			  , data    = model.get('playData') || {}
			  , success = function() {
					this.send('removeModal');
				}.bind(this)
			  , failure = function (error) {
					console.log('responseJSON ', error.responseJSON);
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.responseText){
						alert(error.responseText);
					}
				}
			  , teamPracticeDrillType
			  , newTeamPracticeDrillTypeName;

			if (!model.get('teamEvent.isMatch')) {
				// Practices
				teamPracticeDrillType = this.get('teamPracticeDrillType');
				model.set('description', this.get('description'));

				if(teamPracticeDrillType) {

					model.set('teamPracticeDrillType', teamPracticeDrillType);
					model.save().then(success, failure);

				} else {

					newTeamPracticeDrillTypeName = this.get('newTeamPracticeDrillTypeName');

					if(newTeamPracticeDrillTypeName) {
						// Create new drill type
						this.store.createRecord('teamPracticeDrillType', {
							name : newTeamPracticeDrillTypeName
						}).save().then(function(teamPracticeDrillType) {
							model.set('teamPracticeDrillType', teamPracticeDrillType);
							model.save().then(success, failure);
						}, failure);
					} else {
						// Just save the play with description only
						model.save().then(success, failure);
					}
				}

			} else {
				// Games
				data.ball_on = this.get('ballOn');

				model.setProperties({
					gameDown       : this.get('down')
				  , gameDistance   : this.get('toGo')
				  , gamePossession : this.get('gamePossession')
				  , playType       : this.get('playType')
				  , playResultType : this.get('playResultType')
				  , quarter        : this.get('quarter')
				  , playData       : data
				});

				model.save().then(success, failure);
			}
		},

		createTeamPracticeDrillTypes: function(newTeamPracticeDrillTypeName) {
			this.set('newTeamPracticeDrillTypeName', newTeamPracticeDrillTypeName);
			this.set('teamPracticeDrillType', null);
		}
	}
});
