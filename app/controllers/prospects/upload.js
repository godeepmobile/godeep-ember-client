import Ember from 'ember';

export default Ember.Controller.extend(Ember.Evented, {
	loadData: Ember.inject.service(),
	thisPage: 'Upload Prospect List',
	breadcrumbs: [{
        linkTo  : 'prospects',
        caption : 'Prospect List'
    }],

    tableOptions: {
    	colHeaders : [
			'<span class="required-col">Jersey No</span>',
			'<span class="required-col">First Name</span>',
			'<span class="required-col">Last Name</span>',
			'<span class="required-col">Height</span>',
			'<span class="required-col">Weight</span>',
			'<span class="required-col">Current Position</span>',
			'<span class="required-col">Projected Position</span>',
			'<span class="required-col">School/Team</span>',
			'<span class="required-col">Current Level</span>',
			'<span class="required-col">State</span>',
			'ST Position',
			'DOB'
		], 
		startCols: 12,
		startRows: 13,
		maxCols: 12,
		minSpareCols: 0,
		stretchH: 'all'
	},

	actions: {
		cancelProspectsUpload: function() {
			this.trigger('clearTableData');
			this.transitionTo('prospects');
		}
	}
});
