import Ember from 'ember';

export default Ember.Controller.extend({

    loadData: Ember.inject.service(),

    season: null,

    seasons: [],

    seasonChanged: function() {
        this.send('changeCurrentSeason', this.get('season.value'));
    }.observes('season'),

    formattedSeasons: function() {
        var seasons = Ember.A([]);

        this.get('seasons').forEach(function(season) {
            seasons.push({
                value : season,
                label : season + ' Season'
            });
        });

        return seasons;
    }.property('seasons'),

    arrangedContent :function() {
        return this.get('model').filterBy('season', this.get('loadData.season'));
    }.property('model', 'model.[]', 'loadData.season'),

	actions: {
        addProspect: function() {
            this.send('showModal', 'prospects/new', this.store.createRecord('teamProspect', {
                team: this.get('session.team')
            }));
            this.send('focusFirstInput');
        },

        uploadProspects: function() {
            this.transitionToRoute('prospects.upload');
        },

        deleteProspect: function(prospect) {
            this.send('showModal', 'prospects/delete', prospect);
        }
    }
});
