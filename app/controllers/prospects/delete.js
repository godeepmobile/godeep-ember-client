// controllers/prospects/delete.js

import Ember from 'ember';

export default Ember.Controller.extend({
    attrs: {},
    actions: {
        deleteProspect: function () {
            var prospect = this.get('model');
            var adapter = this.store.adapterFor('application');
            adapter.ajax(this.prospectAssignmentUrl(adapter, prospect), 'DELETE')
                .then(function(response) {
                    console.log('team prospect deleted successfully');
                    prospect.unloadRecord();
                }, this.prospectDeleteFailure);

            //Closing the modal
            this.send('removeModal');
            this.hideProspect(prospect);
        }
    },

    prospectAssignmentUrl: function(adapter, model) {
        return adapter.buildURL('TeamProspectAssignment', model.get('id')) + '/logicalDelete';
    },

    prospectDeleteFailure: function(response) {
        // in case of failure
    },

    hideProspect: function(prospect) {
        var prospectId = '#prospect-' + prospect.get('id');
        setTimeout( function() {
            Ember.$(prospectId).toggleClass( "highlight" );
            setTimeout( function() {
                Ember.$(prospectId).fadeOut( 500 );
              }, 1000
            );
          }, 100
        );
    }
});
