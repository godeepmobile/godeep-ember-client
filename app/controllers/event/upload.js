import Ember from 'ember';

export default Ember.Controller.extend(Ember.Evented, {
	
	thisPage: 'Upload Play Data',
	breadcrumbs: [{
        linkTo  : 'games',
        caption : 'Grading'
    }],

    nonValidHeader: '--',

    requiredFieldsHeaders: function() {
    	return {
    		playNumber : this.get('playNumberHeader.value'),
    		quarter    : this.get('quarterHeader.value'),
    		down       : this.get('downHeader.value'),
    		distance   : this.get('distanceHeader.value'),
    		yardLine   : this.get('yardLineHeader.value')
    	};
    }.property('playNumberHeader', 'quarterHeader', 'downHeader', 'distanceHeader', 'yardLineHeader'),

    hasErrors: function() {
    	var errors = this.get('errors');
    	if (errors) {
	    	return (errors.teamCutup    ||
	    		errors.playNumberHeader ||
				errors.quarterHeader    ||
				errors.downHeader       ||
				errors.distanceHeader   ||
				errors.yardLineHeader);
    	}
    }.property('errors'),

    playDataFieldsHeaders: function() {
        var teamCutup = this.get('newTeamCutup') || this.get('teamCutup');

        if (this.get('session.teamPlatoonConfiguration') && teamCutup) {
            return this.get('session.teamPlatoonConfiguration').findBy('platoonType.id', teamCutup.get('whichPlatoon.id')).get('playDataFields');
        }
    }.property('session.teamPlatoonConfiguration', 'teamCutup', 'newTeamCutup'),

    // Comp property to check if the user changed the column headers
    changeHeaders: function() {
        var requiredFieldsHeaders = this.get('requiredFieldsHeaders')
          , playDataFieldsHeaders = this.get('playDataFieldsHeaders');

        if (!playDataFieldsHeaders) {
            return true;
        }

        return requiredFieldsHeaders.down !== playDataFieldsHeaders.down ||
            requiredFieldsHeaders.quarter !== playDataFieldsHeaders.quarter ||
            requiredFieldsHeaders.distance !== playDataFieldsHeaders.distance ||
            requiredFieldsHeaders.yardLine !== playDataFieldsHeaders.yardLine ||
            requiredFieldsHeaders.playNumber !== playDataFieldsHeaders.playNumber;
    }.property('requiredFieldsHeaders', 'playDataFieldsHeaders'),

    teamCutupChanged: function() {
        var fieldsHeaders = this.get('playDataFieldsHeaders')
          , tableHeaders  = this.get('tableHeaders');

        if (tableHeaders) {
            if (fieldsHeaders) {
                this.set('playNumberHeader', tableHeaders.findBy('value', fieldsHeaders.playNumber));
                this.set('quarterHeader', tableHeaders.findBy('value', fieldsHeaders.quarter));
                this.set('downHeader', tableHeaders.findBy('value', fieldsHeaders.down));
                this.set('distanceHeader', tableHeaders.findBy('value', fieldsHeaders.distance));
                this.set('yardLineHeader', tableHeaders.findBy('value', fieldsHeaders.yardLine));
            } else {
                this.set('playNumberHeader', null);
                this.set('quarterHeader', null);
                this.set('downHeader', null);
                this.set('distanceHeader', null);
                this.set('yardLineHeader', null); 
            }
        }
    }.observes('teamCutup'),

	actions: {
		buildVideoCutupData: function() {
			this.trigger('buildTableOutputData');
		},

        cancelUpload: function() {
            this.set('teamCutup', null);

            this.set('tableHeaders', null);
            this.set('playNumberHeader', null);
            this.set('quarterHeader', null);
            this.set('downHeader', null);
            this.set('distanceHeader', null);
            this.set('yardLineHeader', null);

            this.set('errors', {
                teamCutup        : false,
                //file             : false,
                playNumberHeader : false,
                quarterHeader    : false,
                downHeader       : false,
                distanceHeader   : false,
                yardLineHeader   : false
            });

            this.trigger('clearTableData');

            if (this.get('returnToGrade')) {
                this.transitionTo('game.index', this.get('newTeamCutup'), {
                    queryParams: {
                        play : this.get('currentPlayNumber') || 1
                    }
                });
            }
        }
	}
});
