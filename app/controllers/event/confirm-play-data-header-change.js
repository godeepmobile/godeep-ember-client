import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		setChangeHeaderFlag: function(changeHeader) {
			this.send('savePlayData', changeHeader);
		}
	}
});
