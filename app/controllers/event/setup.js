import Ember from 'ember';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(EmberValidations.Mixin, {
	breadcrumbs: [{
        linkTo  : 'games',
        caption : 'Grading'
    }],

	thisPage: 'Enter Event for Grading',

	displayedOnDesktopBrowser: !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/),

	searchEventByDateAndType: function() {
		var teamEvent =  this.get('teamEvent')
		  , eventType = teamEvent.get('eventType')
		  , date      = teamEvent.get('dateForPicker')
		  , teamEventsForTheDate = [];

		if (date && eventType) {
			teamEventsForTheDate = this.get('model.teamEvents').filter(function(team_event) {
		  		return !team_event.get('isNew') &&
		  			team_event.get('dateForPicker') === (moment.isMoment(date) ? date.format('MM/DD/YYYY') : date) &&
		  			team_event.get('eventType.id') === eventType.get('id');
		  	});
		}

		this.set('teamEvents', teamEventsForTheDate);

	}.observes('teamEvent.date', 'teamEvent.eventType'),

	teams: function() {
		var userTeamLevel = this.get('session.team.organization.conference.level')
		  , isCollege     = userTeamLevel === 2 ||  userTeamLevel === 3
		  , _teams        = this.get('model.organizations').filter(function(organization) {
				if (isCollege) {
					// If the team is a college include rivals from other conferences
					return organization.get('conference.level') === 2 ||    // FBS
						organization.get('conference.level') === 3 ||	    // FCS
						parseInt(organization.get('conference.id')) === 8;  // Independent
				} else {
					return organization.get('conference.level') === userTeamLevel;
				}
			});

		//return this.get('model.organizations').filterBy('conference.level', userTeamLevel);

		return Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
            content        : _teams,
            sortProperties : ['shortName']
        });
	}.property('model.organizations.@each', 'session.team.organization'),

	showRequiredMessage: function() {
        Ember.$('.message').delay(400).fadeIn(400);
    },

	validations: {
		'teamEvent.date': {presence: true},
		'teamEvent.name': {presence: true},
		//'platoonType': {presence: true},
		//'numPlays': {presence: true, numericality: true},

		/*'awayTeam': EmberValidations.validator(function() {
            if (this.get('teamEvent.eventType.isMatch') && !this.get('awayTeam.id')) {
                return "can't be blank";
            }
        }),

		'homeTeam': EmberValidations.validator(function() {
            if (this.get('teamEvent.eventType.isMatch') && !this.get('homeTeam.id')) {
                return "can't be blank";
            }
        }),*/

		'teamEvent.eventType': EmberValidations.validator(function() {
            if (!this.get('teamEvent.eventType.id')) {
                return "can't be blank";
            }
        })
        /*'platoonType': EmberValidations.validator(function() {
        	if (!this.get('isEdit') && !this.get('platoonType')) {
        		return "can't be blank";
        	}
        })*/
	},

	gameSelected: function() {
		return this.get('teamEvent.eventType.isGame');
	}.property('teamEvent.eventType'),

	practiceSelected: function() {
		return this.get('teamEvent.eventType.isPractice');
	}.property('teamEvent.eventType'),

	practiceGameSelected: function() {
		return this.get('teamEvent.eventType.isPracticeGame');
	}.property('teamEvent.eventType'),

	homeTeamChanged: function() {
		var userOrganization = this.get('session.team.organization')
		  , homeTeam         = this.get('homeTeam');

		if (homeTeam && homeTeam.get('id') !== userOrganization.get('id') && !this.get('changedOnSetup')) {
			this.set('awayTeam', userOrganization);
			this.send('setPracticeGameTeam1', null);
			this.send('setPracticeGameTeam2', null);
		}

	}.observes('homeTeam'),

	awayTeamChanged: function() {
		var userOrganization = this.get('session.team.organization')
		  , awayTeam         = this.get('awayTeam');

		if (awayTeam && awayTeam.get('id') !== userOrganization.get('id') && !this.get('changedOnSetup')) {
			this.set('homeTeam', userOrganization);
			this.send('setPracticeGameTeam1', null);
			this.send('setPracticeGameTeam2', null);
		}

	}.observes('awayTeam'),

	actions: {
		validateAndSaveEvent: function(uploadDataNow) {
			this.validate().then(function() {
				this.send('setupEvent', uploadDataNow);
			}.bind(this)).catch(function(errors) {
				console.log('Invalid Team Event Instance', errors);

				this.set('validationErrors.teamEvent.name', errors.teamEvent && errors.teamEvent.name && errors.teamEvent.name.length > 0);
				this.set('validationErrors.teamEvent.date', errors.teamEvent && errors.teamEvent.date && errors.teamEvent.date.length > 0);
				this.set('validationErrors.teamEvent.eventType', errors.teamEvent && errors.teamEvent.eventType && errors.teamEvent.eventType.length > 0);
				//this.set('validationErrors.platoonType', errors.platoonType && errors.platoonType.length > 0);
				//this.set('validationErrors.numPlays', errors.numPlays.length > 0);
				this.set('validationErrors.awayTeam', errors.awayTeam && errors.awayTeam.length > 0);
				this.set('validationErrors.homeTeam', errors.homeTeam && errors.homeTeam.length > 0);

				this.showRequiredMessage();
			}.bind(this));
		},

		changeEventType: function(newEventType) {
			this.set('teamEvent.eventType', newEventType);
		}
	}
});
