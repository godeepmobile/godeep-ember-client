import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		createTeamEvent: function() {
			this.send('saveTeamEvent', this.get('model'));
		},

		updateTeamEvent: function() {
			var newEvent   = this.get('model')
			  , basedEvent = newEvent.get('basedEvent')
			  , properties = newEvent.toJSON();

			properties['eventType'] = newEvent.get('eventType');
			if (newEvent.get('isMatch')) {
				properties['opponent']  = newEvent.get('opponent');
				properties['state']     = newEvent.get('state');
			}

			// This relation is are not required on the team event
			delete properties.basedEvent;

			// This is a promise so we need to resolve it first
			basedEvent.then(function(teamEvent) {
				teamEvent.setProperties(properties);

				// Destroying no longer valid new event
				newEvent.deleteRecord();

				this.send('saveTeamEvent', teamEvent);
			}.bind(this));

		}
	}
});
