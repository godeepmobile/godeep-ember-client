import Ember from 'ember';

export default Ember.Controller.extend({

	isIE11: navigator.userAgent.toLowerCase().indexOf('msie') === -1 && navigator.userAgent.match(/rv:11.0/i),
    displayedOnDesktopBrowser: !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/),

    // Observer to setting the header title
    changePageTitle: function() {
    	var model = this.get('model');
    	this.set('thisPage', model.get('teamEvent.' + (model.get('teamEvent.isPractice') ? 'eventLabel' : 'gameName')) + ' - ' + model.get('teamCutup.platoonName'));
    }.observes('model.teamEvent.gameName', 'model.teamCutup.platoonName', 'model.teamEvent.isPractice'),

    showOneCategory: function() {
       return this.get('session.teamConfiguration.numGradeCategories') === 1;
    }.property('session.teamConfiguration.numGradeCategories'),

    showThreeCategories: function() {
        return this.get('session.teamConfiguration.numGradeCategories') === 3;
    }.property('session.teamConfiguration.numGradeCategories'),

    sortedGameStats: Ember.computed.sort('gameStats', function(statA, statB) {
        var a = parseFloat(statA.get('avgOverallGrade'))
          , b = parseFloat(statB.get('avgOverallGrade'));

        if (a > b) {
            return -1;
        } else {
            if (a < b) {
                return 1;
            }
        }

        return 0;
    })
});
