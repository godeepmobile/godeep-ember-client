import Ember from 'ember';
import config from '../config/environment';

export default Ember.Component.extend({
	classNames: ['flex', 'grade-control-container'],
	grades: {},
  gradeRange: [],
  gradeIndex: 2,

  setupGradeControl: function() {
    var play           = this.get('play')
      , userId         = this.get('session.user.id')
      , teamPlayer     = this.get('playerAssignment.teamPlayer')
      , positionPlayed = this.get('playerAssignment.position1')
      , gradeBase      = this.get('gradeBase')
      , gradeIncrement = this.get('session.teamConfiguration.gradeIncrement') || Math.abs(config.maxGrade - gradeBase) / 2 || 10
      , preSelect      = this.get('preSelect')
      , currentGrade
      , gradeValue
      , gradeRange = [
          gradeBase - (gradeIncrement * 2),
          gradeBase - (gradeIncrement),
          gradeBase,
          gradeBase + (gradeIncrement),
          gradeBase + (gradeIncrement * 2)
        ];

    gradeValue = gradeRange[this.get('gradeIndex')];

    if (!teamPlayer) {
      console.log('Undefined');
    }

    currentGrade = play.get('grades').find(function(grade) {
      return grade.get('teamPlayer.id') === teamPlayer.get('id') && grade.get('user') === userId && !grade.get('isDisabled');
    });

    if (!currentGrade) {
      // If the play has no previous grade for the player then create one
      currentGrade = this.get('store').createRecord('grade', {
        cat1Grade  : gradeValue,
        cat2Grade  : this.get('showTwoCategories') || this.get('showThreeCategories') ? gradeValue : null,
        cat3Grade  : this.get('showThreeCategories') ? gradeValue : null,
        //teamPlay  : play,
        teamPlayer : teamPlayer,
        team       : this.get("session").get('team'),
        isDisabled : !preSelect,
        hard       : false,
        poa        : false,
        user       : userId
      });

      // Setting the position values
      positionPlayed.then(function(position) {
        currentGrade.setProperties({
          positionPlayed    : position,
          platoonType       : position.get('platoonType')
          //teamPositionGroup : position.get('teamPositionGroup')
        });
      });

      if (preSelect) {
        play.get('grades').addObject(currentGrade);
      }

    } else {
      /*currentGrade.get('teamPlayer').then(function(player) {
        play.addGradePlayer(teamPlayer);
      });*/
      play.addGradePlayer(this.get('playerAssignment'));
      play.get('grades').addObject(currentGrade);
    }

    currentGrade.set('gradesIndex', Ember.Object.create({
      cat1Grade : gradeRange.indexOf(currentGrade.get('cat1Grade')),
      cat2Grade : gradeRange.indexOf(currentGrade.get('cat2Grade')),
      cat3Grade : gradeRange.indexOf(currentGrade.get('cat3Grade'))
    }));

    // adding a reference to teamPlayerAssignment
    currentGrade.set('playerAssignment', this.get('playerAssignment'));

    this.set('grade', currentGrade);
    this.set('gradeRange', gradeRange);

    this.set('neutralFactor', this.get('playFactors').findBy('value', 0));
    this.set('positiveFactor', this.get('playFactors').findBy('value', 1));
    this.set('negativeFactor', this.get('playFactors').findBy('value', -1));
  }.on('init'),

	changeGrade: function(gradeType, gradeIndex) {
      var cat   = 'cat' + gradeType + 'Grade'
        , grade = this.get('grade');

      if (gradeIndex >= 0 && gradeIndex < 5) {
        // set the grade
        grade.set(cat, this.get('gradeRange')[gradeIndex]);
        grade.get('gradesIndex').set(cat, gradeIndex);
      }
    },

  positiveFactorInPlay: function() {
    return this.get('grade.playFactor.value') === 1;
  }.property('grade.playFactor'),

  negativeFactorInPlay: function() {
    return this.get('grade.playFactor.value') === -1;
  }.property('grade.playFactor'),

  // Computed properties to change the grade label font size in case
  cat1GradeOverThreshold: function() {
    var grade = this.get('grade.cat1Grade');
    return grade >= 100 || grade <= -100;
  }.property('grade.cat1Grade'),

  cat2GradeOverThreshold: function() {
    var grade = this.get('grade.cat2Grade');
    return grade >= 100 || grade <= -100;
  }.property('grade.cat2Grade'),

  cat3GradeOverThreshold: function() {
    var grade = this.get('grade.cat3Grade');
    return grade >= 100 || grade <= -100;
  }.property('grade.cat3Grade'),

  showOneCategory: function() {
     return this.get('numGradeCategories') === 1;
  }.property('numGradeCategories'),

  showTwoCategories: function() {
      return this.get('numGradeCategories') === 2;
  }.property('numGradeCategories'),

  showThreeCategories: function() {
      return this.get('numGradeCategories') === 3;
  }.property('numGradeCategories'),

	addMarginBottom: function() {
		return this.get('grade.notes') || this.get('grade.customFields.specialPlay');
	}.property('grade.notes', 'grade.customFields.specialPlay'),

	actions: {
    increaseGrade: function(gradeType) {
      this.changeGrade(gradeType, this.get('grade').get('gradesIndex')['cat' + gradeType + 'Grade'] + 1);
    },

    decreaseGrade: function(gradeType) {
      this.changeGrade(gradeType, this.get('grade').get('gradesIndex')['cat' + gradeType + 'Grade'] - 1);
    },

    toggleGrade: function() {
      var isDisabled
        , grade
        , playerAssignment = this.get('playerAssignment')
        , play             = this.get('play');

      if (playerAssignment) {
        grade = this.get('grade');

        // Enabling the UI
        isDisabled = grade.get('isDisabled');
        grade.set('isDisabled', !isDisabled);

        play[isDisabled ? 'addGradePlayer' : 'removeGradePlayer'](playerAssignment);

        if (grade.get('isNew')) {
          play.get('grades')[isDisabled ? 'addObject' : 'removeObject'](grade);
        }

        if (!isDisabled) {
          this.send('togglePosition', true);
        }
      }
    },

    changeGradePosition: function(teamPositionType) {
      if (teamPositionType) {
        this.get('grade').setProperties({
          positionPlayed    : teamPositionType,
          platoonType       : teamPositionType.get('platoonType')
          //teamPositionGroup : teamPositionType.get('teamPositionGroup')
        });
        this.send('togglePosition');
      }
    },

    togglePosition: function(forceHide) {
      var teamPlayer   = this.get('playerAssignment.teamPlayer')
        , positionList = Ember.$('#grade-player-' + teamPlayer.get('id')).find('.player-positions')
        , positions    = positionList.find('.position');

      if (forceHide || positionList.is(':visible')) {
        positionList.hide();
        positions.css({left:0});
      } else {
        positionList.show();
        positions.each(function(position, element) {
          Ember.$(element).animate({left:(position * (this.get('useMinimizedVersion') ? 50 : 35)) + '%'});
        }.bind(this));
      }
    },

    toggleHardAssignment: function() {
      var grade    = this.get('grade')
        , isActive = grade.get('hard');

      grade.set('hard', !isActive);
    },

    changeFactorInPlay: function(fip) {
      var grade      = this.get('grade')
        , currentFip = grade.get('playFactor');

      if (grade.get('playFactor.value') === fip.get('value')) {
        // second click, then deselecting
        fip = this.get('neutralFactor');
      }

      grade.set('playFactor', fip);
    }
	}
});
