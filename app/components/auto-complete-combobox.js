import Ember from 'ember';

export default Ember.Component.extend({

	// Convenience property to reflect if the input box
	// changed due a user selection from the dropdown
	inputValueChangedBySelection: false,

	showOptions: false,
	hasOptions: false,

	textField: Ember.TextField.extend({
		click: function() {
			this.sendAction('clickAction');
		},

		focusOut: function() {
			//this.sendAction('focusOutAction');
		}
	}),

	setupComponent : function() {
		var classNames = this.get('classNames').splice(1)
		  , content    = this.get('content')
		  , selection  = this.get('selection');

		if (classNames.length > 0) {
			this.set('cssClass', classNames[0]);
		}

		if (selection) {
			this.set('inputValueChangedBySelection', true);
			this.set('inputValue', selection.get(this.get('optionLabelPath')));
		}

		this.set('options', content);
		this.set('hasOptions', content.get('length') > 0);

	}.on('init'),

	filterElements: function() {
		var inputValue, options, hasFilteredOptions, labelPath = this.get('optionLabelPath'), inputValueLowerCase;

		if (!this.get('inputValueChangedBySelection')) {
			inputValue = this.get('inputValue');

			if (inputValue) {
				inputValueLowerCase = inputValue.toLowerCase();
			}

			options    = inputValue && inputValue.length > 0 ? this.get('content').filter(function(element) {
				return element.get(labelPath).toLowerCase() && element.get(labelPath).toLowerCase().indexOf(inputValueLowerCase) !== -1;
			}) : this.get('content');

			this.set('options', options);

			hasFilteredOptions = options.get('length') > 0;

			this.set('showOptions', hasFilteredOptions);

			if (!hasFilteredOptions || !inputValue) {
				// The user is entering a new value, proceed to send the
				// respective action, if specified
				this.set('selection', null);

				if (this.get('onNewValue')) {
					this.sendAction('onNewValue', inputValue);
				}
				
			}
		} else {
			this.set('inputValueChangedBySelection', false);
		}

	}.observes('inputValue'),

	contentChanged: function() {
		var content = this.get('content');
		this.set('hasOptions', content.get('length') > 0);
		this.set('options', content);
	}.observes('content'),

	selectionChanged: function() {
		var selection = this.get('selection');
		if (selection) {
			this.set('inputValueChangedBySelection', true);
			this.set('inputValue', selection.get(this.get('optionLabelPath')));
		}
	}.observes('selection'),

	actions : {
		changeSelection: function(newSelection) {
			/*this.set('inputValueChangedBySelection', true);
			this.set('inputValue', newSelection.get(this.get('optionLabelPath')));*/

			this.set('selection', newSelection);

			if (this.get('onSelection')) {
				this.sendAction('onSelection', newSelection);
			}
			
			// Hidding the dropdown
			this.set('showOptions', false);
		},

		toggleDropdown: function() {
			var showOptions = this.get('showOptions');
			this.set('showOptions', !showOptions);
		},

		hideDropdown: function() {
			this.set('showOptions', false);
		}
	}
});
