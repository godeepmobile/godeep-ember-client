import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'button',
    classNames: ['btn btn-gd-menu btn-xl'],
    attributeBindings: ['type'],
    type: "button",
//    focusOut: function() {
//        //console.log('menu button got focusOut, with param '+this.get('param'));
//        //this.sendAction('action','focusOut');
//    },
    click: function() {
        console.log('menu button got click, with param '+this.get('param'));
        this.sendAction('action',this.get('param'));
    }
});
