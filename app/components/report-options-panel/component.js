// app/components/report-options-panel/component.js

import Ember from 'ember';

export default Ember.Component.extend({
    tagName: '',
    //classNames: ['report-info-panel'],
    loadData: Ember.inject.service(),

    // semaphores to prevent cascading change notifications
    changingPeriod: false,
    changingDimension: false,

    dimension: null, // teamPlayer or teamPositionGroup or platoonType
    dimensionValue: null, // teamPlayer id or teamPositionGroup id or platoonType id

    period: null, // teamEvent or season
    periodValue: null, // teamEvent id or season id
    localPlayer: null,
    localGroup: null,
    localPlatoon: null,
    localGame: null,
    localSeason: null,

    // init this component
    setupComponent: function () {
        //console.log('report-options-panel: setupComponent');
        if (this.get('player')) {
            //console.log('  player is ', this.get('player'));
            this.set('localPlayer', this.get('player'));
            this.set('dimension', 'teamPlayer');
            this.set('dimensionValue', this.get('player'));
        }
        if (this.get('group') === undefined || this.get('group') === "undefined") {this.set('group', null);}
        if (this.get('group')) {
            //console.log('  group is ', this.get('group'));
            this.set('localGroup', this.get('group'));
            this.set('dimension', 'teamPositionGroup');
            this.set('dimensionValue', this.get('group'));
        }
        if (this.get('platoon')) {
            //console.log('  platoon is ', this.get('platoon'));
            this.set('localPlatoon', this.get('platoon'));
            this.set('dimension', 'platoonType');
            this.set('dimensionValue', this.get('platoon'));
        }
        if (this.get('game')) {
            //console.log('  game is ', this.get('game'));
            this.set('localGame', this.get('game'));
            this.set('period', 'teamEvent');
            this.set('periodValue', this.get('game'));
        }
        if (this.get('season')) {
            //console.log('  season is ', this.get('season'));
            this.set('localSeason', this.get('season'));
            this.set('period', 'season');
            this.set('periodValue', this.get('season'));
        }
        // If it was explicty indicated to run the report after the transition then send the proper action
        if (this.get('runReportOnInit')) {
            this.send('reportRunReport');
        }
    }.on('init'),

    // Note(carlos) this is a workaround to cover the problem with firefox and x-select on ember
    // causing the last elements to be selected by default, see https://godeep.atlassian.net/browse/SYS-430 for details.
    //
    // THIS SHOULD ONLY BE EXECUTED IF THE CLIENT BROWSER IS FIREFOX!, in order to avoid possible functionalities breaks
    // on other browsers. 
    selectFirstElements: function() {
        if (navigator.userAgent.toLowerCase().indexOf('firefox') !== -1) {
            console.log('selecting first dropdown options on reports');
            Ember.$('form .x-select option:first-child').attr('selected', 'selected');
        }
    }.on('didInsertElement'),

    isRunReportBtnDisabled: function () {
        //console.log('report-options-panel.isRunReportBtnDisabled: dimensionValue is %o, periodValue is %o', this.get('dimensionValue'), this.get('periodValue'));
        return !(
            this.get('dimensionValue') &&
            this.get('periodValue')
        );
    }.property('dimensionValue', 'periodValue'),

    notifySelectionsChanged: function () {
        //console.log('report-options-panel: notifySelectionsChanged');
        this.sendAction('selectionsChanged', {
            period: this.get('period'),
            periodValue: this.get('periodValue'),
            dimension: this.get('dimension'),
            dimensionValue: this.get('dimensionValue'),
            player: this.get('localPlayer'),
            group: this.get('localGroup'),
            platoon: this.get('localPlatoon'),
            game: this.get('localGame'),
            season: this.get('localSeason')
        });
    },
    resetDimensionTo(newDimension, newDimensionValue) {
        //console.log('report-options-panel: resetDimensionTo(%s,%s)', newDimension, newDimensionValue);
        if (this.get('dimension') !== newDimension || this.get('dimensionValue') !== newDimensionValue) {
            //console.log('  changing dimension to %s and dimensionValue to %s', newDimension, newDimensionValue);
            if (!newDimensionValue) {
                if (this.get('dimension') === newDimension) {
                    this.set('dimension', null);
                    this.set('dimensionValue', null);

                }
            } else {
                this.set('dimension', newDimension);
                this.set('dimensionValue', newDimensionValue);
                switch(newDimension) {
                    case "teamPlayer":
                    if (this.get('localGroup')) {
                        this.set('localGroup', null);
                    }
                    if (this.get('localPlatoon')) {
                        this.set('localPlatoon', null);
                    }
                    break;
                    case "teamPositionGroup":
                    if (this.get('localPlayer')) {
                        this.set('localPlayer', null);
                    }
                    if (this.get('localPlatoon')) {
                        this.set('localPlatoon', null);
                    }
                    break;
                    case "platoonType":
                    if (this.get('localPlayer')) {
                        this.set('localPlayer', null);
                    }
                    if (this.get('localGroup')) {
                        this.set('localGroup', null);
                    }
                    break;
                    default:
                    if (this.get('localPlayer')) {
                        this.set('localPlayer', null);
                    }
                    if (this.get('localGroup')) {
                        this.set('localGroup', null);
                    }
                    if (this.get('localPlatoon')) {
                        this.set('localPlatoon', null);
                    }
                    break;
                }
            }
        }
    },
    resetPeriodTo(newPeriod, newPeriodValue) {
        //console.log('report-options-panel: resetPeriodTo(%s,%s)', newPeriod, newPeriodValue);
        if (this.get('period') !== newPeriod || this.get('periodValue') !== newPeriodValue) {
            //console.log('  changing period to %s and periodValue to %s', newPeriod, newPeriodValue);
            if (!newPeriodValue) {
                if (this.get('period') === newPeriod) {
                    this.set('period', null);
                    this.set('periodValue', null);

                }
            } else {
                this.set('period', newPeriod);
                this.set('periodValue', newPeriodValue);
                switch(newPeriod) {
                    case "teamEvent":
                    if (this.get('localSeason')) {
                        this.set('localSeason', null);
                    }
                    break;
                    case "season":
                    if (this.get('localGame')) {
                        this.set('localGame', null);
                    }
                    break;
                    default:
                    if (this.get('localSeason')) {
                        this.set('localSeason', null);
                    }
                    if (this.get('localGame')) {
                        this.set('localGame', null);
                    }
                    break;
                }
            }
        }
    },
    actions: {
        changePlayer: function (value) {
            //console.log('report-options-panel: changePlayer(%s)', value);
            if (!this.get('changingDimension')) {
                this.set('changingDimension', true);
                if (value === undefined) {
                    value = null;
                    this.set('localPlayer', null);
                }
                this.resetDimensionTo('teamPlayer', value);
                // run after render so that select changes have happened.
                //Ember.run.scheduleOnce('afterRender', this, 'notifySelectionsChanged');
                this.set('changingDimension', false);
            }
        },
        changeGroup(value) {
            //console.log('report-options-panel: changeGroup(%s)', value);
            if (!this.get('changingDimension')) {
                this.set('changingDimension', true);
                if (value === undefined || value === "undefined") {
                    value = null;
                    this.set('localGroup', null);
                }
                this.resetDimensionTo('teamPositionGroup', value);
                // run after render so that select changes have happened.
                //Ember.run.scheduleOnce('afterRender', this, 'notifySelectionsChanged');
                this.set('changingDimension', false);
            }
        },
        changePlatoon(value) {
            //console.log('report-options-panel: changePlatoon(%o)', value);
            if (!this.get('changingDimension')) {
                this.set('changingDimension', true);
                if (value === undefined) {
                    value = null;
                    this.set('localPlatoon', null);
                }
                this.resetDimensionTo('platoonType', value);
                // run after render so that select changes have happened.
                //Ember.run.scheduleOnce('afterRender', this, 'notifySelectionsChanged');
                this.set('changingDimension', false);
            }
        },
        changeGame(value) {
            //console.log('report-options-panel: changeGame(%o)', value);
            if (!this.get('changingPeriod')) {
                this.set('changingPeriod', true);
                if (value === undefined) {
                    value = null;
                    this.set('localGame', null);
                }
                this.resetPeriodTo('teamEvent', value);
                // run after render so that select changes have happened.
                //Ember.run.scheduleOnce('afterRender', this, 'notifySelectionsChanged');
                this.set('changingPeriod', false);
            }
        },

        changeSeason(value) {
            //console.log('report-options-panel: changeSeason(%o)', value);
            if (!this.get('changingPeriod')) {
                this.set('changingPeriod', true);
                if (value === undefined) {
                    value = null;
                    this.set('localSeason', null);
                }
                this.resetPeriodTo('season', value);
                //Ember.run.scheduleOnce('afterRender', this, 'notifySelectionsChanged');
                this.set('changingPeriod', false);
            }
        },

        reportRunReport() {
            //console.log('report-options-panel: reportRunReport');
            if (this.get('localPlayer') === undefined || this.get('localPlayer') === "undefined") { this.set('localPlayer', null);}
            if (this.get('localPlatoon') === undefined || this.get('localPlatoon') === "undefined") { this.set('localPlatoon', null);}
            if (this.get('localGroup') === undefined || this.get('localGroup') === "undefined") { this.set('localGroup', null);}
            this.sendAction('runReportWith', {
                period: this.get('period'),
                periodValue: this.get('periodValue'),
                dimension: this.get('dimension'),
                dimensionValue: this.get('dimensionValue'),
                player: this.get('localPlayer'),
                group: this.get('localGroup'),
                platoon: this.get('localPlatoon'),
                game: this.get('localGame'),
                season: this.get('localSeason')
            });
        },
        reportClearFields() {
            //console.log('report-options-panel: reportClearFields()');
            this.set('changingDimension', true);
            this.set('changingPeriod', true);
            this.set('dimension', null);
            this.set('dimensionValue', null);
            this.set('period', null);
            this.set('periodValue', null);
            this.set('localPlayer', null);
            this.set('localGroup', null);
            this.set('localPlatoon', null);
            this.set('localGame', null);
            this.set('localSeason', null);
            //Ember.run.scheduleOnce('afterRender', this, 'notifySelectionsChanged');
            this.set('changingDimension', false);
            this.set('changingPeriod', false);
        }
    }
});
