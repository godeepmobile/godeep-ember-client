// components/bar-charts/component.js

import Ember from 'ember';
import BarchartThemeMixin from '../../mixins/bar-chart-theme';

export default Ember.Component.extend(BarchartThemeMixin, {
  content:      undefined,
  chartOptions: undefined,
  chart:        null,
  baseLineElement: null,  // this element will hold the baseLine Path

  buildOptions: Ember.computed('chartOptions', 'categories', 'content.@each.isLoaded', function() {
    var chartContent, chartOptions, defaults;
    chartOptions = this.getWithDefault('chartOptions', {});
    if (this.get('categories')) {
        this.set('chartOptions.xAxis.categories', this.get('categories'));
    }
    chartContent = this.get('content.length') ? this.get('content') : [
      {
        id: 'noData',
        data: 0,
        color: '#aaaaaa'
      }
    ];
    defaults = {
      series: chartContent
    };
    return Ember.merge(defaults, chartOptions);
  }),

  _renderChart: (function() {
    this.drawLater();
    this.buildTheme();
  }).on('didInsertElement'),


  categoriesDidChange: Ember.observer('categories', function() {
    var chart;
    if (!(this.get('categories') && this.get('chart'))) {
        console.log('  did not get categories or chart, returning imeediately');
      return;
    }
    chart = this.get('chart');
    return chart.xAxis[0].setCategories(this.get('categories'));
  }),

  contentDidChange: Ember.observer('content.@each.isLoaded', function() {
    var chart;
    if (!(this.get('content') && this.get('chart'))) {
      return;
    }
    chart = this.get('chart');
    this.get('content').forEach(function(series, idx) {
      var _ref;
      if ((_ref = chart.get('noData')) != null) {
        _ref.remove();
      }
      if (chart.series[idx]) {
        return chart.series[idx].setData(series.data);
      } else {
          return chart.addSeries(series);
      }
    });

    this.addBaseLine(chart);
  }),

  drawLater: function() {
    Ember.run.scheduleOnce('afterRender', this, 'draw');
  },

  // when we redraw the chart, make sure to redraw the baseLine path
  onRedraw: function(event) {
    if (this.get('baseLine')) {
      this.drawBaseLine(this.chart, this.get('baseLine'));
    }
  },

  // draw a line from left edge to right edge using the TeamConfiguration base as the y-value
  drawBaseLine: function(chart, baseLine) {
    // yaxis has data when chart has data
    var yaxis = chart.get('yaxis');
    if (yaxis.hasData) {
      // do we already have a baseLine drawn?
      if (this.get('baseLineElement')) {
        // yes, remove the old baseLine
        var bl = this.get('baseLineElement');
        bl.destroy();
        this.set('baseLineElement', null);
      }

      // convert the baseLine value to a chart-relative pixel value
      var yPixels = yaxis.toPixels(baseLine, true);
      var plotY = yPixels + chart.plotTop;  // shift it down. plot area may be shifted down due to top margin

      // calculate left and right edges
      var plotXL = chart.plotLeft;  // starting at left edge of plot area
      var plotXR = chart.plotLeft+chart.plotWidth;  // ending at right edge of plot area

      // use the renderer to draw a path using above points, save the result
      // into baseLineElement so that we can replace it during redraw
      this.set('baseLineElement', chart.renderer
        .path(['M', plotXL, plotY, 'L', plotXR, plotY])
        .attr({'stroke-width': 1,stroke: 'black',zIndex: 2, id:'baseLine'})
        .add());
    }
  },

  draw: function() {
    var options = this.get('buildOptions');
    if (options.chart.barHeight) {
      options.chart.height = options.chart.barHeight * options.series[0].data.length;
    }
    options.chart.events = {redraw: this.onRedraw.bind(this)};
    var chart = this.$().highcharts(options).highcharts();
    this.set('chart', chart);
    if (this.get('baseLine')) {
      this.drawBaseLine(chart, this.get('baseLine'));
      this.addBaseLine(chart);
    }
  },

  addBaseLine: function(chart) {
    var min, max, baseLine, changeExtremes = false, yaxis;

    if (this.get('baseLine')) {
      // If the baseLine (grade base) is outside the range, then we need to add it
      // as an edge
      yaxis    = chart.get('yaxis');
      baseLine = this.get('baseLine');
      min      = yaxis.min;
      max      = yaxis.max;

      if (min && baseLine < min) {
        min = baseLine;
        changeExtremes = true;
      }

      if (max && baseLine > max) {
        max = baseLine;
        changeExtremes = true;
      }

      if (changeExtremes) {
        yaxis.setExtremes(min, max);
      }
    }
  },

  _destroyChart: (function() {
    this._super();
    if (this.get('chart')) {
        this.get('chart').destroy();
    }
  }).on('willDestroyElement')
});
