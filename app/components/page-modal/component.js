// components/page-modal/component.js

import Ember from 'ember';

export default Ember.Component.extend({
    closeText: 'Close',
	okText: 'Ok',
	actions: {
		ok: function() {
			//this.$('.modal').modal('hide');
			this.sendAction('ok');
		}
	},
	show: function() {
		var params = {};

		if (this.get('staticBackdrop')) {
			params['backdrop'] = 'static';
		}

		this.$('.modal').modal(params).on('hidden.bs.modal', function() {
			this.sendAction('close');
		}.bind(this));
	}.on('didInsertElement')
});
