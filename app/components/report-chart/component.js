// app/components/report-chart/component.js
import Ember from 'ember';

export default Ember.Component.extend({
    tagName: '',
    //classNames: ['report-info-panel'],
    //loadData: Ember.inject.service(),
    categories: [],

    // use computed properties to funnel property changes from above to the embedded highcharts object
    localContent: function() {
        return this.get('content');
    }.property('content'),

    localBaseLine: function() {
        return this.get('baseLine');
    }.property('baseLine'),

    localLeftFooter: function() {
        return this.get('leftFooter');
    }.property('leftFooter'),

    localCategories: function() {
        return this.get('categories');
    }.property('categories'),

    localChartOptions: function() {
        return Ember.merge(Ember.merge({}, this.get('defaultChartOptions')), this.get('chartOptions'));
    }.property('chartOptions'),

    defaultChartOptions: {
        chart: {
            type: 'line',
            height: 300,
            plotBackgroundColor: '#e6e7e8',
            //plotBorderColor: '#bbbbbb',
            //plotBorderWidth: 3,
            //width:508,
            margin: [80, 50, 50, 80] // top, right, bottom, left
        },
        credits: {
            enabled: false
        },
        legend: {
            align: 'right',
            enabled: true,
            symbolPadding: 10,
            symbolWidth: 0,
            width: 400
        },
        title: {
            text: ''
        },
        tooltip: {
            followPointer: true,
            shared: true
        },
        xAxis: {
            categories: [],
            gridLineWidth: 20,
            gridLineColor: "#ffffff",
            tickWidth: 0,
            lineWidth: 0,
            minorTickLength: 0,
            opposite: true,
            labels: {
                useHTML:true,
                y:-40,
                autoRotation:0
            }
        },
        yAxis: {
            allowDecimals: true,
            gridLineWidth: 0,
            //gridLineColor: "#bbbbbb",
            id: 'yaxis',
            labels: {
                formatter: function () {
                    return this.value;
                }
            },
            //tickAmount: 5,
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                lineWidth: 3,
                marker: {
                    symbol: "circle",
                    fillColor: null,
                    lineColor: null,
                    //lineWidth: 1,
                    radius: 5
                }
            }
        }
    },

    // init this component
    setupComponent: function () {
        console.log('report-chart-by-quarter: setupComponent');
        if (this.get('content')) {
            console.log('  content is ', this.get('content'));
        }
        //['1st Qtr', '2nd Qtr', '3rd Qtr', '4th Qtr']
        if (this.get('categories')) {
            console.log('  categories is ', this.get('categories'));
            this.get('localChartOptions').xAxis.categories = this.get('localCategories');
        }
        if (this.get('chartOptions')) {
            console.log('  chartOptions is ', this.get('chartOptions'));
        }
        if (this.get('baseLine')) {
            console.log('  baseLine is ', this.get('baseLine'));
        }
    }.on('init')
});
