// components/dashboard-top5-stats/component.js

import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'div',
    classNames: ['dashboard-top5-panel'],

    //sortedStats: Ember.computed.sort('stats', 'sortProperties'),

    sortedStats: function() {

        var sort     = this.get('sort').split(':')
          , sortAttr = sort[0]
          , sortDir  = sort[1];

        return Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
            content        : this.get('stats'),
            sortProperties : [sortAttr, 'teamPlayer.lastName'],
            sortFunction   : function(sortAttrA, sortAttrB) {

                if (sortAttrA === sortAttrB) {
                    return 0;
                }

                if (!isNaN(sortAttrA) || !isNaN(sortAttrB)) {
                    sortAttrA = parseFloat(sortAttrA);
                    sortAttrB = parseFloat(sortAttrB);

                    // sorting by numeric value
                    return sortAttrA > sortAttrB ? -1 : 1;

                } else {
                    // sorting by player last name
                    return sortAttrA > sortAttrB ? 1 : -1;
                }

            }
        });

    }.property('stats', 'stats.@each'),

    //TODO: create a computed property that returns average impact


    sortProperties: function() {
        return [this.get('sort')];
    }.property('sort'),

    top5PlayerStats: function() {
        return this.get('sortedStats').slice(0,10);
    }.property('sortedStats.[]'),

    showImpact: function() {
        return this.get('field') !== 'gameOverallGrade';
    }.property('field')
});
