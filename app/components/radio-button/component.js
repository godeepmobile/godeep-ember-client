// {{ radio-button name='dish' value='spam' groupValue=selectedDish selectAction='testAction' }} Spam
// {{ radio-button name='dish' value='eggs' groupValue=selectedDish }} Eggs
//

import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'div',
    classNames: ['radio'],

    checked: function () {
        if (this.get('value') === this.get('groupValue')) {
            Ember.run.once(this, 'takeAction');
            return true;
        } else { return false; }
    }.property('value','groupValue'),

    takeAction: function() {
        this.sendAction('selectAction', this.get('value'));
    },

    change: function () {
        var value = this.get('value');
        if (value !== null && value !== undefined && value !== "undefined") {
            this.set('groupValue', this.get('value'));
        }
    }
});
