// app/components/summry-chart/component.js
import Ember from 'ember';

export default Ember.Component.extend({
    tagName: '',
    classNames: [],//['summary-report-chart'],
    categories: [],

    // use computed properties to funnel property changes from above to the embedded highcharts object
    localContent: function() {
        return this.get('content');
    }.property('content'),

    localBaseLine: function() {
        return this.get('baseLine');
    }.property('baseLine'),

    localCategories: function() {
        var criticals           = this.get('criticals')
          , categories          = this.get('categories')
          , formattedCategories = [];

        if (criticals) {
            categories.forEach(function(category) {
                formattedCategories.push(criticals.indexOf(category) !== -1 ?
                    '<div style="width:230px; text-align:right;"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> ' + category + '</div>' : 
                    '<div style="width:230px; text-align:right;">' + category + '</div>');
            });

            categories = formattedCategories;
        }

        return categories;
    }.property('categories'),

    chartOptions: {
        chart: {
            type: 'bar',
            margin: [10, 10, 70, 250] // top, right, bottom, left
        },
        credits: {
            enabled: false
        },
        legend: {
            reversed: true
        },
        title: {
            text: ''
        },
        tooltip: {
            followPointer: true,
            shared: true
        },
        xAxis: {
            categories: [],
            labels: {
                useHTML: true
            }
        },
        yAxis: {
            min: 0,
            max: 6,
            allowDecimals: false,
            id: 'yaxis',
            title: {
                text: ''
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: false
                },
                grouping: true,
                  groupPadding:0.1,
                  pointWidth:20,
                  pointPadding: 0,
            }
        }
    },

    // init this component
    setupComponent: function () {

        console.log('report-chart-by-quarter: setupComponent');
        if (this.get('content')) {
            console.log('  content is ', this.get('content'));
        }
        if (this.get('categories')) {
            console.log('  categories is ', this.get('categories'));
        }
        if (this.get('chartOptions')) {
            console.log('  chartOptions is ', this.get('chartOptions'));
        }
        if (this.get('baseLine')) {
            console.log('  baseLine is ', this.get('baseLine'));
        }
        if (this.get('barHeight')) {
            console.log('  barHeight is ', this.get('barHeight'));
            this.chartOptions.chart.barHeight = this.get('barHeight');
        }
    }.on('init')
});
