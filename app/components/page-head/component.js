// app/components/page-head/component.js

import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'nav',
    classNames: ["navbar", "navbar-fixed-top", "sb-slide navbar-header-dark"],
    actions: {
        toggleHamburger: function() {
            Ember.$.slidebars.toggle('left');
            Ember.$('#hamburger_button').blur();
        }
    }
});
