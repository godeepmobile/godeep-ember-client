/* global Handsontable */
import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['flex', 'hands-on-table-container'],

	defaultTableOptions: {
		rowHeaders: true,
		//colHeaders: true,
  		contextMenu: true,
  		startRows: 15,
  		startCols: 23
	},

	table: null,
	tableOptions: {},

	setupTable: function() {

		this.get('targetObject').on('buildTableOutputData', this.buildTableOutputData.bind(this));
		this.get('targetObject').on('clearTableData', this.clearTableData.bind(this));

		Ember.run.scheduleOnce('afterRender', this, function() {
			var $container   = Ember.$('#table-container')
			  , tableOptions = Ember.merge(Ember.merge([], this.get('defaultTableOptions')), this.get('tableOptions'))
			  , tableInstance;

			tableInstance = new Handsontable($container[0], tableOptions);

			tableInstance.addHook('afterChange', function(changes, source) {

				var anyHeaderChanged = changes.any(function(change) {
					// a column header was changed
					return change[0] === 0;
				});

				if (anyHeaderChanged) {
					this.sendAction('onFirstRowChanged', this.get('firstRowHeaders') ? this.firstRowColHeader() : this.colHeader());
				}
			}.bind(this));

			this.set('table', tableInstance);
			this.set('tableOptions', tableOptions);
		});
 
	}.on('didInsertElement'),

	firstRowColHeader: function() {
		var headers      = Ember.A([])
		  , tableHeaders = this.get('table').getDataAtRow(0);
		if (tableHeaders) {
			tableHeaders.forEach(function(header, index) {
				headers.push(header && header.trim() ? 
					{value : header, label : header} : 
					{value : 'field_' + (index + 1), label : '--column ' + (index + 1) + '--'}
				);
			});
		}
		return headers;
	},

	colHeader: function() {
		var headers = Ember.A([])
		  , header;

		this.get('table').getColHeader().forEach(function(colHeader) {
			try {
				header = Ember.$(colHeader).text() || colHeader;
			} catch(err) {
				header = colHeader;
			}

			headers.push(header);
		});

		return headers;
	},

	buildTableOutputData: function() {
		var outputData      = Ember.A([])
		  , table           = this.get('table')
		  , firstRowHeaders = this.get('firstRowHeaders')
		  , headers         = firstRowHeaders ? this.firstRowColHeader() : this.colHeader()
		  , jsonData;

		table.getData().forEach(function(row, index) {
			jsonData = {};

			if (row.filter(function(n){ return n !== null; }).length > 0) { 

				headers.forEach(function(header, index) {
		  			jsonData[firstRowHeaders ? header.value : header] = row[index];
		  		});

				outputData.push(jsonData);
			}
		});

		if (firstRowHeaders) {
			outputData.shift();
		}

		this.sendAction('onDidBuildTableOutputData', outputData);
	},

	clearTableData: function() {
		this.get('table').clear();
		if (this.get('tableOptions')) {
			this.get('table').updateSettings(this.get('tableOptions'));
		}
	}
});
