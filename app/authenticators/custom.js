// app/authenticators/custom.js

import Ember from 'ember';
import Base from 'simple-auth/authenticators/base';
import config from '../config/environment';

export
default Base.extend({
    loginEndpoint: config.API.host + '/' + config.API.login,
    logoutEndpoint: config.API.host + '/' + config.API.logout,
    accessToken: null,

    configureExceptionReportingUser(data) {
        if (data) {
            Rollbar.configure({
                payload: {
                    accessToken: data.id,
                    person: {
                        id: data.user.id,
                        username: data.user.username,
                        email: data.user.email
                    }
                }
            });
        } else {
            Rollbar.configure({payload: {accessToken: null, person: {}}});
        }
    },

    restore: function(data) {
        console.log('authenticator: restore() with ', data);
        var self = this;
        return new Ember.RSVP.Promise(function(resolve, reject) {
            if (!Ember.isEmpty(data.token)) {
                console.log('authenticator: data is good');
                self.configureExceptionReportingUser(data);
                resolve(data);
            } else {
                console.log('authenticator: data is bad');
                self.configureExceptionReportingUser(null);
                reject();
            }
        });
    },

    authenticate: function(credentials) {
        var _this = this;
        //console.log('authenticator: authenticate() with ', credentials);
        return new Ember.RSVP.Promise(function(resolve, reject) {
            credentials.username = credentials.identification;
            Ember.$.post(_this.loginEndpoint, credentials).then(function(response) {
                //console.log('login returned %o', response);
                Ember.run(function() {
                    //resolve({ token: response.session.token });
                    _this.set('accessToken', response.id);
                    Ember.$.ajaxSetup({
                        headers: {
                            Authorization: response.id
                        }
                    });
                    _this.configureExceptionReportingUser(response);
                    resolve({
                        token: response.id,
                        user: response.user
                    });
                });
            }, function(xhr, status, error) {
                //console.log('login post returned xhr %o', xhr);
                //console.log('login post returned status %o', status);
                console.log('login post returned error %o', error);
                _this.configureExceptionReportingUser(null);
                var response = null;
                if (xhr.responseText) {
                    response = JSON.parse(xhr.responseText);
                    if (response && response.error) {
                        response = response.error;
                    }
                }
                if (!response) {
                    if (error) {
                        response = error;
                    } else {
                        response = xhr.statusText;
                    }
                }
                Ember.run(function() {
                    reject(response);
                });
            });
        });
    },

    invalidate: function() {
        var _this = this;
        //console.log('authenticator: invalidate()');

        var url = this.logoutEndpoint + '?access_token=' + this.get('accessToken');

        return new Ember.RSVP.Promise(function(resolve) {
            Ember.$.ajax({
                url: _this.logoutEndpoint,
                type: 'POST'
            }).always(function() {
                resolve();
                _this.configureExceptionReportingUser(null);
            });
        });
    }
});
