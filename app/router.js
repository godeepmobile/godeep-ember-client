import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: '/coach/'
});

/*
You can use resource to take in a param in order to get a specific record.
You can nest things under resource.

You use route to specify some new UI that doesn’t need a specific record.
route is a dead end – you cannot nest things under it. It can not take in params.

'.'  is simply an alternative to using camel case.
user.new could just as well be userNew.
Both of these will look for a series of objects who’s names start with UserNew.
*/

Router.map(function() {
  // splash / entry point
  this.route('index',    { path: '/login' });

  // for user/account management
  this.route('forgotpassword');

  // for dashboards
  this.resource('dashboard', {path:'/'}, function() {
          this.route('topNewPlayers');
          this.route('veteranWatchList');
          this.route('bestUnitImpact');
          this.route('defense');
          this.route('offense');
          this.route('myGroups');
      }
  );

  // for player management
  // this.route("players",   { path: '/players' });
  this.resource('players', { path: '/players' }, function() {
          //this.route('new');
          this.route('position');
          this.route('upload');
      }
  );

  this.resource('player', { path: '/players/:player_id' }, function() {
          this.route('games');
          this.route('evaluations');
          this.route('edit');
      }
  );

  this.resource('prospects', { path: '/prospects' }, function() {
        this.route('upload');
      }
  );

  this.resource('prospect', { path: '/prospects/:prospect_id' }, function() {
  });

  this.resource('evaluation', { path: '/evaluation/:player_type/:player_id' }, function() {
      this.resource('evaluation.position', { path: '/position/:position_id' }, function() {
          this.route('criteriaType', { path: '/criteriaType/:criteria_type_id' });
      });
  });
  this.route('evaluationSummary', { path: '/evaluation/summary/:player_type/:player_id/:evaluation_id/:assignment_id' });

  this.resource('rank', { path: '/rank' }, function() {
          this.route('playerRank');
      }
  );

  // for games and grading
  this.route("games",     { path: '/games' });
  this.resource('game',   { path: '/game/:game_id' }, function() {
    this.route('plays');
  });

  // for reports
  this.route("reports", function() {
      this.route('gamePerformance');
      this.route('thirdDownPerformance');
      this.route('redZonePerformance');
      this.route('view');
      this.route('gameGroupReview');
      this.route('playerComparison');
      this.route('gameDownDistanceAnalysis');
      this.route('gameFieldPositionAnalysis');
      this.route('gamePlayCallPersonnelAnalysis');
  });

  // for administrative
  this.resource('admin', { path: '/admin' }, function() {
          this.route('team');
          this.route('users');
          this.route('reports');
          this.route('positionsAndGroups');
          this.route('grading');
          this.resource('admin.playerEvaluation', { path: '/playerEvaluation'}, function() {
              this.route('position', { path: '/position/:position_id' });
          });
      });

  this.resource("organizations",      { path: '/organizations' });
  //this.resource("organizationTypes",  { path: '/organizationTypes' });
  //this.resource("conferences",        { path: '/conferences' });

  // for testing only
  this.resource('about', function() {
      this.route('location');
      this.route('product');
  });
  this.route('grade', { path: '/grade/:grade_id' });

  this.route('event', function() {
    this.route('setup');
    this.route('upload');
  });

  this.route('prospects');
  this.route('grading');

  this.route('evaluate');

  this.route('criteria', function() {
    this.route('setup');
  });
  this.route('offline-grade');
  this.route('offline-games');
});

export default Router;
