// app/mixins/fetch-all-models.js

// fetchAllModelsNamed()
//   parameters
//     modelName - name of model to load from Adaptor
//
//   returns
//     a dynamic array with modelName models from the store
//

import Ember from 'ember';

export default Ember.Mixin.create({
    fetchAllModelsNamed: function(modelName, query) {
        var store = this.store;
        //console.log("loadMore %s", modelName);

        // start loading models from server. store.find will call the RESTful API
        // when the response is received, then loadMore()
        if (query) {
            store.find(modelName,query).then(loadMore, failure);
        }
        else {
            store.find(modelName).then(loadMore, failure);
        }
        // return our model which is the dynamic array returned from store.filter. as more models
        // are added to the store, they will be added to the model array automagically
        return store.filter(modelName, function (model) {
            return true;
        });
        function failure(error) {
            console.log("loadMore %s error is %o", modelName, error);
        }
        // loadMore() is a recursive function that is called after a successful call to store.find()
        // we will continue calling store.find() with increasing offset to load all records
        function loadMore() {
            // get the meta data returned by the API server
            var meta = store.metadataFor(modelName);
            //console.log("loadMore %s offset is %d", modelName, meta.offset);
            // check to see if we have more records to request by comparing offset to total
            if (meta.offset + meta.limit < meta.total) {
                // yes, we need to request for more
                var options = query || {};
                options.offset = meta.offset+meta.limit;
                //console.log("continuing with options %o", options);
                store.find(modelName,options).then(loadMore,failure);
            }
        }
    },
    fetchAllModelsWithCallback: function(modelName, next, query) {
        var callback = next;
        var store = this.store;
        // start loading models from server. store.find will call the RESTful API
        // when the response is received, then loadMore()
        if (query) {
            store.find(modelName,query).then(loadMore, failure);
        }
        else {
            store.find(modelName).then(loadMore, failure);
        }
        // return our model which is the dynamic array returned from store.filter. as more models
        // are added to the store, they will be added to the model array automagically
        return store.filter(modelName, function (model) {
            return true;
        });
        function failure(error) {
            console.log("loadMore error is %o", error);
            callback(error);
        }
        // loadMore() is a recursive function that is called after a successful call to store.find()
        // we will continue calling store.find() with increasing offset to load all records
        function loadMore() {
            // get the meta data returned by the API server
            var meta = store.metadataFor(modelName);
            //console.log("loadMore %s offset is %d", modelName, meta.offset);
            // check to see if we have more records to request by comparing offset to total
            if (meta.offset + meta.limit < meta.total) {
                // yes, we need to request for more
                var options = query || {};
                options.offset = meta.offset+meta.limit;
                //console.log("continuing with options %o", options);
                store.find(modelName,options).then(loadMore,failure);
            } else {
                callback();
            }
        }
    }
});
