import Ember from 'ember';

export function displayEach(params) {
	var result      = ''
	  , object      = params[0]
	  , hiddenAttrs = params[1] || []
	  , noDataMsg   = params[2] || 'No data to display.'
	  , key
	  , value;

	for (key in object) {
		value = object[key];
		if (value && (value.trim ? value.trim() : true) && hiddenAttrs.indexOf(key) === -1 && key.split('_')[0] !== 'field') {
			result += '<span><b>' + key + ':</b> ' + value + '</span>';
		}
	}
  return (result ? result : '<span class="no-data-msg">' + noDataMsg + '</span>').htmlSafe();
}

export default Ember.HTMLBars.makeBoundHelper(displayEach);
