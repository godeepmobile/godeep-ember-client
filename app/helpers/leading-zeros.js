import Ember from 'ember';

export function leadingZeros(params) {
	var number = parseInt(params[0]);
	if (number < 10) {
		return '00' + number;
	}
	if (number < 100) {
		return '0' + number;
	}
  return number;
}

export default Ember.HTMLBars.makeBoundHelper(leadingZeros);
