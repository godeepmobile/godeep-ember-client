import Ember from 'ember';

export function numPlays(params) {
	var _numPlays = params[0];
  	return _numPlays + ' ' + (_numPlays === 1 ? 'play': 'plays');
}

export default Ember.HTMLBars.makeBoundHelper(numPlays);
