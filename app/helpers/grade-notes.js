import Ember from 'ember';

export function gradeNotes(params) {
	var play           = params[0]
	  , type           = params[1]
	  , condition      = params[2]
	  , conditionRange = params[3]
	  , noteView       = '<tr class="tr">' +
			                '<td class="td"></td>' +
			                '<td class="td">Note:</td>' +
			                '<td class="td text-left" colspan="9">' + play.notes + '</td>' +
			            '</tr>';

	if (type === 'gameGroupReview' && condition === 'includeCoachesOnlyNotes') {
		return play.playersCanSeeNotes || conditionRange === 'true' ? noteView : undefined;
	}

	return noteView;
}

export default Ember.HTMLBars.makeBoundHelper(gradeNotes);
