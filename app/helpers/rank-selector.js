import Ember from 'ember';

export function rankSelector(params) {
    var rank           = params[0]
      , rankBasedValue = params[1]
      , result         = null;
    if (!rankBasedValue) {
        result = rank.get('rankOverall');
    } else {
        result = rank.get(rankBasedValue);
    }
    return (result) ? result : '--';
}

export default Ember.HTMLBars.makeBoundHelper(rankSelector);
