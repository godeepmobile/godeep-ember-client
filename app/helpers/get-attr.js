import Ember from 'ember';

export function getAttr(params) {
	var object = params[0]
	  , key    = params[1];

	return object.get(key);
}

export default Ember.HTMLBars.makeBoundHelper(getAttr);
