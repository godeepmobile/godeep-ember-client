import Ember from 'ember';

export function isPlayerAdded(params) {
	var playsWhereAdded = params[0]
	  , play            = params[1]
	  , cssClass        = params[2];
	  
  return playsWhereAdded && playsWhereAdded.get('play_' + play.get('id')) ? cssClass : '';
}

export default Ember.HTMLBars.makeBoundHelper(isPlayerAdded);
