import Ember from 'ember';

export function isCurrentUser(params) {
	var userId1 = parseInt(params[0])
	  , userId2 = parseInt(params[1]);
  	return userId1 === userId2;
}

export default Ember.HTMLBars.makeBoundHelper(isCurrentUser);
