import Ember from 'ember';

export function initialLetter(params/*, hash*/) {
 	var label = params && params.length > 0 ? params[0] : undefined;

 	if (label) {
 		return label.substring(0, 1).toUpperCase();
 	}
}

export default Ember.HTMLBars.makeBoundHelper(initialLetter);
