import Ember from 'ember';

export function eventNumber(params) {
	var id           = params[0]
	  , eventNumbers = params[1]
	  , _eventNumber;

	if (id && eventNumbers && eventNumbers.length > 0) {
		_eventNumber = eventNumbers.findBy('id', id);
  		return _eventNumber && _eventNumber.number ? _eventNumber.number : undefined;
  	}
}

export default Ember.HTMLBars.makeBoundHelper(eventNumber);
