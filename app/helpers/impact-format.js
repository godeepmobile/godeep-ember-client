import Ember from 'ember';

export function impactFormat(params) {
	var impact = parseFloat(params[0])
	  , res;

	if (impact > 0) {
		res = '+' + params[0];
	} else {
		if (impact < 0) {
			res = params[0];
		} else {
			res = '--';
		}
	}

	return res;
}

export default Ember.HTMLBars.makeBoundHelper(impactFormat);
