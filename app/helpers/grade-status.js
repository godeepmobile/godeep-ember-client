import Ember from 'ember';

export function gradeStatus(params) {
	var userGradeStatuses = params[0]
	  , teamEvent         = params[1]
	  , status            = userGradeStatuses.find(function(gradeStatus) {
	  		return gradeStatus.get('teamEvent').get('id') === teamEvent.get('id');
	  	});

	if (status) {

		if (status.get('isInProcess')) {
			return 'resume grading';
		}
		
		if (status.get('isCompleted')) {
			return 'view grades';
		}
	}
  	return 'grade game';
}

export default Ember.HTMLBars.makeBoundHelper(gradeStatus);
