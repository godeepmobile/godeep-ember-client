import Ember from 'ember';

export function numPlayers(params) {
	var _numPlayers = params[0];
  	return _numPlayers + ' ' + (_numPlayers === 1 ? 'player': 'players');
}

export default Ember.HTMLBars.makeBoundHelper(numPlayers);
