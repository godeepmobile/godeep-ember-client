import Ember from 'ember';

export function scoreSelected(params) {
    var score = params[0]
      , value = params[1];
    return score === value ? 'selected' : '';
}

export default Ember.HTMLBars.makeBoundHelper(scoreSelected);
