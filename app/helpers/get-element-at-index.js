import Ember from 'ember';

export function getElementAtIndex(params) {
	var array     = params[0]
	  , index     = params[1]
	  , colors    = params[2]
	  , threshold = params[3]
	  , label     = '<span style="color: __color__">__value__</span>'
	  , value     = array && array.length && !isNaN(index) ? array[index] : null;

	if (colors && threshold && value) {
		if (value !== threshold) {
			label = label.replace('__color__', value > threshold ? colors.good : colors.bad);
			value = label.replace('__value__', value);
		}
	}

	return value;
}

export default Ember.HTMLBars.makeBoundHelper(getElementAtIndex);
