import Ember from 'ember';

export function trimStr(params) {
    var str   = params[0]
      , limit = params[1];
    if (str && str.length > limit) {
        return str.substring(0, limit) + '...';
    }
    return str;
}

export default Ember.HTMLBars.makeBoundHelper(trimStr);
