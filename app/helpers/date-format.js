import Ember from 'ember';

export function dateFormat(params) {
    var date   = new Date(params[0])
      , format = params[1];
    return moment(date).format(format);
}

export default Ember.HTMLBars.makeBoundHelper(dateFormat);
