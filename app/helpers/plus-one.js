import Ember from 'ember';

export function plusOne(params) {
    var index = params[0];

    return index + 1;
}

export default Ember.HTMLBars.makeBoundHelper(plusOne);
