import Ember from 'ember';

export function rankAtPostSelector(params) {
    var rank           = params[0]
      , rankBasedValue = params[1]
      , result         = null;
    if (!rankBasedValue || rankBasedValue === 'rankOverall') {
        if (rank.get('rankOverall')) {
            result = rank.get('rankAtOverallPost');
        }
    } else {
        if (rankBasedValue === 'overallEvaluationRank') {
            result = rank.get('rankAtEvaluationPost');
        } else {
            result = rank.get('rankAtGradePost');
        }
    }
    return (result) ? result : '--';
}

export default Ember.HTMLBars.makeBoundHelper(rankAtPostSelector);
