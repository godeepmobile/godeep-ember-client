import Ember from 'ember';

export function noValueFormat(params) {
	var value = params[0]
	  , sign  = params[1];

	if (value && value > 0) {
		return value;
	} else {
    if (sign) {
		  return sign;
    } else {
      return '--';
    }
	}
}

export default Ember.HTMLBars.makeBoundHelper(noValueFormat);
