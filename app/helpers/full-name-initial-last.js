import Ember from 'ember';

export function fullNameInitialLast(params/*, hash*/) {
	var player = params && params.length > 0 ? params[0] : undefined;

	if (player) {
		return player.lastName + ', ' + (player.firstName ? player.firstName.substring(0, 1) : '') + '.';
	}

}

export default Ember.HTMLBars.makeBoundHelper(fullNameInitialLast);
