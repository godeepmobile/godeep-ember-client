import Ember from 'ember';

export function isSamePosition(params) {
	var postion1 = params[0]
	  , postion2 = params[1];
  	return postion1.get('id') === postion2.get('id');
}

export default Ember.HTMLBars.makeBoundHelper(isSamePosition);
