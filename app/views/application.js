import Ember from "ember";
import config from '../config/environment';

export default Ember.View.extend({
    classNames: ['sb-site-container'],

    timeSinceLastActivity: null,
    sessionTimer: null,
    sessionTimerDisabled: false,

    didInsertElement: function () {
        this._super();
        // initialize slidebars
        Ember.$.slidebars();

        // now load the slidebars object from the jQuery instance
        var slidebars = Ember.$.slidebars;
        if (slidebars) {
            Ember.run(function() {
                // confirm that slidebars is initalized
                if ( !slidebars.init() ) {
                    console.log( 'slidebars is not initialized' );
                }
            });
        } else {
            console.log('slidebars is not loaded');
        }

        this.restartSessionTimer();
    },

    restartSessionTimer: function() {
        if (config.environment !== 'development') {
            //Note(carlos) the session timeout will not be controlled on dev machines
            this.get('controller').send('startSessionTimer');
        }
    },

    touchStart: function() {
        this.restartSessionTimer();
    },

    keyDown: function() {
        this.restartSessionTimer();
    },

    click: function() {
        this.restartSessionTimer();
    }
});
