// app/initializers/custom-session.js
import Ember from 'ember';
import Session from 'simple-auth/session';

// custom session overrides the default session as defined in ember-simple-auth
// this allows us to return current user information and make that available in
// the session, which is injected into every route and controller.

var SessionWithCurrentUser = Session.extend({
    _team: null,
    _profile: null,
    _teamPositionGroups: null,
    _teamConfiguration: null,
    _teamPlatoonConfiguration: null,
    _teamRoles: null,
    _isAdmin: false,
    _isCoach: false,

    currentUser: function() {
        var user = this.get('user');
        if (user) {
            return user.firstName + ' ' + user.lastName;
        } else {
            console.log('custom-session could not find user for currentUser');
            return 'unknown';
        }
    }.property('user'),

    profile: Ember.computed( '_profile', {
        get: function() {
            return this.get('_profile');
        },
        set: function(key, value) {
            this.set('_profile', value);
            return value;
        }
    }),
/*
    profile: function(key, value, previousValue) {
        // setter
        if (arguments.length > 1) {
            this.set('_profile', value);
        }

        // getter
        return this.get('_profile');
    }.property(),
*/
    team: Ember.computed( 'profile', {
        get: function() {
            return this.get('profile.team');
        },
        set: function(key, value) {
            this.set('_team', value);
            return value;
        }
    }),
/*    team: function(key, value, previousValue) {
        // setter
        if (arguments.length > 1) {
            this.set('_team', value);
        }

        // getter
        return this.get('_team');
    }.property(),
*/
    teamConfiguration: Ember.computed( {
        get: function() {
            return this.get('_teamConfiguration');
        },
        set: function(key, value) {
            this.set('_teamConfiguration', value);
            return value;
        }
    }),
    /*
    teamConfiguration: function(key, value, previousValue) {
        // setter
        if (arguments.length > 1) {
            this.set('_teamConfiguration', value);
        }

        // getter
        return this.get('_teamConfiguration');
    }.property(),
*/
    teamPositionGroups: Ember.computed( {
        get: function() {
            return this.get('_teamPositionGroups');
        },
        set: function(key, value) {
            this.set('_teamPositionGroups', value);
            return value;
        }
    }),
    /*
    teamPositionGroups: function(key, value, previousValue) {
        // setter
        if (arguments.length > 1) {
            this.set('_teamPositionGroups', value);
        }

        // getter
        return this.get('_teamPositionGroups');
    }.property(),
*/
    teamPlayers: function() {
        // getter
        var teamPlayers = this.get('_teamPlayers');
        if (teamPlayers) {
            return teamPlayers;
        } else {
            return [];
        }
    }.property('_teamPlayers'),

    teamRoles: Ember.computed( {
        get: function() {
            return this.get('_teamRoles');
        },
        set: function(key, value) {
            this.set('_teamRoles', value);
            return value;
        }
    }),

    isAdmin: Ember.computed( {
        get: function() {
            return this.get('_isAdmin');
        },
        set: function(key, value) {
            this.set('_isAdmin', value);
            return value;
        }
    }),

    isCoach: Ember.computed( {
        get: function() {
            return this.get('_isCoach');
        },
        set: function(key, value) {
            this.set('_isCoach', value);
            return value;
        }
    }),

    positionTypes: function() {
        // getter
        var positionTypes = this.get('_positionTypes');
        if (positionTypes) {
            return positionTypes;
        } else {
            return [];
        }
    }.property('_positionTypes'),

    teamPlatoonConfiguration: Ember.computed( {
        get: function() {
            return this.get('_teamPlatoonConfiguration');
        },
        set: function(key, value) {
            this.set('_teamPlatoonConfiguration', value);
            return value;
        }
    }),

    teamName: function() {
        // getter
        var team = this.get('_team');
        if (team) {
            return team.get('shortName') + ' ' + team.get('mascot');
        } else {
            return "unknown";
        }
    }.property('_team'),

    teamLogo: function() {
        var team = this.get('_team');
        if (team && team.get('assetBaseUrl')) {
            return team.get('assetBaseUrl')+'/team_logo.png';
        } else {
            return "https://s3.amazonaws.com/godeeppublic/img/missing_team_logo.png";
        }
    }.property('_team'),

    positionGroupAssignments: function() {
        var user = this.get('user')
          , positionGroupAssignments = user ? user.teamUserPositionGroupAssignments : null;
        if (positionGroupAssignments && positionGroupAssignments.length > 0) {
            return positionGroupAssignments;
        } else {
            console.log('custom-session could not find positionGroupAssignments for currentUser');
            return [];
        }
    }.property('user'),
    invalidateSession: function() {
        console.log('invalidating custom-session');
        this.user = null;
        this.set('_team', null);
        this.set('_profile', null);
        this.set('_teamPositionGroups', null);
        this.set('_teamConfiguration', null);
        this.set('_teamRoles', null);
        this.set('_teamPlatoonConfiguration', null);
        this.set('_isAdmin', false);
        this.set('_isCoach', false);
    }
});


export default {
    name: 'custom-session',
    before: 'simple-auth',
    initialize: function(container/*, application*/) {
        container.register('session:custom', SessionWithCurrentUser);
    }
};
