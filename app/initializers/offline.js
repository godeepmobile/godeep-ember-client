export function initialize(container, application) {

	var offline = window.Offline;

	application.register('offline:main', offline, {instantiate:false});
  	application.inject('service:offline', 'offline', 'offline:main');

}

export default {
  name: 'offline',
  initialize: initialize
};
