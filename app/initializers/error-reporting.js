// app/initializers/error-reporting.js

import Ember from 'ember';
import config from '../config/environment';

var currentEnv = config.environment;

function getContext(router) {
    try {
        var currentState = router.currentState;
        var prettyRouteName = 'n/a';
        var routeName = 'n/a';
        if (currentState) {
            var infos = router.currentState.routerJsState.handlerInfos;
            routeName = infos[infos.length - 1].name;
            var firstSegments = routeName.replace(".index", "").replace(/\./g, ' ');
            prettyRouteName = Ember.String.capitalize(firstSegments);
        }

        var url = router.get('location').getURL();

        return prettyRouteName + " (" + routeName + ", " + url + ")";
    } catch(e) {
        console.log('error-reporting initializer: got error in getContext ', e);
    }
    return 'got error';
}

function generateError(cause, stack) {
    var error = new Error(cause);
    error.stack = stack;
    return error;
}


export default {
    name: 'error-reporting',
    before: 'deploy-cli-session',

    initialize: function(container) {
        if (config.rollbarDisabled) {
            Rollbar.configure({enabled: false});
            console.log("disabling Rollbar exception reporting for %s environment", config.environment);
            return;
        }
        console.log('error-reporting initializer: container is %o', container);
        var appController = container.lookup('controller:application');
        console.log('error-reporting initializer: appController is %o', appController);

        var RollbarTransformer = function(payload) {
            payload.data.context = getContext(container.lookup('router:main'));
            console.log('RollbarTransformer: payload is %o', payload);
        };
        console.log('error-reporting initializer: environment is %s', config.environment);
        Rollbar.configure({transform: RollbarTransformer});

        Ember.onerror = function(error) {
            var context = getContext(container.lookup('router:main'));
            Rollbar.configure({
                payload: {
                    context: context
                }
            });

            Rollbar.error(error);
        };

        Ember.RSVP.on('error', function(error) {
            var context = getContext(container.lookup('router:main'));
            Rollbar.configure({
                payload: {
                    context: context
                }
            });

            Rollbar.error(error);
        });

        Ember.Logger.error = function(message, cause, stack) {
            var context = getContext(container.lookup('router:main'));
            Rollbar.configure({
                payload: {
                    context: context
                }
            });

            Rollbar.error(generateError(cause, stack), message);
        };
    }
};
