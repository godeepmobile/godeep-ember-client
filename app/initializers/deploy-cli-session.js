// app/initializers/deploy-cli-session.js
import Ember from 'ember';
import config from '../config/environment';

function setup() {
    var Configuration = config['simple-auth'];
    //console.log('environment is ', config);

    var key = Configuration.localStorageKey || 'ember_simple_auth:session';
    var storedSession = localStorage.getItem(key);
    //console.log('deploy-cli-session: storedSession is ', storedSession);

    //if (storedSession) {
        //console.log('deploy-cli-session: storedSession is ', storedSession);
    //} else {
        /*{
        "authenticator":"authenticator:custom",
        "token":"kDAlEgnbqOKaboNulmliuDWaZ6ny5ARa3BIhBXaDGWlCNNX3l7OTkrfjxdv7PTrc",
        "user":{"username":"jperry","firstName":"James","middleName":null,"lastName":"Perry","jobTitle":null,"officePhone":null,"mobilePhone":null,"email":null,"id":39}
        }*/
        var data = Ember.$("meta[name='GD-session']").attr('content');
        //console.log('deploy-cli-session: got meta session = ', data);
        try {
            data = JSON.parse(data);
            data.authenticator = "authenticator:custom";
            //console.log('deploy-cli-session: initializing localStorage with initial session');
            localStorage.setItem(key, JSON.stringify(data));
        } catch (err) {
            console.log('deploy-cli-session: got an error parsing meta session ', err);
        }
    //}
}

export default {
    name: 'deploy-cli-session',
    before: 'custom-session',
    initialize: function(container /*, application*/ ) {
        setup();
    }
};
