import DS from 'ember-data';
import config from '../config/environment';

export default DS.RESTAdapter.extend({
    namespace: config.API.nameSpace,
    host: config.API.host,

    ajaxError: function(jqXHR, error) {
      var errObj;

    	if (jqXHR.status === 403) {
    		location.href = '/'; // returning to login page
    	} else {
        if (jqXHR.status === 500) {

          errObj = JSON.parse(error);

          if (errObj && errObj.error) {
            error = errObj.error;
          }

          console.log('Error on request:', error);
          
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.message){
						alert(error.message);
					}
        }
      }
    }
});
