import Ember from 'ember';
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';
import config from './config/environment';

var App;

Ember.MODEL_FACTORY_INJECTIONS = true;

App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  Resolver: Resolver
});
var inflector = Ember.Inflector.inflector;

inflector.uncountable('team-report-data');
inflector.uncountable('team-scouting-eval-criteria');
//for debugging

//TODO: remove for production 
/*
Ember.RSVP.configure('onerror', function(e) {
  if (e.message) {console.log('app.onerror: ', e.message);}
  if (e.stack) {console.log('app.onerror: ', e.stack);}
});

Ember.onerror = function (error) {
    console.log('Unhandled Error: ', error);
};

Ember.RSVP.on('error', function(error) {
    console.log('Unhandled Promise Error: ', error);
});

Ember.Logger.error = function (message, cause, stack) {
    console.log('Error from Logger: message: %s, cause %s, stack %s', message, cause, stack);
};
*/
loadInitializers(App, config.modulePrefix);

export default App;
