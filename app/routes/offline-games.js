import Ember from 'ember';

export default Ember.Route.extend({

	actions: {
		gradeOffline: function(teamCutup) {
			var offlineData = this.get('offlineData');
			this.transitionTo('offline-grade').then(function(route) {
				route.get('controller').setupController(offlineData, teamCutup);
			});
		}
	}
});
