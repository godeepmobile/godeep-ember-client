// app/routes/player/index.js

import Ember from 'ember';

export default Ember.Route.extend({

    afterModel: function(model) {
        var adapter = this.store.adapterFor('application')
          , self    = this
          , seasons = Ember.A([]);

        return adapter.ajax(self.evaluationStatsURL(adapter, model.get('id')), 'GET').then(function(response) {
            self.set('evaluationStats', response.stat);

            return model.get('teamPlayerCareerStat').then(function(careerStat) {

                return self.store.find('teamPlayerSeasonStat', {
                    "filter[where][teamPlayer]" : model.get('teamPlayer.id')
                }).then(function(stats) {
                    stats.forEach( function(stat,index) {
                        var obj = {};
                        Ember.set(obj, 'year',stat.get('season'));
                        Ember.set(obj, 'isExpanded', true);
                        Ember.set(obj, 'data', stat);
                        seasons.push(obj);
                    });

                    self.set('seasons', seasons.sortBy('year'));
                    self.set('careerStat', careerStat);

                });
            });
        });


    },

    setupController: function(controller , model) {
        controller.set('attrs.seasons', this.get('seasons'));
        controller.set('attrs.career', this.get('careerStat'));
        controller.set('playerAssignment', model);

        this.get('evaluationStats').forEach(function(stat) {
            stat.seasons_data.forEach(function(season) {
                Ember.set(season, 'isExpanded', true);
            });
        });

        controller.set('evaluationStats', this.get('evaluationStats'));

        controller.set('isAllExpanded', true);
        controller.set('isAllEvaluationsExpanded', true);
        controller.set('isCareerExpanded', true);
        controller.set('displayGrades', true);
        controller.set('displayEvaluations', false);
    },

    evaluationStatsURL: function(adapter, playerAssignmentId) {
        return adapter.buildURL('TeamPlayerAssignments', playerAssignmentId) + '/evaluationStats';
    },

    actions: {
        error: function(reason) {
            console.log('player.index.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Player Information",
                breadcrumbs: [
                    {
                        linkTo : "players",
                        caption : "Roster & Positions"
                    }
                ]
            };
            this.send('setBreadcrumbs',obj);
            return true;    // bubble transitions up
        },

        showPlayerGrades: function(player, season, isPractice) {
            this.transitionTo('player.games', {queryParams: {
                    season    : season,
                    isPractice: isPractice
                }
            });
        },

        showPlayerEvaluation: function(playerId, scoreGroup, season) {

            this.store.find('teamPlayerAssignment', {filter : { 
                where : { 
                    season: season,
                    teamPlayer: playerId
                }
            }}).then(function(playerAssignment) {
                this.transitionTo('/evaluation/summary/teamPlayer/'+ playerId + '/' + scoreGroup + '/' + playerAssignment.get('firstObject.id'));
            }.bind(this));

        },

        changeView: function(newView) {
            switch(newView) {
                case 'grades':
                    this.set('controller.displayGrades', true);
                    this.set('controller.displayEvaluations', false);
                    break;
                case 'evaluations':
                    this.set('controller.displayGrades', false);
                    this.set('controller.displayEvaluations', true);
                    break;
            }
        }
    },

    renderTemplate: function() {
        this.render();
    }
});
