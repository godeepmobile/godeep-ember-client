// routes/player/games.js

import Ember from 'ember';

export default Ember.Route.extend({
    filter: null,
    beforeModel: function(transition){
        var filter           = {}
          , season           = transition.queryParams.season
          , isPractice       = transition.queryParams.isPractice
          , playerAssignment = this.modelFor('player');

        if (season === 'career') {
            filter = {
                filter : {
                    where : { 
                        playerId: playerAssignment.get('teamPlayer.id'),
                        isPractice: isPractice
                    }
                }
            };
        } else {
            filter = {
                filter : {
                    where : {
                        playerId : playerAssignment.get('teamPlayer.id'),
                        season: season, isPractice: isPractice
                    }
                }
            };
        }
        this.set('filter', filter);
    },

    model: function(params) {
        return this.store.find('teamPlayerEventGameGrade', this.get('filter'));
    },

    setupController: function (controller, model) {
        this.controller.set('model', model);
    },

    renderTemplate: function() {
        this.render();
    },

    actions: {
        didTransition: function() {
            var filter       = this.get('filter').filter.where;
            var season       = filter.season ? filter.season : "Career";
            var gamePractice = filter.isPractice === 'false' ? "Games" : "Practices";

            var crumbs = {
                thisPage: season +' '+ gamePractice,
                breadcrumbs: [
                    {
                        linkTo : "players",
                        caption: "Roster & Positions"
                    },
                    {
                        linkTo : "player",
                        caption: "Player Information"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        },

        reportGamePerformance: function(game) {
            var playerAssignment = this.modelFor('player');
            this.transitionTo('reports.view', {
                queryParams: {
                    type          : 'gamePerformance',
                    filter        : this.get('controller.isPractice') === 'false' ? 1 : 2,
                    criteria      : 'players',
                    criteriaRange : [playerAssignment.get('teamPlayer.id')],
                    scope         : 'event',
                    scopeRange    : game.get('id')
                }
            });
        }
    }
});
