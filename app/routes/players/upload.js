import Ember from 'ember';

export default Ember.Route.extend({

	loadData: Ember.inject.service(),

	buildUploadRosterURL: function(adapter) {
		return adapter.buildURL('TeamPlayers') + '/upload';
	},

	setupController: function(controller) {
		var _seasons = Ember.A([]);

		this.get('loadData.seasons').forEach(function(season) {
			_seasons.push({
				value    : season
			  , selected : true
			});
		});

		controller.set('seasons',_seasons);
	},

	actions: {
		uploadRoster: function(data) {
			var headers    = this.get('colHeaders')
			  , rosterData = []
			  , adapter    = this.store.adapterFor('application')
			  , seasons    = this.get('controller.seasons').filterBy('selected').getEach('value')
			  , saveConfirmation      = function() {
			  		Ember.run.later(this, function() {
            			this.send('temporalTooltip', 'tooltip/saved', {message: 'The roster data was successfully updated'});
            		}, 1000);
			  	}.bind(this)
			  , errorHandler          = function(error) {
	            	this.set('videoCutupData', null);
					// console.log('responseJSON ', error.responseJSON);
					// if (error.responseJSON) {
					// 	alert(error.responseJSON.error.message);
					// } else if (error.responseText){
					// 	alert(error.responseText);
					// }
				}.bind(this);

			// Mapping the roster data
			data.forEach(function(record) {
				rosterData.push({
					jerseyNumber : record['Jersey No'],
					firstName    : record['First Name'],
					lastName     : record['Last Name'],
					height       : record['Height'],
					weight       : record['Weight'],
					position1    : record['Position'],
					position2    : record['2nd Position'],
					positionST   : record['ST Position'],
					highSchool   : record['School/Team'],
					state        : record['State'],
					birthDate    : record['DOB']
				});
			});

			adapter.ajax(this.buildUploadRosterURL(adapter), 'POST', {
				data : {
					players : rosterData
				  , seasons : seasons
				}
			}).then(function() {
				this.get('loadData').reload('players');
				this.transitionTo('players').then(saveConfirmation);
			}.bind(this), errorHandler);
		},

		buildRosterData: function() {
			this.get('controller').trigger('buildTableOutputData');
			this.send('removeModal');
		},

		displayUploadConfirmation: function() {
			this.send('showModal', 'players/confirm-upload');
		}
	}
});
