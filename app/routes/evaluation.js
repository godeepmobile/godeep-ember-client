// app/routes/evaluation.js

import Ember from 'ember';

export default Ember.Route.extend( {

	loadData: Ember.inject.service(),

    model: function(params) {
    	var self = this;

        return this.store.findById(params.player_type, params.player_id).then(function(teamPlayer) {
        	return self.store.find(params.player_type + 'Assignment', {filter : { 
        		where : { 
        			season: self.get('loadData.season'),
        			teamPlayer: params.player_id
        		}
        	}}).then(function(_assignment) {
                var assignment = _assignment.get('firstObject');
                teamPlayer.set('assignment', assignment);
        		self.set('assignment', assignment);
        		return teamPlayer;
        	});
        });
    },

    setupController: function(controller, model) {
    	this._super(controller, model);

    	controller.set('assignment', this.get('assignment'));
    },

    actions: {
    	toggleInfoPanelSize: function() {
        	this.get('controller').toggleProperty('infoPanelStretched');
        }
    }
});
