// app/routes/player.js

import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    loadData: Ember.inject.service(),
    
    setupController: function(controller , model) {
        controller.set('model', model);
    },

    model: function(params) {
        return this.store.findById('teamPlayerAssignment',params.player_id).then(function(teamPlayerAssignment) {
            return teamPlayerAssignment.get('teamPlayer').then(function() {
                return teamPlayerAssignment;
            });
        });
    },

    renderTemplate: function(/*controller, model*/) {
        this.render();
    },

    actions: {
        error: function(reason) {
            console.log('player.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        setBreadcrumbs: function(newCrumbs) {
            this.controller.set('thisPage', newCrumbs.thisPage);
            this.controller.set('breadcrumbs', newCrumbs.breadcrumbs);
        },
        willTransition: function(transition) {
            var currentSeason    = this.get('loadData.currentSeason')
              , controller       = this.controllerFor('players.index')
              , formattedSeasons = controller.get('formattedSeasons');

            if (transition.targetName.indexOf('players') === -1 && transition.targetName.indexOf('player') === -1 && controller.get('season.value') !== currentSeason) {
                controller.set('season', formattedSeasons.findBy('value', currentSeason));
            }
        }
    }
});
