import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    loadData: Ember.inject.service(),
    beforeModel() {
        //console.log('AdminRoute: beforeModel()');
        return this.get('loadData.profile');
    },
    actions: {
        adminShowTeam: function(btnId) {
            this.transitionTo('admin.team');
            this.selectButton(btnId);
        },
        adminShowUsers: function(btnId) {
            this.transitionTo('admin.users');
            this.selectButton(btnId);
        },
        adminShowReports: function(btnId) {
            this.transitionTo('admin.reports');
            this.selectButton(btnId);
        },
        adminShowPositionAndGroups: function(btnId) {
            this.transitionTo('admin.positionsAndGroups');
            this.selectButton(btnId);
        },
        adminShowGrading: function(btnId) {
            this.transitionTo('admin.grading');
            this.selectButton(btnId);
        },

        adminShowPlayerEval: function(btnId) {
            // default first position type 8: Center
            this.transitionTo('admin.playerEvaluation.position', 8);
            this.selectButton(btnId);
        },

        setBreadcrumbs: function(crumbs) {
            this.controller.set('thisPage', crumbs.thisPage);
            this.controller.set('breadcrumbs', crumbs.breadcrumbs);
        }
    },

    selectButton: function (id) {
        Ember.$(".admin-btn").removeClass("active");
        Ember.$('#' + id).addClass("active");
    }
});
