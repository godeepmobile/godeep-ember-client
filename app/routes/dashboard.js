// app/routes/dashboard.js

import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    //TODO: set selectedView based on user preference
    selectedView: 'team',

    loadData: Ember.inject.service(),

    setupController: function(controller , model) {
        var formattedSeasons
           ;

        if (!controller.get('attrs')) {
            controller.set('attrs', {});
        }
        controller.set('attrs.selectedView', this.get('selectedView'));
        controller.set('model', model);

        controller.set('seasons', this.get('loadData.seasons'));
        formattedSeasons = controller.get('formattedSeasons');

        Ember.run.scheduleOnce('afterRender', controller, function() {
            var season = this.get('loadData.season');
            /*if (this.get('noStatusForCurrentSeason')) {
                season -= 1;
                //this.send('changeCurrentSeason', season);
            }*/
            controller.set('season', formattedSeasons.findBy('value', season));
        }.bind(this));
    },

    model: function() {
        return this.get('loadData.teamPlayerSeasonStats');
    },

    renderTemplate: function(/*controller, model*/) {
        this.render();
    },

    buildQueryParams: function(_view) {
        return {queryParams : {
            view : _view
        }};
    },

    actions: {
        error: function(reason) {
            console.log('dashboard.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        dashboardViewChanged: function(newValue) {

            var views = this.get('controller.VIEWS');

            if (this.get('selectedView')!== newValue) {
                console.log('dashboardViewChanged to %s', newValue);

                this.set('selectedView', newValue);

                console.log('dashboardRefreshData selectedView: %s', newValue);

                this.transitionTo(this.buildQueryParams(newValue));
            }

        },
        dashboardUpdateSeasonTotals: function(isChangingSeason) {
            console.log('dashboardUpdateSeasonTotals');

            var self    = this
              , adapter = this.store.adapterFor('application')
              , url     = adapter.buildURL('TeamPlayerSeasonStats', 'updateSeasonStats')
              , season  = self.get('loadData.season');

            this.send('clearDashboardStats');

            adapter.ajax(url, 'POST', {
                data : {
                    season : season
                }
            }).then(function() {
                console.log('dashboardUpdateSeasonTotals finished');
                self.get('loadData').reload('teamPlayerSeasonStats', isChangingSeason);
                // Note(Carlos) this control will avoid calling this action when the dashboard is not visible
                if (self.controllerFor('application').get('currentRouteName').indexOf('dashboard') !== -1) {
                    self.send('reloadDashboardStats');
                }
            });
        },
        setBreadcrumbs: function(newCrumbs) {
            this.controller.set('thisPage', newCrumbs.thisPage);
            this.controller.set('breadcrumbs', newCrumbs.breadcrumbs);
        },
        playersShowPlayer: function(teamPlayerAssignment) {
            /*this.store.find('teamPlayerAssignment', {filter : {
                where : {
                    season: season,
                    teamPlayer: playerId
                }
            }}).then(function(assignment) {*/
                this.transitionTo('player', teamPlayerAssignment);
            //}.bind(this));
        },
        willTransition: function(transition) {
            var currentSeason    = this.get('loadData.currentSeason')
              , controller       = this.get('controller')
              , formattedSeasons = controller.get('formattedSeasons');

            if (transition.targetName.indexOf('dashboard') === -1 && controller.get('season.value') !== currentSeason) {
                controller.set('season', formattedSeasons.findBy('value', currentSeason));
            }
        }
    }
});
