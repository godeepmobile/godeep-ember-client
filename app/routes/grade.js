import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';
import FetchAllModelsMixin from '../mixins/fetch-all-models';
import config from '../config/environment';

export default Ember.Route.extend(AuthenticatedRouteMixin, FetchAllModelsMixin, {

    websockets: Ember.inject.service('socket-io'),

	model: function(params) {
        return this.store.findById('teamEventGradeStatus', params.grade_id).then(function(gradeStatus) {
        	return Ember.RSVP.all([gradeStatus.get('teamEvent'), gradeStatus.get('teamCutup')]).then(function() {
                return gradeStatus;
        	});
        });
    },

    afterModel: function(gradeStatus) {
    	var self        = this
          , adapter     = this.store.adapterFor('application')
          , teamCutupId = gradeStatus.get('teamCutup.id');

        // Update the game stats
        return adapter.ajax(this.updateGameStatsURL(adapter), 'POST', {
            data : {
                cutupId : teamCutupId
            }
        }).then(function() {
            return self.store.find('teamPlayerGameCutupStat', {
                filter : {
                    where : {
                        teamCutup : teamCutupId
                    }
                }
            }).then(function(playerGameCutupStats) {
                self.set('gameStats', playerGameCutupStats);

                return adapter.ajax(self.numCompletedPlaysURL(adapter, gradeStatus.get('id')), 'GET').then(function(response) {
                    self.set('numCompletedPlays', response.completedPlays);
                });
            });
        });
    },

    setupController: function(controller, model) {
    	var gamePlayerStats = Ember.A([])
    	  , self            = this
    	  , gameStat
    	  , teamPlayer
    	  , numPlays
          , playerAssignments
          , setGameStats    = function() {
                var gameStats = self.get('gameStats');

                gameStats.forEach(function(stat) {
                    stat.set('playerAssignment', playerAssignments.findBy('teamPlayer.id', stat.get('teamPlayer.id')));
                });

                if (gameStats.get('length') > 0) {
                    controller.set('gameStats', gameStats);
                } else {
                    controller.set('noGameStats', true);
                }
            };

    	controller.set('model', model);
    	//controller.set('gameStats', this.get('gameStats'));
        controller.set('isCompleted', false);
        controller.set('noGameStats', false);
        controller.set('numCompletedPlays', this.get('numCompletedPlays'));

        playerAssignments = this.fetchAllModelsWithCallback('teamPlayerAssignment', setGameStats, {
            filter : { 
                where : { season: model.get('teamEvent.season')}}
        });
    },

    updateGameStatsURL: function(adapter) {
        return adapter.buildURL('TeamPlayerGameCutupStat', 'updateCutupStats');
    },

    playsInfoURL: function(adapter, teamCutupId) {
        return adapter.buildURL('TeamCutups', teamCutupId) + '/playsInfo';
    },

    increaseValue: function(object, attr, inc) {
    	object.set(attr, object.get(attr) + inc);
    },

    numCompletedPlaysURL: function(adapter, gradeStatusId) {
        return adapter.buildURL('TeamEventGradeStatus', gradeStatusId) + '/numCompletedPlays';
    },

    actions: {

    	resumeGrade: function() {
            var gradeStatus = this.get('currentModel')
              , resumePlay  = gradeStatus.get('lastGradedPlay');

            if (gradeStatus.get('lastGradedPlay')) {
                this.transitionTo('game', gradeStatus.get('teamCutup'), {
                    queryParams: {
                        resumePlay : resumePlay
                      , play       : resumePlay
                    }
                }).then(function(route) {
                    route.get('controller').addJQueryLogic();
                });
                /*this.transitionTo('game', gradeStatus.get('teamCutup')).then(function(gradeRoute) {
                    gradeRoute.changeCurrentPlay(gradeStatus.get('lastGradedPlay') + 1);
                });*/
            } else {
                this.transitionTo('game', gradeStatus.get('teamCutup'));
            }
    	},

        generateReports: function() {
            //this.transitionTo('reports', {queryParams: {game: this.get('currentModel.teamEvent.id')}});
            // TODO(carlos) logic to generate reports or call the process for it must be added here
            var model = this.get('currentModel');
            this.send('showModal', 'grade/reports', model);
        },

        reportGamePerformance: function(teamPlayer) {
            var teamEvent = this.get('currentModel.teamEvent');
            
            this.transitionTo('reports.view', {
                queryParams: {
                    type          : 'gamePerformance',
                    filter        : teamEvent.get('eventType.id'),
                    criteria      : 'players',
                    criteriaRange : [teamPlayer.get('id')],
                    scope         : 'event',
                    scopeRange    : teamEvent.get('id')
                }
            });
        },

        gradeInSmallWindow: function() {
            var gradeStatus = this.get('currentModel')
              , resumePlay  = gradeStatus.get('lastGradedPlay')
              , url         = window.location.origin + 
                this.get('router.location.rootURL') + 
                'game/' +
                gradeStatus.get('teamCutup.id') + 
                '?minimizedVersion=true&sid=' + this.get('websockets').socketFor(config.API.host).socket.id;

            if (resumePlay) {
                url += '&play=' + resumePlay;
            }

            window.open(url, 'small-grading-window', 'location=0,status=0,scrollbars=0,titlebar=0,menubar,width=490,height=768,left=' + (document.documentElement.clientWidth-490));
        }
    }
});
