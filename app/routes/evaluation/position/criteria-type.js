// routes/evaluation/position/criteria-type.js
import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),
    queryParams: {
      scoreGroup: {
        refreshModel: true
      }
    },

    beforeModel: function(transition) {
        var scoreGroup = transition.queryParams.scoreGroup;
        this.set('scoreGroup', scoreGroup);
        this.set('allowTransition', false);
        this.set('player', this.modelFor('evaluation'));
    },

    model: function(params) {
        this.set('criteriaTypeId', params.criteria_type_id);
        return this.store.find('teamScoutingEvalScore', {
            filter: {
                where: {
                    teamScoutingEvalScoreGroup: this.get('scoreGroup'),
                    scoutingEvalCriteriaType  : params.criteria_type_id
                }
            }
        });
    },

    setupController: function(controller , model) {
        controller.set('model', model);
        controller.set('player', this.get('player'));
        var positionController = this.controllerFor('evaluation.position');
        positionController.set('criteriaTypeId', this.get('criteriaTypeId'));
    },

    playerCountNotScoredCriteriaUrl: function(adapter) {
        return adapter.buildURL('TeamScoutingEvalScoreGroup', this.get('scoreGroup')) + '/countNotScoredCriteria';
    },

    actions: {
        addNotes:function() {
            this.controller.send('addEvalNotes');
        },

        addNote:function(criteria) {
            this.controller.send('addEvalNote', criteria);
        },

        finishEvaluation:function() {
            this.controller.send('finishEval');
        },

        willTransition: function(transition) {
          var self = this;
          if(!this.get('allowTransition')) {
            var validTarges = ["evaluation.position.criteriaType", "evaluationSummary"];
            if (Ember.$.inArray(transition.targetName, validTarges) === -1) {
              transition.abort();
              var adapter = this.store.adapterFor('application');
              var url = this.playerCountNotScoredCriteriaUrl(adapter);
              adapter.ajax(url, 'GET').then(function (response) {
                if (parseInt(response.data.notscoredcriteria) === 0) {
                  self.set('allowTransition', true);
                  transition.retry();
                } else {
                  self.send('showModal', 'evaluation/position/warning',
                            { trans:transition,
                              router: self,
                              scoreGroupId: self.get('scoreGroup')
                            });
                }
              }, function (error) {
                console.log('error: ' , error);
              });
          }
        }
      }
    }
});
