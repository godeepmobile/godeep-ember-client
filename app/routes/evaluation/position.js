// routes/evaluation/position.js

import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),

    model: function(params) {
        return this.store.findById('positionType', params.position_id);
    },

    afterModel(model) {
        var scoutingEvalCriteriaTypes = this.get('loadData.scoutingEvalCriteriaTypes');
        this.set('scoutingEvalCriteriaTypes', scoutingEvalCriteriaTypes);
    },

    setupController: function(controller , model) {
        controller.set('model', model);
        controller.set('scoutingEvalCriteriaTypes', this.get('scoutingEvalCriteriaTypes'));

    }
});
