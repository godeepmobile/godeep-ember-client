// app/routes/prospects.js

import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),

    model: function() {
        return this.get('loadData.prospectAssignments');
    },

    actions: {
        prospectDetail: function(prospect) {
            this.transitionTo('prospect', prospect);
        }
    }
});