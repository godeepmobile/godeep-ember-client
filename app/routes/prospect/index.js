import Ember from 'ember';

export default Ember.Route.extend({
	afterModel: function(model) {
        var adapter = this.store.adapterFor('application')
          , self    = this;

        return adapter.ajax(self.evaluationStatsURL(adapter, model.get('id')), 'GET').then(function(response) {
            self.set('evaluationStats', response.stat);
        });
    },

    setupController: function(controller , model) {
    	controller.set('model', model);
    	this.get('evaluationStats').forEach(function(stat) {
            stat.seasons_data.forEach(function(season) {
                Ember.set(season, 'isExpanded', true);
            });
        });

        controller.set('evaluationStats', this.get('evaluationStats'));
        controller.set('isAllEvaluationsExpanded', true);
    },

    evaluationStatsURL: function(adapter, prospectId) {
        return adapter.buildURL('TeamProspectAssignments', prospectId) + '/evaluationStats';
    },

    actions: {
    	showProspectEvaluation: function(prospectId, scoreGroup, season) {
            this.store.find('teamProspectAssignment', {filter : { 
                where : { 
                    season: season,
                    teamProspect: prospectId
                }
            }}).then(function(prospectAssignment) {
                this.transitionTo('/evaluation/summary/teamProspect/'+ prospectId + '/' + scoreGroup + '/' + prospectAssignment.get('firstObject.id'));
            }.bind(this));
        }
    }
});
