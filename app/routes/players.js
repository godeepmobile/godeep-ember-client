// app/routes/players.js

import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    loadData: Ember.inject.service(),

    setupController: function(controller, model) {
        controller.set('model', model);
    },
    model: function() {
        this.set('positionTypes', this.get('loadData.positionTypesForTeam'));
        return this.get('loadData.playerAssignments');
    },
    reactivateTeamPlayerAssignmentUrl: function(adapter, playerAssignment) {
         return adapter.buildURL('TeamPlayerAssignment', playerAssignment.get('id')) + '/reactivate';
    },
    actions: {
        playersShowPlayer: function(playerAssignment) {
            this.transitionTo('player', playerAssignment);
        },
        showModalPlayerPosition: function(player, position, positionLabel) {

            var platoonTypeId      = 1,
                positionController = this.controller.get('controllers.players/position'),
                teamPositionTypes;

            this.send('showModal', 'players/position', player);

            var teamPositionTypesOff = this.get('positionTypes').filter(function(item, index, enumerable) {
                return item.get('platoonType') === 1;
            })
            , teamPositionTypesDef = this.get('positionTypes').filter(function(item, index, enumerable) {
                return item.get('platoonType') === 2;
            })
            , teamPositionTypesST = this.get('positionTypes').filter(function(item, index, enumerable) {
                return item.get('platoonType') === 3;
            });

            // Note(carlos) no need to access the position through a promise here, it's already fullfilled
            // when the roster page is loaded
            /*var curPositionPromise = player.get(position);
            if (curPositionPromise) {
                curPositionPromise.then(function(curPositionModel) {
                    if (curPositionModel) {
                        platoonTypeId = curPositionModel.get('platoonType');
                    }
                });
            }*/

            platoonTypeId = player.get(position).get('platoonType');

            positionController.set('attrs.isDisabled', false);
            positionController.set('attrs.isSTPos', false);
            positionController.set('attrs.selectedPlatoonTypeId', platoonTypeId );

            if ( platoonTypeId === 1 ) {
                teamPositionTypes = teamPositionTypesOff;
            } else if ( platoonTypeId === 2 ) {
                teamPositionTypes = teamPositionTypesDef;
            } else if ( platoonTypeId === 3 ) {
                teamPositionTypes = teamPositionTypesST;
            } else {
                teamPositionTypes = teamPositionTypesOff;
                positionController.set('attrs.isDisabled', true);
                positionController.set('attrs.selectedPlatoonTypeId', 1 ); // Setting offense by default
            }

            if (position === "position1") {
                positionController.set('attrs.isPos1', true);
            } else {
                positionController.set('attrs.isPos1', false);

            }
            if (position === "positionST") {
                positionController.set('attrs.isSTPos', true);
                positionController.set('attrs.selectedPlatoonTypeId', 3 );
            }

            positionController.set('attrs.position', position);
            positionController.set('attrs.positionLabel', positionLabel);
            positionController.set('teamPositionTypes', teamPositionTypes);
            positionController.set('teamPositionTypesOff', teamPositionTypesOff);
            positionController.set('teamPositionTypesDef', teamPositionTypesDef);
            positionController.set('teamPositionTypesST', teamPositionTypesST);

            if ( player.get(position).get('id') ) {
                positionController.set('attrs.isEditing', true);
                positionController.set('selectedPositionType', player.get(position));
            } else {
                positionController.set('attrs.isEditing', false);
                positionController.set('selectedPositionType', teamPositionTypes.toArray()[0]);
            }
        },
        showModalDeletePlayer: function(player) {
            this.send('showModal', 'players/delete', player);
            //Deleting the player
            // player.deleteRecord();
            // player.save();
        },
        reactivatePlayer: function(teamPlayerAssignment) {
            var adapter = this.store.adapterFor('application');

            adapter.ajax(this.reactivateTeamPlayerAssignmentUrl(adapter, teamPlayerAssignment), 'PUT').then(function() {
                teamPlayerAssignment.reload();
            });
        },
        willTransition: function(transition) {
            var currentSeason    = this.get('loadData.currentSeason')
              , controller       = this.controllerFor('players.index')
              , formattedSeasons = controller.get('formattedSeasons');

            if (transition.targetName.indexOf('players') === -1 && transition.targetName.indexOf('player') === -1 && controller.get('season.value') !== currentSeason) {
                controller.set('season', formattedSeasons.findBy('value', currentSeason));
            }
        }
    }
});
