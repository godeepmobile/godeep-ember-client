import Ember from 'ember';

export default Ember.Route.extend({
  renderTemplate: function(){
console.log('route=about');
    this.render();
    this.render("about/product", {into:"about", outlet: "aboutproduct"});
    this.render("about/location", {into:"about", outlet: "aboutlocation"});
  },
    actions: {
        playerViewPlayerInformation: function() {
            console.log('playerViewPlayerInformation');
            this.transitionTo('players');
        },
        playerCreateScoutingReport: function() {
            console.log('playerCreateScoutingReport');
            this.transitionTo('players');
        },
        playerManageRoster: function() {
            console.log('playerManageRoster');
            this.transitionTo('players');
        },

        gameViewGameList: function() {
            console.log('gameViewGameList');
            this.transitionTo('games');
        },
        gameSetUpGame: function() {
            console.log('gameSetUpGame');
            this.transitionTo('games');
        },
        gameGradePlayers: function() {
            console.log('gameGradePlayers');
            this.transitionTo('games');
        },

        reportGamePerformance: function() {
            console.log('reportGamePerformance');
            this.transitionTo('reports');
        },
        reportThirdDownPerformance: function() {
            console.log('reportThirdDownPerformance');
            this.transitionTo('reports');
        },
        reportRedZonePerformance: function() {
            console.log('reportRedZonePerformance');
            this.transitionTo('reports');
        },
        reportScoutingReportSummary: function() {
            console.log('reportScoutingReportSummary');
            this.transitionTo('reports');
        },

        adminManageAccountInformation: function() {
            console.log('adminManageAccountInformation');
            this.transitionTo('admin');
        },
        adminSetUpPositionGroups: function() {
            console.log('adminSetUpPositionGroups');
            this.transitionTo('admin');
        },
        adminManageUserSecurityGroups: function() {
            console.log('adminManageUserSecurityGroups');
            this.transitionTo('admin');
        },
        adminSetReportDistributionPreferences: function() {
            console.log('adminSetReportDistributionPreferences');
            this.transitionTo('admin');
        }
    }
});
