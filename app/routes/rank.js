// app/routes/ranking.js

import Ember from 'ember';
import FetchAllModelsMixin from '../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
    loadData: Ember.inject.service(),
    model: function() {
        return [];
    },

    setupController: function(controller, model) {
        controller.set('model', model);
    },

    actions: {
        viewProspectsRank: function() {
            this.transitionTo('rank');
        },

        viewPlayersRank: function() {
            this.transitionTo('rank.playerRank');
        },

        selectRank: function(rank) {
            this.controller.set('rankSelected', rank);
        },

        didTransition: function() {
            var thisPage = "Player Value Ranking - " + this.get('loadData.season') + " Season";
            this.controller.set('thisPage', thisPage);
            this.controller.set('breadcrumbs', []);
            return true;
        },

        willTransition: function(transition) {
            var currentSeason    = this.get('loadData.currentSeason')
              , controller       = this.controllerFor('rank.index')
              , formattedSeasons = controller.get('formattedSeasons');

            if (transition.targetName.indexOf('rank') === -1 && controller.get('season.value') !== currentSeason) {
                controller.set('season', formattedSeasons.findBy('value', currentSeason));
            }
        }
    }

});
