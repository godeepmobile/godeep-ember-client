// app/routes/evaluation-summary.js

import Ember from 'ember';
import FetchAllModelsMixin from '../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
    loadData: Ember.inject.service(),

    model: function(params) {
        this.set('playerType', params.player_type);
        this.set('playerId', params.player_id);
        this.set('evaluationId', params.evaluation_id);
        this.set('assignmentId', params.assignment_id);
        return [];
        // this.store.find('teamScoutingEvalScore', {
        //     filter: {
        //         where: {
        //             teamScoutingEvalScoreGroup: params.evaluation_id
        //         }
        //     }
        // });
    },

    afterModel(model) {
        var self = this, whereFilter;

        return Ember.RSVP.hash({
          player : this.store.findById(this.get('playerType'), this.get('playerId')),
          teamScoutingEvalScoreGroup : this.store.findById('teamScoutingEvalScoreGroup', this.get('evaluationId')),
          playerAssignment : this.store.findById(this.get('playerType') + 'Assignment', this.get('assignmentId'))
        }).then(function(context) {
          self.set('player', context.player);
          self.set('teamScoutingEvalScoreGroup', context.teamScoutingEvalScoreGroup);
          self.set('playerAssignment', context.playerAssignment);

          whereFilter = {
              filter: {
                  where: {
                      team: context.teamScoutingEvalScoreGroup.get('positionType.id'),
                      positionType: context.teamScoutingEvalScoreGroup.get('positionType.id')
                  }
              }
          };

          return Ember.RSVP.hash({
            teamPositionImportances  : self.store.find('teamPositionImportance', whereFilter),
            teamScoutingEvalCriterias : self.fetchAllModelsNamed('teamScoutingEvalCriteria', whereFilter)
          }).then(function(evalContext) {
            self.set('teamPositionImportance', evalContext.teamPositionImportances.get('firstObject'));
            self.set('teamScoutingEvalCriterias', evalContext.teamScoutingEvalCriterias);
          });
        });
    },

    setupController: function(controller , model) {
        controller.set('player', this.get('player'));
        controller.set('playerAssignment', this.get('playerAssignment'));
        controller.set('teamScoutingEvalScoreGroup', this.get('teamScoutingEvalScoreGroup'));
        controller.set('model', model);
        controller.set('playerType', this.get('playerType'));
        controller.set('playerId', this.get('playerId'));
        controller.set('message', 'loading report...');
        controller.set('summaryData', []);
        controller.set('currentView', controller.get('VIEWS.printFriendly'));
        this.playerEvaluationReport();
    },

    playerEvaluationReport: function () {
      var self = this;
      var adapter = this.store.adapterFor('application');
      var url = this.playerEvaluationReportUrl(adapter);

      function ajaxSuccess(response) {
          var reportData = response.data;
          self.controller.set('summaryData', self.generateReportData(reportData.records));
          self.controller.set('playerScoreOverall', reportData.playerScoreOverall.toFixed(2));
          self.controller.set('targetMatch', reportData.targetMatch.toFixed(0));
          self.controller.set('ranks', reportData.ranks);
      }

      function ajaxFailure(error) {
          console.log('reportAggregateGrades error %o', error);
      }
      adapter.ajax(url, 'GET').then(ajaxSuccess, ajaxFailure);
    },

    generateReportData: function(data) {
      var summaryData = [];
      data.forEach( function(item) {
        var chartData = [];
          chartData[0] = {
              name: 'Target',
              data: item.targetScores
          };
          chartData[1] = {
              name: 'Player',
              data: item.playerScores
          };
          summaryData.push({
            criteriaTypeId: item.id,
            criteriaType  : item.name.toUpperCase(),
            chartData     : chartData,
            categories    : item.categories,
            criticals     : item.criticals,
            scoreAvg      : item.scoreAvg.toFixed(2),
            targetAvg     : item.targetScoreAvg.toFixed(2),
            notes         : item.notes
          });
      });
      return summaryData;
    },

    playerEvaluationReportUrl: function(adapter) {
        return adapter.buildURL('TeamScoutingEvalScoreGroup', this.get('evaluationId')) + '/playerEvaluationReport';
    },

    actions: {
      displayEvaluationCriteriaSetup: function(positionType) {
        this.send('showModal', 'evaluation/criteria/setup', {
            criteriaHasEvaluations    : true,
            positionType              : positionType,
            importances               : this.get('loadData.importances'),
            teamScoutingEvalCriterias : this.get('teamScoutingEvalCriterias'),
            teamPositionImportance    : this.get('teamPositionImportance'),
            afterSave                 : 'refreshEvaluationReportData'
        });
      },

      refreshEvaluationReportData: function() {
        this.afterModel();
        this.setupController(this.get('controller'), this.get('currentModel'));
      }
    }
});
