// routes/dashboard/best-unit-impact.js

import Ember from 'ember';

export default Ember.Route.extend({

    VIEWS: {
        team        : 'team'
      , offense     : 'offense'
      , defense     : 'defense'
      , gradeGroups : 'grade-group'
    },

    loadData: Ember.inject.service(),

    renderTemplate: function(controller, model) {
        this.render('dashboard.top-players', {  // template to render
            controller:'dashboard.best-unit-impact'
        });
    },

    allStatsOffenseFilter: function(stat){
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && playerAssignment.get('isOffense') && playerAssignment.get('season') === season && !playerAssignment.get('endDate');
    },

    allStatsDefenseFilter: function(stat){
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && playerAssignment.get('isDefense') && playerAssignment.get('season') === season && !playerAssignment.get('endDate');
    },

    allStatsFilter: function(stat){
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && playerAssignment.get('season') === season && !playerAssignment.get('endDate');
    },

    setControllerStatsByView: function(controller, view) {
        var views         = this.get('VIEWS')
          , store         = this.store
          , season        = this.get('loadData.season')
          , positionTypes = this.get('session.positionTypes').getEach('id')
          , playerAssignment;

        if (controller) {

            controller.set('attrs.allStats', null);

            switch (view) {
                case views.offense:
                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', this.allStatsOffenseFilter.bind(this)));
                    break;

                case views.defense:
                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', this.allStatsDefenseFilter.bind(this)));
                    break;

                case views.gradeGroups:

                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', function(stat) {
                        playerAssignment = stat.get('teamPlayerAssignment');

                        return playerAssignment && playerAssignment.get('season') === season && !playerAssignment.get('endDate') && ( 
                            positionTypes.indexOf(playerAssignment.get('position1.id')) ||
                            positionTypes.indexOf(playerAssignment.get('position2.id')) ||
                            positionTypes.indexOf(playerAssignment.get('position3.id')) ||
                            positionTypes.indexOf(playerAssignment.get('positionST.id'))
                        );
                    }));

                    break;

                default:

                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', this.allStatsFilter.bind(this)));
            }
        }
    },

    actions: {
        error: function(reason) {
            console.log('dashboard.best-unit-impact.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Best Unit Impact",
                breadcrumbs: [
                    {
                        linkTo :    "dashboard",
                        caption : "Home"
                    }
                ]
            };
            this.send('setBreadcrumbs',obj);
            return true;    // bubble transitions up
        },

        queryParamsDidChange: function(changed) {

            var view = changed ? changed.view : undefined;

            if (this.get('controller')) {
                this.setControllerStatsByView(this.get('controller'), view);
            }
        },

        clearDashboardStats: function() {
            this.set('controller.attrs.allStats', null);
        },

        reloadDashboardStats: function() {
            var controller = this.get('controller');
            this.setControllerStatsByView(controller, controller.get('controllers.dashboard.view'));
        }
    },

    setupController: function(controller, model) {
        this.setControllerStatsByView(controller, controller.get('controllers.dashboard.view'));
    }
});
