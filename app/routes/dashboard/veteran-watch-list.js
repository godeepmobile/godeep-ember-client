// routes/dashboard/veteran-watch-list.js

import Ember from 'ember';

export default Ember.Route.extend({
    renderTemplate: function(controller, model) {
        this.render('dashboard.top-players', {  // template to render
            controller:'dashboard/veteran-watch-list'
            //model:this.get('vetStats')
        });
    },
    actions: {
        error: function(reason) {
            console.log('dashboard.veteran-watch-list.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Veteran Watch List",
                breadcrumbs: [
                    {
                        linkTo :    "dashboard",
                        caption : "Home"
                    }
                ]
            };
            this.send('setBreadcrumbs',obj);
            return true;    // bubble transitions up
        }
    },

    setupController: function(controller, model) {
        //this._super(controller, model);
        controller.set('attrs.allStats', this.store.filter('teamPlayerSeasonStat', function(stat) {
            return stat.get('isVeteran') && !stat.get('teamPlayerAssignment.endDate');
        }));
    },

    model: function() {
        return this.modelFor('dashboard');
    }
});
