// routes/dashboard/index.js

import Ember from 'ember';

export default Ember.Route.extend({
    attrs: {},

    VIEWS: {
        team        : 'team'
      , offense     : 'offense'
      , defense     : 'defense'
      , gradeGroups : 'grade-group'
    },

    loadData: Ember.inject.service(),

    newStatsOffenseFilter: function(stat) {
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && playerAssignment.get('isOffense') && stat.get('season') === season && !playerAssignment.get('endDate') && !playerAssignment.get('isVeteran');
    },

    allStatsOffenseFilter: function(stat){
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && playerAssignment.get('isOffense') && stat.get('season') === season && !playerAssignment.get('endDate');
    },

    newStatsDefenseFilter: function(stat) {
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && playerAssignment.get('isDefense') && stat.get('season') === season && !playerAssignment.get('endDate') && !playerAssignment.get('isVeteran');
    },

    allStatsDefenseFilter: function(stat){
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');

        return playerAssignment && playerAssignment.get('isDefense') && stat.get('season') === season && !playerAssignment.get('endDate');
    },

    newStatsFilter: function(stat) {
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && stat.get('season') === season && !playerAssignment.get('endDate') && !playerAssignment.get('isVeteran');
    },

    allStatsFilter: function(stat){
        var playerAssignment = stat.get('teamPlayerAssignment')
          , season           = this.get('loadData.season');
        return playerAssignment && stat.get('season') === season && !playerAssignment.get('endDate');
    },

    setupController: function(controller , model) {

        this.setControllerStatsByView(controller, controller.get('controllers.dashboard.view'));
    },

    setControllerStatsByView: function(controller, view) {
        var views         = this.get('VIEWS')
          , store         = this.store
          , season        = this.get('loadData.season')
          , positionTypes = this.get('session.positionTypes').getEach('id')
          , playerAssignment;

        if (controller) {

            controller.set('attrs.allStats', null);
            controller.set('attrs.newStats', null);

            switch (view) {
                case views.offense:
                    
                    controller.set('attrs.newStats', store.filter('teamPlayerSeasonStat', this.newStatsOffenseFilter.bind(this)));

                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', this.allStatsOffenseFilter.bind(this)));

                    break;

                case views.defense:

                    controller.set('attrs.newStats', store.filter('teamPlayerSeasonStat', this.newStatsDefenseFilter.bind(this)));

                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', this.allStatsDefenseFilter.bind(this)));

                    break;

                case views.gradeGroups:

                    controller.set('attrs.newStats', store.filter('teamPlayerSeasonStat', function(stat) {
                        playerAssignment = stat.get('teamPlayerAssignment');

                        return playerAssignment && playerAssignment.get('season') === season && !playerAssignment.get('endDate') && !playerAssignment.get('isVeteran') && ( 
                            positionTypes.indexOf(playerAssignment.get('position1.id')) ||
                            positionTypes.indexOf(playerAssignment.get('position2.id')) ||
                            positionTypes.indexOf(playerAssignment.get('position3.id')) ||
                            positionTypes.indexOf(playerAssignment.get('positionST.id'))
                        );
                    }));

                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', function(stat) {
                        playerAssignment = stat.get('teamPlayerAssignment');

                        return playerAssignment && playerAssignment.get('season') === season && !playerAssignment.get('endDate') && ( 
                            positionTypes.indexOf(playerAssignment.get('position1.id')) ||
                            positionTypes.indexOf(playerAssignment.get('position2.id')) ||
                            positionTypes.indexOf(playerAssignment.get('position3.id')) ||
                            positionTypes.indexOf(playerAssignment.get('positionST.id'))
                        );
                    }));

                    break;

                default:

                    controller.set('attrs.newStats', store.filter('teamPlayerSeasonStat', this.newStatsFilter.bind(this)));

                    controller.set('attrs.allStats', store.filter('teamPlayerSeasonStat', this.allStatsFilter.bind(this)));
            }
        }
    },

    actions: {
        error: function(reason) {
            console.log('dashboard.index.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Home",
                breadcrumbs: []
            };
            this.send('setBreadcrumbs',obj);
            return true;    // bubble transitions up
        },

        queryParamsDidChange: function(changed) {

            var view = changed ? changed.view : undefined;

            if (this.get('controller')) {
                this.setControllerStatsByView(this.get('controller'), view);
            }
        },

        clearDashboardStats: function() {
            this.set('controller.attrs.allStats', null);
            this.set('controller.attrs.newStats', null);
        },

        reloadDashboardStats: function() {
            var controller = this.get('controller');
            this.setControllerStatsByView(controller, controller.get('controllers.dashboard.view'));
        }
    }
});
