// routes/dashboard/defense.js

import Ember from 'ember';

export default Ember.Route.extend({
    attrs: {},
    filteredStats: null,
    loadData: Ember.inject.service(),

    filterFunction: function(stat) {
        return (stat.get('gameNumPlays') > 60);
    },
    setupController: function(controller , model) {
        var indexController = this.controllerFor('dashboard.index');
        //var allStats = model.allStats;
        //var allPlayers = model.allPlayers;
        if (!indexController.get('attrs')) {
            indexController.set('attrs', {});
        }
        var season = this.get('loadData.season');
        var filteredStats = model.filter(function(stat){
            var playerAssignment = stat.get("teamPlayerAssignment");
            var flag = playerAssignment && playerAssignment.get('isDefense') && playerAssignment.get('season') === season;
            //console.log('filtering player %s, isDefense is %s', player.get('lastName'), flag ? 'true' : 'false');
            return flag;
        });
        indexController.set('attrs.newStats', filteredStats.filter(function(stat) {
            return !stat.get('isVeteran');
        }.bind(this)));
        indexController.set('attrs.vetStats', filteredStats.filter(function(stat) {
            return stat.get('isVeteran');
        }.bind(this)));
        indexController.set('attrs.allStats', filteredStats);
    },

    actions: {
        error: function(reason) {
            console.log('dashboard.defense.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Home",
                breadcrumbs: []
            };
            this.send('setBreadcrumbs',obj);
            return true;    // bubble transitions up
        }
    },
    /*ensureModels: function(msg) {
        var store = this.store;
        var allStat = store.all('teamPlayerSeasonStat');
    },*/
    model: function() {
        return this.get('loadData.teamPlayerSeasonStats');
    },

    renderTemplate: function(controller, model) {
        this.render('dashboard.index', {  // template to render
            controller:'dashboard.index'
        });
    }
});
