// routes/dashboard/my-groups.js

import Ember from 'ember';

export default Ember.Route.extend({
    attrs: {},
    filteredStats: null,
    loadData: Ember.inject.service(),

    filterFunction: function(stat) {
        return (stat.get('gameNumPlays') > 60);
    },
    setupController: function(controller , model) {
        var indexController = this.controllerFor('dashboard.index');
        //var allStats = model.allStats;
        //var allPlayers = model.allPlayers;
        //var myPlayers = this.get("session.teamPlayers");
        var positionTypes = this.get('session.positionTypes').getEach('id');
        if (!indexController.get('attrs')) {
            indexController.set('attrs', {});
        }
        var season = this.get('loadData.season');
        var filteredStats = [];
        /*myPlayers.forEach(function(player) {
            player.get("teamPlayerSeasonStats").then(function(stats){
                if (stats && stats.length) {
                    filteredStats.pushObject(stats.get("firstObject"));
                }
            });
        });*/
        filteredStats = model.filter(function(stat) {
            var playerAssignment = stat.get("teamPlayerAssignment");
            var flag = playerAssignment && playerAssignment.get('season') === season && ( 
                positionTypes.indexOf(playerAssignment.get('position1.id')) ||
                positionTypes.indexOf(playerAssignment.get('position2.id')) ||
                positionTypes.indexOf(playerAssignment.get('position3.id')) ||
                positionTypes.indexOf(playerAssignment.get('positionST.id'))
            );
            return flag;
        });
        indexController.set('attrs.newStats', filteredStats.filter(function(stat) {
            return !stat.get('isVeteran');
        }.bind(this)));
        indexController.set('attrs.vetStats', filteredStats.filter(function(stat) {
            return  stat.get('isVeteran');
        }.bind(this)));
        indexController.set('attrs.allStats', filteredStats);
    },

    actions: {
        error: function(reason) {
            console.log('dashboard.my-groups.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Home",
                breadcrumbs: []
            };
            this.send('setBreadcrumbs',obj);
            return true;    // bubble transitions up
        }
    },
    /*ensureModels: function(msg) {
        var store = this.store;
        var allStat = store.all('teamPlayerSeasonStat');
    },*/
    model: function() {
        return this.get('loadData.teamPlayerSeasonStats');
    },

    renderTemplate: function(controller, model) {
        this.render('dashboard.index', {  // template to render
            controller:'dashboard.index'
        });
    }
});
