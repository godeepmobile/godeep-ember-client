import Ember from 'ember';

export default Ember.Route.extend({

	loadData: Ember.inject.service(),

	buildUploadProspectsURL: function(adapter) {
		return adapter.buildURL('TeamProspects') + '/upload';
	},

	actions: {
		uploadProspects: function(data) {
			var headers       = this.get('colHeaders')
			  , prospectsData = []
			  , adapter       = this.store.adapterFor('application')
			  , errorHandler  = function(error) {
					console.log('responseJSON ', error.responseJSON);
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.responseText){
						alert(error.responseText);
					}
				}.bind(this);

			// Mapping the prospects data
			data.forEach(function(record) {
				prospectsData.push({
					jerseyNumber      : record['Jersey No'],
					firstName         : record['First Name'],
					lastName          : record['Last Name'],
					height            : record['Height'],
					weight            : record['Weight'],
					currentPosition   : record['Current Position'],
					projectedPosition : record['Projected Position'],
					highSchool        : record['School/Team'],
					currentLevel      : record['Current Level'],
					positionST        : record['ST Position'],
					state             : record['State'],
					birthDate         : record['DOB']
				});
			});

			adapter.ajax(this.buildUploadProspectsURL(adapter), 'POST', {
				data : {
					prospects : prospectsData
				}
			}).then(function(response) {

				if (response && response.upload && response.upload.msg) {
					this.send('showModal', 'prospects/notification', response.upload.msg);
				} else {
					this.send('returnToProspectList');
				}

			}.bind(this));
		},

		returnToProspectList: function() {
			this.get('loadData').reload('prospects');
			this.send('removeModal');
			this.transitionTo('prospects').then(function() {
		  		Ember.run.later(this, function() {
        			this.send('temporalTooltip', 'tooltip/saved', {message: 'The prospect list was successfully updated'});
        		}, 1000);
		  	}.bind(this));
		},

		buildProspectsData: function() {
			this.get('controller').trigger('buildTableOutputData');
			this.send('removeModal');
		},

		displayUploadProspectsConfirmation: function() {
			this.send('showModal', 'prospects/confirm-upload');
		}
	}
});
