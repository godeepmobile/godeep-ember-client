import Ember from 'ember';

export default Ember.Route.extend({

	loadData: Ember.inject.service(),

	setupController: function(controller, model) {
		var formattedSeasons;
		this._super(controller, model);
        
        controller.set('seasons', this.get('loadData.seasons'));

        formattedSeasons = controller.get('formattedSeasons');

        controller.set('season', formattedSeasons.findBy('value', this.get('loadData.season')));
    }
});
