// app/routes/rank/player-rank.js

import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
    loadData: Ember.inject.service(),

    model: function() {
        return this.get('loadData.teamPlayerSeasonRankStats');
    },

    setupController: function(controller, model) {
        var rankBased = [
                  { id:'rankOverall', name:"Grades and Evaluations"},
                  //{ id:'overallGradeGameRank', name:"Grades"},
                  { id:'overallEvaluationRank', name:"Evaluations Only"}
              ],
            formattedSeasons;

        controller.set('model', this.getFilteredStats());

        controller.set('rankBased', rankBased);
        controller.set('rankBasedValue', rankBased[0].id);

        controller.set('seasons', this.get('loadData.seasons'));
        formattedSeasons = controller.get('formattedSeasons');

        Ember.run.schedule("afterRender", controller, function() {
            controller.set('season', formattedSeasons.findBy('value', this.get('loadData.season')));
            //this.send('refreshPlayerRanking');
        }.bind(this));

    },

    finishLoading: function() {
        //this.get('loadData').fetchTeamPlayerSeasonRankStat();
        this.set('controller.isUpdatingPlayerSeasonStats', false);

        //NOTE(carlos) to run on IE only, this is a workaround in order to avoid the refresh button's icon
        //to continue spinning after the animation class was removed, this force the icon to be redrawn on
        //this browser on particular
        if (/*@cc_on!@*/false || !!document.documentMode) {
            Ember.$('.btn-loading .glyphicon-refresh').css('fontStyle', 'normal');
        }
    },

    getFilteredStats: function() {
        var season = this.get('loadData.season');
        return this.store.filter('playerRankOverall', function(stat) {
            return stat.get('season') === season;
        });
    },

    actions: {
        didTransition: function() {
            this.send('selectRank', 'players');
            return true;
        },
        refreshPlayerRanking: function() {
            console.log('player-rank UpdateSeasonStats');

            var self       = this
              , adapter    = this.store.adapterFor('application')
              , url        = adapter.buildURL('PlayerRankOverall', 'updateSeasonRankingStats')
              , statsUrl   = adapter.buildURL('TeamPlayerSeasonStats', 'updateSeasonStats')
              , curentView = this.get('controller.rankBasedValue')
              , controller = this.get('controller');

            controller.set('model', []);
            this.set('controller.isUpdatingPlayerSeasonStats', true);

            adapter.ajax(url, 'POST', {
                data : {
                    season : self.get('loadData.season')
                }
            }).then(function() {
                console.log('player-rank UpdateSeasonStats finished');
                //self.get('loadData').fetchTeamPlayerSeasonRankStat();

                if (curentView === 'rankOverall' || curentView === 'overallGradeGameRank') {
                    adapter.ajax(statsUrl, 'POST', {
                        data : {
                            season : self.get('loadData.season')
                        }
                    }).then(function() {
                        console.log('dashboardUpdateSeasonTotals finished');
                        //self.get('loadData').reload('teamPlayerSeasonStats');
                        self.get('loadData').reload('teamPlayerSeasonRankStats');

                        controller.set('model', self.getFilteredStats());
                        self.finishLoading();
                    });
                } else {
                    controller.set('model', self.getFilteredStats());
                    self.finishLoading();
                }
            });
        },

        showPlayer: function(player) {
            this.transitionTo('player', player);
        }
    }
});
