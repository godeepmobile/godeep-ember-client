// app/routes/ranking.js

import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {

    loadData: Ember.inject.service(),

    model: function() {
        return this.get('loadData.prospectEvaluationRankOveralls');
    },

    setupController: function(controller, model) {
        var formattedSeasons
          , self = this;

        controller.set('model', this.getFilteredStats());

        controller.set('seasons', this.get('loadData.seasons'));
        formattedSeasons = controller.get('formattedSeasons');

        Ember.run.schedule("afterRender", controller, function() {
            controller.set('season', formattedSeasons.findBy('value', this.get('loadData.season')));
            //this.send('refreshProspectRanking');
        }.bind(this));

    },

    getFilteredStats: function() {
        var season = this.get('loadData.season');
        return this.store.filter('prospectEvaluationRankOverall', function(stat) {
            return stat.get('season') === season || stat.get('season') === null;
        });
    },

    actions: {
        didTransition: function() {
            this.send('selectRank', 'prospects');
            return true;
        },
        refreshProspectRanking: function() {
            var controller = this.get('controller');

            console.log('propect-rank UpdateSeasonStats');

            controller.set('model', []);

            var self      = this
                , adapter = this.store.adapterFor('application')
                , url = adapter.buildURL('ProspectEvaluationRankOverall', 'updateSeasonRankingStats');

            this.set('controller.isUpdatingProspectSeasonStats', true);

            adapter.ajax(url, 'POST', {
                data : {
                    season : self.get('loadData.season')
                }
            }).then(function() {
                console.log('propect-rank UpdateSeasonStats finished');
                self.get('loadData').fetchProspectEvaluationRankOverall();
                self.set('controller.isUpdatingProspectSeasonStats', false);

                controller.set('model', self.getFilteredStats());

                //NOTE(carlos) to run on IE only, this is a workaround in order to avoid the refresh button's icon
                //to continue spinning after the animation class was removed, this force the icon to be redrawn on
                //this browser on particular
                if (/*@cc_on!@*/false || !!document.documentMode) {
                    Ember.$('.btn-loading .glyphicon-refresh').css('fontStyle', 'normal');
                }
            });
        },

        showProspect: function(prospect) {
            this.transitionTo('prospect', prospect);
        }
    }

});
