// app/routes/game/index.js

import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';
import config from '../../config/environment';

export default Ember.Route.extend(FetchAllModelsMixin, {
    loadData: Ember.inject.service(),
    websockets: Ember.inject.service('socket-io'),

    afterModel: function(teamCutup) {
        var self        = this
          , teamEvent   = teamCutup.get('teamEvent')
          , userId      = this.get('session.user.id')
          , initialPlay = this.get('initialPlay')
          , cutupFilter = {
              filter : {
                where : {
                  teamCutup : teamCutup.get('id')
                }
              },
              order : ['playNumber']
            };

        if (initialPlay) {
          cutupFilter.filter.where['playNumber'] = initialPlay;
        }

        // flushing work vars
        this.set('lastGradedPlay', null);

        // Recovering plays and players associated with this event
        return Ember.RSVP.hash({
          //teamPlayers     : this.get('loadData.players'),
          playTypes       : this.get('loadData.playTypes'),
          playResultTypes : this.get('loadData.playResultTypes'),
          playFactors     : this.store.find('playFactor'),
          gradeStatus     : this.store.find('teamEventGradeStatus', {filter : {where : {teamCutup : teamCutup.get('id'), user : userId}}}),
          firstPlay       : this.store.find('teamPlay', cutupFilter), // The first should be recover with preference
          teamPlays       : this.fetchAllModelsNamed('teamPlay', {filter : { where : { teamCutup: teamCutup.get('id')}}}), // The other plays will be recovered on background
          teamPracticeDrillTypes : this.fetchAllModelsNamed('teamPracticeDrillType')
        }).then(function(hash) {
          //self.set('teamPlayers', hash.teamPlayers);
          self.set('firstPlay', hash.firstPlay.get('firstObject'));
          self.set('playResultTypes', hash.playResultTypes);
          self.set('playFactors', hash.playFactors);
          self.set('playTypes', hash.playTypes);
          self.set('gradeStatus', hash.gradeStatus.get('firstObject'));
          self.set('teamPracticeDrillTypes', hash.teamPracticeDrillTypes);
        });
    },

    setupController: function(controller, model, transition) {
      var teamEvent   = model.get('teamEvent')
        , currentPlay = this.get('firstPlay') || this.store.createRecord('teamPlay', {
            playNumber : 1,
            teamEvent  : teamEvent
          })
        , playerAssignments
        , setTeamPlayers = function() {
            var teamPlayers
              , positionTypes = this.get('session.positionTypes');

            // removing deactivated players, if they don't have already added grades on the cutup to grade
            playerAssignments = playerAssignments.reject(function(_playerAssignment) {
              if (_playerAssignment.get('temporarilyInactive')) {
                return !_playerAssignment.get('teamCutupsWithGrades').isAny('id', model.get('id')) || _playerAssignment.get('season') !== teamEvent.get('season'); // Note(Carlos) this will avoid other seasons players that were cached
              }
              return _playerAssignment.get('endDate') || _playerAssignment.get('season') !== teamEvent.get('season');
            });

            teamPlayers       = playerAssignments.getEach('teamPlayer');

            this.set('teamPlayers', teamPlayers);
            this.set('playerAssignments', playerAssignments);

            currentPlay.set('assignedPlayers', playerAssignments.filter(function(playerAssignment) {
              return positionTypes.isAny('id', playerAssignment.get('position1.id') + '') ||
                positionTypes.isAny('id', playerAssignment.get('position2.id') + '') ||
                positionTypes.isAny('id', playerAssignment.get('position3.id') + '') ||
                positionTypes.isAny('id', playerAssignment.get('positionST.id') + '');
            }));
            controller.set('teamPlayers', teamPlayers);
            controller.set('playerAssignments', playerAssignments);

            /*if (currentPlay.get('assignedPlayers').length === 0) {
              controller.set('noPlayersAssigned', true);
            }*/

            this.addGradedPlayersToList();

            controller.set('isLoading', false);
          }.bind(this);

      this._super(controller, model);

      controller.set('isLoading', true);

      this.getGrades(currentPlay.get('id'), function() {
//        currentPlay.set('assignedPlayers', this.get('session.teamPlayers'));

        controller.set('currentPlay', currentPlay);
        //controller.set('teamPlayers', this.get('teamPlayers'));
        controller.set('playFactors', this.get('playFactors'));
        controller.set('playResultTypes', this.get('playResultTypes'));

        // Flushing objects
        controller.set('previousPlay', null);
        controller.set('previousDrill', null);

        controller.set('playDataPanelStretched', true);

        playerAssignments = this.fetchAllModelsWithCallback('teamPlayerAssignment', setTeamPlayers, {filter :
          { where : { season: teamEvent.get('season')}}
        });

        //this.addGradedPlayersToList();
      }.bind(this));
    },

    addGradedPlayersToList: function() {
      var play            = this.get('controller.currentPlay')
        , assignedPlayers = play.get('assignedPlayers')
        , promises        = []
        , teamPlayer;
      play.get('grades').forEach(function(grade) {
        teamPlayer = grade.get('teamPlayer');
        if (!assignedPlayers.isAny('teamPlayer.id', teamPlayer.get('id'))) {
          this.addPlayerToGrade(this.get('playerAssignments').findBy('teamPlayer.id', teamPlayer.get('id')));
          //promises.push(this.store.find('teamPlayerAssignment', {filter : { where : { teamPlayer: teamPlayer.get('id')}}}));
        }
      }.bind(this));

      this.addPlayersToGrade();

      /*this.store.find('teamPlayerAssignment', {filter : { where : { teamPlayer: teamPlayersIds}}}, function(teamPlayerAssignments) {
        teamPlayerAssignments.forEach(function(playerAssignment) {
          // Adding player to the graded players on the play
          this.addPlayerToGrade(playerAssignment);
        });
        this.addPlayersToGrade();
      }.bind(this));*/

      /*Ember.RSVP.all(promises).then(function(teamPlayerAssignments) {
        teamPlayerAssignments.forEach(function(playerAssignment) {
          // Adding player to the graded players on the play
          this.addPlayerToGrade(playerAssignment);
        }.bind(this));
        this.addPlayersToGrade();
      }.bind(this));*/
    },

    currentPlayNumber: function() {
      return this.controller.get('currentPlay').get('playNumber');
    },

    changeCurrentPlay: function(playNumber) {
      var newPlay
        , currentPlay  = this.get('controller.currentPlay')
        , previousPlay = this.get('controller.previousPlay')
        , currentPlayHasGrades = currentPlay.get('grades.content.length') > 0
        , gradePlayers
        , newPlayAssignedPlayers
        , previousDrill;

      this.send('showModal', 'loadingDialog', {
        message : currentPlayHasGrades ? 'Saving grades ...' : 'Loading play data ...'
      });

      this.get('currentModel.teamPlays').then(function(teamPlays) {
        newPlay = teamPlays.findBy('playNumber', playNumber);
        if (newPlay) {

          // Saving previous grades and status
          //this.saveGradeAndStatus({
          this.saveGrades(function() {

              // Getting the grades from the new play
              this.getGrades(newPlay.get('id'), function() {

                // Once the grades are recovered proceed to render the page

                /*if (!newPlay.get('assignedPlayers')) {
                  gradePlayers = currentPlay.get('gradePlayers');
                  newPlayAssignedPlayers = currentPlay.get('assignedPlayers').toArray();

                  if (gradePlayers) {
                    this.removeDuplicatedPlayers(gradePlayers, newPlayAssignedPlayers);

                    if (previousPlay && previousPlay.get('addedPlayers')) {
                      this.removeDuplicatedPlayers(previousPlay.get('addedPlayers'), newPlayAssignedPlayers);
                    }

                    newPlayAssignedPlayers = gradePlayers.concat(newPlayAssignedPlayers);
                  }

                  newPlay.set('assignedPlayers', newPlayAssignedPlayers);
                }*/

                newPlay.set('assignedPlayers', currentPlay.get('assignedPlayers').toArray());

                this.controller.set('previousPlay', currentPlay);
                this.controller.set('currentPlay', newPlay);

                if (!this.controller.get('model.teamEvent.isMatch')) {
                  previousDrill = currentPlay.get('teamPracticeDrillType');
                  if (previousDrill.get('id')) {
                    this.controller.set('previousDrill', previousDrill);
                  }
                }

                this.addGradedPlayersToList();

                // Scrolling back to the top when changing the play
                Ember.$(window).scrollTop(0);

                this.send('removeModal');

                if (currentPlayHasGrades) {
                  this.send('temporalTooltip', 'tooltip/saved', {
                    message  : 'Your grades have been saved'
                  , cssClass : 'grades-saved-message'
                  });

                  //Ember.$('#gotooltip').addClass('grades-saved-message');
                }

              }.bind(this));

            }.bind(this));

        }
      }.bind(this));
    },

    removeDuplicatedPlayers: function(array1, array2) {
      var index;
      array1.forEach(function(player) {
        index = array2.indexOf(player);
        if (index !== -1) {
          array2.splice(index, 1);
        }
      });
    },

    addPlayerToGrade: function(teamPlayerAssignment) {
      var play       = this.get('controller.currentPlay')
        , isSelected = teamPlayerAssignment.get('isSelected');

      if (!teamPlayerAssignment.get('playsWhereAdded')) {
        // Initialiting the property
        teamPlayerAssignment.set('playsWhereAdded', Ember.Object.create());
      }

      teamPlayerAssignment.get('playsWhereAdded').set('play_' + play.get('id'), !isSelected);
    },

    addPlayersToGrade: function() {
      var play = this.get('controller.currentPlay');
      play.set('addedPlayers', this.get('playerAssignments').filterBy('playsWhereAdded.play_' + play.get('id')));
    },

    saveGrades: function(success, failure) {
      var currentPlay = this.get('controller.currentPlay')
        , grades      = currentPlay.get('grades.content')
        , adapter     = this.store.adapterFor('application')
        , url         = adapter.buildURL('TeamPlayerSeasonStats', 'updateSeasonStats')
        , season      = this.get('currentModel.teamEvent.season')
        , fail        = failure ? failure : this.handleFailure.bind(this);

      //saving play info
      //currentPlay.save().then(function() {
        if (grades.get('length') > 0) {

          grades.filterBy('isDisabled').forEach(function(grade) {
            grade.destroyRecord();
            grade.get('teamPlayer').then(function(teamPlayer) {
              if (teamPlayer.get('playsWhereAdded')) {
                teamPlayer.get('playsWhereAdded').set('play_' + currentPlay.get('id'), false);
              }
            });
          });

          this.set('lastGradedPlay', currentPlay);

          grades.setEach('teamPlay', currentPlay);
          grades.save().then(success, fail);
        } else {
          success();
        }
      //}.bind(this)).catch(fail);

    },

    saveGradeStatus: function(success, failure) {
      var gradeStatus     = this.get('gradeStatus')
        , now             = moment().toDate()
        , _lastGradedPlay = this.get('lastGradedPlay.playNumber');

      if (_lastGradedPlay) {

        if (!gradeStatus) {
          // There's no grade status for this user on this event
          this.set('gradeStatus', this.store.createRecord('teamEventGradeStatus', {
            teamEvent      : this.get('currentModel.teamEvent')
          , teamCutup      : this.get('currentModel')
          , user           : this.get('session.user.id')
          , team           : this.get('session.team')
          , startedDate    : now
          , lastSubmitDate : now
          , lastGradedPlay : _lastGradedPlay
          }));

          this.get('gradeStatus').save().then(success, failure);
        } else {
          // We already have a grade status
          gradeStatus.setProperties({
            lastSubmitDate : now
          , lastGradedPlay : _lastGradedPlay
          });

          gradeStatus.save().then(success, failure);
        }
      } else {
        success(gradeStatus);
      }

    },

    saveGradeAndStatus: function(callback) {
      var success    = callback.success || function() {}
        , failure    = callback.failure || this.handleFailure.bind(this)
        , saveStatus = function() {
            this.saveGradeStatus(success, failure);
          }.bind(this);

      this.saveGrades(saveStatus, failure);
    },

    handleFailure: function(reason) {
      alert('There was a problem saving the grades: ' + reason);
    },

    buildQueryParams: function(playNumber) {
      return {queryParams : {
        play : playNumber
      }};
    },

    getGrades: function (teamPlayId, callback) {
      this.store.find('grade', {filter: {
        where: {
          teamPlay : teamPlayId,
          user     : this.get('session.user.id')
        }
      }}).then(callback);
    },

    buildNotifyUpsertURL: function() {
      return this.store.adapterFor('application').buildURL('Grades', 'notifyUpsert');
    },

    actions: {
        didTransition: function() {
            var header = this.controllerFor('header');
            header.setPageName('Grade Players');
            header.setMessage2Left('');
            header.setMessage2Right('');
            return true; // Bubble the didTransition event
        },

        willTransition: function(transition) {
          if (!this.get('switchToMinimizedVersion')) {
            this.saveGradeAndStatus({
              failure : function(reason) {
                transition.abort();
                this.handleFailure(reason);
              }.bind(this)
            });
          } else {
            this.set('switchToMinimizedVersion', false);
          }
        },

        queryParamsDidChange: function(changed) {
          if (this.currentModel) {
            if (changed && changed.play) {
              if (changed.resumePlay) {
                this.set('initialPlay', changed.resumePlay);
              } else {
                if (changed.play) {
                  this.changeCurrentPlay(parseInt(changed.play));
                }
              }
            }
          } else {
            // Is the first time the page loads and includes the query parameter
            this.set('initialPlay', changed && changed.play ? changed.play : 1);
          }
        },

        goToNextPlay: function() {
          if (this.get('controller.hasNextPlay')) {
            this.transitionTo(this.buildQueryParams(this.get('controller.currentPlay.nextPlayNumber')));
          }
        },

        goToPrevPlay: function() {
          if (this.get('controller.hasPrevPlay')) {
            this.transitionTo(this.buildQueryParams(this.get('controller.currentPlay.prevPlayNumber')));
          }
        },

        goToPlay: function(playNumber) {
          if (playNumber) {
            this.transitionTo(this.buildQueryParams(playNumber));
          }
        },

        editCurrentPlay: function(currentPlay) {
          var editPlayController = this.controllerFor('game/play/edit')
            , previousPlay       = this.get('controller.previousPlay');

          editPlayController.set('playResultTypes', this.get('playResultTypes'));
          editPlayController.set('playTypes', this.get('playTypes'));
          editPlayController.set('model', currentPlay);
          editPlayController.set('teamPracticeDrillTypes', this.get('teamPracticeDrillTypes'));
          editPlayController.set('store', this.store);

          if (previousPlay) {
            editPlayController.set('previousPlay', previousPlay);
          }

          editPlayController.setupValues();

          this.send('showModal', 'game/play/edit');
        },

        pauseGrade: function() {
          var self = this;
       	  this.saveGradeStatus(function(gradeStatus) {
            self.saveGrades(function() {
              if (self.get('controller.minimizedVersion')) {

                if (gradeStatus && gradeStatus.get('id')) {
                  self.store.adapterFor('application').ajax(self.buildNotifyUpsertURL(), 'POST', {data: {
                    gradeStatus : gradeStatus.get('id'),
                    socketId    : self.get('controller.sid')
                  }}).then(function() {
                    window.close();
                  });
                } else {
                  window.close();
                }
              } else {
                if (gradeStatus) {
                  self.transitionTo('grade', gradeStatus);
                } else {
                  self.transitionTo('games');
                }
              }
            });
       	  });

        },

        selectPlayerToGrade: function(teamPlayerAssignment) {
          this.addPlayerToGrade(teamPlayerAssignment);
        },

        addPlayersToCurrentGrade: function(teamPlayersList) {
          this.addPlayersToGrade(teamPlayersList);
        },

        enterPlayData: function() {
          this.transitionTo('event.upload').then(function(uploadRoute) {
            uploadRoute.set('controller.newTeamCutup', this.get('controller.model'));
            uploadRoute.set('controller.returnToGrade', true);
            uploadRoute.set('controller.currentPlay', this.get('controller.currentPlay'));

            Ember.run.later(this, function() {
              this.send('temporalTooltip', 'tooltip/saved', {message: 'Your grades have been saved'});
            }, 1000);
          }.bind(this));
        },

        toggleInfoPanelSize: function() {
          this.get('controller').toggleProperty('infoPanelStretched');
        },

        togglePlayDataPanel: function() {
          this.get('controller').toggleProperty('playDataPanelStretched');
        },

        gradeInSmallWindow: function() {
          var teamCutupId = this.get('currentModel.id')
            , gradeStatus = this.get('gradeStatus')
            , play        = this.get('controller.currentPlay')
            , url         = window.location.origin +
              this.get('router.location.rootURL') +
              'game/' +
              teamCutupId +
              '?minimizedVersion=true&play=' +
              play.get('playNumber') + '&sid=' + this.get('websockets').socketFor(config.API.host).socket.id;

          this.set('switchToMinimizedVersion', true);

          window.open(url, 'small-grading-window', 'location=0,status=0,scrollbars=0,titlebar=0,menubar,width=490,height=768,left=' + (document.documentElement.clientWidth-490));

          //rolling back the grades
          play.get('grades.content').forEach(function(grade){
            grade.rollback();
          });

          if (gradeStatus && gradeStatus.get('id')) {
            this.transitionTo('grade', gradeStatus);
          } else {
            this.transitionTo('games');
          }
        }
    }
});

/*
player order starts with order from previous page, i.e. selected players from last play are automatically
first in current play

play data is not needed to start grading.

game grading can be paused and resumed

need logged in user to determine position group, i.e. which players / positions this user normally grades.

need change play type menu; highlight [play type] in yellow when play is changed from default

need player selection screen in order to "add player"

need player depth chart menu

need notes text area overlays

need grades for multiple players per play

need current game information

need toggle button

need player component that allows position from depth chart
 */
