import Ember from 'ember';

export default Ember.Route.extend({
    setupController: function (controller, context) {
        controller.set('model', context.organizations);
    },

    model: function() {
        //TODO:does not need to be a hash
        return Ember.RSVP.hash({
            organizations: this.store.find('organization')
        });
    }
});
