import Ember from 'ember';

export default Ember.Route.extend({
	actions: {
		goToGradePlayerPerformance: function() {
			this.transitionTo('games');
		},

		goToEnterNewCutup: function() {
			this.transitionTo('event.setup');
		},

		goToUploadVideoCutupData: function() {
			this.transitionTo('event.upload');
		},

		displayLearnMoreGradingPopup: function() {
			this.send('showModal', 'grade/learnMoreGrading');
		},

		displayLearnMorePlayDataPopup: function() {
			this.send('showModal', 'grade/learnMorePlayData');
		},

		displayLearnMoreVideoCutupPopup: function() {
			this.send('showModal', 'grade/learnMoreVideoCutup');
		}
	}
});
