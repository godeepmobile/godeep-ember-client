import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
	loadData: Ember.inject.service(),

	model: function() {
		return Ember.RSVP.hash({
			teamEvents    : this.fetchAllModelsNamed('teamEvent', {"order":["date DESC"]})
		  , organizations : this.fetchAllModelsNamed('organization')
		  , states        : this.get('loadData.states')
		  , platoonTypes  : this.get('loadData.platoonTypes')
		  , teamCutups    : this.store.find('teamCutup', {limit:1})
		});
	},

	afterModel: function() {
		return this.store.find('eventType').then(function(eventTypes) {
			this.set('eventTypes', eventTypes);
		}.bind(this));
	},

	setupController: function(controller, context) {
		var gameEventType
		  , practiceEventType
		  , practiceGameEventType
		  , eventTypes = this.get('eventTypes');

		this._super(controller, context);
		
		controller.set('validationErrors', {
			teamEvent: {
				name: false,
				date: false,
				eventType: false
			},
			//platoonType: false,
			numPlays: false,
			awayTeam: false,
			homeTeam: false
		});

		//Clearing form
		//controller.set('numPlays', null);
		//controller.set('platoonType', null);
		controller.set('homeTeam', null);
		controller.set('awayTeam', null);
		controller.set('homeScore', null);
		controller.set('awayScore', null);
		controller.set('isEdit', null);
		controller.set('isAddingPlatoon', null);

		this.set('newAwayTeam', null);
		this.set('newHomeTeam', null);
		this.set('practiceGameTeam1', null);
		this.set('practiceGameTeam2', null);

		gameEventType     = eventTypes.findBy('isGame');
		practiceEventType = eventTypes.findBy('isRegularPractice');
		practiceGameEventType = eventTypes.findBy('isPracticeGame');
		
		controller.set('gameEventType', gameEventType);
		controller.set('practiceEventType', practiceEventType);
		controller.set('practiceGameEventType', practiceGameEventType);
		controller.set('eventTypes', eventTypes);

		//setting an event of game event type as default
		controller.set('teamEvent', this.store.createRecord('teamEvent', {
			eventType : gameEventType
		}));

		/*if (!controller.get('isEdit')) {
			controller.set('teamEvent.eventType', gameEventType);
		}*/
	},

	buildUpdateConfigurationURL: function() {
		return this.store.adapterFor('application').buildURL('teamConfiguration', this.get('session.teamConfiguration.id')) + '/updateSettings';
	},

	actions: {
		cancelEventSetup: function() {
			var teamCutups = this.get('currentModel.teamCutups');

			this.transitionTo(teamCutups && teamCutups.get('firstObject') ? 'games' : 'grading');
		},

		setupEvent: function(uploadDataNow) {
			var teamEvent  = this.get('controller.teamEvent')
			  , basedEvent = teamEvent.get('basedEvent')
			  , teamId     = this.get('session.team.organization.id')
			  , homeTeam   = this.get('controller.homeTeam')
			  , homeScore  = this.get('controller.homeScore')
			  , awayTeam   = this.get('controller.awayTeam')
			  , awayScore  = this.get('controller.awayScore')
			  , isGame     = teamEvent.get('eventType.isMatch')
			  , newOrganization = this.get('newAwayTeam') || this.get('newHomeTeam')
			  , practiceGameTeam1 = this.get('practiceGameTeam1')
			  , practiceGameTeam2 = this.get('practiceGameTeam2')
			  , _saveTeamEvent  = function(teamEventToSave) {
			  		if (newOrganization) {
						this.store.createRecord('organization', {
							name             : newOrganization,
							shortName        : newOrganization,
							organizationType : this.get('session.team.organization.organizationType'),
							conference       : this.get('session.team.organization.conference')
						}).save().then(function(opponentOrganization) {
							teamEventToSave.set('opponent', opponentOrganization);
							this.send('saveTeamEvent', teamEventToSave);
						}.bind(this));
					} else {
						this.send('saveTeamEvent', teamEventToSave);
					}
			  	}.bind(this)
			  , isHomeGame
			  , properties;

			this.set('uploadDataAfterSave', uploadDataNow);

			if (teamEvent.get('eventType.isPracticeGame')) {

				teamEvent.set('practiceGameTeam1', practiceGameTeam1);
				teamEvent.set('opponent', null);
				
				if (practiceGameTeam1) {
					awayTeam = null;
				} else {
					if (awayTeam && awayTeam.get('id') !== teamId) {
						teamEvent.set('opponent', awayTeam);
					}
				}

				teamEvent.set('practiceGameTeam2', practiceGameTeam2);
				if (practiceGameTeam2) {
					homeTeam = null;
				} else {
					if (homeTeam && homeTeam.get('id') !== teamId) {
						teamEvent.set('opponent', homeTeam);
					}
				}

				newOrganization = null;
			} else {
				teamEvent.set('practiceGameTeam1Score', null);
				teamEvent.set('practiceGameTeam2Score', null);
			}

			if(isGame && ((homeTeam && awayTeam) || newOrganization)) {
				isHomeGame = homeTeam && homeTeam.get('id') === teamId;
				teamEvent.set('score', isHomeGame ? homeScore : awayScore);
				teamEvent.set('opponent', isHomeGame ? awayTeam : homeTeam);
				teamEvent.set('opponentScore', isHomeGame ? awayScore : homeScore);

				teamEvent.set('isHomeGame', isHomeGame);
			}

			if (teamEvent.get('isBasedOnAnEvent')) {
				if (teamEvent.get('name') !== basedEvent.get('name')) {
					// Event name changed, prompt if create a new event or not
					this.send('showModal', 'event/confirm', teamEvent);
				} else {
					// This is a promise so we need to resolve it first
					basedEvent.then(function(model) {

						properties = teamEvent.toJSON();
						properties['eventType'] = teamEvent.get('eventType');
						if (isGame) {
							properties['opponent']  = teamEvent.get('opponent');
							properties['state']     = teamEvent.get('state');
						}

						// This relation is not required on the team event
						delete properties.basedEvent;

						model.setProperties(properties);

						// Destroying no longer valid new event
						teamEvent.deleteRecord();

						//this.send('saveTeamEvent', model);
						_saveTeamEvent(model);
					}.bind(this));
				}
			} else {
				//this.send('saveTeamEvent', teamEvent);
				_saveTeamEvent(teamEvent);
			}

		},

		changeTeamEvent: function(basedTeamEvent) {
			var controller    = this.get('controller')
			  , teamEvent     = controller.get('teamEvent')
			  , properties    = basedTeamEvent.toJSON()
			  , isHomeGame    = basedTeamEvent.get('isHomeGame')
			  , team          = this.get('session.team.organization')
			  , opponent      = basedTeamEvent.get('opponent')
			  , opponentTeam  = controller.get('model.organizations').findBy('id', opponent.get('id'))
			  , score         = basedTeamEvent.get('score')
			  , opponentScore = basedTeamEvent.get('opponentScore');

			// Setting same relationships
			properties['eventType'] = basedTeamEvent.get('eventType');
			properties['opponent']  = opponent;
			properties['state']     = basedTeamEvent.get('state');

			// Cloning the event
			properties['basedEvent'] = basedTeamEvent;
			teamEvent.setProperties(properties);

			controller.set('homeTeam', isHomeGame ? team : opponentTeam);
			controller.set('awayTeam', isHomeGame ? opponentTeam : team);

			controller.set('homeScore', isHomeGame ? score : opponentScore);
			controller.set('awayScore', isHomeGame ? opponentScore : score);
		},

		changeTeamEventName: function(eventName) {
			this.set('controller.teamEvent.name', eventName);
		},

		saveTeamEvent: function(teamEvent) {
			var successTransition
			  , cutups  = teamEvent.get('teamCutups')
			  , self    = this
			  , date
			  , year
			  , adapter = this.store.adapterFor('application');

			function errorHandler(error){
				console.log('responseJSON ', error.responseJSON);
				if (error.responseJSON) {
					alert(error.responseJSON.error.message);
				} else if (error.responseText){
					alert(error.responseText);
				}
			}

			successTransition = function(teamCutup) {
		  		/*if (this.get('uploadDataAfterSave') && teamCutup) {
					this.transitionTo('event.upload').then(function(uploadRoute) {
						// Sending the new created cutup to the upload controller,
						// in order to prepopulate it into the form
						uploadRoute.set('controller.newTeamCutup', teamCutup);
					}, self.errorHandler);
				} else {*/
					this.transitionTo('games');
				//}
		  	}.bind(this);

		  	if (this.get('session.teamConfiguration.createCutupForEachPlatoon')) {

		  		cutups.addObject(this.store.createRecord('teamCutup', {
				    whichPlatoon : this.get('currentModel.platoonTypes').findBy('isOffense')
				  , teamEvent    : teamEvent
				}));

				cutups.addObject(this.store.createRecord('teamCutup', {
				    whichPlatoon : this.get('currentModel.platoonTypes').findBy('isDefense')
				  , teamEvent    : teamEvent
				}));

				cutups.addObject(this.store.createRecord('teamCutup', {
				    whichPlatoon : this.get('currentModel.platoonTypes').findBy('isSpecialTeams')
				  , teamEvent    : teamEvent
				}));

		  	} else {
		  		cutups.addObject(this.store.createRecord('teamCutup', {
				    whichPlatoon : this.get('currentModel.platoonTypes').findBy('isAllPlatoon')
				  , teamEvent    : teamEvent
				}));
		  	}

		  	// Calculating season (from March to super bowl (February) belongs to the same season)
		  	date = moment.utc(teamEvent.get('date'));
		  	year = date.get('year');

		  	if (date.get('month') < 2) {
		  		// Date on January or February (super bowl)
		  		year -= 1;
		  	}

			teamEvent.set('season', year);

			teamEvent.save()
					.then(function(_teamEvent) {
						_teamEvent.set('isPractice', _teamEvent.get('eventType.isPractice'));
						_teamEvent.set('isMatch', _teamEvent.get('eventType.isMatch'));
						if (this.get('controller.isEdit')) {
							successTransition();
						} else {
								adapter.ajax(this.buildUpdateConfigurationURL(), 'PUT', {
									data : {
										createCutupForEachPlatoon : this.get('session.teamConfiguration.createCutupForEachPlatoon')
									}
								}).then(function() {
									cutups.then(function(_cutups) {
										_cutups.save().then(successTransition, errorHandler);
									});
								});
						}
						
					}.bind(this), errorHandler);
		},

		willTransition: function(transition) {
			if (!this.get('controller.teamEvent.id')) {
				this.store.deleteRecord(this.get('controller.teamEvent'));
			}
		},

		addAwayTeam: function(newAwayTeam) {
			this.set('newAwayTeam', newAwayTeam);
			if (newAwayTeam) {
				this.set('controller.homeTeam', this.get('session.team.organization'));
			}
			this.set('controller.awayTeam', null);
			this.set('newHomeTeam', null);
		},

		addHomeTeam: function(newHomeTeam) {
			this.set('newHomeTeam', newHomeTeam);
			if (newHomeTeam) {
				this.set('controller.awayTeam', this.get('session.team.organization'));
			}
			this.set('controller.homeTeam', null);
			this.set('newAwayTeam', null);
		},

		setPracticeGameTeam1: function(practiceGameTeam1) {
			this.set('practiceGameTeam1', practiceGameTeam1 && practiceGameTeam1.length > 0 ? practiceGameTeam1 : null);
		},

		setPracticeGameTeam2: function(practiceGameTeam2) {
			this.set('practiceGameTeam2', practiceGameTeam2 && practiceGameTeam2.length > 0 ? practiceGameTeam2 : null);
		}
	}
});
