import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {

	model: function() {
		return this.fetchAllModelsNamed('teamCutup');
	},

	setupController: function(controller, model) {

		this._super(controller, model);

		//Clearing form
		controller.set('teamCutup', null);
		controller.set('uploadStatus', null);
		controller.set('file', null);
		controller.set('fileName', null);
		controller.set('newTeamCutup', null);
		controller.set('tableHeaders', null);
		controller.set('playNumberHeader', null);
		controller.set('quarterHeader', null);
		controller.set('downHeader', null);
		controller.set('distanceHeader', null);
		controller.set('yardLineHeader', null);

		controller.set('errors', {
			teamCutup        : false,
			//file             : false,
			playNumberHeader : false,
			quarterHeader    : false,
			downHeader       : false,
			distanceHeader   : false,
			yardLineHeader   : false
		});
	},

	buildUploadURL: function() {
		return this.store.adapterFor('application').buildURL('storages', 'files') + '/upload';
	},

	showRequiredMessage: function() {
        Ember.$('.message').delay(400).fadeIn(400);
    },

    hideRequiredMessage: function() {
    	Ember.$('.message').hide();
    },

    buildSaveUploadURL: function(adapter, teamCutupId) {
    	return adapter.buildURL('TeamCutups', teamCutupId) + '/playsData';
    },

	actions: {
		setVideoCutupData: function(file) {
			this.set('controller.fileName', file.get('name'));
			this.set('file', file);
		},

		uploadVideoCutupData: function() {
			var teamCutup = this.get('controller.newTeamCutup') || this.get('controller.teamCutup')
			  , file      = this.get('file');


			if (teamCutup && file) {
				this.set('controller.errors', {
					teamCutup : false,
					file      : false
				});

				this.hideRequiredMessage();

				file.upload(this.buildUploadURL(), {
					headers : {
						'Authorization' : this.get('session.token')
					},
					data : {
						dataType  : teamCutup.get('teamEvent.isMatch') ? 'team_play' : 'team_play_practice',
						teamCutup : teamCutup.get('id')
					}
				}).then(function(response) {
					this.set('controller.uploadStatus', 'The play data was successfully saved');
					this.send('removeModal');
					//teamCutup.reload();
				}.bind(this), function(res) {
					var error = JSON.parse(res.response).error;
					this.set('controller.uploadStatus', 'There was an error trying to upload the play data. Error ' + error.status + ': ' + error.message);
					this.send('removeModal');
				}.bind(this));

				this.send('showModal', 'file-loading');
			} else {
				this.set('controller.errors', {
					teamCutup : !teamCutup,
					file      : !file
				});

				this.showRequiredMessage();
			}
			
		},

		videoCutupHeadersChanged: function(tableHeaders) {
			var controller    = this.get('controller')
			  , fieldsHeaders = controller.get('playDataFieldsHeaders');

			this.set('controller.tableHeaders', tableHeaders);

			if (fieldsHeaders) {
				// Some play data were saved before for a cutup on this platoon type
				controller.set('playNumberHeader', tableHeaders.findBy('value', fieldsHeaders.playNumber));
				controller.set('quarterHeader', tableHeaders.findBy('value', fieldsHeaders.quarter));
				controller.set('downHeader', tableHeaders.findBy('value', fieldsHeaders.down));
				controller.set('distanceHeader', tableHeaders.findBy('value', fieldsHeaders.distance));
				controller.set('yardLineHeader', tableHeaders.findBy('value', fieldsHeaders.yardLine));
			}
		},

		saveUploadData: function(videoCutupData) {
			var controller            = this.get('controller')
			  , teamCutup             = controller.get('newTeamCutup') || controller.get('teamCutup')
			  , requiredFieldsHeaders = controller.get('requiredFieldsHeaders');

			controller.set('errors', {
				teamCutup        : !teamCutup,
				playNumberHeader : !requiredFieldsHeaders.playNumber,
				quarterHeader    : !requiredFieldsHeaders.quarter,
				downHeader       : !requiredFieldsHeaders.down,
				distanceHeader   : !requiredFieldsHeaders.distance,
				yardLineHeader   : !requiredFieldsHeaders.yardLine
			});

			if (!controller.get('hasErrors')) {

				this.set('videoCutupData', videoCutupData);

				if (controller.get('changeHeaders')) {
					if (!controller.get('playDataFieldsHeaders')) {
						// There wasn't any headers before
						this.send('savePlayData', true);
					} else {
						// There was headers before but were changed by the user
						this.send('showModal', 'event/confirm-play-data-header-change');
					}
				} else {
					this.send('savePlayData');
				}
			}
		},

		savePlayData: function(_changeHeaders) {
			var adapter               = this.store.adapterFor('application')
			  , videoCutupData        = this.get('videoCutupData')
			  , requiredFieldsHeaders = this.get('controller.requiredFieldsHeaders')
			  , teamCutup             = this.get('controller.newTeamCutup') || this.get('controller.teamCutup')
			  , saveConfirmation      = function() {
			  		Ember.run.later(this, function() {
            			this.send('temporalTooltip', 'tooltip/saved', {message: 'The play data was successfully saved'});
            		}, 1000);
			  	}.bind(this)
			  , errorHandler          = function(error) {
	            	this.set('videoCutupData', null);
					console.log('responseJSON ', error.responseJSON);
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.responseText){
						alert(error.responseText);
					}
				}.bind(this);

			adapter.ajax(this.buildSaveUploadURL(adapter, teamCutup.get('id')), 'POST', {
				data : {
					playsData     : videoCutupData,
					headers       : requiredFieldsHeaders,
					changeHeaders : _changeHeaders
				}
			}).then(function(response) {

				this.set('videoCutupData', null);

				if (this.get('controller.returnToGrade')) {
					// Reloading current play in order to avoid data conflicts
					this.get('controller.currentPlay').reload().then(function() {
						this.transitionTo('game.index', teamCutup, {
		                    queryParams: {
		                        play : this.get('controller.currentPlay.playNumber') || 1
		                    }
		                }).then(saveConfirmation);
					}.bind(this), errorHandler);
	            } else {
	            	this.transitionTo('games').then(saveConfirmation);
	            }

            }.bind(this), errorHandler);
		}
	}
});
