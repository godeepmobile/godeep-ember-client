// app/routes/reports.js

import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    loadData: Ember.inject.service(),
    report: null,
    params: null,

    CRITERIA: {
      platoon       : 'platoon',
      positionGroup : 'position_group',
      players       : 'players'
    },

    SCOPE: {
      event    : 'event',
      season   : 'season',
      period   : 'period',
      partGame : 'part_game'
    },

    REPORT_TYPES_TITLES: {
      gamePerformance: 'Game Performance',
      gameGroupReview: 'Group Game Film Review',
      playerComparison: 'Player Comparison',
      gameDownDistanceAnalysis: 'Down and/or Distance Analysis',
      gameFieldPositionAnalysis: 'Field Position Analysis (Red Zone, etc)',
      gamePlayCallPersonnelAnalysis: 'Play Call, Personnel, or Play Data'
    },

    setupController: function (controller, model) {
        controller.set('model', model);
        controller.set('savedReports', this.get('savedReports'));

        /*this.send('setBreadcrumbs', {
          thisPage    : 'Player Performance Reports',
          breadcrumbs : []
        });*/
    },
    beforeModel(transition) {
        //console.log('reports.route: beforeModel, transition to ', transition.targetName);
        return this.get('loadData.profile');
    },
    model: function() {

        var seasons = [];

        this.get('loadData.seasons').forEach(function(season) {
          seasons.push({
            id: season,
            name: season
          });
        });

        return {
            reportTypes:[
                //{ id:null,         name:''},
                { id:'game',       name:"Game Performance"}
                // NOTE(carlos) Temporaly disabling these report types cause they don't have any work done for them
                /*{ id:'third down', name:"Third Down Performance"},
                { id:'red zone',   name:"Red Zone Performance"}*/
            ],
            games: this.get('loadData.events'),
            playerAssignments: this.get('loadData.playerAssignments'),
            groups: this.get('loadData.teamPositionGroups'),
            platoons: this.get('loadData.platoonTypesWithoutAll'),
            eventTypes: this.get('loadData.eventTypes'),
            seasons: seasons,
            CRITERIA: this.get('CRITERIA'),
            SCOPE: this.get('SCOPE'),
            REPORT_TYPES_TITLES: this.get('REPORT_TYPES_TITLES')
        };
    },

    afterModel: function() {
      return this.store.find('teamReport').then(function(savedReports) {
          this.set('savedReports', savedReports);
      }.bind(this));
    },

    actions: {
        error: function(reason) {
            console.log('reports.route got error %o', reason);
            //alert(reason.statusText); // "FAIL"
            //window.history.back();
            return true;    // let the global error handler see this error
        },
        loading: function(transition, originRoute) {
              transition.then(function(){
                  //console.log('reports.route: loading transition to %s complete', transition.targetName);
                  switch(transition.targetName) {
                      case 'reports.gamePerformance':
                        this.controller.set('report', 'game');
                      break;
                      case 'reports.thirdDownPerformance':
                        this.controller.set('report', 'third down');
                      break;
                      default:
                        this.controller.set('report', null);
                      break;
                  }
              }.bind(this),function(error){
                console.log('reports.route loading got error %o', error);
              }.bind(this));

              // Return true to bubble this event to ApplicationRoute
              return true;
        },
        setBreadcrumbs: function(newCrumbs) {
            this.controller.set('thisPage', newCrumbs.thisPage);
            this.controller.set('breadcrumbs', newCrumbs.breadcrumbs);
        },
        setCurrentReport(value) {
            //console.log('reports.route: setCurrentReport(%s)', value);
            if (this.controller.get('report') !== value) {
                this.controller.set('report', value);
            }
        },
      reportChanged: function(value) {
        //console.log('reports.route: reportChanged to %s', value);
        switch(value) {
          case "game":
            this.transitionTo('reports.gamePerformance');
            /*this.transitionTo('reports.gamePerformance', {queryParams: {
              player: null,
              platoon: null,
              group: null,
              game: null,
              season: null
            }});*/
            break;
          case "third down":
            this.transitionTo('reports.thirdDownPerformance');
            break;
          default:
            this.transitionTo('reports.index');
            break;
        }
      },

      cancelReport: function() {
        this.transitionTo('reports.index');
      },

      buildReportParams: function(reportType, controller) {
        var CRITERIA       = this.get('CRITERIA'),
            SCOPE          = this.get('SCOPE'),
            criteria       = controller.get('selectedCriteria'),
            scope          = controller.get('selectedScope'),
            display        = controller.get('selectedDisplay'),
            condition      = controller.get('condition'),
            conditionRange = controller.get('conditionRange'),
            params         = {type : reportType, filter: controller.get('selectedFilter')},
            errors         = {},
            criteriaRange,
            scopeRange,
            displayRange;

        switch (criteria) {
            case CRITERIA.platoon :
                criteriaRange = controller.get('selectedPlatoon.id') || controller.get('selectedPlatoon');
                break;
            case CRITERIA.positionGroup :
                criteriaRange = controller.get('selectedGroup.id') || controller.get('selectedGroup');
                break;

            case CRITERIA.players :
                criteriaRange = controller.get('playerAssignmentsForSeason').filterBy('selectedForReport').getEach('teamPlayer.id').join(',');
                break;
        }

        params['criteria']      = criteria;
        params['criteriaRange'] = criteriaRange;

        switch (scope) {
            case SCOPE.event :
                scopeRange = controller.get('selectedGame.id');
                break;
            case SCOPE.season :
                scopeRange = controller.get('selectedSeason.id');
                break;
            case SCOPE.period :
                scopeRange = controller.get('fromDate').format('MM/DD/YYYY') + ',' +
                    controller.get('toDate').format('MM/DD/YYYY');
                break;
            case SCOPE.partGame :
                if (controller.get('selectedGame.id') && controller.get('initPlay') && controller.get('toPlay')) {
                  scopeRange = controller.get('selectedGame.id') + ',' + controller.get('initPlay') + ',' + controller.get('toPlay');
                } else {
                  errors['scope'] = true;
                }
                break;
        }

        params['condition']      = condition;
        params['conditionRange'] = conditionRange;

        params['scope']      = scope;
        params['scopeRange'] = scopeRange;

        if (!params.criteria || !params.criteriaRange) {
          errors['criteria'] = true;
        }

        if (!params.scope || !params.scopeRange) {
          errors['scope'] = true;
        }

        if (!params.filter) {
          errors['filter'] = true;
        }

        if (errors && (errors.criteria || errors.scope || errors.filter)) {
          controller.set('errors.criteria', errors.criteria);
          controller.set('errors.filter', errors.filter);
          controller.set('errors.scope', errors.scope);
        } else {

          if (!(scope === SCOPE.period && controller.get('errors.season'))) {
            this.send('goToReport', params, null, controller.get('teamReport'));
          }

        }
      },

      populateTeamReport: function(teamReport, controller) {
        var CRITERIA       = this.get('CRITERIA'),
            SCOPE          = this.get('SCOPE'),
            criteria       = teamReport.get('criteria'),
            scope          = teamReport.get('scope'),
            condition      = teamReport.get('condition'),
            conditionRange = teamReport.get('conditionRange'),
            criteriaRange  = teamReport.get('criteriaRange'),
            scopeRange     = teamReport.get('scopeRange'),
            selectedPlayers,
            periodDates,
            partialGame,
            dnDist,
            fields,
            dataField,
            playDataField,
            playDataFields = Ember.A([]);

        switch (criteria) {
            case CRITERIA.platoon :
                controller.set('selectedPlatoon', this.get('currentModel.platoons').findBy('id', criteriaRange));
                break;
            case CRITERIA.positionGroup :
                controller.set('selectedGroup', this.get('currentModel.groups').findBy('id', criteriaRange));
                break;
            case CRITERIA.players :
                selectedPlayers = criteriaRange.split(',');
                controller.get('players').forEach(function(player) {
                  if (selectedPlayers.indexOf(player.get('id')) !== -1) {
                    player.set('selectedForReport', true);
                  }
                });
                break;
          }

        switch (scope) {
            case SCOPE.event :
                controller.set('selectedGame', this.get('currentModel.games').findBy('id', scopeRange));
                break;
            case SCOPE.season :
                controller.set('selectedSeason', this.get('currentModel.seasons').findBy('id', scopeRange));
                break;
            case SCOPE.period :
                periodDates = scopeRange.split(',');
                controller.set('fromDate', periodDates[0]);
                controller.set('toDate', periodDates[1]);
                break;
            case SCOPE.partGame :
                partialGame = scopeRange.split(',');
                controller.set('selectedGame', this.get('currentModel.games').findBy('id', partialGame[0]));
                controller.set('initPlay', parseInt(partialGame[1]));
                controller.set('toPlay', parseInt(partialGame[2]));
                break;
        }

        switch (condition) {
            case 'playerToCompare' :
              controller.set('playerToCompare', controller.get('players').findBy('id', conditionRange));
              break;

            case 'downDistance' :
              dnDist = conditionRange.split(',');

              controller.set('selectedDown', parseInt(dnDist[0]));
              controller.set('selectedDistance', controller.get('distances').findBy('value', dnDist[1]));

              break;

            case 'fieldPosition' :
              fields = conditionRange.split(',');

              controller.set('initField', controller.get('fields').findBy('value', parseInt(fields[0])));
              controller.set('endField', controller.get('fields').findBy('value', parseInt(fields[1])));

              break;

            case 'playDataFields' :
              conditionRange.split(',').forEach(function(dataFieldString, index) {

                dataField = dataFieldString.split('=');

                playDataField = controller.get('fields').findBy('key', dataField[1]);

                playDataFields.addObject({
                  field: playDataField, value: dataField[2], inclusion: dataField[0], inclusionName: 'inclusion_' + index
                });
              });

              controller.set('playDataFields', playDataFields);

              break;
        }

        controller.set('selectedFilter', teamReport.get('eventType.id'));
      },

      goToGenerateReport: function(report) {
          this.transitionTo('reports.' + report).then(function(reportRoute) {
            if (reportRoute.resetForm) {
              reportRoute.resetForm();
            }
          });
      },

      goToReport: function(params, teamReport, teamReportToBeUpdated) {
        if (params) {
          this.transitionTo('reports.view', {queryParams: params}).then(function(viewRoute) {
            if (teamReport && viewRoute.routeName) { //TODO(carlos): this control needs improvement, if we're transition only by changing query parameters then the teamReport model will not be changed
              viewRoute.set('controller.teamReport', teamReport);
            }

            if (teamReportToBeUpdated && viewRoute.routeName) {
              viewRoute.set('controller.teamReportToBeUpdated', teamReportToBeUpdated);
            }
          }.bind(this));

        }
      },

      runTeamReport: function(teamReport) {
        var params;
        if (teamReport) {

          teamReport.get('type').then(function(reportType) {
            params = teamReport.toJSON();

            params['type']   = reportType.get('path');
            params['filter'] = teamReport.get('eventType.id');

            // deleting non required values
            delete params.user;
            delete params.eventType;
            delete params.name;

            this.send('goToReport', params, teamReport);
          }.bind(this));
        }
      },

      toggleReportSize: function() {
        this.get('controller').toggleProperty('reportExpanded');

        // Resizing the chart after the containers (a delay of less than quarter second will be enough)
        Ember.run.later(this, function() {
            var reportWrapper = Ember.$('.highcharts-wrapper');

            if (reportWrapper.length > 0) {
              reportWrapper.highcharts().reflow();
            }
        }, 100);
      }
    } // actions
});
