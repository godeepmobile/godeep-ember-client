import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {

    model: function(params) {
		return this.store.findById('teamCutup', params.game_id).then(function(teamCutup) {
			return teamCutup.get('teamEvent').then(function() {
				return teamCutup;
			});
		});
    }
});
