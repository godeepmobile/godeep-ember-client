import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
	loadData: Ember.inject.service(),

	model: function() {

		var self = this;
        return  Ember.RSVP.hash({
            importances              : this.get('loadData.importances'),
            teamPositionImportances  : this.fetchAllModelsNamed('teamPositionImportance'),
            teamScoutingEvalCriteria : this.fetchAllModelsNamed('teamScoutingEvalCriteria', {
                filter: {
                    where: {
                        team: this.get('session.team.id')
                    }
                }
            }),
            teamPositionGroups : this.store.find('teamPositionGroup', {
                filter: {
                    where: {
                        endDate: 'null'
                    }
                }
            })
            .then(function (teamPositionGroups) {
                teamPositionGroups.forEach(function (teamPositionGroup) {
                    var teamPositionTypes = self.store.find('teamPositionType', {
                        filter: {
                            where: {
                                teamPositionGroup: teamPositionGroup.get('id'),
                                platoonType: teamPositionGroup.get('platoonType'),
                                endDate: 'null',
                                positionType : {nin: [25,27,29]}
                            }
                        }
                    });
                    teamPositionGroup.set('teamPositionTypes', teamPositionTypes);
                });
                return teamPositionGroups;
            })
        });

	},

    buildVerifyEvaluationsURL: function(adapter, positionTypeId) {
        return adapter.buildURL('PositionTypes', positionTypeId) + '/hasEvaluations';
    },

    actions: {
        displayEvaluationCriteriaSetup: function(teamPositionType) {
            var criteriaSetupController = this.controllerFor('evaluation/criteria/setup')
              , positionTypeId          = teamPositionType.get('positionType.id')
              , adapter                 = this.store.adapterFor('application');

            criteriaSetupController.clearForm();

            adapter.ajax(this.buildVerifyEvaluationsURL(adapter, positionTypeId), 'GET').then(function(reponse) {

                this.store.filter('teamScoutingEvalCriteria', function(criteria){
                    return criteria.get('positionType') === parseInt(positionTypeId);
                }).then(function(teamScoutingEvalCriteriasByPosition) {

                    this.send('showModal', 'evaluation/criteria/setup', {
                        criteriaHasEvaluations    : reponse.hasEvaluations,
                        positionType              : teamPositionType.get('positionType'),
                        importances               : this.get('currentModel.importances'),
                        teamScoutingEvalCriterias : teamScoutingEvalCriteriasByPosition,
                        teamPositionImportance    : this.get('currentModel.teamPositionImportances').findBy('positionType.id', positionTypeId)
                    });

                }.bind(this));

            }.bind(this));
        }
    }
});
