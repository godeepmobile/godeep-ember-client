import Ember from 'ember';

export default Ember.Route.extend({

	offline: Ember.inject.service(),

	addPlayerToGrade: function(teamPlayerAssignment) {
		var play   = this.get('controller.currentPlay')
	  , isSelected = teamPlayerAssignment.get('isSelected');

		if (!teamPlayerAssignment.get('playsWhereAdded')) {
			// Initialiting the property
    		teamPlayerAssignment.set('playsWhereAdded', Ember.Object.create());
  		}

		teamPlayerAssignment.get('playsWhereAdded').set('play_' + play.get('id'), !isSelected);
    },

    addPlayersToGrade: function() {
    	var play = this.get('controller.currentPlay');
    	play.set('addedPlayers', this.get('controller.playerAssignments').filterBy('playsWhereAdded.play_' + play.get('id')));
    },

    saveGradesOnMemory: function() {
        var currentPlay = this.get('controller.currentPlay')
          , gradeStatus = this.get('controller.gradeStatus')
          , offline     = this.get('offline');

        currentPlay.get('grades').forEach(function(grade) {
            offline.addToGradesQueue(grade);
        });

        gradeStatus.setProperties({
            lastSubmitDate : moment().toDate()
          , lastGradedPlay : currentPlay.get('playNumber')
        });

        offline.addToTeamEventGradeStatusQueue(gradeStatus);

        this.get('offline').check();
    },

	actions: {

		goToNextPlay: function() {
        	if (this.get('controller.hasNextPlay')) {
        		this.send('goToPlay', this.get('controller.currentPlay.nextPlayNumber'));
        	}
        },

        goToPrevPlay: function() {
        	if (this.get('controller.hasPrevPlay')) {
        		this.send('goToPlay', this.get('controller.currentPlay.prevPlayNumber'));
        	}
        },

        goToPlay: function(playNumber) {
        	var newPlay
        	  , controller = this.get('controller');

        	if (controller.get('teamPlays') && playNumber) {

        		newPlay = controller.get('teamPlays').find(function(teamPlay) {
        			return teamPlay.get('teamCutup.id') === controller.get('teamCutup.id') && teamPlay.get('playNumber') === playNumber;
        		});

        		if (newPlay) {
                    this.saveGradesOnMemory();

        			controller.set('previousPlay', controller.get('currentPlay'));
        			controller.set('currentPlay', newPlay);
        		}
        	}
        },

        selectPlayerToGrade: function(teamPlayerAssignment) {
        	this.addPlayerToGrade(teamPlayerAssignment);
        },

        addPlayersToCurrentGrade: function() {
        	this.addPlayersToGrade();
        },

        toggleInfoPanelSize: function() {
        	this.get('controller').toggleProperty('infoPanelStretched');
        },

        sendGradesToSync: function() {
            this.transitionTo('offline-games');
        },

        willTransition: function() {
            this.saveGradesOnMemory();
        }
	}
});
