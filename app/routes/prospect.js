// app/routes/prospect.js

import Ember from 'ember';

export default Ember.Route.extend({
    model: function(params) {
        return this.store.findById('teamProspectAssignment',params.prospect_id).then(function(teamProspectAssignment) {
            return teamProspectAssignment.get('teamProspect').then(function() {
                return teamProspectAssignment;
            });
        });
    },

    actions: {
        
    }
});
