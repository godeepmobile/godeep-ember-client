// routes/admin/grading.js
import Ember from 'ember';

export default Ember.Route.extend({
    model: function() {
        return this.store.find('teamConfiguration')
            .then(function(teamConfigurations) {
            return teamConfigurations.toArray()[0];
        });
    },

    setupController: function (controller, model) {
        model.set('currentState.isDirty', false);
        var numPlayGrades = this.get('session.team.numPlayGrades');
        var hasPlayGrades = numPlayGrades && numPlayGrades > 0;
        this.controller.set('disableFrom', hasPlayGrades);
        this.controller.set('model', model);
    },

    renderTemplate: function() {
        this.render();
    },

    actions: {
        didTransition: function() {
            var crumbs = {
                thisPage: "Grading Configurations",
                breadcrumbs: [
                    {
                        linkTo : "admin",
                        caption: "Settings"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        }
    }
});
