// routes/admin/users.js
import Ember from 'ember';

export default Ember.Route.extend({

    model: function() {
        return this.store.find('teamUser')
            .then(function(teamUsers) {
                return teamUsers;
            });
    },

    setupController: function (controller, users) {
        this.controller.set('users', users);
    },

    renderTemplate: function() {
        this.render();
    },

    actions: {
        didTransition: function() {
            var crumbs = {
                thisPage: "Manage Users",
                breadcrumbs: [
                    {
                        linkTo : "admin",
                        caption: "Settings"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        }
    }
});
