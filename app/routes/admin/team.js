// routes/admin/team.js
import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),
    beforeModel() {
        return this.get('loadData.profile');
    },
    loadTeam: function(teamId) {
        return this.store.findById('team', teamId);
    },
    model: function() {
        return Ember.RSVP.hash({
            team         : this.loadTeam(this.get('session.team.id')),
            states       : this.get('loadData.state'),
            conferences  : this.get('loadData.conference'),
            surfaceTypes : this.get('loadData.surfaceType')
        });
    },

    setupController: function (controller, context) {
        this.controller.set('states', context.states);
        this.controller.set('conferences', context.conferences);
        this.controller.set('surfaceTypes', context.surfaceTypes);

        //Force to reload team model from rest api.
        //context.team.reload();
        this.controller.set('model', context.team);
    },

    renderTemplate: function() {
        this.render();
    },

    actions: {
        didTransition: function() {
            var crumbs = {
                thisPage: "Team Information",
                breadcrumbs: [
                    {
                        linkTo : "admin",
                        caption: "Settings"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        }
    }
});
