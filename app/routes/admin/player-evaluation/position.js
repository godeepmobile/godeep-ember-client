// routes/admin/player-evaluation/position.js
import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),
    queryParams: {
      criteriaType: {
        refreshModel: true
      }
    },
    criteriaType: null,
    position: null,
    beforeModel: function(transition){
        var criteriaType = transition.queryParams.criteriaType;
        this.set('criteriaType', criteriaType);
    },

    afterModel(model) {
        var importances = this.get('loadData.importances');
        this.set('importances', importances);
    },

    model: function(params) {
        this.set('position', params.position_id);
        var criteriaType = this.get('criteriaType') || '1';
        return this.store.find('teamScoutingEvalCriteria', {
            filter: {
                where: {
                    positionType: params.position_id,
                    team: this.get('session.team.id'),
                    scoutingEvalCriteriaType: criteriaType
                }
            }
        });
    },

    setupController: function (controller, model) {
        model = model ? model : [];
        controller.set('teamScoutingEvalCriterias', model);
        controller.set('importances', this.get('importances'));
        var positionType = this.get('position');
        if (positionType) {
            this.controller.set('positionType', positionType);
            this.send('setPosition', positionType);
        }
    }
});
