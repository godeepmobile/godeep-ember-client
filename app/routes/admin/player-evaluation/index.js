// routes/admin/player-evaluation/index.js
import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),

    loadTeamConf: function() {
        return this.store.find('teamConfiguration')
            .then(function(teamConfigurations) {
            return teamConfigurations.toArray()[0];
        });
    },

    model: function() {
        return this.store.find('teamConfiguration')
            .then(function(teamConfigurations) {
            return teamConfigurations.toArray()[0];
        });
    },

    setupController: function (controller, model) {
        var numPlayGrades = this.get('session.team.numPlayGrades');
        var hasPlayGrades = numPlayGrades && numPlayGrades > 0;
        this.controller.set('disableFrom', hasPlayGrades);
        this.controller.set('model', model);
    },

    actions: {
        didTransition: function() {
            var crumbs = {
                thisPage: "Player Evaluation Configuration",
                breadcrumbs: [
                    {
                        linkTo : "admin",
                        caption: "Settings"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        }
    }
});
