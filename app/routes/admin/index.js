import Ember from 'ember';

export default Ember.Route.extend({
    setupController: function () {
    },
    model: function() {
        return [];
    },

    renderTemplate: function() {
        this.render();
    },

    actions: {
    	didTransition: function() {
            this.transitionTo('admin.team');
        }
    }
});
