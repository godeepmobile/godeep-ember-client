// routes/admin/player-evaluation.js
import Ember from 'ember';

export default Ember.Route.extend( {
    loadData: Ember.inject.service(),
    position: null,
    criteriaType: null,

    beforeModel: function(transition){
        var criteriaType = transition.queryParams.criteriaType;
        this.set('criteriaType', criteriaType);
    },

    model: function() {
        return this.get('loadData.positionTypes');
    },

    afterModel(model) {
        var scoutingEvalCriteriaTypes = this.get('loadData.scoutingEvalCriteriaTypes');
        this.set('scoutingEvalCriteriaTypes', scoutingEvalCriteriaTypes);
    },

    setupController: function (controller, model) {
        controller.set('model', model);
        controller.set('criteriaType', this.get('criteriaType'));
        controller.set('scoutingEvalCriteriaTypes', this.get('scoutingEvalCriteriaTypes'));
    },

    actions: {
        positionChanged: function( position ) {
          if (position) {
              this.transitionTo('admin.playerEvaluation.position', position);
          } else {
              this.transitionTo('admin.playerEvaluation');
          }
          this.set('position', position);
        },

        didTransition: function() {
            var crumbs = {
                thisPage: "Player Evaluation Criteria",
                breadcrumbs: [
                    {
                        linkTo : "admin",
                        caption: "Settings"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        },

        setPosition: function(value) {
            if (this.controller.get('position') !== value) {
                this.controller.set('position', value);
                this.set('position', value);
            }
        }
    }
});
