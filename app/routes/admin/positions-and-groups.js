// routes/admin/positions-and-groups.js
import Ember from 'ember';

export default Ember.Route.extend({
    loadData: Ember.inject.service(),

    model: function () {
        var self = this;
        return this.store.find('teamPositionGroup', {
            filter: {
                where: {
                    endDate: 'null'
                }
            }
        })
        .then(function (teamPositionGroups) {
            teamPositionGroups.forEach(function (teamPositionGroup) {
                var teamPositionTypes = self.store.find('teamPositionType', {
                    filter: {
                        where: { and: [
                              { teamPositionGroup: teamPositionGroup.get('id')},
                              { platoonType: teamPositionGroup.get('platoonType')},
                              { endDate: 'null'}]}}
                });
                teamPositionGroup.set('teamPositionTypes', teamPositionTypes);
            });
            return teamPositionGroups;
        });
    },

    afterModel: function(teamPositionGroups) {
        // Recovering user positions types
        var self = this;
        var store = this.store;
        var positionTypes = this.get('loadData.positionTypes');
        var positionTypeOffensives = store.filter('positionType', function(item) {
            return item.get('platoonType') === 1;
        });
        var positionTypeDefenses = store.filter('positionType', function(item) {
            return item.get('platoonType') === 2;
        });
        var positionTypeSpecialTeams = store.filter('positionType', function(item) {
            return item.get('platoonType') === 3;
        });
        self.set('positionTypeOffensives', positionTypeOffensives);
        self.set('positionTypeDefenses', positionTypeDefenses);
        self.set('positionTypeSpecialTeams', positionTypeSpecialTeams);
        return positionTypes;

/*        return Ember.RSVP.hash({
            positionTypes: this.get('loadData.positionTypes')
            //positionTypes: this.store.find('positionType', { limit: 100 }),
        }).then(function(hash) {
            var positionTypeOffensives = hash.positionTypes.filter(function(item, index, enumerable) {
                return item.get('platoonType') === 1;
            });
            var positionTypeDefenses = hash.positionTypes.filter(function(item, index, enumerable) {
                return item.get('platoonType') === 2;
            });
            var positionTypeSpecialTeams = hash.positionTypes.filter(function(item, index, enumerable) {
                return item.get('platoonType') === 3;
            });
            self.set('positionTypeOffensives', positionTypeOffensives);
            self.set('positionTypeDefenses', positionTypeDefenses);
            self.set('positionTypeSpecialTeams', positionTypeSpecialTeams);
        });
    */
    },

    setupController: function (controller, model) {
        this.controller.set('model', model);
        this.controller.set('positionTypeOffensives', this.get('positionTypeOffensives'));
        this.controller.set('positionTypeDefenses', this.get('positionTypeDefenses'));
        this.controller.set('positionTypeSpecialTeams', this.get('positionTypeSpecialTeams'));
    },

    renderTemplate: function() {
        this.render();
    },

    actions: {
        didTransition: function() {
            var crumbs = {
                thisPage: "Positions and Groups",
                breadcrumbs: [
                    {
                        linkTo : "admin",
                        caption: "Settings"
                    }
                ]
            };
            this.send('setBreadcrumbs', crumbs);
            return true;
        }
    }
});
