import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';
import FetchAllModelsMixin from '../mixins/fetch-all-models';

export default Ember.Route.extend(AuthenticatedRouteMixin, FetchAllModelsMixin, {
    actions: {
        didTransition: function() {
            return true; // Bubble the didTransition event
        },
        createGame: function() {
            console.log('createGame ');
        },
        uploadPlayData: function(game) {
            console.log('uploadPlayData ', game);
        },
        enterDataManually: function(game) {
            //this.transitionTo('data/add',game);
        },

        gradeCutup: function(teamCutup) {
            var status;
            this.store.find('teamEventGradeStatus', {
                    filter: {
                        where: { and: [
                              { user: this.get("session.user.id")},
                              { teamCutup: teamCutup.get('id')}]}
                        }
                }
            ).then(function(teamEventGradeStatus) {
                status = teamEventGradeStatus.get('firstObject');

                if (status && status.get('isInProcess')) {
                    this.transitionTo('grade', status);
                } else {
                    this.transitionTo('game', teamCutup, {
                        queryParams: {
                            resumePlay : 1
                          , play       : 1
                        }
                    });
                }
            }.bind(this));

        },

        deleteCutup: function(teamCutup) {
            teamCutup.deleteRecord();
            teamCutup.save().catch(function() {
                teamCutup.rollback();
                this.send('showModal', 'games/no-delete-message');
            }.bind(this));
        },

        editEvent: function(event) {
            this.transitionTo('event.setup').then(function(eventSetupRoute) {
                var isHomeGame = event.get('isHomeGame')
                  , practiceGameTeam1 = event.get('practiceGameTeam1')
                  , practiceGameTeam2 = event.get('practiceGameTeam2')
                  , self = this;

                eventSetupRoute.set('controller.isEdit', true);
                eventSetupRoute.set('controller.teamEvent', event);

                if (event.get('eventType.isPracticeGame') && (practiceGameTeam1 || practiceGameTeam2)) {

                    if (!practiceGameTeam1) {
                        event.get('opponent').then(function(opponent) {
                            eventSetupRoute.set('controller.changedOnSetup', true);
                            eventSetupRoute.set('controller.awayTeam', opponent || self.get('session.team.organization'));
                            eventSetupRoute.set('controller.changedOnSetup', false);
                        });
                    } else {
                        eventSetupRoute.set('controller.awayTeam', {
                            get: function(attr) {
                                if (attr === 'id') {
                                    return self.get('session.team.organization.id');
                                }
                                return practiceGameTeam1;
                            }
                        });
                    }

                    if (!practiceGameTeam2) {
                        event.get('opponent').then(function(opponent) {
                            eventSetupRoute.set('controller.changedOnSetup', true);
                            eventSetupRoute.set('controller.homeTeam', opponent || self.get('session.team.organization'));
                            eventSetupRoute.set('controller.changedOnSetup', false);
                        });
                    } else {
                        eventSetupRoute.set('controller.homeTeam', {
                            get: function(attr) {
                                if (attr === 'id') {
                                    return self.get('session.team.organization.id');
                                }
                                return practiceGameTeam2;
                            }
                        });
                    }

                    eventSetupRoute.set('practiceGameTeam1', practiceGameTeam1);
                    eventSetupRoute.set('practiceGameTeam2', practiceGameTeam2);
                } else {

                    event.get('opponent').then(function(opponent) {
                        eventSetupRoute.set('controller.homeTeam', isHomeGame ? self.get('session.team.organization') : opponent);
                        eventSetupRoute.set('controller.homeScore', isHomeGame ? event.get('score') : event.get('opponentScore'));

                        eventSetupRoute.set('controller.awayTeam', isHomeGame ? opponent : self.get('session.team.organization'));
                        eventSetupRoute.set('controller.awayScore', isHomeGame ? event.get('opponentScore') : event.get('score'));
                    });

                }

                event.get('eventType').then(function(eventType) {
                    eventSetupRoute.set('controller.isGame', eventType.get('isGame'));
                    eventSetupRoute.set('controller.isPracticeGame', eventType.get('isPracticeGame'));
                    eventSetupRoute.set('controller.isRegularPractice', eventType.get('isRegularPractice'));
                });

            }.bind(this));
        },

        addEvent: function(eventType) {
            this.transitionTo('event.setup').then(function(eventSetupRoute) {
                eventSetupRoute.set('controller.teamEvent.eventType', eventType);
            });
        },

        addNewPlatoon: function(event) {
            this.transitionTo('event.setup').then(function(eventSetupRoute) {
                var isHomeGame = event.get('isHomeGame');

                eventSetupRoute.set('controller.teamEvent', event);
                eventSetupRoute.set('controller.isAddingPlatoon', true);

                eventSetupRoute.set('controller.homeTeam', isHomeGame ? this.get('session.team.organization') : event.get('opponent'));
                eventSetupRoute.set('controller.homeScore', isHomeGame ? event.get('score') : event.get('opponentScore'));

                eventSetupRoute.set('controller.awayTeam', isHomeGame ? event.get('opponent') : this.get('session.team.organization'));
                eventSetupRoute.set('controller.awayScore', isHomeGame ? event.get('opponentScore') : event.get('score'));
            }.bind(this));
        }
    },
    model: function() {
        var self = this;
        return this.store.find('teamEvent',{'order':['date DESC'], limit: 100})
        .then(function (teamEvents) {
            teamEvents.forEach(function (teamEvent) {
                self.store.find('teamCutup', {
                    filter: {
                        where: { teamEvent: teamEvent.get('id')}
                    },
                    limit: 100
                }).then(function(teamCutups) {
                    teamEvent.set('teamCutups', teamCutups);
                });
            });
            return teamEvents;
        });
    },

    setupController: function(controller, context) {
        var gameEventType, practiceEventType, practiceGameEventType;

        this._super(controller, context);

        this.store.find('eventType').then(function(eventTypes) {
            gameEventType     = eventTypes.findBy('isGame');
            practiceEventType = eventTypes.findBy('isRegularPractice');
            practiceGameEventType = eventTypes.findBy('isPracticeGame');

            controller.set('gameEventType', gameEventType);
            controller.set('practiceEventType', practiceEventType);
            controller.set('practiceGameEventType', practiceGameEventType);
        });
    }
});
