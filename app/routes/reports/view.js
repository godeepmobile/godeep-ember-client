import Ember from 'ember';

export default Ember.Route.extend({

	chartColorsForPlayerComparison: [
        '#f45b5b',
        '#258be2',
        '#666666',
        '#8085e9',
        '#8d4654',
        '#7798bf',
        '#aaeeee',
        '#ff0066',
        '#eeaaee',
        '#55bf3b',
        '#df5353',
        '#7798bf',
        '#aaeeee'
    ],

    afterModel: function(context) {
    	return this.store.find('reportType').then(function(reportTypes) {
    		this.set('reportTypes', reportTypes);

    		return Ember.RSVP.all(context.playerAssignments.getEach('teamPlayer')).then(function() {
    			return Ember.RSVP.all(context.playerAssignments.getEach('position1'));
    		});

    	}.bind(this));
    },

	setupController: function(controller, context, transition) {
		var adapter      = this.store.adapterFor('application')
		  , errorHandler = function(error) {
				console.log('responseJSON ', error.responseJSON);
				if (error.responseJSON) {
					alert(error.responseJSON.error.message);
				} else if (error.responseText){
					alert(error.responseText);
				}
			}.bind(this);

		this._super(controller, context);

		controller.set('REPORT_TYPES_TITLES', context.REPORT_TYPES_TITLES);

		//clearing previuos report

		controller.set('compareCriteria', null);
		controller.set('reportData', null);
		controller.set('chartData', null);
		controller.set('chartCategories', null);
		controller.set('chartOptions', null);
		controller.set('noReportDataFound', false);
		controller.set('chartFooter', null);
		controller.set('twoLineChartFooter', false);
		controller.set('isAllExpanded', false);
		controller.set('teamReport', null);
		controller.set('teamReportToBeUpdated', null);
		controller.set('chartBaseLine', null);

		this.send('setBreadcrumbs', {
	    	thisPage    : context.REPORT_TYPES_TITLES[controller.get('type')],
	    	breadcrumbs : [{
	    		linkTo  : 'reports',
	    		caption : 'Player Performance Reports'
	    	}]
        });

		this.loadReportData(transition.queryParams);

	},

	buildRunURL: function(adapter) {
    	return adapter.buildURL('TeamReport', 'run');
    },

    loadReportData: function(queryParams) {
    	var adapter      = this.store.adapterFor('application')
    	  , controller   = this.get('controller')
    	  , numGradeCats = this.get('session.teamConfiguration.numGradeCategories')
    	  , i
    	  , j
		  , errorHandler = function(error) {
				console.log('responseJSON ', error.responseJSON);
				if (error.responseJSON) {
					alert(error.responseJSON.error.message);
				} else if (error.responseText){
					alert(error.responseText);
				}
			}.bind(this);

    	if (queryParams) {

			adapter.ajax(this.buildRunURL(adapter), 'GET', {data: queryParams}).then(function(response) {
				var graphDetails
				  , reportData
				  , controller      = this.get('controller')
				  , chartCategories = []
				  , eventNumbers    = []
				  , chartOptions    = {}
				  , playerToCompare
				  , comparisonPlayer
				  , noQtrCategory
				  , chartFooter     = ''
				  , totalNumPlays   = 0
				  , comparisonPlayerDetail
				  , pointPlacement  = -0.2
				  , chartData       = []
			      , setPlayerDetail = function(detail) {
						chartData[i].data.push(detail['individualPlayerOverall_' + i]);
					};

				switch (numGradeCats) {
					case 1:
						chartData = [{
				            name: this.get('session.teamConfiguration.gradeCat1Label'),
				            pointPlacement: 0,
				            data: []
				        }];
				        break;

				    case 2:
				    	chartData = [{
				            name: this.get('session.teamConfiguration.gradeCat1Label'),
				            pointPlacement: -0.2,
				            data: []
				        }, {
				            name: this.get('session.teamConfiguration.gradeCat2Label'),
				            pointPlacement: 0.2,
				            data: []
				        }];
				        break;

				    case 3:
				    	chartData = [{
				            name: this.get('session.teamConfiguration.gradeCat1Label'),
				            pointPlacement: -0.2,
				            data: []
				        }, {
				            name: this.get('session.teamConfiguration.gradeCat2Label'),
				            pointPlacement: 0,
				            data: []
				        }, {
				            name: this.get('session.teamConfiguration.gradeCat3Label'),
				            pointPlacement: 0.2,
				            data: []
				        }];
				        break;
				}

				if (response && response.report && response.report.data) {
					reportData = response.report.data;

					graphDetails = reportData.graphDetails;

					if (graphDetails) {

						playerToCompare = graphDetails.player;
						noQtrCategory   = graphDetails.data ? graphDetails.data.findBy('category', 'no Qtr') : graphDetails.findBy('category', 'no Qtr');

						if (noQtrCategory) {
							chartFooter  = '<br/>Plays excluded from graph because no play data available: ' + noQtrCategory.numPlays;
							if (graphDetails.data) {
								graphDetails.data = graphDetails.data.without(noQtrCategory);
							} else {
								graphDetails = graphDetails.without(noQtrCategory);
							}
							controller.set('twoLineChartFooter', true);
						}

						if (playerToCompare) {
							// It's a player comparison report
							chartData = [{
								name           : playerToCompare.firstName + ' ' + playerToCompare.lastName + ' #' + playerToCompare.jerseyNumber + ' / ' + playerToCompare.position,
								pointPlacement : pointPlacement,
								data           : []
							}];

							if (graphDetails.playersGroup) {
								// Compating against a group of players
								chartData.push({
									name           : graphDetails.playersGroup.name || graphDetails.playersGroup,
									pointPlacement : pointPlacement * -1,
									data           : []
								});

								graphDetails.data.forEach(function(detail) {
									chartData[0].data.push(detail.playerOverall);
									chartData[1].data.push(detail.playersGroupOverall);
								}); 
							} else {
								// Comparing against max 3 players

								chartOptions['legend'] = {
						            align: 'right',
						            enabled: true,
						            symbolPadding: 10,
						            symbolWidth: 0,
						            width: 400,
						            padding: 1
						        };

								graphDetails.data.forEach(function(detail) {
									chartData[0].data.push(detail.playerOverall);
								});

								for (i = 1; i<=3 ; i++) {
									comparisonPlayer = graphDetails['individualPlayer_' + i];

									if (comparisonPlayer) {
										pointPlacement += 0.1;

										chartData.push({
											name           : comparisonPlayer.firstName + ' ' + comparisonPlayer.lastName + ' #' + comparisonPlayer.jerseyNumber + ' / ' + comparisonPlayer.position + (!reportData.tableDetails.isAny('id', comparisonPlayer.id) ? ' (no data)' : ''),
											pointPlacement : pointPlacement,
											data           : []
										});

										graphDetails.data.forEach(setPlayerDetail); 
									}

								}
								
							}

							// Adding another order to the chart colors in order the player to compare series to always be red
							chartOptions['colors'] = this.get('chartColorsForPlayerComparison');
							comparisonPlayerDetail = reportData.tableDetails.findBy('id', playerToCompare.id);

							if (comparisonPlayerDetail) {
								// The player to compare has data
								Ember.set(comparisonPlayerDetail, 'highlighted', true);
							} else {
								// The player to compare has no data, then adding it as an empty record
								Ember.set(playerToCompare, 'highlighted', true);
								Ember.set(playerToCompare, 'overalls', {
									avgCat1Grade : 0,
									avgCat2Grade : 0,
									avgCat3Grade : 0,
									avgOverallGrade : 0,
									numPlays : 0 
								});
								reportData.tableDetails.addObject(playerToCompare);

								// Adding the 'no data' label for the player to compare chart category
								chartData[0].name += ' (no data)';
							}

						} else {

							graphDetails.forEach(function(detail) {

								for (j = 0; j < numGradeCats ; j++) {
									chartData[j].data.push(detail['avgCat' + (j+1) + 'Grade']);
								}
								
							});

						}

						(graphDetails.data || graphDetails).forEach(function(detail) {
							if (detail.eventNumber) {
	                        	chartCategories.push({
	                        		label : controller.get('eventLabel') + ' ' + detail.eventNumber,
	                        		name  : detail.name
	                        	});
	                        	eventNumbers.push({
	                        		id     : detail.id,
	                        		number : detail.eventNumber
	                        	});
	                    	} else {
	                    		chartCategories.push(detail.category);
	                    	}

	                    	totalNumPlays += detail.numPlays;
						});

						controller.set('chartFooter', 'Total Plays in graph (above): ' + totalNumPlays + chartFooter);

						if (!controller.get('isSingleEventReport')) {

							chartOptions['tooltip'] = {
								followPointer: true,
        						shared: true,
								headerFormat: '<span style="font-size: 10px">{point.key.label} - {point.key.name}</span><br/>'
							};

							chartOptions['xAxis'] = {
					            categories: [],
					            gridLineWidth: 20,
					            gridLineColor: "#ffffff",
					            tickWidth: 0,
					            lineWidth: 0,
					            minorTickLength: 0,
					            opposite: true,
					            labels: {
					                useHTML:true,
					                y:-40,
					                autoRotation:0,
					                formatter: function() {
					                	return this.value.label;
					                }
					            }
					        };
						}

						if (Object.keys(chartOptions).length > 0) {
							controller.set('chartOptions', chartOptions);
						}

        				controller.set('chartBaseLine', this.get('session.teamConfiguration.gradeBase'));
						controller.set('chartData', chartData);
						controller.set('chartCategories', chartCategories);
						controller.set('eventNumbers', eventNumbers);

					}

					controller.set('reportData', reportData);

				} else {
					controller.set('noReportDataFound', true);
				}
			}.bind(this), errorHandler);

		} else {
			controller.set('noReportDataFound', true);
		}
    },

	actions: {

		toggleExpandPlayer: function(player, isSingleEventReport) {
            if (!Ember.get(player,'isExpanded')) {
                Ember.set(player,'isExpanded',true);
                if (isSingleEventReport) {
                	player.events.setEach('isExpanded', true);
                }
            } else {
                Ember.set(player,'isExpanded',false);
                player.events.setEach('isExpanded', false);
            }
        },

        toggleExpandEvent: function(event) {
            if (!Ember.get(event,'isExpanded')) {
                Ember.set(event,'isExpanded',true);
            } else {
                Ember.set(event,'isExpanded',false);
            }
        },

        toggleExpandPlay: function(play) {
        	if (!Ember.get(play,'isExpanded')) {
                Ember.set(play,'isExpanded',true);
            } else {
                Ember.set(play,'isExpanded',false);
            }
        },

        toggleExpandAllPlayerReports: function() {
        	var controller = this.get('controller')
        	  , flag;

            controller.toggleProperty('isAllExpanded');

            flag = controller.get('isAllExpanded');

            controller.get('reportData.tableDetails').forEach(function(item) {
                Ember.set(item,'isExpanded', flag);
                if (item.events) {
                	item.events.setEach('isExpanded', flag);
            	}
            });
        },

        goToUploadPlayData: function() {
            this.transitionTo('event.upload');
        },

        displaySaveReportDialog: function() {
        	var saveTeamReportController = this.controllerFor('reports/save');
        	saveTeamReportController.clearForm();
        	this.send('showModal', 'reports/save');
        },

        displayEditReportDialog: function(teamReportToBeUpdated) {
        	this.send('showModal', 'reports/edit', teamReportToBeUpdated);
        },

        saveReport: function(name, isPublic, teamReportToUpdate) {
        	var controller   = this.get('controller')
        	  , errorHandler = function(error) {
	            	this.set('videoCutupData', null);
					console.log('responseJSON ', error.responseJSON);
					if (error.responseJSON) {
						alert(error.responseJSON.error.message);
					} else if (error.responseText){
						alert(error.responseText);
					}
				}.bind(this)
			  , teamReport = teamReportToUpdate || this.get('store').createRecord('teamReport');

        	teamReport.setProperties({
        		type           : this.get('reportTypes').findBy('path', controller.get('type')),
        		criteria       : controller.get('criteria'),
        		criteriaRange  : controller.get('criteriaRange'),
        		scope          : controller.get('scope'),
        		scopeRange     : controller.get('scopeRange'),
        		condition      : controller.get('condition') !== 'undefined' ? controller.get('condition') : null,
        		conditionRange : controller.get('conditionRange') !== 'undefined' ? controller.get('conditionRange') : null,
        		user           : this.get('session.user.id'),
        		eventType      : controller.get('model.eventTypes').findBy('id', controller.get('filter')),
        		name           : name,
        		isPublic       : isPublic
        	});

        	teamReport.save().then(function(teamReport) {
        		controller.set('teamReport', teamReport);
        		if (teamReportToUpdate) {
        			this.send('temporalTooltip', 'tooltip/saved', {message: 'The report has been updated'});
        			controller.set('teamReportToBeUpdated', null);
        		} else {
        			this.send('temporalTooltip', 'tooltip/saved', {message: 'The report has been saved'});
        		}
        	}.bind(this), errorHandler);
        },

        goToCriteriaPageToEdit: function(teamReport) {
        	this.transitionTo('reports.' + this.get('controller.type')).then(function(criteriaPageView) {
        		criteriaPageView.set('controller.teamReport', teamReport);
        	});
        },

        queryParamsDidChange: function(changed, queryParams) {
        	var controller = this.get('controller');
        	
        	if (this.currentModel) {
        		// The page was already loaded, so we need to fetch the new data

        		//clearing previuos report

				controller.set('compareCriteria', null);
				controller.set('reportData', null);
				controller.set('chartData', null);
				controller.set('chartCategories', null);
				controller.set('chartOptions', null);
				controller.set('noReportDataFound', false);
				controller.set('chartFooter', null);
				controller.set('twoLineChartFooter', false);
				controller.set('isAllExpanded', false);
				controller.set('teamReport', null);
				controller.set('teamReportToBeUpdated', null);
				controller.set('chartBaseLine', null);

        		this.loadReportData(queryParams);
        	}
        }
	}

});
