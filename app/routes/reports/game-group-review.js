import Ember from 'ember';

export default Ember.Route.extend({
	model: function () {
        var model = this.modelFor('reports');
        return model;
    },

	setupController: function (controller, context) {

		var plays = [], i;

		controller.set('platoons', context.platoons);
        controller.set('games', context.games);
        controller.set('playerAssignments', context.playerAssignments);
        controller.set('groups', context.groups);
        controller.set('seasons', context.seasons);

        controller.set('CRITERIA', context.CRITERIA);
        controller.set('SCOPE', context.SCOPE);

        for (i = 1; i <= 150; i++) {
        	plays.push(i);
        }

        controller.set('plays', plays);
        controller.set('condition', 'includeCoachesOnlyNotes');
        controller.set('conditionRange', null);

        // reset the form
        controller.set('errors', {});

        this.send('setBreadcrumbs', {
            thisPage    : context.REPORT_TYPES_TITLES.gameGroupReview,
            breadcrumbs : [{
                linkTo  : 'reports',
                caption : 'Player Performance Reports'
            }]
        });

	},

    resetForm: function() {
        var controller = this.get('controller');
        controller.set('selectedPlatoon', null);
        controller.set('selectedGroup', null);
        controller.get('playerAssignments').setEach('selectedForReport', false);

        controller.set('selectedGame', null);
        controller.set('selectedFilter', null);
        controller.set('selectedCriteria', null);
        controller.set('selectedScope', null);
    },

	actions: {
		runReport: function() {
            this.send('buildReportParams', 'gameGroupReview', this.get('controller'));
        }
	}
});
