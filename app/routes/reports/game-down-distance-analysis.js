import Ember from 'ember';

export default Ember.Route.extend({
	numberOfDowns: 4,
	numberOfDistances: 99,

	model: function () {
        var model = this.modelFor('reports');
        return model;
    },

	setupController: function (controller, context) {

		controller.set('platoons', context.platoons);
        controller.set('games', context.games);
        controller.set('playerAssignments', context.playerAssignments);
        controller.set('groups', context.groups);
        controller.set('seasons', context.seasons);
        controller.set('eventTypes', context.eventTypes);

        controller.set('CRITERIA', context.CRITERIA);
        controller.set('SCOPE', context.SCOPE);

        controller.set('downs', new Array(this.get('numberOfDowns')).join().split(',').map(function(item, index){ return ++index;}));
        //controller.set('distances', new Array(this.get('numberOfDistances')).join().split(',').map(function(item, index){ return ++index;}));
        
        // - indicates limit i.e -:5 = lower limit to 5, 20:1 = from 20 to upper limit 
        controller.set('distances', Ember.A([
            {label: '<5', value: '-:5'},
            {label: '<10', value: '-:10'},
            {label: '10-15', value: '10:15'},
            {label: '15-20', value: '15:20'},
            {label: '>20', value: '20:-'}
        ]));

        controller.set('condition', 'downDistance');

        // reset the form
        controller.set('errors', {});

        this.send('setBreadcrumbs', {
            thisPage    : context.REPORT_TYPES_TITLES.gameDownDistanceAnalysis,
            breadcrumbs : [{
                linkTo  : 'reports',
                caption : 'Player Performance Reports'
            }]
        });

        Ember.run.scheduleOnce('afterRender', controller, function() {
            if (!controller.get('selectedFilter')) {
                controller.set('selectedFilter', context.eventTypes.get('firstObject.id'));
            }
        });

	},

    resetForm: function() {
        var controller = this.get('controller');

        controller.set('selectedPlatoon', null);
        controller.set('selectedGroup', null);
        controller.get('playerAssignments').setEach('selectedForReport', false);

        controller.set('selectedGame', null);
        controller.set('selectedSeason', null);
        controller.set('fromDate', null);
        controller.set('toDate', null);

        controller.set('selectedDown', null);
        controller.set('selectedDistance', null);

        controller.set('selectedFilter', controller.get('eventTypes.firstObject.id'));
        controller.set('selectedCriteria', null);
        controller.set('selectedScope', null);
    },

	actions: {
		runReport: function() {
			var controller = this.get('controller');

            if (controller.get('selectedDown') || controller.get('selectedDistance')) {

    			controller.set('conditionRange', controller.get('selectedDown') + ',' + controller.get('selectedDistance.value'));
                this.send('buildReportParams', 'gameDownDistanceAnalysis', this.get('controller'));
            } else {
                controller.set('errors.dnDist', true);
            }
        }
	}
});
