import Ember from 'ember';

export default Ember.Route.extend({
	loadData: Ember.inject.service(),

	model: function () {
        var model = this.modelFor('reports');
        return model;
    },

	setupController: function (controller, context) {

		controller.set('platoons', context.platoons);
        controller.set('games', context.games);
        controller.set('playerAssignments', context.playerAssignments);
        //controller.set('players', context.playerAssignments.getEach('teamPlayer'));
        controller.set('groups', context.groups);
        controller.set('seasons', context.seasons);
        controller.set('eventTypes', context.eventTypes);

        controller.set('CRITERIA', context.CRITERIA);
        controller.set('SCOPE', context.SCOPE);

        controller.set('condition', 'playerToCompare');

        // reset the form
        controller.set('errors', {});

        this.send('setBreadcrumbs', {
            thisPage    : context.REPORT_TYPES_TITLES.playerComparison,
            breadcrumbs : [{
                linkTo  : 'reports',
                caption : 'Player Performance Reports'
            }]
        });

        Ember.run.scheduleOnce('afterRender', controller, function() {
            if (!controller.get('selectedFilter')) {
                controller.set('selectedFilter', context.eventTypes.get('firstObject.id'));
            }
        });

	},

    resetForm: function() {
        var controller = this.get('controller');
        
        controller.set('playerToCompare', null);
        controller.set('selectedPlatoon', null);
        controller.set('selectedGroup', null);
        controller.get('playerAssignments').setEach('selectedForReport', false);

        controller.set('selectedGame', null);
        controller.set('selectedSeason', null);
        controller.set('fromDate', null);
        controller.set('toDate', null);

        controller.set('selectedFilter', controller.get('eventTypes.firstObject.id'));
        controller.set('selectedCriteria', null);
        controller.set('selectedScope', null);
    },

	actions: {
		runReport: function() {
			var controller      = this.get('controller')
			  , playerToCompare = controller.get('playerToCompare')
			  , teamPositionGroup;

			if (playerToCompare) {

				controller.set('conditionRange', playerToCompare.get('teamPlayer.id'));

				if (controller.get('selectedCriteria') !== controller.get('CRITERIA.players')) {
					teamPositionGroup = this.get('loadData.teamPositionGroups').find(function(teamPositionGroup) {
						return teamPositionGroup.get('positionTypeIds').indexOf(parseInt(playerToCompare.get('position1.id'))) !== -1;
					});

					if (teamPositionGroup) {
						controller.set('selectedGroup', teamPositionGroup.get('id'));
						controller.set('selectedPlatoon', teamPositionGroup.get('platoonType'));
					}
				}
				
				this.send('buildReportParams', 'playerComparison', controller);
			} else {
				controller.set('errors.playerToCompare', true);
			}
        }
	}
});
