// routes/reports/gamePerformance.js

import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
    loadData: Ember.inject.service(),
    params: null,
    //attrs: {},
    chartOptions: {
        chart: {
            type: 'line',
            height: 300,
            plotBackgroundColor: '#e6e7e8',
            //plotBorderColor: '#bbbbbb',
            //plotBorderWidth: 3,
            //width:508,
            margin: [50, 50, 50, 80] // top, right, bottom, left
        },
        credits: {
            enabled: false
        },
        legend: {
            align: 'right',
            enabled: true,
            symbolPadding: 10,
            symbolWidth: 0
        },
        title: {
            text: ''
        },
        tooltip: {
            followPointer: true,
            shared: true
        },
        xAxis: {
            categories: ['1st Qtr', '2nd Qtr', '3rd Qtr', '4th Qtr'],
            gridLineWidth: 20,
            gridLineColor: "#ffffff",
            tickWidth: 0,
            lineWidth: 0,
            minorTickLength: 0,
            opposite: true,
            labels: {
                style: {
                    //fontSize:14,
                    //WhiteSpace:"nowrap"
                }
            }
        },
        yAxis: {
            allowDecimals: false,
            gridLineWidth: 0,
            //gridLineColor: "#bbbbbb",
            id: 'yaxis',
            labels: {
                formatter: function () {
                    return this.value;
                }
            },
            tickAmount: 5,
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                lineWidth: 3,
                marker: {
                    symbol: "circle",
                    fillColor: null,
                    lineColor: null,
                    //lineWidth: 1,
                    radius: 5
                }
            }
        }
    },
    chartData: [
        /*        {
                    name: 'Mental',
                    //lineColor: '#0e4b8c',
                    data: [85, 84, 83,82]
                }, {
                    name: 'Technique',
                    //lineColor: '#07254e',
                    data: [90, 89, 88,90]
                }, {
                    name: 'Effort',
                    //lineColor: '#01042e',
                    data: [88, 87, 92,95]
                }
                */
    ],

    setupController: function (controller, model) {
        controller.set('model', model);

        // add a data series to plot the average value line on the chart
        var config = this.get('session.teamConfiguration');
        controller.set('chartBaseLine', config.get('gradeBase'));
        controller.set('chartOptions', this.get('chartOptions'));

        controller.set('cat1Grade', null);
        controller.set('cat2Grade', null);
        controller.set('cat3Grade', null);
        controller.set('overallGrade', null);

        controller.set('cat1Label', config.get('gradeCat1Label'));
        controller.set('cat2Label', config.get('gradeCat2Label'));
        controller.set('cat3Label', config.get('gradeCat3Label'));
        controller.set('message', 'select options then click "Run Report" button');
    },

    model: function (params) {
        console.log('reports.third-down-performance.route: model() params = %o', params);
        this.set('params', params);
        var model = this.modelFor('reports');
        return model;
    },

    renderTemplate: function (controller, model) {
        this.render();
        var optionsModel = {};
        optionsModel.params = this.get('params');
        optionsModel.picklists = {};
        optionsModel.picklists.games = model.games;
        optionsModel.picklists.seasons = model.seasons;
        optionsModel.picklists.players = model.players;
        optionsModel.picklists.groups = model.groups;
        optionsModel.picklists.platoons = model.platoons;
        this.render('reports.third-down-performance-report-options', { // template to render
            into: 'reports',
            outlet: 'report-options',
            model: optionsModel
        });
    },

    buildReport(reportParams) {
        var self = this;
        var where = {};
        var store = this.store;

        var quarters = [{
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }, {
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }, {
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }, {
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }];

        // let the user know that we are working
        this.controller.set('message', 'loading report...');

        // clear out old values before running the new report
        this.set('reportData', []);
        this.set('reportDataByQuarter', []);

        // a bug in Ember-Data causes an unloadAll following by a find to fail. this is the workaround
        //store.unloadAll('teamReportData');    // this is what we actually want to do
        var records = store.all('teamReportData');
        records.forEach(function (record) {
            store.unloadRecord(record);
        });
        //store.unloadAll('teamReportPlayerEventDataByQuarter');
        records = store.all('teamReportPlayerEventDataByQuarter');
        records.forEach(function (record) {
            store.unloadRecord(record);
        });

        if (reportParams.dimension === 'teamPlayer') {
            this.store.findById('teamPlayer', reportParams.dimensionValue).then(function (player) {
                player.get('teamPlayerSeasonStats').then(function (stats) {
                    var season = self.get('loadData.season');
                    stats.every(function (stat, index) {
                        if (Ember.get(stat, 'season') === season) {
                            self.controller.set('cat1Grade', Ember.get(stat, 'allEventCat1Grade'));
                            self.controller.set('cat2Grade', Ember.get(stat, 'allEventCat2Grade'));
                            self.controller.set('cat3Grade', Ember.get(stat, 'allEventCat3Grade'));
                            self.controller.set('overallGrade', Ember.get(stat, 'allEventOverallGrade'));
                            return false;   // returning false breaks out of the stats.every() iterator
                        }
                        return true;    // keep iterating
                    });
                });
            });
        }

        // configure the where clause based on selection dimension and period
        where[reportParams.dimension] = reportParams.dimensionValue;
        where[reportParams.period] = reportParams.periodValue;

        // execute the reports
        var whereFilter = { "filter": { "where": where } };
        //pick model based on report dimension
        this.reportDataByQuarter = this.fetchAllModelsWithCallback('teamReportPlayerEventDataByQuarter', allDoneByQuarter.bind(this), whereFilter)
            .then(function (data) {
                this.set('reportDataByQuarter', data);
                this.controller.set('reportDataByQuarter', quarters);
            }.bind(this));

        this.reportData = this.fetchAllModelsNamed('teamReportData', whereFilter)
            .then(function (data) {
                this.set('reportData', data);
                this.controller.set('reportData', data);
                if (data.get('length') === 0) {
                    this.controller.set('message', 'nothing to report');
                }
            }.bind(this));

        // process the quarter results when all done
        function allDoneByQuarter(error) {
            if (error) {
                console.log('reports.third-down-performance.route: runReportWith fetch by quarter returned error %o', error);
                return;
            }
            console.log('reports.third-down-performance.route: runReportWith fetch by quarter, data = %o', this.reportDataByQuarter);
            this.reportDataByQuarter.forEach(function (row, idx) {
                var quarter = row.get('quarter');
                if (quarter && quarter >= 1 && quarter <= 4) {
                    var q = quarters[quarter - 1];
                    if (q) {
                        Ember.set(q, 'cat1Grade', row.get('cat1Grade'));
                        Ember.set(q, 'cat2Grade', row.get('cat2Grade'));
                        Ember.set(q, 'cat3Grade', row.get('cat3Grade'));
                        Ember.set(q, 'overallGrade', row.get('avgOverallGrade'));
                        Ember.set(q, 'numPlays', row.get('numPlays'));
                    }
                }
            });
            var chartData = [];
            chartData[0] = {
                name: 'Mental',
                pointPlacement: -0.2,
                data: [quarters[0].cat1Grade, quarters[1].cat1Grade, quarters[2].cat1Grade, quarters[3].cat1Grade]
            };
            chartData[1] = {
                name: 'Technique',
                pointPlacement: 0,
                data: [quarters[0].cat2Grade, quarters[1].cat2Grade, quarters[2].cat2Grade, quarters[3].cat2Grade]
            };
            chartData[2] = {
                name: 'Effort',
                pointPlacement: 0.2,
                data: [quarters[0].cat3Grade, quarters[1].cat3Grade, quarters[2].cat3Grade, quarters[3].cat3Grade]
            };
            this.controller.set('chartData', chartData);
        } // allDoneByQuarter()
    }, // buildReport()

    actions: {
        error: function(reason) {
            console.log('reports.third-down-performance.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        queryParamsDidChange() {
            this.refresh();
        },
        didTransition: function() {
            var obj = {
                thisPage: "Third Down Performance",
                breadcrumbs: [
                    {
                        linkTo :  "reports",
                        caption : "Reports"
                    }
                ]
            };
            this.send('setBreadcrumbs',obj);
            this.send('setCurrentReport', 'third down');  // tell parent route which report is currently selected just in case we got here by manually specifying the URL of this route
            return true;    // bubble transitions up
        },
        runReportWith(reportParams) {
            console.log('reports.third-down-performance.route: runReportWith, reportParams ', JSON.stringify(reportParams,null,4));
            // update query parameters
            if (this.controller.get('player') !== Ember.get(reportParams, 'player')) {
                this.controller.set('player', Ember.get(reportParams,'player'));
            }
            if (this.controller.get('group') !== Ember.get(reportParams, 'group')) {
                this.controller.set('group', Ember.get(reportParams,'group'));
            }
            if (this.controller.get('platoon') !== Ember.get(reportParams, 'platoon')) {
                this.controller.set('platoon', Ember.get(reportParams,'platoon'));
            }

            if (this.controller.get('game') !== Ember.get(reportParams, 'game')) {
                this.controller.set('game', Ember.get(reportParams,'game'));
            }
            if (this.controller.get('season') !== Ember.get(reportParams, 'season')) {
                this.controller.set('season', Ember.get(reportParams,'season'));
            }
            this.buildReport(reportParams);
        }
    } // actions:
});
