// routes/reports/index.js

import Ember from 'ember';

export default Ember.Route.extend({
    actions: {
        error: function(reason) {
            console.log('reports.index.route got error %o', reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
        },
        didTransition: function() {
            console.log('reports.index.route: didTransition()');
            var obj = {
                thisPage: "Player Performance Reports",
                breadcrumbs: []
            };
            this.send('setBreadcrumbs',obj);
            this.send('setCurrentReport', null);  // tell parent route which report is currently selected just in case we got here by manually specifying the URL of this route
            return true;    // bubble transitions up
        },
        goToUploadPlayData: function() {
            this.transitionTo('event.upload');
        }
    } // actions:
});
