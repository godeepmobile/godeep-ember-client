import Ember from 'ember';

export default Ember.Route.extend({
	model: function () {
        var model = this.modelFor('reports');
        return model;
    },

	setupController: function (controller, context) {

        var fields = Ember.A([]), a, b;

		controller.set('platoons', context.platoons);
        controller.set('games', context.games);
        controller.set('playerAssignments', context.playerAssignments);
        controller.set('groups', context.groups);
        controller.set('seasons', context.seasons);
        controller.set('eventTypes', context.eventTypes);

        controller.set('CRITERIA', context.CRITERIA);
        controller.set('SCOPE', context.SCOPE);

        fields.push({label : 'Plus Goal Line', value: 0.1});
        
        for (b = 1; b<50; b++) {
            fields.push({label : '+' + b, value: b});
        }

        fields.push({label : '50', value: 50});

        for (a = -49; a<0; a++) {
            fields.push({label : a, value: a});
        }

        fields.push({label : 'Minus Goal Line', value: -0.1});

        controller.set('fields', fields);

        controller.set('condition', 'fieldPosition');

        // reset the form
        controller.set('errors', {});

        this.send('setBreadcrumbs', {
            thisPage    : context.REPORT_TYPES_TITLES.gameFieldPositionAnalysis,
            breadcrumbs : [{
                linkTo  : 'reports',
                caption : 'Player Performance Reports'
            }]
        });

        Ember.run.scheduleOnce('afterRender', controller, function() {
            if (!controller.get('selectedFilter')) {
                controller.set('selectedFilter', context.eventTypes.get('firstObject.id'));
            }
        });
	},

    resetForm: function() {
        var controller = this.get('controller');

        controller.set('selectedPlatoon', null);
        controller.set('selectedGroup', null);
        controller.get('playerAssignments').setEach('selectedForReport', false);

        controller.set('selectedGame', null);
        controller.set('selectedSeason', null);
        controller.set('fromDate', null);
        controller.set('toDate', null);

        controller.set('initField', null);
        controller.set('endField', null);

        controller.set('selectedFilter', controller.get('eventTypes.firstObject.id'));
        controller.set('selectedCriteria', null);
        controller.set('selectedScope', null);
    },

	actions: {
		runReport: function() {
			var controller = this.get('controller')
              , initValue  = controller.get('initField.value')
              , endValue   = controller.get('endField.value');

            if (initValue !== null && endValue !== null) {
			 
                controller.set('conditionRange', initValue + ',' + endValue);

                this.send('buildReportParams', 'gameFieldPositionAnalysis', controller);
            } else {
                controller.set('errors.fields', true);
            }
        }
	}
});
