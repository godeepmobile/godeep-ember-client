import Ember from 'ember';

export default Ember.Route.extend({

    INCLUSIONS: {
        and: 'and'
      , or: 'or'
    },

    NOT_TO_DISPLAY_FIELDS: [
        'DN',
        'DIST'
    ],

	model: function () {
        var model = this.modelFor('reports');
        return model;
    },

    afterModel: function() {
        var adapter      = this.store.adapterFor('application')
          , errorHandler = function(error) {
                console.log('responseJSON ', error.responseJSON);
                if (error.responseJSON) {
                    alert(error.responseJSON.error.message);
                } else if (error.responseText){
                    alert(error.responseText);
                }
            }.bind(this);

        return adapter.ajax(this.buildPlayDataURL(adapter), 'GET').then(function(response) {
            if (response && response.data) {
                this.set('playData', response.data.reject(function(field) {
                    return this.get('NOT_TO_DISPLAY_FIELDS').indexOf(field.key) !== -1; 
                }.bind(this)));
            }
        }.bind(this), errorHandler);
    },

	setupController: function (controller, context) {

		controller.set('platoons', context.platoons);
        controller.set('games', context.games);
        controller.set('playerAssignments', context.playerAssignments);
        controller.set('groups', context.groups);
        controller.set('seasons', context.seasons);
        controller.set('eventTypes', context.eventTypes);

        controller.set('CRITERIA', context.CRITERIA);
        controller.set('SCOPE', context.SCOPE);
        controller.set('INCLUSIONS', this.get('INCLUSIONS'));

        controller.set('condition', 'playDataFields');
        controller.set('fields', this.get('playData'));

        // reset the form
        controller.set('errors', {});

        this.send('setBreadcrumbs', {
            thisPage    : context.REPORT_TYPES_TITLES.gamePlayCallPersonnelAnalysis,
            breadcrumbs : [{
                linkTo  : 'reports',
                caption : 'Player Performance Reports'
            }]
        });

        Ember.run.scheduleOnce('afterRender', controller, function() {
            if (!controller.get('selectedFilter')) {
                controller.set('selectedFilter', context.eventTypes.get('firstObject.id'));
            }
        });

        if (!controller.get('playDataFields')) {
            controller.set('playDataFields', Ember.A([{
                field: undefined, value: undefined
            }]));
        }
	},

    resetForm: function() {
        var controller = this.get('controller');
        
        controller.set('selectedPlatoon', null);
        controller.set('selectedGroup', null);
        controller.get('playerAssignments').setEach('selectedForReport', false);

        controller.set('selectedGame', null);
        controller.set('selectedSeason', null);
        controller.set('fromDate', null);
        controller.set('toDate', null);

        controller.set('selectedFilter', controller.get('eventTypes.firstObject.id'));

        controller.set('playDataFields', Ember.A([{
            field: undefined, value: undefined
        }]));

        controller.set('selectedCriteria', null);
        controller.set('selectedScope', null);
    },

    buildPlayDataURL: function(adapter) {
        return adapter.buildURL('TeamReport', 'playData');
    },

	actions: {
		runReport: function() {
			var controller             = this.get('controller')
              , playDataConditionRange = []
              , hasError               = false;

            controller.get('playDataFields').setEach('hasError', false);

            controller.get('playDataFields').forEach(function(playDataField) {
                if (!playDataField.field || !playDataField.value) {
                    hasError = true;
                    Ember.set(playDataField, 'hasError', hasError);
                } else {
                    playDataConditionRange.push((playDataField.inclusion || '') + '=' + playDataField.field.key + '=' + playDataField.value);
                }
                
            });

            if (!hasError) {
                controller.set('conditionRange', playDataConditionRange.join(','));
                this.send('buildReportParams', 'gamePlayCallPersonnelAnalysis', controller);
            } else {
                controller.set('errors.fields', true);
            }
        },

        addPlayDataField: function() {
            var playDataFields = this.get('controller.playDataFields')
              , name;
            
            if (playDataFields) {
                name = 'inclusion_' + playDataFields.length; // a helper var to have radios spedific for each item

                playDataFields.addObject({
                    field: undefined, value: undefined, inclusion: this.get('INCLUSIONS.and'), inclusionName: name
                });
            }
        }
	}
});
