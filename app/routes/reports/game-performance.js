// routes/reports/gamePerformance.js

import Ember from 'ember';
import FetchAllModelsMixin from '../../mixins/fetch-all-models';

export default Ember.Route.extend(FetchAllModelsMixin, {
    loadData: Ember.inject.service(),
    params: null,
    chartData: [],
    reportData: [],
    eventData: [],
    reportEventDataByPeriod: [],

    setupController: function (controller, context) {

        controller.set('platoons', context.platoons);
        controller.set('games', context.games);
        controller.set('playerAssignments', context.playerAssignments);
        controller.set('groups', context.groups);
        controller.set('seasons', context.seasons);
        controller.set('eventTypes', context.eventTypes);

        controller.set('CRITERIA', context.CRITERIA);
        controller.set('SCOPE', context.SCOPE);

        // reset the form
        controller.set('errors', {});

        this.send('setBreadcrumbs', {
            thisPage    : context.REPORT_TYPES_TITLES.gamePerformance,
            breadcrumbs : [{
                linkTo  : 'reports',
                caption : 'Player Performance Reports'
            }]
        });

        Ember.run.scheduleOnce('afterRender', controller, function() {
            if (!controller.get('selectedFilter')) {
                controller.set('selectedFilter', context.eventTypes.get('firstObject.id'));
            }
        });
        
    },

    resetForm: function() {
        var controller = this.get('controller');
        
        controller.set('errors', {});
        controller.set('selectedPlatoon', null);
        controller.set('selectedGroup', null);
        controller.get('playerAssignments').setEach('selectedForReport', false);

        controller.set('selectedGame', null);
        controller.set('selectedSeason', null);
        controller.set('fromDate', null);
        controller.set('toDate', null);

        controller.set('selectedFilter', controller.get('eventTypes.firstObject.id'));
        controller.set('teamReport', null);
        controller.set('selectedCriteria', null);
        controller.set('selectedScope', null);
    },

    model: function (params) {
        //console.log('reports.game-performance.route: model() params = %o', params);
        //this.set('params', params);
        var model = this.modelFor('reports');
        return model;
    },

    renderTemplate: function (controller, model) {
        // render the main part of the template
        this.render();

        // setup values to render the options panel
        var optionsModel = {};
        optionsModel.params = this.get('params');
        optionsModel.picklists = {};
        optionsModel.picklists.games = model.games;
        optionsModel.picklists.seasons = model.seasons;
        optionsModel.picklists.players = model.players;
        optionsModel.picklists.groups = model.groups;
        optionsModel.picklists.platoons = model.platoons;

        // render the options panel into the sidebar of the parent report page
        this.render('reports.game-performance-report-options', { // template to render
            into: 'reports',
            outlet: 'report-options',
            model: optionsModel
        });
    },

    clearData() {
        var store = this.store;
        // clear out old values before running the new report
        this.set('chartData', []);
        this.set('reportData', []);
        this.set('reportEventDataByPeriod', []);
        this.controller.set('chartData', []);    // this will update the chart
        this.controller.set('reportData', []);    // this will update the chart
        this.controller.set('eventData', []);
        this.controller.set('cat1Grade', null);
        this.controller.set('cat2Grade', null);
        this.controller.set('cat3Grade', null);
        this.controller.set('overallGrade', null);
        this.controller.set('isAllExpanded', null);

        // a bug in Ember-Data causes an unloadAll following by a find to fail. this is the workaround
        //store.unloadAll('teamReportData');    // this is what we actually want to do
        var reportModels = [
            'teamReportData',
            'teamReportPlayerEventDataByQuarter',
            'teamReportPlatoonEventDataByQuarter',
            'teamReportPlayerEventDataByEvent'
        ];
        reportModels.forEach(function(model){
            var records = store.all(model);
            records.forEach(function (record) {
                store.unloadRecord(record);
            });
        });
    },

    dimensionWhere(reportParams, where) {
        if (reportParams.dimension === 'teamPositionGroup') {
            var group = this.store.getById('teamPositionGroup', reportParams.dimensionValue);
            var msg;
            if (group) {
                console.log('group is ', group);
                if (group.get('positionTypeIds') && group.get('positionTypeIds').length) {
                    /*  to test an id in an array of ids:
                    {
                        where: {
                            id: {
                                inq: [123, 234]
                            }
                        }
                    } */
                    where['positionType'] = {inq: group.get('positionTypeIds')};
                } else {
                    //TODO: give the user a warning message here
                    msg = 'did not find any positions for teamPositionGroup ' + reportParams.dimensionValue;
                    this.send('error', new Ember.Error(msg));
                    where['positionType'] = -1;
                }
            } else {
                msg = 'did not find teamPositionGroup ' + reportParams.dimensionValue;
                this.send('error',new Ember.Error(msg));
                where['positionType'] = -1;
            }
        } else {
            where[reportParams.dimension] = reportParams.dimensionValue;
        }
        where[reportParams.period] = reportParams.periodValue;
        return where;
    },

    reportsURL(adapter, reportParams, period) {
        var dimension;
        switch(reportParams.dimension) {
            case "teamPlayer":
                dimension = 'player';
            break;
            case "platoonType":
                dimension = 'platoon';
            break;
            case "teamPositionGroup":
                dimension = 'position-group';
            break;
            default:
                this.send( 'error', new Ember.Error('unknown report dimension'));
        }
        return adapter.buildURL('Team', 'reportAggregateGrades') +
            '?report=game-performance' +
            '&dimension='+dimension +
            '&dimensionId='+reportParams.dimensionValue +
            '&period='+period +
            '&periodId='+ reportParams.periodValue;
    },

    // build a season report using the user's parameters
    buildSeasonReport(reportParams) {
        var self = this;
        var where = {};     // will collect where clause values for the query
        var whereFilter;    // will collect the actual where filter that is sent to the server
        var store = this.store;
        var adapter = this.store.adapterFor('application');

        // collect game stats here
        var games = [];

        // let the user know that we are working
        this.controller.set('message', 'loading report...');

        // configure the where clause based on selection dimension and period
        this.dimensionWhere(reportParams,where);
        whereFilter = { "filter": { "where": where } };

        // process the periodic results when all done
        var chartData = [],
            chartCategories = [];
            /*
                {
                        name: 'Mental',
                        pointPlacement: -0.2,
                        data: [85, 84, 83, 82]
                    }, {
                        name: 'Technique',
                        pointPlacement: 0,
                        data: [90, 89, 88, 90]
                    }, {
                        name: 'Effort',
                        pointPlacement: 0.2,
                        data: [88, 87, 92, 95]
                    }
                    */
        chartData[0] = {
            name: 'Mental',
            pointPlacement: -0.2,
            data: []
        };
        chartData[1] = {
            name: 'Technique',
            pointPlacement: 0,
            data: []
        };
        chartData[2] = {
            name: 'Effort',
            pointPlacement: 0.2,
            data: []
        };

        function ajaxSuccess(response) {
            console.log('reportAggregateGrades returned %s', JSON.stringify(response,null,4));
            response = response.data;
            if (response && response.detailGrades && response.detailGrades[0]) {
                if (response.detailGrades.length > 1) {
                    self.send('error', new Ember.Error('season report should return only 1 response'));
                    return;
                } else {
                    var details       = response.detailGrades[0]
                      , summary       = Ember.A(response.summaryGrades)
                      , overallGrades = response.overallGrades[0]
                      , evtSummary;
                    if (details) {
                        // expand first event
                        Ember.set(details.events[0], 'isExpanded', true);

                        // Setting the respective label so the user can match which event belongs
                        // to which column on the graph
                        details.events.forEach(function(_event) {
                            evtSummary = summary.findBy('id', _event.id);
                            if (evtSummary) {
                                _event['label'] = evtSummary.series;
                            }
                        });

                        self.set('eventData', details.events);
                        self.controller.set('eventData', details.events);
                        self.controller.set('hasIndividualPlays', response.groupByPlayNumber);
                        self.controller.set('hasPlayDetails', response.hasPlayDetails);

                        var groupByName = response.groupByName;
                        if (groupByName === 'position-group') {
                            self.controller.set('groupByName', 'Group');
                        } else if (groupByName === 'platoon') {
                            self.controller.set('groupByName', 'Platoon');
                        } else {
                            self.controller.set('groupByName', groupByName);
                        }
                        self.controller.set('groupByValue', response.groupByValue);
                    }
                    //var summary = response.summaryGrades;
                    if (summary) {
                        summary.forEach(function (row, idx) {
                            console.log('period row = %s', JSON.stringify(row,null,4));
                            chartData[0].data.push(row.cat1Grade);
                            chartData[1].data.push(row.cat2Grade);
                            chartData[2].data.push(row.cat3Grade);
                            chartCategories.push(row.series);
                        });
                        this.controller.set('chartData', chartData);
                        this.controller.set('chartCategories', chartCategories);

                        if (overallGrades) {
                            this.controller.set('cat1Grade', overallGrades.cat1Grade);
                            this.controller.set('cat2Grade', overallGrades.cat2Grade);
                            this.controller.set('cat3Grade', overallGrades.cat3Grade);
                            this.controller.set('overallGrade', overallGrades.overallGrade);
                        }

                        return;
                    }
                }
            }
            self.controller.set('message', 'nothing to report');
        }
        function ajaxFailure(error) {
            console.log('reportAggregateGrades error %o', error);
            self.send('error', error);
        }

        // new method of returning table data
        var url = this.reportsURL(adapter, reportParams, 'season' );
        adapter.ajax(url, 'GET').then(ajaxSuccess.bind(this),ajaxFailure.bind(this));
    },

    // build a game report using the user's parameters
    buildGameReport(reportParams) {
        var self = this;
        var where = {};     // will collect where clause values for the query
        var whereFilter;    // will collect the actual where filter that is sent to the server
        var store = this.store;
        var adapter = this.store.adapterFor('application');
        var dimensionModel;

        var quarters = [{
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }, {
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }, {
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }, {
            cat1Grade: null,
            cat2Grade: null,
            cat3Grade: null,
            overallGrade: null,
            numPlays: null
        }];

        // let the user know that we are working
        this.controller.set('message', 'loading report...');

        // configure the where clause based on selection dimension and period
        this.dimensionWhere(reportParams,where);
        whereFilter = { "filter": { "where": where } };

        switch(reportParams.dimension) {
            case "teamPlayer":
            dimensionModel = 'teamReportPlayerEventDataByQuarter';
            break;
            case "platoonType":
            dimensionModel = 'teamReportPlatoonEventDataByQuarter';
            break;
            case "teamPositionGroup":
            dimensionModel = 'teamReportPositionTypeEventDataByQuarter';
            break;
            default:
            throw new Ember.Error('unknown report dimension');
        }

        function ajaxSuccess(response) {
            console.log('reportAggregateGrades returned %o', response);
            response = response.data;
            if (response && response.detailGrades && response.detailGrades[0]) {
                if (response.detailGrades.length > 1) {
                    self.send('error', new Ember.Error('game report should return only 1 response'));
                    return;
                } else {
                    var details       = response.detailGrades[0]
                      , overallGrades = response.overallGrades[0]
                      , firstEvent    = details.events[0];
                    if (details) {
                        // expand first event
                        Ember.set(firstEvent, 'isExpanded', true);

                        if (firstEvent && firstEvent.teamPlayers) {
                            firstEvent.teamPlayers = Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
                                content        : firstEvent.teamPlayers,
                                sortAscending  : false,
                                sortProperties : ['gradesSummary.overallGrade']
                            });
                        }

                        self.set('eventData', details.events);
                        self.controller.set('eventData', details.events);
                        self.controller.set('hasIndividualPlays', response.groupByPlayNumber);
                        self.controller.set('hasPlayDetails', response.hasPlayDetails);
                        var groupByName = response.groupByName;
                        if (groupByName === 'position-group') {
                            self.controller.set('groupByName', 'Group');
                        } else if (groupByName === 'platoon') {
                            self.controller.set('groupByName', 'Platoon');
                        } else {
                            self.controller.set('groupByName', groupByName);
                        }
                        self.controller.set('groupByValue', response.groupByValue);

                        if (overallGrades) {
                            self.controller.set('cat1Grade', overallGrades.cat1Grade);
                            self.controller.set('cat2Grade', overallGrades.cat2Grade);
                            self.controller.set('cat3Grade', overallGrades.cat3Grade);
                            self.controller.set('overallGrade', overallGrades.overallGrade);
                        }

                        return;
                    }
                }
            }
            self.controller.set('message', 'nothing to report');
        }
        function ajaxFailure(error) {
            console.log('reportAggregateGrades error %o', error);
            self.send('error', error);
        }

        // new method of returning table data
        var url = this.reportsURL(adapter, reportParams, 'event' );
        adapter.ajax(url, 'GET').then(ajaxSuccess,ajaxFailure);
/*
// old way to get table data
        this.reportData = this.fetchAllModelsNamed('teamReportData', whereFilter)
            .then(function (data) {
                this.set('reportData', data);
                this.controller.set('reportData', data);    // this will update the report detail table
                if (data.get('length') === 0) {
                    this.controller.set('message', 'nothing to report');
                }
            }.bind(this));
*/
        // execute the reports
        this.reportEventDataByPeriod =  this.fetchAllModelsWithCallback(dimensionModel, allDone.bind(this), whereFilter)
            .then(function (data) {
                console.log('reports.game-performance.route: buildGameReport - fetchAllModelsWithCallback.then()');
                //this.set('reportEventDataByPeriod', data);
                this.set('reportEventDataByPeriod', data);
            }.bind(this));

        // process the periodic results when all done
        function allDone(error) {
            if (error) {
                console.log('reports.game-performance.route: buildGameReport.allDone() error = ', error);
                return;
            }
            console.log('reports.game-performance.route: allDoneByQuarter, data = ', this.reportEventDataByPeriod);
            this.reportEventDataByPeriod.forEach(function (row, idx) {
                var quarter = row.get('quarter');
                if (quarter && quarter >= 1 && quarter <= 4) {
                    var q = quarters[quarter - 1];
                    if (q) {
                        Ember.set(q, 'cat1Grade', row.get('cat1Grade'));
                        Ember.set(q, 'cat2Grade', row.get('cat2Grade'));
                        Ember.set(q, 'cat3Grade', row.get('cat3Grade'));
                        Ember.set(q, 'overallGrade', row.get('avgOverallGrade'));
                        Ember.set(q, 'numPlays', row.get('numPlays'));
                    }
                }
            });
            var chartData = [];
                /*        {
                            name: 'Mental',
                            pointPlacement: -0.2,
                            data: [85, 84, 83, 82]
                        }, {
                            name: 'Technique',
                            pointPlacement: 0,
                            data: [90, 89, 88, 90]
                        }, {
                            name: 'Effort',
                            pointPlacement: 0.2,
                            data: [88, 87, 92, 95]
                        }
                        */
            chartData[0] = {
                name: 'Mental',
                pointPlacement: -0.2,
                data: [
                    quarters[0].cat1Grade ? parseFloat(quarters[0].cat1Grade) : null, 
                    quarters[1].cat1Grade ? parseFloat(quarters[1].cat1Grade) : null, 
                    quarters[2].cat1Grade ? parseFloat(quarters[2].cat1Grade) : null, 
                    quarters[3].cat1Grade ? parseFloat(quarters[3].cat1Grade) : null
                ]
            };
            chartData[1] = {
                name: 'Technique',
                pointPlacement: 0,
                data: [
                    quarters[0].cat2Grade ? parseFloat(quarters[0].cat2Grade) : null, 
                    quarters[1].cat2Grade ? parseFloat(quarters[1].cat2Grade) : null, 
                    quarters[2].cat2Grade ? parseFloat(quarters[2].cat2Grade) : null, 
                    quarters[3].cat2Grade ? parseFloat(quarters[3].cat2Grade) : null
                ]
            };
            chartData[2] = {
                name: 'Effort',
                pointPlacement: 0.2,
                data: [
                    quarters[0].cat3Grade ? parseFloat(quarters[0].cat3Grade) : null, 
                    quarters[1].cat3Grade ? parseFloat(quarters[1].cat3Grade) : null, 
                    quarters[2].cat3Grade ? parseFloat(quarters[2].cat3Grade) : null, 
                    quarters[3].cat3Grade ? parseFloat(quarters[3].cat3Grade) : null
                ]
            };
            this.controller.set('chartData', chartData);    // this will update the chart
            this.controller.set('chartCategories',['1st Qtr', '2nd Qtr', '3rd Qtr', '4th Qtr']);
        } // allDoneByQuarter()
    }, // buildGameReport()

    actions: {
        error: function(reason) {
            console.log('reports.game-performance.route got error %o', reason);
            var message = reason.message || reason.statusText;
            if (reason.responseJSON && reason.responseJSON.error) {
                message = reason.responseJSON.error.stack ||
                reason.responseJSON.error.message;
            }
            alert(message);
        },

        runReport: function() {
            this.set('errors', null);
            this.send('buildReportParams', 'gamePerformance', this.get('controller'));
        },

        toggleExpand(event) {
            //console.log('reports.game-performance.route toggleExpand(%o)', event);
            if (!Ember.get(event,'isExpanded')) {
                // isExpanded is either undefined, or false
                Ember.set(event,'isExpanded',true);
            } else {
                // isExpanded is true
                Ember.set(event,'isExpanded',false);
            }
        },

        toggleExpandPlayer(player) {
            if (!Ember.get(player,'isExpanded')) {
                Ember.set(player,'isExpanded',true);
            } else {
                Ember.set(player,'isExpanded',false);
            }
        },

        /*queryParamsDidChange() {
            Ember.run.once(this, this.refresh);
        },*/

       /* didTransition: function() {
            var obj = {
                thisPage: "Game Performance",
                breadcrumbs: [
                    {
                        linkTo :  "reports",
                        caption : "Player Performance Reports"
                    }
                ]
            };
            this.send('setBreadcrumbs',obj);
            this.send('setCurrentReport', 'game');  // tell parent route which report is currently selected just in case we got here by manually specifying the URL of this route
            return true;    // bubble transitions up
        },*/
        runReportWith(reportParams) {
            var self = this;
            console.log('reports.game-performance.route: runReportWith, reportParams ', JSON.stringify(reportParams,null,4));
            // update query parameters
            if (this.controller.get('player') !== Ember.get(reportParams, 'player')) {
                this.controller.set('player', Ember.get(reportParams,'player'));
            }
            if (this.controller.get('group') !== Ember.get(reportParams, 'group')) {
                this.controller.set('group', Ember.get(reportParams,'group'));
            }
            if (this.controller.get('platoon') !== Ember.get(reportParams, 'platoon')) {
                this.controller.set('platoon', Ember.get(reportParams,'platoon'));
            }

            if (this.controller.get('game') !== Ember.get(reportParams, 'game')) {
                this.controller.set('game', Ember.get(reportParams,'game'));
            }
            if (this.controller.get('season') !== Ember.get(reportParams, 'season')) {
                this.controller.set('season', Ember.get(reportParams,'season'));
            }
            this.controller.set('chartPeriod', Ember.get(reportParams,'period'));

            this.clearData();   // clear out any data from another report before proceeding

            //TODO: replace with logic to calculate overall grades for whatever criteria is selected
            /*if (reportParams.dimension === 'teamPlayer') {
                this.store.findById('teamPlayer', reportParams.dimensionValue).then(function (player) {
                    player.get('teamPlayerSeasonStats').then(function (stats) {
                        var season = self.get('loadData.season');
                        stats.every(function (stat, index) {
                            if (Ember.get(stat, 'season') === season) {
                                self.controller.set('cat1Grade', Ember.get(stat, 'allEventCat1Grade'));
                                self.controller.set('cat2Grade', Ember.get(stat, 'allEventCat2Grade'));
                                self.controller.set('cat3Grade', Ember.get(stat, 'allEventCat3Grade'));
                                self.controller.set('overallGrade', Ember.get(stat, 'allEventOverallGrade'));
                                return false;   // returning false breaks out of the stats.every() iterator
                            }
                            return true;    // keep iterating
                        });
                    });
                }, function(error){
                    console.log('reports.game-performance.route: find teamPlayer by ID got error %o', error);
                });
            }*/

            if (Ember.get(reportParams, 'game')) {
                this.buildGameReport(reportParams);
            } else if (Ember.get(reportParams, 'season')) {
                this.buildSeasonReport(reportParams);
            }
        }
    } // actions:
});
