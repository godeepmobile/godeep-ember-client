// app/routes/application.js

import Ember from 'ember';
import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin';
import config from '../config/environment';
import FetchAllModelsMixin from '../mixins/fetch-all-models';

export default Ember.Route.extend(ApplicationRouteMixin, FetchAllModelsMixin, {
    loadData: Ember.inject.service(),
    websockets: Ember.inject.service('socket-io'),
    offline: Ember.inject.service(),

    sessionTimer: null,
    sessionTimerIsEnabled: true,

    getUserProfile: function(transition) {
        //console.log('ApplicationRoute: getUserProfile()');
        return this.get('loadData.profile').then(
            function(profile){
                //console.log('ApplicationRoute: got user profile %o', profile);
                var session = this.get('session');
                session.set('team', profile.get('team'));
                session.set('profile', profile);
                session.set('teamConfiguration', profile.get('teamConfiguration'));
                session.set('teamPlayers', profile.get('teamPlayers'));
                session.set('positionTypes', profile.get('positionTypes'));
                session.set("teamPositionGroups", profile.get("teamPositionGroups"));
                session.set('teamPlatoonConfiguration', profile.get('teamPlatoonConfiguration'));
                session.set('teamRoles', profile.get('teamRoles'));
                session.set('isAdmin', profile.get('teamRoles').isAny('isAdmin'));
                session.set('isCoach', profile.get('teamRoles').isAny('isCoach'));
            }.bind(this),
            function(err){
                console.log('ApplicationRoute: error %o reading profile', err);
                if (err.status === 403) {
                    return transition.send('invalidateSession');
                }
            }.bind(this)
        );
    },
    beforeModel: function() {
        this._super();
        var session = this.get('session');
        if (!session.get('isAuthenticated')) {
            session.restore();
        }
    },

    model: function(model, transition) {
        var session = this.get('session');
        if (session.get('isAuthenticated')) {
            return this.store.find('teamUser', {filter:{where:{role:4}}}).then(function(teamAdmins) {
                this.set('teamAdmins', teamAdmins);
                return this.getUserProfile(transition);
            }.bind(this));
        }
    },

    /*afterModel: function() {
        var session = this.get('session');
        if (session.get('isAuthenticated')) {
            return this.store.find('teamUser', {filter:{where:{role:4}}}).then(function(teamAdmins) {
                this.set('teamAdmins', teamAdmins);
            }.bind(this));
        }
    },*/

    setupController: function(controller, model) {
        var self = this
          , socket;

        this._super(controller, model);
        Ember.run.schedule("afterRender", controller, function() {
            //this refers to the sub route
            if (this.get('currentPath').indexOf('dashboard') !== -1) {
                // display these tooltips only on the dashboard page
                if (self.get('session.user.firstTimeLogin')) {
                    self.send('showModal', 'tooltip/mainMenu');
                } /*else {
                    if (self.get('session.user.displayWelcomeDialog')) {
                        self.send('displayWelcomePopup');
                    }
                }*/
            }

            self.set('sessionTimerIsEnabled', true);
            
            // Note(Carlos) for some reason Ember does not recognize these attrs when
            // rendering the page, so they can be used with a handlebars helper to avoid
            // rendering the not allowed options (which is the ember way)

            // removing admin-options
            if (!self.get('session.isAdmin')) {
                Ember.$('.admin-option').remove();
            }

            // removing evaluation-options
            if (!self.get('session.teamConfiguration.canEvaluate')) {
                Ember.$('.evaluation-option').remove();
            }
        });
        
        // init some services
        socket = this.get('websockets').socketFor(config.API.host);
        socket.on('connect', function() {
            socket.on('refresh', this.wsMessageReceived, this);
        }, this);

        //this.get('offline').check();
    },

    wsMessageReceived: function(message) {
        var messageSplitted
          , route
          , data;
        if (message) {

            messageSplitted = message.split('::');
            // 0 - command, 1 - route, 2 - additional info (like a model id)
            route           = messageSplitted[0];
            data            = messageSplitted[1];

            if (route === 'grade' && data) {
                Ember.run.later(function() {
                    //this.refresh();
                    if (this.get('controller.currentRouteName') === 'grade') {
                        this.refresh();
                    } else {
                        this.store.findById('teamEventGradeStatus', data).then(function(gradeStatus) {
                            gradeStatus.reload();
                            this.transitionTo('grade', gradeStatus);
                        }.bind(this));
                    }
                }.bind(this), 500);
            }
        }
    },

    displaySessionTimeoutDialog: function() {
        var sessionTimeoutDialogController = this.controllerFor('sessionTimeoutDialog');

        sessionTimeoutDialogController.startCountdown();
        this.send('showModal', 'sessionTimeoutDialog');
    },

    actions: {

        changeCurrentSeason: function(season) {
            if (season && this.get('loadData.season') !== season) {
                this.set('loadData.season', season);
            }
        },

        sessionInvalidationSucceeded: function() {
            if (!Ember.testing) {
                window.location.replace(config.API.host + '/');
            }
        },
        sessionAuthenticationSucceeded: function() {
            this.getUserProfile();
        },
        reloadUserProfile: function() {
            this.get('loadData.profile').then(function(profile) {
                profile.reload().then(function() {
                    this.getUserProfile();
                }.bind(this));
            }.bind(this));
        },
        //        appHideHamburgerMenu: function() {
        //            console.log('application.appHideHamburgerMenu');
        //            var header = this.controllerFor('header');
        //            header.hideHamburgerMenu();
        //        },
        authorizationFailed: function() {
            console.log('ApplicationRoute: session authorization failed');
        },
        toggleHamburger: function() {
            Ember.$.slidebars.toggle('left');
            Ember.$('#hamburger_button').blur();
        },

        goBack: function() {
            console.log('goBack');
            window.history.back();
        },

        appMenuHome: function() {
            console.log('appMenuHome');
            this.transitionTo('dashboard');
        },
        appMenuRosterAndPositions: function() {
            console.log('appMenuRosterAndPositions');
            this.transitionTo('players');
        },
        appMenuProspectList: function() {
            console.log('appMenuProspectList');
            this.transitionTo('prospects');
        },
        appMenuGradePlayerPerformance: function() {
            console.log('appMenuGradePlayerPerformance');
            //this.transitionTo('games'); // uncomment and comment the next line so this option goes directly to the grade player performance screen
            this.transitionTo(this.get('session.isAdmin') ? 'grading' : 'games');
        },
        appMenuEvaluatePlayersAndProspects: function() {
            console.log('appMenuEvaluatePlayersAndProspects');
            this.transitionTo('evaluate');
        },
        appMenuUploadVideoCutupData: function() {
            console.log('appMenuUploadVideoCutupData');
            this.transitionTo('event.upload');
        },
        appMenuReports: function() {
            console.log('appMenuReports');
            this.transitionTo('reports');
        },
        appMenuRank: function() {
            var rankController = this.controllerFor('rank');
            console.log('appMenuRank');
            this.transitionTo(rankController && rankController.get('rankSelected') === 'prospects' ? 'rank' : 'rank.playerRank');
        },
        appMenuIntro: function() {
            console.log('appMenuIntro');
            //TODO: handle URL on EVN variables
            window.open(
              'https://docsend.com/view/rqmxnw6',
              '_blank'
            );
        },
        appMenuSettings: function() {
            console.log('appMenuSettings');
            this.transitionTo('admin');
        },
        appMenuAskQuestion: function() {
            console.log('appMenuAskQuestion');

            this.controllerFor('question').clearForm();
            this.send('showModal', 'question');
        },
        error: function(reason) {
            console.log('application route got error %o', reason);
            Rollbar.error(reason);
            alert(reason.statusText); // "FAIL"
            //window.history.back();
            return true;
        },

        showModal: function(name, model) {
            this.render(name, {
                into: 'application',
                outlet: 'modal',
                model: model
            });
        },
        removeModal: function() {
            Ember.$('.modal').modal('hide');
            this.disconnectOutlet({
                outlet: 'modal',
                parentView: 'application'
            });
        },
        didTransition: function() {
            Ember.$('.modal').modal('hide');
            this.send('removeModal');
            this.send('removeTooltip');
        },

        focusFirstInput: function() {
            var firstInput = 'form:first *:input[type!=hidden]:enabled:first';
            var checkExist = setInterval(function() {
                if (Ember.$(firstInput).length) {
                    setTimeout( function() {
                            Ember.$(firstInput).focus();
                        }, 500
                    );
                    clearInterval(checkExist);
                }
            }, 100);

            // wait for html element for 5 seconds
            setTimeout( function() {
                    clearInterval(checkExist);
                }, 5000
            );
        },

        showTooltip: function(name, model) {
            this.render(name, {
                into: 'application',
                outlet: 'tooltip',
                model: model
            });
            setTimeout( function() {
                    Ember.$('#gotooltip').fadeIn( 500 );
                    if (model && model.cssClass) {
                        Ember.$('#gotooltip').addClass(model.cssClass);
                    }
                }, 100
            );
        },

        removeTooltip: function() {
            var self = this;
            Ember.$('#gotooltip').fadeOut( 500 );
            setTimeout( function() {
                    self.disconnectOutlet({
                        outlet: 'tooltip',
                        parentView: 'application'
                    });
                }, 550
            );
        },

        temporalTooltip: function(name, model) {
            var self = this;
            self.send('showTooltip', name, model);
            setTimeout( function() {
                    self.send('removeTooltip', name, model);
                }, 3000
            );
        },

        displayIntroTooltip: function() {
            this.send('removeModal');
            Ember.run.later(function() {
                Ember.$.slidebars.toggle('left');
                this.send('showModal', 'tooltip/intro');
            }.bind(this), 500);
        },

        disableFirstTimeLoginTooltips: function() {
            Ember.$.slidebars.toggle('left');
            this.send('removeModal');
            this.store.find('goUser', this.get('session.user.id')).then(function(user) {
                user.set('firstTimeLogin', false);
                user.save();

                /*Ember.run.later(function() {
                    this.send('displayWelcomePopup');
                }.bind(this), 500);*/

            }.bind(this));
        },

        displayWelcomePopup: function() {
            this.send('showModal', 'welcome');
        },

        setWelcomePopupDefaultState: function(displayWelcomeDialog) {
            if (this.get('session.user')) {
                this.store.find('goUser', this.get('session.user.id')).then(function(user) {
                    user.set('displayWelcomeDialog', displayWelcomeDialog);
                    user.save();
                });
            }
        },

        willDestroy: function() {
            // closing the websocket connection
            this.get('websockets').socketFor(config.API.host);
        },

        prepareOfflineMode: function() {
            var eventListDialog = this.controllerFor('games/popup/list');

            eventListDialog.set('adminUsers', this.get('teamAdmins'));
            eventListDialog.set('isRecoveringData', false);

            this.send('showModal', 'games/popup/list', this.get('loadData.events'));
        },

        getRequiredDataForOfflineMode: function(teamEvent) {
            var self = this
              , teamPlays
              , playerAssignments;

            this.store.find('teamCutup', {
                filter: {
                    where: { teamEvent: teamEvent.get('id')}
                }
            }).then(function(teamCutups) {
                teamEvent.set('teamCutups', teamCutups);

                self.store.find('teamEventGradeStatus', {
                    filter: {
                        where: { and: [
                              { user: self.get("session.user.id")},
                              { teamEvent: teamEvent.get('id')}]}
                        }
                }
                ).then(function(teamEventGradeStatus) {

                    Ember.RSVP.hash({
                        playTypes              : self.get('loadData.playTypes'),
                        playResultTypes        : self.get('loadData.playResultTypes'),
                        platoonTypes           : self.get('loadData.platoonTypes'),   
                        positionTypes          : self.get('loadData.positionTypes'),      
                        playFactors            : self.store.find('playFactor'),
                        teamPracticeDrillTypes : self.fetchAllModelsNamed('teamPracticeDrillType')
                    }).then(function(offlineData) {

                        offlineData['teamEvent']   = teamEvent;
                        offlineData['gradeStatus'] = teamEventGradeStatus;

                        teamPlays = self.fetchAllModelsWithCallback('teamPlay', function() {

                            offlineData['teamPlays'] = teamPlays;

                            self.fetchAllModelsWithCallback('grade', function() {

                                playerAssignments = self.fetchAllModelsWithCallback('teamPlayerAssignment', function() {

                                    offlineData['playerAssignments'] = playerAssignments.filterBy('season', teamEvent.get('season')); //Note(Carlos) workaround to avoid some assignments already stored on memory which do not belong to the event's season

                                    self.fetchAllModelsWithCallback('teamPlayer', function() {
                                        self.transitionTo('offline-games').then(function(route) {
                                            route.set('offlineData', offlineData);
                                            route.set('controller.teamEvent', teamEvent);

                                            //disabling session timer
                                            self.set('sessionTimerIsEnabled', false);
                                            Ember.run.cancel(self.get('sessionTimer'));
                                        });
                                    });

                                }, {filter : 
                                    { where : { season: teamEvent.get('season')}}
                                });


                            }, {filter: {
                                where : {
                                    user     : self.get('session.user.id')
                                  , teamPlay : {inq: teamPlays.getEach('id')}
                                }
                            }});
                            
                        }, {filter : { where : { teamEvent: teamEvent.get('id')}}});
                    });

                });

            });

        },

        appMenuRetunToOfflineModeLandingPage: function() {
            this.transitionTo('offline-games');
        },

        appMenuOfflineModeHelp: function() {
            this.send('showModal', 'grade/learnMoreGrading');
        },

        enableSessionTimer:function() {
            this.set('sessionTimerIsEnabled', true);
            this.send('startSessionTimer');
        },

        startSessionTimer: function() {
            // var self = this;
            // Ember.run.cancel(this.get('sessionTimer'));
            // if (this.get('sessionTimerIsEnabled')) {
            //     this.set('sessionTimer', Ember.run.later(function() {
            //         self.displaySessionTimeoutDialog();
            //         self.set('sessionTimerIsEnabled', false);
            //     }, 180000)); // = 3 mins
            // }
        },

        resumeOnlineMode: function() {
            this.send('appMenuHome');
            this.send('enableSessionTimer');
        }
    }
});
