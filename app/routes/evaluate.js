import Ember from 'ember';

export default Ember.Route.extend({
	loadData: Ember.inject.service(),

	model: function() {
		return Ember.RSVP.hash({
			playerAssignments   : this.get('loadData.playerAssignments'),
			prospectAssignments : this.get('loadData.prospectAssignments') 
		});
	},

	actions: {
		goToReviewEditEvaluationCriteria: function() {
			this.transitionTo('criteria.setup');
		},

		displayLearnMoreEvaluationsPopup: function() {
			this.send('showModal', 'evaluation/learnMoreEvaluation');
		},

		goToEvaluatePlayer: function() {
			var playerAssignments       = this.get('currentModel.playerAssignments').filterBy('season', this.get('loadData.season'))
			  , playerChooserController = this.controllerFor('evaluation/playerChooser');

        	playerChooserController.clearForm();
        	playerAssignments.setEach('isSelectedToEvaluate', false);

			this.send('showModal', 'evaluation/playerChooser', Ember.ArrayController.create({
				content: playerAssignments,
				sortProperties: ['teamPlayer.lastName']
			}));
		},

		goToEvaluateProspect: function() {
			var prospectAssignments       = this.get('currentModel.prospectAssignments').filterBy('season', this.get('loadData.season'))
			  , prospectChooserController = this.controllerFor('evaluation/prospectChooser');

        	prospectChooserController.clearForm();
        	prospectAssignments.setEach('isSelectedToEvaluate', false);

			this.send('showModal', 'evaluation/prospectChooser', Ember.ArrayController.create({
				content: prospectAssignments,
				sortProperties: ['teamProspect.lastName']
			}));
		},

		goToPlayerValueRanking: function() {
			this.transitionTo('rank.playerRank');
		},

		goToProspectValueRanking: function() {
			this.transitionTo('rank');
		}
	}
});
