/* jshint node: true */

module.exports = function(environment) {
    var ENV = {
        modulePrefix: 'godeeptest',
        environment: environment,
        //baseURL: '/',
        baseURL: '/coach/',
        locationType: 'auto',
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary build
                // e.g. 'with-controller': true
            }
        },

        APP: {
            // Here you can pass flags/options to your application instance
            // when it is created
        },

        // rollbar.com, rollbar exception reporting tool
        rollbarToken: "cb79fa36453942ada85947c6232e01e9",
        rollbarVerbose: false,

        API: {
            nameSpace   : 'api',
            login       : 'api/gousers/login?include=user',
            logout      : 'api/gousers/logout',
            testSession : '/api/States/1/exists'
        }
    };

    if (environment === 'development') {
        // specifics for loopback server running on localhost
        ENV.API.host = 'http://localhost:3000';

        // debugging flags
        // ENV.APP.LOG_RESOLVER = true;
        // ENV.APP.LOG_ACTIVE_GENERATION = true;
        // ENV.APP.LOG_TRANSITIONS = true;
        // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        // ENV.APP.LOG_VIEW_LOOKUPS = true;
        ENV.rollbarDisabled = true;
    }

    if (environment === 'test') {
        // Testem prefers this...

//        ENV.baseURL = '/';
        ENV.baseURL = '/coach/';
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;
        ENV.APP.rootElement = '#ember-testing';
        ENV.rollbarDisabled = true;
    }

    if (environment === 'production') {
        // specifics for gd-api-loopback server running at Heroku
        ENV.API.host = 'http://gd-api-loopback.herokuapp.com';
    }

    if (environment === 'prod-local-3000') {
        // specifics for api server running with index.html served from at Heroku
        ENV.API.host = 'http://localhost:3000';
    }

    if (environment === 'prod-heroku') {
        // specifics for api server running with index.html served from at Heroku
        ENV.API.host = 'http://app.godeepmobile.com';
    }

    if (environment === 'qa-heroku') {
        // specifics for api server running with index.html served from at Heroku
        ENV.API.host = 'http://gd-api-qa.herokuapp.com';
    }

    if (environment === 'uat-heroku') {
        // specifics for api server running with index.html served from at Heroku
        ENV.API.host = 'http://gd-api-uat.herokuapp.com';
    }

    ENV.contentSecurityPolicy = {
        'default-src': "http://gd-app-assets.s3.amazonaws.com https://s3.amazonaws.com http://s3.amazonaws.com",
        //'script-src': "'self' 'unsafe-inline' https://s3.amazonaws.com http://s3.amazonaws.com http://gd-app-assets.s3.amazonaws.com https://gd-app-assets.s3.amazonaws.com",
        'script-src': "'self' 'unsafe-inline' 'unsafe-eval' https://d37gvrvc0wt4s1.cloudfront.net https://s3.amazonaws.com http://s3.amazonaws.com http://gd-app-assets.s3.amazonaws.com https://gd-app-assets.s3.amazonaws.com",
        'font-src': "'self' https://s3.amazonaws.com http://s3.amazonaws.com http://gd-app-assets.s3.amazonaws.com https://gd-app-assets.s3.amazonaws.com",
        //'connect-src': "'self' http://localhost:3000 http://localhost:3000/coach http://server.local http://gd-api-loopback.herokuapp.com http://gd-api-qa.herokuapp.com http://gd-api-uat.herokuapp.com",
        'connect-src': "'self' http://api.rollbar.com https://api.rollbar.com http://localhost:3000 ws://localhost:3000 http://localhost:3000/coach http://server.local http://gd-api-loopback.herokuapp.com http://gd-api-qa.herokuapp.com http://gd-api-uat.herokuapp.com ws://gd-api-loopback.herokuapp.com ws://gd-api-qa.herokuapp.com ws://gd-api-uat.herokuapp.com",

        //'img-src': "'self' data: https://s3.amazonaws.com http://s3.amazonaws.com",
        'img-src': "'self' data: https://s3.amazonaws.com http://s3.amazonaws.com",
        'style-src': "'self' 'unsafe-inline' https://s3.amazonaws.com http://s3.amazonaws.com http://gd-app-assets.s3.amazonaws.com https://gd-app-assets.s3.amazonaws.com",
        'media-src': "'self' https://s3.amazonaws.com http://s3.amazonaws.com http://gd-app-assets.s3.amazonaws.com https://gd-app-assets.s3.amazonaws.com"
    };

    ENV['simple-auth'] = {
        authorizer:                     'authorizer:custom',
        store:                          'simple-auth-session-store:local-storage',
        authenticationRoute:            '/login',                  // need beginning /
        routeAfterAuthentication:       '/',
        routeIfAlreadyAuthenticated:    '/',
        session:                        'session:custom',
        localStorageKey:                'ember_simple_auth:session',
        crossOriginWhitelist:           [ENV.API.host],
        applicationRootUrl:             ENV.baseURL
    };

    ENV.maxGrade = 100;

    return ENV;
};
