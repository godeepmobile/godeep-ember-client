import { fullNameInitialLast } from '../../../helpers/full-name-initial-last';
import { module, test } from 'qunit';

module('Unit | Helper | full name initial last');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = fullNameInitialLast(42);
  assert.ok(result);
});
