import {
  isPlayerAdded
} from '../../../helpers/is-player-added';
import { module, test } from 'qunit';

module('IsPlayerAddedHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = isPlayerAdded(42);
  assert.ok(result);
});
