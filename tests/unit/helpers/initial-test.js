import { initial } from '../../../helpers/initial';
import { module, test } from 'qunit';

module('Unit | Helper | initial');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = initial(42);
  assert.ok(result);
});
