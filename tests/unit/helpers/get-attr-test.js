import { getAttr } from '../../../helpers/get-attr';
import { module, test } from 'qunit';

module('Unit | Helper | get attr');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = getAttr(42);
  assert.ok(result);
});
