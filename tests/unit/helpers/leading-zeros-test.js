import {
  leadingZeros
} from '../../../helpers/leading-zeros';
import { module, test } from 'qunit';

module('LeadingZerosHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = leadingZeros(42);
  assert.ok(result);
});
