import { getElementAtIndex } from '../../../helpers/get-element-at-index';
import { module, test } from 'qunit';

module('Unit | Helper | get element at index');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = getElementAtIndex(42);
  assert.ok(result);
});
