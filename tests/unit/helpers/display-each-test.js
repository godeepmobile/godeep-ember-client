import {
  displayEach
} from '../../../helpers/display-each';
import { module, test } from 'qunit';

module('DisplayEachHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = displayEach(42);
  assert.ok(result);
});
