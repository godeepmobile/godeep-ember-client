import {
  moduleForModel,
  test
} from 'ember-qunit';

moduleForModel('team-player-season-stat', 'TeamPlayerSeasonStat', {
  // Specify the other units that are required for this test.
  needs: []
});

test('it exists', function() {
  var model = this.subject();
  // var store = this.store();
  ok(!!model);
});
