import { moduleForModel, test } from 'ember-qunit';

moduleForModel('team-player-assignment', 'Unit | Serializer | team player assignment', {
  // Specify the other units that are required for this test.
  needs: ['serializer:team-player-assignment']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  var record = this.subject();

  var serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
