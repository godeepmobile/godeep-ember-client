import { moduleForModel, test } from 'ember-qunit';

moduleForModel('team-player-season-stat', 'Unit | Serializer | team player season stat', {
  // Specify the other units that are required for this test.
  needs: ['serializer:team-player-season-stat']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  var record = this.subject();

  var serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
