import { moduleForModel, test } from 'ember-qunit';

moduleForModel('team-report', 'Unit | Serializer | team report', {
  // Specify the other units that are required for this test.
  needs: ['serializer:team-report']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  var record = this.subject();

  var serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
