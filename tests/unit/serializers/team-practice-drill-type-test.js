import { moduleForModel, test } from 'ember-qunit';

moduleForModel('team-practice-drill-type', 'Unit | Serializer | team practice drill type', {
  // Specify the other units that are required for this test.
  needs: ['serializer:team-practice-drill-type']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  var record = this.subject();

  var serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
