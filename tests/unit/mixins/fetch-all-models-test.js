// tests/unit/mixins/fetch-all-models.test.js

import Ember from 'ember';
import FetchAllModelsMixin from '../../../mixins/fetch-all-models';
import { module, test } from 'qunit';

module('FetchAllModelsMixin');

// Replace this with your real tests.
test('it works', function(assert) {
  var FetchAllModelsObject = Ember.Object.extend(FetchAllModelsMixin);
  var subject = FetchAllModelsObject.create();
  assert.ok(subject);
});
