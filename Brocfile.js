/* global require, module, process */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var Deploy = require('./deploy.json');
var version = require('git-repo-version')(null);
console.log('app version = ', version);

var emberEnvironment = process.env.EMBER_ENV;
var isProductionLikeBuild = (emberEnvironment.indexOf("prod") === 0);
var isQABuild = (emberEnvironment.indexOf("qa") === 0);
var fingerprint;

// for Rollbar
var config = require('./config/environment')(emberEnvironment);

if (Deploy && Deploy[emberEnvironment]) {
    fingerprint = Deploy[emberEnvironment].fingerprint;
}
if (fingerprint === null || fingerprint === undefined) {
    fingerprint = (isProductionLikeBuild || (isQABuild));
}

// bugs with enabling/disabling sourcemaps.
// when it's enabled, you may see 2 403 errors in console for non-production builds.
// it must be disabled for production-like builds.
var app = new EmberApp({
    storeConfigInMeta: false,
    vendorFiles: {
        'handlebars.js': null
    },
    sourcemaps: {
        //enabled: !isProductionLikeBuild
        enabled: true
    },
    minifyCSS: { enabled: isProductionLikeBuild },
    //minifyJS: { enabled: isProductionLikeBuild },
    minifyJS: { enabled: false },
    tests: process.env.EMBER_CLI_TEST_COMMAND || !isProductionLikeBuild,
    hinting: process.env.EMBER_CLI_TEST_COMMAND || !isProductionLikeBuild,
    fingerprint: {
        enabled: fingerprint,
        prepend: 'https://gd-app-assets.s3.amazonaws.com/'
    },
    // for Rollbar
    inlineContent: {
        "rollbar" : {
          file: "bower_components/rollbar/dist/rollbar.snippet.js",
          postProcess: function(content) {
            return 'var _rollbarConfig = { accessToken: "' + config.rollbarToken +
            '", verbose: "' + config.rollbarVerbose +
            '", captureUncaught: true, payload: { environment: "' + config.environment +
            '", client: { javascript: { source_map_enabled: true, code_version: "' + version +
            '", guess_uncaught_frames: true}} } };\n' +
            content;
          }
        }
    }
});

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

// import bootstrap.js into vendor.js
app.import('bower_components/bootstrap/dist/js/bootstrap.min.js');

// import highcharts.js into vendor.js
app.import('bower_components/highcharts/highcharts.js');

// import moment.js
app.import('bower_components/moment/min/moment.min.js');

// import handsontable.js
app.import('bower_components/handsontable/dist/handsontable.full.min.js');

// import offline.js
app.import('bower_components/offline/offline.min.js');

module.exports = app.toTree();
