#!/bin/bash
# deploy-map-to-rollbar
# first parameter is version from package.json i.e. "1.0.0"
# second parameter is website url base, i.e. http://gd-api-qa.herokuapp.com
# third parameter is environment, i.e. production or qa or uat

if [ $# -ne 3 ]; then
    echo "must provide version, website url base, and environment as parameters"
    exit 1
fi
app_version=$1
app_url_base=$2
environment=$3

#last_commit=$(git rev-parse HEAD) #use this for full SHA
last_commit=$(git rev-parse  --short=8 HEAD) #use this for shortened SHA, this matches the ember-cli-deploy logic.
echo "last git commit = $last_commit"
version="$app_version.$last_commit"
echo "version = $version"

api_upload_url="https://api.rollbar.com/api/1/sourcemap"
echo "api upload url = $api_upload_url"

# Upload files for the given release
# Note: The filename should be the *full* url that this
# would be referenced as in production.
# first parameter is full filename (with extension and path)
function upload {
  source_path=$1
  map_path=$2
  echo "uploading $source_path"

  base_name=$(basename "$source_path")     # filename without path

  #filename="${basename%.*}"     # filename base without extension
  #echo "filename is $filename";

    curl $api_upload_url -F access_token=802c2b1fab5b4ff794143cd1db308ea8 \
      -F version="$version" \
      -F minified_url="$app_url_base/coach/assets/$base_name" \
      -F source_map=@"$map_path"

#  curl "$api_upload_url" -u b6dd6bfa6baa4b298c837abee31244b5: -X POST \
#    -F file=@$filepath \
#    -F name="$app_url_base/coach/assets/$basename"
  echo
}

function create_deployment {
    echo "creating $2 deployment version $1"
    curl https://api.rollbar.com/api/1/deploy/ -F access_token=802c2b1fab5b4ff794143cd1db308ea8 \
      -F revision=$1 \
      -F environment=$2
    echo
}

#create_deployment $last_commit
create_deployment $version $environment

mkdir dist/assets/maptmp
cp dist/assets/godeep*.js dist/assets/maptmp
cp dist/assets/vendor*.js dist/assets/maptmp
cp dist/assets/godeep*.map dist/assets/maptmp
cp dist/assets/vendor*.map dist/assets/maptmp

source_file=dist/assets/maptmp/godeep*.js
map_file=dist/assets/maptmp/godeep*.map
upload $source_file $map_file

source_file=dist/assets/maptmp/vendor*.js
map_file=dist/assets/maptmp/vendor*.map
upload $source_file $map_file

rm dist/assets/maptmp/*
rm -r dist/assets/maptmp

